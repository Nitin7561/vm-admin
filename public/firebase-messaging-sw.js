importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-messaging.js");

// console.log("messaging",messaging);

firebase.initializeApp({
    // Project Settings => Add Firebase to your web app
    apiKey: "AIzaSyBiYOHgCRU1nHLUYeMFQSzn201WSleGNa4",
    authDomain: "chat-app-23529.firebaseapp.com",
    databaseURL: "https://leanotron.firebaseio.com",
    projectId: "chat-app-23529",
    storageBucket: "chat-app-23529.appspot.com",
    messagingSenderId: "1088102145471",
    appId: "1:1088102145471:android:2df06ad699c3d5699d150a"
});

const messaging = firebase.messaging();
// if(messaging.isSupported()){

messaging.setBackgroundMessageHandler(function(payload) {
  const promiseChain = clients
    .matchAll({
      type: "window",
      includeUncontrolled: true
    })
    .then(windowClients => {
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i];
        windowClient.postMessage(payload);
      }
    })
    .then(() => {
      return registration.showNotification("my notification title");
    });
  return promiseChain;
});
self.addEventListener('notificationclick', function(event) {
  // do what you want
  // ...
});
// }
