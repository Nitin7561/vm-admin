import React from "react";
import logo from "./logo.svg";
import "./App.scss";
import "./assets/lean-icons/iconStyle.scss";
import {
    BrowserRouter as Router, Switch, Route, Link
} from "react-router-dom";



import AddAppointment from "./layouts/AddAppointment/AddAppointment";
import AddAppointmentInAModal from "./layouts/AddAppointment/AddAppointmentInAModal";
import VisitorMobileRegistration from "./layouts/VisitorMobileRegistration/VisitorMobileRegistration";
import WebVisitorRegistrationForm from './layouts/WebVisitorRegistrationForm/WebVisitorRegistrationForm';
import OtpFormMobileRegistration from '../src/layouts/OtpFormMobileRegistration/OtpFormMobileRegistration';
import ThanksForRegMobileVisitor from '../src/layouts/ThanksForRegMobileVisitor/ThanksForRegMobileVisitor';
import VisitorTable from '../src/components/Tables/ListOfVisitorsTable';
import MyAppointments from '../src/components/Tables/MyAppointmentsTable';
import MyFrequentVisitor from '../src/components/Tables/FrequentVisitors';
import Modal from './components/Modal/FunctionalModal';
import ModalOne from './components/Modal/StatusModal';
import UserAppointmentsModal from './layouts/UserAppointmentsModal/UserAppointmentsModal'
import { HeaderComponent } from './components/HeaderComponent/HeaderComponent';
import { ModalContextProvider } from "./utils/ModalContext";
import ModalManager from "./utils/ModalManager";
import ChatBox from "../src/layouts/Chat/ChatBox"
import ChatButton from "../src/layouts/Chat/ChatButton"
import ReportUser from "./layouts/Report/ReportUser"
import RejectVis from "./layouts/RejectVisitor/RejectVis"
import CancelAppointment from "./layouts/CancelAppt/CancellAppointment"
import LeanSidebarWithNoti from "./layouts/LeanSidebarWithNoti/LeanSidebarWithNoti";
import LeanSidebarWithNotiResp from "./layouts/LeanSidebarWithNoti/LeanSidebarWithNotiResp";
import LeanAppSideBar from "./components/LeanAppSideBar/LeanAppSideBar";
import ProfileModal from './components/Modal/myProfile/MyProfile';
import { Button, TextField, IconButton, Drawer, useMediaQuery, useTheme } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import { connect } from "react-redux";
import { getDetailForVisitor } from './redux/actions/chatAction/getDetailForVisitor';
import { fireDrawer } from './redux/actions/chatAction/fireDrawer';

import VisitorInfoModal from './components/Modal/visitor-reg-approve-reject/VisitorApproveReject';
// import ListOfSupport from './layouts/ListOfSupport/MyBookings'

import { getUserInfoAction } from "./layouts/LeanSidebarWithNoti/LeanSideBarContainer/actions/user";
import VmDashboard from "./layouts/VmAdmin/VmDashboard/VmDashboard";
import VmSetting from "./layouts/VmAdmin/VmSetting/VmSetting";

import PreloaderDummy from "./components/preloader/PreLoaderDummy";
import { getApplicationsAction, getTenantAction } from "./layouts/LeanSidebarWithNoti/LeanSideBarContainer/actions/homeStaticData";
import { applicationsRequired } from "./utils/constants";
import { setAllApplications, verifyApps } from "./redux/actions/utils";
import MyAppointmentsCards from "./components/Tables/MyAppointmentsCards";
import ListOfVisitorsCards from "./components/Tables/ListOfVisitorsCards";
import FrequentVisitorsCards from "./components/Tables/FrequentVisitorsCards";



function App(props) {

    let { userReducer, appCookieRecv, getUserInfoAction, getApplicationsAction, getTenantAction, getApplicationReducer, setAllApplications, verifyApps } = props


    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

    React.useEffect(() => {
        setAllApplications(applicationsRequired)
        getUserInfoAction();
    }, [])

    let { isLoading: userLoading, data: userData, isError: userIsError, error: userError } = userReducer;
    let { isLoading: cookieLoading, apps: cookieApps, isError: cookieIsError, error: cookieError, count: cookieCount } = appCookieRecv
    React.useEffect(() => {
        if (!userLoading && !userIsError) {
            // getApplicationsAction();
            getTenantAction()
        }
    }, [userReducer])
    React.useEffect(() => {
        if (!getApplicationReducer.isLoading && !getApplicationReducer.isError) {
            getApplicationCookies()
        }
    }, [getApplicationReducer])
    const getApplicationCookies = () => {
        console.log("getApplicationReducer", getApplicationReducer);
        verifyApps()
    }


    return (

        <ModalContextProvider>
            <ModalManager />
            <Router basename="/vm/web/pages">
                {(false) ? <PreloaderDummy /> : false ? <PreloaderDummy /> :

                // {(userLoading) ? <PreloaderDummy /> : cookieCount !== applicationsRequired.length ? <PreloaderDummy /> :
                    (<>

                        {fullScreen ?
                            <LeanSidebarWithNotiResp hover={false} /> : (
                                <>
                                    <LeanSidebarWithNoti hover={true} />
                                    <LeanAppSideBar />
                                </>
                            )}


                        <Drawer variant="persistent" anchor="right" open={props.fireDrawerReducerNow.fireDrawer} >
                            <IconButton classes={{ root: 'lean-icon-button-changes-for-chat' }} color="inherit" aria-label="open drawer" onClick={(e) => { props.fireDrawer("fireDrawer") }}> <CloseIcon /> </IconButton>
                            <ChatBox />
                        </Drawer>

                        <div className="lean-main-app-content">

                            <Switch>

                                <Route exact path="/">
                                    <VisitorMobileRegistration />
                                </Route>

                                <Route exact path="/MyAppointments">
                                    {fullScreen ?
                                        <MyAppointmentsCards /> : <MyAppointments />
                                    }
                                </Route>

                                <Route exact path="/VisitorTable">
                                    {fullScreen ?
                                        <ListOfVisitorsCards /> : <VisitorTable />
                                    }

                                </Route>

                                <Route exact path="/MyFrequentVisitor">
                                    {fullScreen ?
                                        <FrequentVisitorsCards /> : <MyFrequentVisitor />
                                    }
                                </Route>

                                

                                <Route exact path="/profileModal">
                                    <ProfileModal />
                                </Route>

                                <Route exact path="/WebVisitorRegistrationForm">
                                    <WebVisitorRegistrationForm />
                                </Route>

                                <Route exact path="/UserAppointmentsModal">
                                    <UserAppointmentsModal />
                                </Route>

                                <Route exact path="/AddAppointmentInAModal">
                                    <AddAppointmentInAModal />
                                </Route>

                                <Route exact path="/ThanksForRegMobileVisitor">
                                    <ThanksForRegMobileVisitor />
                                </Route>


                                {/* //http://localhost:3000/vm/web/pages/vmAdminDashboard */}
                                <Route exact path="/VmDashboard">
                                    <VmDashboard />
                                </Route>
                                <Route exact path="/VmSetting">
                                    <VmSetting />
                                </Route>

                                {/* <Route exact path="/ListOfSupport">
                                    <ListOfSupport />
                                </Route> */}

                            </Switch>

                        </div>
                    </>

                    )}
            </Router>
        </ModalContextProvider>
    );
}

const mapStateToProps = state => {
    const { userReducer, getApplicationReducer, appCookieRecv, fireDrawerReducer } = state;
    console.log("fireDrawerReducer-->", fireDrawerReducer);
    return { userReducer, getApplicationReducer, appCookieRecv, fireDrawerReducerNow: fireDrawerReducer };
};
export default connect(mapStateToProps, { getUserInfoAction, getApplicationsAction, getTenantAction, setAllApplications, verifyApps, fireDrawer })(
    App
);