import React from 'react';
import { Grid } from "@material-ui/core";
import PinInput from '../../components/PinInput/PinInput'
import AcceptImage from '../../assets/accept.png'
import Button from '../../components/Button/Button';
import "./ThanksForRegMobileVisitor.scss"


export default function OtpFormMobileRegistration(props) {
    return (
        <Grid container>
            <Grid className="lean-grid-item-Heading" item xs={12}>
              <img className="acceptImage" src={AcceptImage}/>
            </Grid>
            <Grid className="lean-grid-item-Heading" item xs={12}>
                <div className="lean-section-text-header">Thank You for Registering with our Visitor Management System</div>
            </Grid>
            <Grid className="lean-grid-item-SubHeading" item xs={12}>
                <div className="lean-section-text-header">Your registeration has been sent to Kay Totleben for approval you will receive the notification once its approved</div>
            </Grid>
            <Grid className="lean-grid-item-Custombutton" item xs={12}>
                <Button
                    text="Ok"
                    size="small-btn"
                    variant="contained"
                    color="primary"
                    // onClick={}
                />
            </Grid>
        </Grid>
    );
}


