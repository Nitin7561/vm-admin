import React from 'react'
import { Grid, CircularProgress, Hidden, useTheme, useMediaQuery } from '@material-ui/core'

import TableFilters from '../TableFilters/TableFilters';
import TableHeaderFilters from '../../components/TimeFiltersAndDropDowns/TableHeaderFilters';
import TableHeaderFilterConfig from '../../layouts/TableFilters/TableHeaderFiltersConfig';
import ListOfSupportTable from '../../components/ListOfSupportTable/ListOfSupportTable';
import { ModalContext } from "../../utils/ModalContext";
import './myBookings.scss';
import { connect } from 'react-redux';
import { STATUS_REV_ENUM } from '../../utils/Enums';

// import { loadMyBookings } from '../../actions/myBookings';
// import TableCard from '../../components/Tables/TableCard';


const ListOfSupport = ({ refresher }) => {

    const [page, setPage] = React.useState(1);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [sortColumnIndex, setSortIndex] = React.useState(3);
    const [sortOrder, setSortOrder] = React.useState(-1);
    const [bookings, changeBookings] = React.useState([]);

    // let { isLoading, singleBookings, pagination, isError, error } = myBookings;

    // let totalNoOfPages = pagination ? (pagination.uniqueDocs <= rowsPerPage ? 1 : Math.ceil((pagination.uniqueDocs)/rowsPerPage)) : 0;

    // let totalNoOfDocs = pagination ? (pagination.uniqueDocs <= rowsPerPage ? singleBookings.length : pagination.uniqueDocs) : 0;

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [status, setStatus] = React.useState("All");
    const [timeStatus, changeTimeStatus] = React.useState("All");
    const [modules, changeModule] = React.useState("All Modules");
    const [severity, changeSeverity] = React.useState("All");
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });


    const [timing, setTiming] = React.useState({ fromTime: Math.ceil((((new Date()).getTime())) - 2 * 365 * 24 * 60 * 60 * 1000), toTime: Math.ceil((((new Date()).getTime())) + 2 * 365 * 24 * 60 * 60 * 1000) });
    
    let rxdBatchIds = [];

    let onChangeRX = (newRX) => {
        console.log("newRX", newRX);
        rxdBatchIds = [...newRX]
    }


    const statusValues = [
        { value: "All", name: "All" },
        { value: "Upcoming", name: "Upcoming" },
        { value: "Cancelled", name: "Cancelled" },
        { value: "Completed", name: "Completed" },
        { value: "Ongoing", name: "Ongoing" },
    ]

    const moduleValues = [
        { value: "All Modules", name: "All Modules" },
        { value: "Visitor Managment", name: "Visitor Managment" },
        { value: "Mother App", name: "Mother App" },
        { value: "Room Booking", name: "Room Booking" },
    ]

    const severityValues = [
        { value: "All", name: "All" },
        { value: "Critical", name: "Critical" },
        { value: "Error", name: "Error" },
        { value: "Warning", name: "Warning" },
    ]

    
    const timeStatusValues = [
        { value: "All", name: <div className="lean-calendar-image" > <span className="lean-calendar-image lean-calendar-image-text">All</span></div> },
        { value: "Today", name: <div className="lean-calendar-image" > <span className="lean-calendar-image lean-calendar-image-text">Today</span></div> },
        { value: "Last Week", name: <div className="lean-calendar-image" > <span className="lean-calendar-image lean-calendar-image-text">Last Week</span></div> },
        { value: "Last Month", name: <div className="lean-calendar-image"> <span className="lean-calendar-image lean-calendar-image-text">Last Month</span></div> },
        { value: "Last 6 Months", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text"> Last 6 Months</span></div> },
        { value: "Last Year", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Last Year</span></div> },
        { value: "Custom Date", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Custom Date</span></div> }
    ]

    const onTimeRangeChange = (newRange) => {

        changeTimeStatus(newRange)
        switch (newRange) {
            case "All": openAll(); break;
            case "Today": openToday(); break;
            case "Last Week": openLastWeek(); break;
            case "Last Month": openLastMonth(); break;
            case "Last 6 Months": openLastSixMonths(); break;
            case "Last Year": openLastYear(); break;
            case "Custom Date": openCustomDateModal(); break;
        }
    }

    var lastday = function (y, m) {
        return new Date(y, m + 1, 0).getDate();
    }

    function openAll() {



        var fromTime = 0
        var toTime = 0

        filterMyBookings(fromTime, toTime)
        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
        // console.log("New Date", fromTime, toTime);
        // console.log("New Time", timestamp);
    }

    function openToday() {
        var newDate = new Date();
        var fromTime = newDate.setHours("00", "00");
        var toTime = newDate.setHours("23", "59");

        filterMyBookings(fromTime, toTime)
        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
        // console.log("New Date", fromTime, toTime);
        // console.log("New Time", timestamp);
    }

    function openLastWeek() {
        var curr = new Date();
        var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay() - 6));
        var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
        var fromTime = firstday.setHours("00", "00");
        var toTime = lastday.setHours("23", "59");

        filterMyBookings(fromTime, toTime)

        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
        console.log("New Time", firstday, lastday);
    }

    function openLastMonth() {
        var lastMonth = new Date().getMonth() - 1;  //return number
        var setLastMonthDate = new Date().setMonth(lastMonth)  //set month
        var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
        var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
        var lastDayOfMonth = lastday(new Date(fromTime).getFullYear(), lastMonth)
        var temp = new Date(setLastMonthDate).setDate(lastDayOfMonth);
        var toTime = new Date(temp).setHours("23", "59")

        filterMyBookings(fromTime, toTime)


        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })

        console.log("openLastMonth->", fromTime, toTime);
    }

    function openLastSixMonths() {
        var lastSixMonth = new Date().getMonth() - 6;  //return number
        var setLastMonthDate = new Date().setMonth(lastSixMonth)  //set month
        var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
        var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
        var lastMonth = (new Date().getMonth() - 1)
        var lastMonthDate = new Date().setMonth(lastMonth)
        console.log(lastMonth, lastMonthDate)
        var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
        var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
        var toTime = new Date(temp).setHours("23", "59");

        filterMyBookings(fromTime, toTime)

        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });

        console.log("openLastSixMonths->", toTime)
    }

    function openLastYear() {
        //Check last year
        var lastElevenMonth = new Date().getMonth() - 12;  //return number
        var setLastMonthDate = new Date().setMonth(lastElevenMonth)  //set month
        var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
        var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")

        console.log("From time ===>" + fromTime);
        var lastMonth = (new Date().getMonth() - 1)
        var lastMonthDate = new Date().setMonth(lastMonth)
        console.log(lastMonth, lastMonthDate)
        var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
        var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
        var toTime = new Date(temp).setHours("23", "59");
        console.log("openLastYear->", fromTime, toTime)

        filterMyBookings(fromTime, toTime)

        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });
    }

    let openCD = (fromTime, toTime) => {
        console.log("Ntitit", fromTime, toTime);
        filterMyBookings(fromTime, toTime)
        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });
    }

    function openCustomDateModal() {
        console.log("On click of custom date");
        openModal({ name: 'CustomDate', props: { openCD }})

    }

    const onChangeStatus = (newStatus) => {

        // console.log("onChangeStatus", newStatus, JSON.stringify({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: rowsPerPage,
        //     pageNumber: 1,
        //     sortColumnIndex,
        //     sortOrder,
        //     statusFilter: STATUS_REV_ENUM[newStatus],
        //     rxdBatchIds: [],
        // }))
        setStatus(newStatus);
        setPage(1)

        // changeBookings([])
        // loadMyBookings({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: rowsPerPage,
        //     pageNumber: 1,
        //     sortColumnIndex,
        //     sortOrder,
        //     statusFilter: STATUS_REV_ENUM[newStatus],
        //     rxdBatchIds: []
        // });
    }

    let onClickSort = (newSI) => {

        if (newSI === null) {
            return;
        }
        let newSortOrder = sortColumnIndex;
        if (newSI === sortColumnIndex) {
            if (sortOrder === -1) {
                newSortOrder = 1;
            } else {
                newSortOrder = -1
            }
        } else {
            newSortOrder = 1
        }

        setSortIndex(newSI)
        setSortOrder(newSortOrder)

        // loadMyBookings({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: rowsPerPage,
        //     pageNumber: 1,
        //     sortColumnIndex: newSI,
        //     sortOrder: newSortOrder,
        //     statusFilter: STATUS_REV_ENUM[status],
        //     rxdBatchIds: []
        // });

    }

    const handleChangePage = (event, newPage, pagination) => {
        setPage(newPage);

        // console.log("newRx", newPage, JSON.stringify({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: rowsPerPage,
        //     pageNumber: newPage,
        //     sortColumnIndex,
        //     sortOrder,
        //     statusFilter: STATUS_REV_ENUM[status],
        //     rxdBatchIds,
        // }));

        // loadMyBookings({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: rowsPerPage,
        //     pageNumber: newPage,
        //     sortColumnIndex,
        //     sortOrder,
        //     statusFilter: STATUS_REV_ENUM[status],
        //     rxdBatchIds,
        // });

        // onLoad({
        //   fromTime: Math.ceil((((new Date()).getTime())/1000) - 2 * 365 * 24 * 60 * 60 ),
        //   toTime: Math.ceil((((new Date()).getTime())/1000) + 2 * 365 * 24 * 60 * 60 ),
        //   rowsPerPage: rowsPerPage,
        //   actualCount: pagination ? pagination.pages[newPage].actualCount : 0,
        //   sortColumnIndex: 1,
        //   sortOrder: 1,
        //   statusFilter: 4
        // });
    };

    // const

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(1);

        // loadMyBookings({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: event.target.value,
        //     pageNumber: 1,
        //     sortColumnIndex,
        //     sortOrder,
        //     statusFilter: STATUS_REV_ENUM[status],
        //     rxdBatchIds: []
        // });

        // onLoad({
        //   fromTime: Math.ceil((((new Date()).getTime())/1000) - 2 * 365 * 24 * 60 * 60 ),
        //   toTime: Math.ceil((((new Date()).getTime())/1000) + 2 * 365 * 24 * 60 * 60 ),
        //   rowsPerPage: ,
        //   actualCount: 0,
        //   sortColumnIndex: 3,
        //   sortOrder: 1,
        //   statusFilter: 4
        // });
    };

    const filterMyBookings = (fromTime, toTime) => {

        console.log("filterMyBookings", fromTime, toTime);
        setTiming({ fromTime, toTime });
        setPage(1);

        // changeBookings([]);

        // loadMyBookings({
        //     fromTime: Math.floor(fromTime / 1000),
        //     toTime: Math.floor(toTime / 1000),
        //     rowsPerPage: rowsPerPage,
        //     pageNumber: 1,
        //     sortColumnIndex,
        //     sortOrder,
        //     statusFilter: STATUS_REV_ENUM[status],
        //     rxdBatchIds: []
        // });
    }



    // React.useEffect(() => {
    //     // loadMyBookings({
    //     //     fromTime: Math.floor(timing.fromTime / 1000),
    //     //     toTime: Math.floor(timing.toTime / 1000),
    //     //     rowsPerPage: rowsPerPage,
    //     //     pageNumber: page,
    //     //     sortColumnIndex,
    //     //     sortOrder,
    //     //     statusFilter: STATUS_REV_ENUM[status],
    //     //     rxdBatchIds,
    //     // });
    // }, [refresher.myBookings]);


    // React.useEffect(() => {
    //     if(fullScreen) {
    //         // console.log("bookings", singleBookings, bookings);
    //         // changeBookings([...bookings, ...singleBookings])
    //     }
    // }, []);


    const handleScroll = (e) => {

            if( !fullScreen ) return;

            // if( isLoading || isError || (totalNoOfPages < (page+1))) return;

            if(e.currentTarget.offsetHeight + e.currentTarget.scrollTop == e.currentTarget.scrollHeight)
            {
                handleChangePage(null, page+1, null);
                console.log('SCROLL eop')
            }
    }

    return (
        <div style={{height: "100%"}} onScroll={(e) => handleScroll(e)}> 
            <Grid container className="lean-book-table" >

                
                <Grid item xs={12} className="lean-grid-item">
                    <TableFilters filterMyBookings={filterMyBookings} status={status} setStatus={onChangeStatus} fullScreen={fullScreen} totalNoOfDocs={10} />
                </Grid>

                <Grid item xs={12} className="lean-grid-item">
                   <TableHeaderFilterConfig statusValues={statusValues} timeStatusValues={timeStatusValues} modules={modules} moduleValues={moduleValues} severity={severity} severityValues={severityValues}
                    status={status} setStatus={setStatus} changeModule={changeModule} changeSeverity = {changeSeverity}
                    timeStatus={timeStatus}  onTimeRangeChange={onTimeRangeChange}  fullScreen={fullScreen} totalNoOfDocs={10} />
                </Grid>

                <Grid item xs={12} className="lean-grid-item">
                    {/* <Hidden smUp implementation="css">
                        <TableCard myBookings={{...myBookings, singleBookings: bookings}} page={page} setPage={setPage} rowsPerPage={rowsPerPage} setRowsPerPage={setRowsPerPage} handleChangePage={handleChangePage} handleChangeRowsPerPage={handleChangeRowsPerPage}
                        totalNoOfDocs={totalNoOfDocs}
                            onChangeRX={onChangeRX} onClickSort={onClickSort} sortColumnIndex={sortColumnIndex} sortOrder={sortOrder} />
                    </Hidden> */}

                    <Hidden xsDown implementation="css">
                        <ListOfSupportTable page={page} setPage={setPage} rowsPerPage={rowsPerPage} setRowsPerPage={setRowsPerPage} handleChangePage={handleChangePage} handleChangeRowsPerPage={handleChangeRowsPerPage}
                            onChangeRX={onChangeRX} onClickSort={onClickSort} sortColumnIndex={sortColumnIndex} sortOrder={sortOrder}
                        />
                    </Hidden>

                </Grid>
            </Grid>
        </div>
    )
}

const mapStateToProps = state => {
    let { refresher } = state;
    console.log("ListOfSupport", refresher);
    return { refresher };
};

export default connect(mapStateToProps, {})(ListOfSupport);
