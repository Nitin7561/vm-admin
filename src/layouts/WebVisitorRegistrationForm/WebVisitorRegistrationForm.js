import React from "react";
import VisitorMobileRegistration from "../VisitorMobileRegistration/VisitorMobileRegistration";


import './WebVisitorRegistrationForm.scss'


export default function WebVisitorRegistrationForm() {

    return (
        <div className="lean-visitor-card-background">
            <div className="lean-visitor-card lean-card">
                <VisitorMobileRegistration/>
            </div>
        </div>
    );
}