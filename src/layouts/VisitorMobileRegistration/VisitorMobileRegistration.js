import React from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import MaterialButton from '@material-ui/core/Button';

import StepLabel from '@material-ui/core/StepLabel';
import { Grid } from "@material-ui/core";
import Button from '../../components/Button/Button';
import Typography from '@material-ui/core/Typography';
import RegisterForm from './RegistrationForm'
import FDC from '../../components/FaceDetectCam/FaceDetectCam'
import FDC1 from '../../components/FaceDetectCam1/FaceDetectCam1'
import OtpFormMobileRegistration from '../../layouts/OtpFormMobileRegistration/OtpFormMobileRegistration'
import ThanksForRegMobileVisitor from '../ThanksForRegMobileVisitor/ThanksForRegMobileVisitor'
import "./VisitorMobileRegistration.scss"
import PhotoCapture from '../../components/PhotoCapture/PhotoCapture'
import { Link } from 'react-router-dom'
import FaceDetectCam from '../../components/FaceDetectCam/FaceDetectCam';

// var Link = require('react-router').Link

function getSteps() {
    return ['Personal Info', 'OTP Verification', 'Upload Photo', 'Upload ID Proof'];
}

function getStepContent(step) {
    switch (step) {
        case 0:
            return 'Personal Info';
        case 1:
            return 'OTP Verification';
        case 2:
            return 'Upload Photo';
        case 3:
            return 'Upload ID Proof';
        default:
            return 'Unknown stepIndex';
    }
}

function GetEachStep(step, steps, setActiveStep) {

    const [dataToAddVisitor, setData] = React.useState({
        name: "",
        mobile: "",
        email: "",
        typeOfPurpose: "",
        whereFrom: "",
        password: "",
        img1: "",
        govId: ""
    });
    console.log("----------------------", dataToAddVisitor)

    function submit(e) {
        var requestOptions = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(dataToAddVisitor)
        }
        fetch("https://fe.leantron.dev:3003/api/ver1.2/visitor/addVisitor", requestOptions)
            .then(response => response.text())
            .then(result => console.log("11111111111111111", result))
            .catch(error => console.log('error111111111111', error));
    }

    switch (step) {
        case 0:
            return <RegisterForm sendFirstForm={(e) => { setData({ ...dataToAddVisitor, ...e }) }} />;
        case 1:
            return <OtpFormMobileRegistration step={step} steps={steps} setActiveStep={setActiveStep} />;
        case 2:
            return <PhotoCapture type={"photo"} sendImages={(e) => { setData({ ...dataToAddVisitor, img1: e[0] }) }} FDC={FDC} />;
            
        case 3:
            return <PhotoCapture type={"id"} sendImages={(e) => {
                console.log(e)
                setData({ ...dataToAddVisitor, govId: e[0] })
                submit()
            }} FDC={FDC1} />;
        default:
            return <></>;
    }
}


export default function CustomizedSteppers() {

    const [imageCaptured, setImageCaptured] = React.useState(false);

    const [activeStep, setActiveStep] = React.useState(0);

    const steps = getSteps();

    const handleNext = () => {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    return (
        <Grid container>
            <Grid className="lean-grid-item-Heading" item xs={12}>
                <div className="lean-section-text-header">Visitor Registration Form</div>
            </Grid>
            <Grid className="lean-grid-item-SubHeading" item xs={12}>
                <div className="lean-section-text-header">Please fill the registration form</div>
            </Grid>

            <Grid className="lean-grid-item" item xs={12}>
                <Stepper activeStep alternativeLabel activeStep={activeStep}>
                    {steps.map((label, index) => (
                        <Step classes={{ alternativeLabel: 'lean-stepper-alternativeLabel' }} key={label}>
                            <StepLabel classes={{ iconContainer: `${index === activeStep ? "lean-stepper-label-active" : "lean-stepper-label"}` }}>{label}</StepLabel>
                        </Step>
                    ))}
                </Stepper>

            </Grid>


            <Grid className="lean-grid-item" item xs={12}>

                {activeStep === steps.length ? (

                    <>
                        {/* <Button text="Reset" onClick={handleReset}></Button> */}
                    </>

                ) : (


                    GetEachStep(activeStep, steps, setActiveStep)


                    )}

                <Grid item className="lean-grid-item" xs={12}>

                    <div className="stepsandNextButton">


                        {activeStep === steps.length - 1 ? (

                            <Link to="/ThanksForRegMobileVisitor">
                                <Button
                                    text="Finish"
                                    size="small-btn"
                                    variant="contained"
                                    color="primary"

                                />

                            </Link>

                        ) : (activeStep === 1 ? (
                            <div>
                                <span className="spanDisplayNone">Steps {activeStep + 1 + "/" + steps.length}</span>
                                <Button
                                    extraClasses="buttonDisplayNone"
                                />
                            </div>

                        ) : (
                                <>
                                    <span className="stepsSpan">Steps {activeStep + 1 + "/" + steps.length}</span>
                                    <Button
                                        text="Next"
                                        size="small-btn"
                                        variant="contained"
                                        color="primary"
                                        onClick={handleNext}
                                    />
                                </>

                            )
                            )}
                    </div>
                </Grid>
            </Grid>
        </Grid>
    );
}
