import React from 'react';
import { Grid } from "@material-ui/core";
import { InputTextField } from "../../components/InputTextField/InputTextField";
import { InfoStaticText } from "../../components/InputTextField/InfoStaticText";
import { DropdownField } from "../../components/InputTextField/DropdownField";
import Checkbox from '@material-ui/core/Checkbox';
import "./VisitorMobileRegistration.scss"
import {connect} from "react-redux"
import {sendOtp} from "../../redux/actions/MobileVerification/sendOtp"

function RegisterForm(props) {

  const [name, changeName] = React.useState("");
  const [mobileNumber, changeMobileNumber] = React.useState("");
  const [email, changeEmail] = React.useState("");
  const [typeOfPurpose, changeTypeOfPurpose] = React.useState("Official");
  const [whereFrom, changeWhereFrom] = React.useState();
  const [meetingPerson, changeMeetingPerson] = React.useState();
  const [password, changePassword] = React.useState();
  const [confirmPassword, changeConfirmPassword] = React.useState();

  const handleChange = event => {
    if(name=="" || mobileNumber==""){
        alert("please fill every fields out there, #NRC")
    }else{
        var obj={
            name,
            mobile:mobileNumber,
            email,
            typeOfPurpose,
            whereFrom,
            password
            
        }
        props.sendFirstForm?props.sendFirstForm(obj):console.log("no func sent");
    }
    // to send otp 
    props.sendOtp({
        mobile:mobileNumber
    })
    // if(props.sendOtpReducer.error){
    //     console.log(props.sendOtpReducer)
    // }
  };


  

  return (
    <Grid container>     
          <Grid className="lean-grid-item" item xs={12} md={6}>
          <InputTextField 
              value={name}
              eventHandler={changeName}
              label="Name *"
              placeholder="Please enter your name"
          />
          </Grid>
          <Grid className="lean-grid-item" item xs={12} md={6}>
          <InputTextField 
              type = "number" 
              value={mobileNumber}
              eventHandler={changeMobileNumber}
              label="Mobile Number *"
              placeholder="Please enter mobile number"/>
          </Grid>
          <Grid className="lean-grid-item" item xs={12} md={6}>
          <InputTextField 
              value={email}
              eventHandler={changeEmail}
              label="Email *"
              placeholder="Please enter Email-ID"
          />
          </Grid>
          <Grid className="lean-grid-item" item xs={12} md={6}>
          <DropdownField
              label="Type of Purpose *"
              value={typeOfPurpose}
              eventHandler={changeTypeOfPurpose}
              menuItems={[
              { value: "Official", name: "Official" },
              { value: "Official", name: "Official" }
              ]}
          />
          </Grid>
          <Grid className="lean-grid-item" item xs={12} md={6}>
          <InputTextField 
              value={whereFrom}
              eventHandler={changeWhereFrom}
              label="Where are you From *"
              placeholder="Please enter where are you from?"
          />
          </Grid>
          <Grid className="lean-grid-item" item xs={12} md={6}>
          <InputTextField 
              disabled="true"
              value={meetingPerson}
              eventHandler={changeMeetingPerson}
              label="Meeting Person *"
              placeholder="Please enter whom you are meeting"
          />
          </Grid>

          <Grid className="lean-grid-item" item xs={12} md={6}>
          <InputTextField 
              type="password"
              value={password}
              eventHandler={changePassword}
              label="Password *"
              placeholder="Please enter your password"
          />
          </Grid>

          <Grid className="lean-grid-item" item xs={12} md={6}>
          <InputTextField 
              type="password"
              value={confirmPassword}
              eventHandler={changeConfirmPassword}
              label="Confirm Password *"
              placeholder="Please confirm your password"
          />
          </Grid>

          <Grid className="lean-grid-item-termsandconditions" item xs={12}>

          <Checkbox
              value="default"
              inputProps={{ 'aria-label': 'checkbox with default color' }}
              style = {{color:"#030240"}}
              onChange={handleChange}
          />
          <InfoStaticText 
              disabled="true"
              value={meetingPerson}
              eventHandler={changeMeetingPerson}
              label="I read and agree to Terms & Conditions."
              placeholder="Please enter whom you are meeting"
          />
          </Grid>
    </Grid>
  );
}
const mapStateToProps = state => {
    const {  sendOtpReducer } = state;
    console.log("sendOtp-->", sendOtpReducer);
    return {  sendOtpReducer: sendOtpReducer };
};
export default connect(mapStateToProps, { sendOtp: sendOtp })(
    RegisterForm
);