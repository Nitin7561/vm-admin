import React from 'react';
import { Grid } from "@material-ui/core";
import PinInput from '../../components/PinInput/PinInput'
import Button from '../../components/Button/Button'
import { connect } from "react-redux";
import { verifyOtp } from '../../redux/actions/MobileVerification/verifyOtp'
import { sendOtp } from '../../redux/actions/MobileVerification/sendOtp'
import "./OtpFormMobileRegistration.scss"


 function OtpFormMobileRegistration(props) {

    const [otp,setotp]=React.useState(0)
   function verifyOtp1(otp){
    console.log("Button is clicked",otp)
       //TODO 
       if(otp){ 
           console.log("inside verifyPOtp1",props.sendOtpReducer)
           try{
               props.verifyOtp({
                   session_id:props.sendOtpReducer.session_id,
                   otp:otp
               })  
           }catch(err){
               console.log("*************",err)
           }
       }
       else{
        //    alert("enter otp please")
       }
       console.log("*****************88",props.getVerifyOtpReducer.data)
       if(props.getVerifyOtpReducer.error){
            alert("otp error")
       }
       else{
        //    alert("go on ahead, you bitch")
           props.setActiveStep(props.step + 1) 
       }
   }
   
    const resendOtp = () => {
        //to send otp `
        console.log("resending otp in ", props.mobile)
        props.sendOtp({
            mobile:props.mobile
        })
      };
    return (
        <Grid container>
            <Grid className="lean-grid-item-Heading" item xs={12}>
                <div className="lean-section-text-header">Enter the <b>OTP</b> sent to <b>mobile_number</b></div>
            </Grid>
            <Grid className="lean-grid-item-Heading" item xs={12}>
                <PinInput height="44px" onComplete={(e) => { 
                    setotp(e) }} width="44px" length={6}/>
            </Grid>
            <Grid className="stepsandNextButtonfromOtpVerifyScreen" item xs={12}>
                <span className="stepsSpanfromOtpVerifyScreen">Steps {props.step + 1 +"/"+ props.steps.length}</span>
                <Button onClick={()=>{// props.setActiveStep(props.step + 1) 
                                        console.log("Button is clicked")
                                         verifyOtp1(otp) }} size="big-btn" extraClasses="verifyOtpCenter" text="Verify OTP">Verify Otp</Button>
            </Grid>
            <Grid className="lean-grid-item-Heading" item xs={12}>
            <span>Didn't receive the OTP? <button onClick={()=>{resendOtp()}}  className="button1"><b>Resend OTP</b></button></span>
            </Grid>
        </Grid>
    );
}
const mapStateToProps = state => {
    const {  getVerifyOtpReducer,sendOtpReducer } = state;
    // console.log("Message-->", getDetailForVisitorReducer);
    return { sendOtpReducer,getVerifyOtpReducer };
};
export default connect(mapStateToProps, { verifyOtp: verifyOtp ,sendOtp:sendOtp})(
    OtpFormMobileRegistration
);