import React from 'react'
import { HeaderComponent } from '../../components/TimeFiltersAndDropDowns/HeaderComponent'
import EventNoteIcon from '@material-ui/icons/EventNote';
import { ModalContext } from "../../utils/ModalContext";
import { Grid } from '@material-ui/core';

const TableFilters = ({ filterMyBookings, status, setStatus,  fullScreen, totalNoOfDocs }) => {
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    const [timeStatus, changeTimeStatus] = React.useState("All");


    const statusValues = [
        { value: "All", name: "All" },
        { value: "Upcoming", name: "Upcoming" },
        { value: "Cancelled", name: "Cancelled" },
        { value: "Completed", name: "Completed" },
        { value: "Ongoing", name: "Ongoing" },
    ]

    var lastday = function (y, m) {
        return new Date(y, m + 1, 0).getDate();
    }

    function openAll() {



        var fromTime = 0
        var toTime = 0

        filterMyBookings(fromTime, toTime)
        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
        // console.log("New Date", fromTime, toTime);
        // console.log("New Time", timestamp);
    }

    function openToday() {
        var newDate = new Date();
        var fromTime = newDate.setHours("00", "00");
        var toTime = newDate.setHours("23", "59");

        filterMyBookings(fromTime, toTime)
        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
        // console.log("New Date", fromTime, toTime);
        // console.log("New Time", timestamp);
    }

    function openLastWeek() {
        var curr = new Date();
        var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay() - 6));
        var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
        var fromTime = firstday.setHours("00", "00");
        var toTime = lastday.setHours("23", "59");

        filterMyBookings(fromTime, toTime)

        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
        console.log("New Time", firstday, lastday);
    }

    function openLastMonth() {
        var lastMonth = new Date().getMonth() - 1;  //return number
        var setLastMonthDate = new Date().setMonth(lastMonth)  //set month
        var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
        var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
        var lastDayOfMonth = lastday(new Date(fromTime).getFullYear(), lastMonth)
        var temp = new Date(setLastMonthDate).setDate(lastDayOfMonth);
        var toTime = new Date(temp).setHours("23", "59")

        filterMyBookings(fromTime, toTime)


        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })

        console.log("openLastMonth->", fromTime, toTime);
    }

    function openLastSixMonths() {
        var lastSixMonth = new Date().getMonth() - 6;  //return number
        var setLastMonthDate = new Date().setMonth(lastSixMonth)  //set month
        var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
        var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
        var lastMonth = (new Date().getMonth() - 1)
        var lastMonthDate = new Date().setMonth(lastMonth)
        console.log(lastMonth, lastMonthDate)
        var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
        var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
        var toTime = new Date(temp).setHours("23", "59");

        filterMyBookings(fromTime, toTime)

        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });

        console.log("openLastSixMonths->", toTime)
    }

    function openLastYear() {
        //Check last year
        var lastElevenMonth = new Date().getMonth() - 12;  //return number
        var setLastMonthDate = new Date().setMonth(lastElevenMonth)  //set month
        var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
        var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")

        console.log("From time ===>" + fromTime);
        var lastMonth = (new Date().getMonth() - 1)
        var lastMonthDate = new Date().setMonth(lastMonth)
        console.log(lastMonth, lastMonthDate)
        var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
        var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
        var toTime = new Date(temp).setHours("23", "59");
        console.log("openLastYear->", fromTime, toTime)

        filterMyBookings(fromTime, toTime)

        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });
    }

    let openCD = (fromTime, toTime) => {
        console.log("Ntitit", fromTime, toTime);
        filterMyBookings(fromTime, toTime)
        // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });
    }

    function openCustomDateModal() {
        console.log("On click of custom date");
        openModal({ name: 'CustomDate', props: { openCD }})

    }


    const onTimeRangeChange = (newRange) => {

        changeTimeStatus(newRange)
        switch (newRange) {
            case "All": openAll(); break;
            case "Today": openToday(); break;
            case "Last Week": openLastWeek(); break;
            case "Last Month": openLastMonth(); break;
            case "Last 6 Months": openLastSixMonths(); break;
            case "Last Year": openLastYear(); break;
            case "Custom Date": openCustomDateModal(); break;
        }
    }

    const timeStatusValues = [
        { value: "All", name: <div className="lean-calendar-image" > <span className="lean-calendar-image lean-calendar-image-text">All</span></div> },
        { value: "Today", name: <div className="lean-calendar-image" > <span className="lean-calendar-image lean-calendar-image-text">Today</span></div> },
        { value: "Last Week", name: <div className="lean-calendar-image" > <span className="lean-calendar-image lean-calendar-image-text">Last Week</span></div> },
        { value: "Last Month", name: <div className="lean-calendar-image"> <span className="lean-calendar-image lean-calendar-image-text">Last Month</span></div> },
        { value: "Last 6 Months", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text"> Last 6 Months</span></div> },
        { value: "Last Year", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Last Year</span></div> },
        { value: "Custom Date", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Custom Date</span></div> }
    ]

    return (
        <div>
            <HeaderComponent isRequired="yes" isStatusRequired="no" titleName="List Of Support" ButtonText="" TimeStatusValues={timeStatusValues} StatusValues={statusValues} status={status} setStatus={setStatus} onTimeRangeChange={onTimeRangeChange} timeStatus={timeStatus} />
        </div>
    )
}

export default TableFilters
