import React from 'react'
import { TableHeaderFilter } from '../../components/TimeFiltersAndDropDowns/TableHeaderFilters'
import EventNoteIcon from '@material-ui/icons/EventNote';
import { ModalContext } from "../../utils/ModalContext";
import { Grid } from '@material-ui/core';

const TableHeaderFilterConfig = (props) => {

    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    return (
        <div>
            <TableHeaderFilter isRequired="yes" isStatusRequired="yes" titleName="List Of Support" ButtonText="Add Appointment" statusValues={props.statusValues} timeStatusValues={props.timeStatusValues} modules={props.modules} moduleValues={props.moduleValues} severity={props.severity} severityValues={props.severityValues} status={props.status} setStatus={props.setStatus} changeModule={props.changeModule} changeSeverity = {props.changeSeverity} timeStatus={props.timeStatus}  onTimeRangeChange={props.onTimeRangeChange}  fullScreen={props.fullScreen} totalNoOfDocs={10} />
        </div>
    )
}

export default TableHeaderFilterConfig
