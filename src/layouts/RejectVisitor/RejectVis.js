import React, { useEffect } from "react";
import { InfoStaticText } from "../../components/InputTextField/InfoStaticText";
import { Divider, Radio, RadioGroup, FormControl, FormLabel, Button, FormControlLabel, Grid, ListItem, List, ListItemAvatar, ListItemText, Avatar,useTheme, useMediaQuery } from '@material-ui/core'
import { MLInputTextField } from "../../components/InputTextField/MLInputTextField.js"
import { connect } from "react-redux";
// import { rejectUser } from '../../redux/actions/reject';
import { ModalContext } from "../../utils/ModalContext";
import "./reject.scss";
import EmptySVG from '../../assets/emptyFav.svg';
import FullPageLoading from "../../components/Loaders/FullPageLoading";
import { getAllEnums } from "../../redux/actions/getAllEnums";
import {rejectUser} from '../../redux/actions/reject';
import {rejectUserRegistration} from '../../redux/actions/rejectUserRegistration'

const RejectVis = (props) => {
    const [description, changeDescription] = React.useState("");
    const [value, setValue] = React.useState('');
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => {console.log(name); setCurrentModal({ name, props })};
    const handleChange = event => {
        setValue(event.target.value);
        console.log(value)
    };

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

     React.useEffect(
        () => {
            console.log("Reject Appt -> on load")
            props.getTheEnumsNow();
        },[]
    );


    // console.log("Vis ID==>",visitorId)
    console.log("Vis ID==>",props.visitorId)


    const handleClick = e => {
        if (value == "") {
            alert("please select any reason")
        }

        if(props.registrationRejection)
        {
            console.log("From list of visitors reject")
            props.rejectUserRegistrationNow({
                "visitorSub":props.visitorSub,
                "approve":false,
                "reason":value,
                "description":description
            })
            openModal({name:'RejectedVisitorRegistrationModal',props:{}})
        }
        else
        {
            props.rejectUserNow({
                "appointmentId":props.appointmentId,
                "approve":false,
                "reason":value,
                "description":description
            })
            openModal({name:'RejectedVisitor',props:{}})
        }
        
    }

    let { isLoading, data, isError } = props.getAllEnumsReducer;

    console.log("DAAAATTEEEEEEEEEEE=>",data)
    console.log("GET ALL ENUMS",props.getAllEnumsReducer)

    return ( 
        <div style={{ position: 'relative', minWidth:fullScreen?'unset':'400px',minHeight:'350px' }}>
        {isLoading ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading> : (
            <>
            {data["rejectApptUser"].length == 0 ?
            (
                
                <>
                    <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                    <img src={EmptySVG} />
                    <span className="lean-table-empty-screen-text">No Visitors</span>
                    </Grid>
                </> 
            )
                  
            :
            (
                <div className="mainBody">
                    <div className="radioDiv">
                        <FormControl component="fieldset" >
                            <FormLabel component="legend">Reason</FormLabel>
                            <RadioGroup aria-label="position" name="position" value={value} onChange={handleChange} row>
                            {data["rejectApptUser"].map(( eachItemInArray =>{
                        
                            return(
                                        <FormControlLabel
                                            value= {eachItemInArray}
                                            control={<Radio />}
                                            label= {eachItemInArray}
                                            labelPlacement="end"
                                        />
                                )
                                
                            }))}
                            </RadioGroup>
                        </FormControl>
                    </div>

                    

                    <div className="descriptionDiv" >
                        <MLInputTextField
                            label="Description (optional)"
                            value={description}
                            eventHandler={changeDescription}
                            inputProps={{ maxLength: 150 }}
                        />
                    </div>

                    <div style={{ flex: 1 }}>
                        <Button classes={{ root: "cancel-appointment-button" }} onClick={(e) => handleClick(e)} >
                            Submit
                        </Button>
                    </div>

                </div>
            )
            }
            </>

        )}
        </div>
    );
};

const mapStateToProps = state => {
  const { rejectUserReducer,getAllEnumsReducer } = state;
  return { rejectUserReducer,getAllEnumsReducer };
};


export default connect(mapStateToProps, {rejectUserNow:rejectUser,rejectUserRegistrationNow:rejectUserRegistration,getTheEnumsNow:getAllEnums})(RejectVis);
