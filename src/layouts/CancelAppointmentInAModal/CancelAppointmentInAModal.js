import React from "react";
import Modal from '../../components/Modal/FunctionalModal';
import CancelAppointment from '../CancelAppt/CancellAppointment'

export default function AddAppointmentInAModal({closeModal,...props}) {
    // let {visitorName,visitorEmail,isDisabled} =props
    // console.log('Name', visitorName);
    // console.log('Email',visitorEmail);
    // console.log('isDisabled',isDisabled);
    return (
       <>
<Modal title="Cancel Appointment" handleClose={closeModal} children={<CancelAppointment visitorId={props.visitorId} appointmentId={props.appointmentId} roomName={props.roomName}/>} classStyles={{root:'modal-max-height'}}/>
       </>
    );
}
