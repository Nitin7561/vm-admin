import React from 'react';

import './Button.scss'

function Button({type, size, onClick, text, extraClasses,float}){    
    return(
        <button 
        className = {`${size?size:'large-btn'} ${type?type:'primary'} ${extraClasses?extraClasses:""} ${float?float:""}`} 
        onClick = {onClick?onClick:()=>{console.log("no func recd")}}>
                {text?text:"Button"}
        </button>
    )
}

export default Button;