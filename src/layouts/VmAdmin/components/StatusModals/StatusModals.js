import React,{useState,useEffect} from 'react';

import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { makeStyles } from "@material-ui/core/styles";

import Button from '../Button/Button'

import successImage from './Success.svg';
import failImage from './Oval.png';
import users2 from './users2.png';
import users from './users.png';
import devices from './devices.png';
import devices2 from './devices2.png'
import {connect} from 'react-redux'
import { ModalContext } from "../../../../utils/ModalContext";
import {deleteUser} from '../../VmSetting/Redux/actions/deleteUser'
import './StatusModals.scss'

const useStyles = makeStyles({
    input: {
      height: 44,
      width: "265px"
    },
    support:{
      height: 108,
      width: "450px",
    },
    dd:{
      height: 98,
      width: "580px",
    }
  });

function SuccessFailModals(props){
    const classes = useStyles();
 
    let [action, changeAction] = React.useState("")
    let [issue, changeIssue] = React.useState("")
    let [description, changeDescription] = React.useState("")
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({name, props}) => setCurrentModal({ name, props });
    const closeModal = () => setCurrentModal(null);

    console.log(props);


if( props.type === "submitSuccess"){
      return ModalBase(
        successImage,
        {
          num: 1,
          type: ["primary"],
          text: ["Close"],
          onClick: [
            () => {
              props.closeModal()
            }
          ]
        },
        "Success",
        "Your Suport Query has been submitted"
      );
    }
    else if (props.type == "deletesubmitFail"){
        return ModalBase(
          users2,
            {
              num: 1,
              type: ["primary"],
              text: ["Close"],
              onClick: [
                () => {
                  props.closeModal()
                }
              ]
            },
            "Deletion Of User Failed",
            "Please Try it Again"
          );
    } else if (props.type == "blockSubmitFail"){
      return ModalBase(
        users2,
          {
            num: 1,
            type: ["primary"],
            text: ["Close"],
            onClick: [
              () => {
                props.closeModal()
              }
            ]
          },
          " Blocking Of User Failed",
          "Please Try it Again"
        );
  }
  else if (props.type == "unBlockSubmitFail"){
    return ModalBase(
      users2,
        {
          num: 1,
          type: ["primary"],
          text: ["Close"],
          onClick: [
            () => {
              props.closeModal()
            }
          ]
        },
        " unBlocking Of User Failed",
        "Please Try it Again"
      );
}else if (props.type == "areYouSureDevice"){
        return ModalBase(
            devices2,
            {
              num: 2,
              type: ["primary","secondary"],
              text: ["Yes","No"],
              onClick: [
                () => {
                  props.closeModal()
                },
                () => {
                    props.closeModal()
                  }
              ]
            },
            "Delete",
            "Are you sure you want to delete this device?"
          );
    }else if (props.type == "devicesSuccess"){
        return ModalBase(
            devices,
            {
              num: 1,
              type: ["primary"],
              text: ["Close"],
              onClick: [
                () => {
                  props.closeModal()
                }
              ]
            },
            "Success",
            "This device has been deleted"
          );
    }else if (props.type == "areYouSureUser"){
        return ModalBase(
            users2,
            {
              num: 2,
              type: ["secondary","primary"],
              text: ["Cancel","Yes"],
              onClick: [
                () => {
                  props.closeModal()
                },
                () => {
                    let reqBody = {
                      visitorId:props.visitorInfo.visitorId,
                      visitorSub:props.visitorInfo.visitorSub
                    }
                    props.deleteUser(reqBody)
                    openModal({name:'VMSLoaderModal', props:{modalTypeSuccess:"deleteUserSuccess",modalTypeFail:"deletesubmitFail",closeModal:closeModal}})
                }
              ]
            },
            "Are you sure you want to delete this user?",
            "Everything related to it will get erased"
          );
    }else if (props.type == "deleteUserSuccess"){
        return ModalBase(
            users,
            {
              num: 1,
              type: ["primary"],
              text: ["Close"],
              onClick: [
                () => {
                  props.closeModal()
                }
              ]
            },
            "Visitor Successfully Deleted",
            "LOrem Epsum"
          );
    }else if (props.type == "blockUserSuccess"){
      return ModalBase(
          users,
          {
            num: 1,
            type: ["primary"],
            text: ["Close"],
            onClick: [
              () => {
                props.closeModal()
              }
            ]
          },
          "Visitor Successfully Blocked",
          "Lorem Epsum"
        );
  }else if (props.type == "unBlockUserSuccess"){
    return ModalBase(
        users,
        {
          num: 1,
          type: ["primary"],
          text: ["Close"],
          onClick: [
            () => {
              props.closeModal()
            }
          ]
        },
        "Visitor Successfully UnBlocked",
        "Lorem Epsum"
      );
}
    else if (props.type == "success"){
        return ModalBase(
            successImage,
            {
              num: 1,
              type: ["primary"],
              text: ["Close"],
              onClick: [
                () => {
                  props.closeModal()
                }
              ]
            },
            "Success",
            "This device has been deleted"
          );
    }



  function ModalBase(img, btn, heading, text,smaller) {
    return (
      <div>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className="modalU"
          open={true}
          onClose={props.closeModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={true}>
            <div className={smaller === "smaller"?"contentPaperSmaller":"contentPaper"}>
              <img className="modalImage" src={img} />
              <p className="modalHeading"> {heading} </p>
              <p className="modalText">{text}</p>
              {ButtonBase(btn)}
            </div>
          </Fade>
        </Modal>
      </div>
    );
  }

  
  function ButtonBase(btn) {
    if (btn.num === 1) {
      return (
        <div className="modalButtons">
          <Button
            text={btn.text[0]}
            onClick={btn.onClick[0]}
            type={btn.type[0]}
            size="small-btn"
          />
        </div>
      );
    } else if (btn.num === 2) {
      return (
        <div className="modalButtons">
          <Button
            text={btn.text[0]}
            onClick={btn.onClick[0]}
            type={btn.type[0]}
            size="small-btn"
          />
          <Button
            text={btn.text[1]}
            onClick={btn.onClick[1]}
            type={btn.type[1]}
            size="small-btn"
          />
        </div>
      );
    } else {
      return null;
    }
  }



}


const mapStateToProps = state => {
  let { vmsetting} = state;
  return {vmsetting};
};

export default connect(mapStateToProps, {deleteUser})(SuccessFailModals)
