import React from 'react';
// import Button from '../Button/Button';
import { DropdownField } from '../../components/InputTextField/DropdownField'
// import { InputAutoField } from "../InputTextField/InputAutoField";
import './HeaderComponent.scss';
import { ModalContext } from "../../utils/ModalContext";
// import Ancestor from '../Ancestor/Ancestor'
import { Button, InputAdornment } from '@material-ui/core';
import EventNoteIcon from '@material-ui/icons/EventNote';
import DescriptionIcon from '@material-ui/icons/Description';
import { InputAutoField } from "../../components/InputTextField/InputAutoField";

export const TableHeaderFilter = props => {
    
    // const { setCurrentModal } = React.useContext(ModalContext);
    // const openModal = ({ name, props }) => setCurrentModal({ name, props });

    const handleNext = () => {
        // openModal({ name: 'AddAppointmentInAModal', props: { "isDisabled": false } })
    }

    console.log("Statuses from Blah",props)
    return (


        <div className="lean-header-table-page">
                
            <div style={{paddingBottom:20}} className="lean-header-table-page-item three-dropdowns">
            
                {props.isRequired === "yes" ? (
                    <>
                        { props.isStatusRequired ==="yes"? (<>

                        {/* <span className="lean-header-table-page-item lean-status-label">All Modules</span> */}

                        {/* <Grid item xs={12} className="lean-card-filters-item-ancestors-campusName"> */}
                            

                          {props.isAncestorsNeeded=="yes"?(
                        
                          <></>
                          ):(<></>)}  
                       
                        {props.isAutocompleteNeeded=="yes"?(
                        <div style={{width:"300px",marginRight:"40.5vw"}}>
                            <InputAutoField />
                        </div>):(<></>)}
                        {/* <div className="lean-header-table-page-item lean-status-dropdown">
                            <DropdownField
                                // label="Status"
                                id="lean-header-comp-status"
                                value={props.modules}
                                eventHandler={props.changeModule}
                                menuItems={props.moduleValues ? props.moduleValues : null}
                                inputClass="lean-table-filters"

                            />
                        </div> */}

                        <div className="lean-header-table-page-item lean-time-status-dropdown">
                            <DropdownField
                                // label="Status"
                                id="lean-header-comp-time-range"
                                value={props.status}
                                eventHandler={props.setStatus}
                                menuItems={props.statusValues ? props.statusValues : null}
                                inputClass="lean-table-filters"
                                extraInputProps = {{
                                    // startAdornment: <InputAdornment>
                                    //     <EventNoteIcon fontSize='small'/>
                                    // </InputAdornment>
                                }}
                            />
                                                       
                                           
                        </div>
                        {/* <div style={{width:200, marginRight:8}}>
                        <DropdownField
                                    // label="Status"
                                    id="lean-header-comp-time-range"
                                    value={props.timeStatus}
                                    eventHandler={props.onTimeRangeChange}
                                    menuItems={props.TimeStatusValues ? props.TimeStatusValues : null}
                                    inputClass="lean-table-filters"
                                    extraInputProps = {{
                                        startAdornment: <InputAdornment>
                                            <EventNoteIcon fontSize='small'/>
                                        </InputAdornment>
                                    }}
                                /> 
                            </div> */}
                        {props.isAutocompleteNeeded === "yes"?(
                              <></>
                          ):(<>
                            <div style={{marginLeft:"10px"}} className="lean-header-table-page-item lean-time-status-dropdown">
                            <DropdownField
                                // label="Status"
                                id="lean-header-comp-time-range"
                                value={props.status}
                                eventHandler={props.setStatus}
                                menuItems={props.statusValues ? props.statusValues : null}
                                inputClass="lean-table-filters"
                                extraInputProps = {{
                                    // startAdornment: <InputAdornment>
                                    //     <EventNoteIcon fontSize='small'/>
                                    // </InputAdornment>
                                }}
                            />
                        </div>
                        </>)}

                        {props.isButtonRequired == "yes"?(<>
                            <div className="lean-header-table-page-item lean-header-table-page-item lean-addAppointment-button" >

                            <Button
                            style={{marginLeft:"10px", marginTop:"4px"}}
                                variant="contained"
                                disableElevation
                                className="lean-btn-primary lean-addAppointment-button"
                            // onClick={() => openModal({ name: 'BookingModal', props: room })}
                            >
                                {props.buttonText}
                            </Button>


                            </div>
                        
                        </>):(<></>) }

                        <button className = "exportXLSSSButton" onClick={()=>{console.log("No func sent")}}>
                                <DescriptionIcon  fontSize="medium"/>
                        </button>

                        {/* <div className="lean-header-table-page-item lean-addAppointment-button" >
                            <Button
                                variant="contained"
                                disableElevation
                                className="lean-header-table-page-item lean-btn-primary lean-addAppointment-button"
                            // onClick={() => openModal({ name: 'BookingModal', props: room })}
                            >
                                Book
                            </Button>


                        </div> */}
                    </>
                    )
                    :
                    (
                        <>
                        <div className="lean-header-table-page-item lean-time-status-dropdown">
                            <DropdownField
                                // label="Status"
                                id="lean-header-comp-time-range"
                                value={props.timeStatus}
                                eventHandler={props.onTimeRangeChange}
                                menuItems={props.TimeStatusValues ? props.TimeStatusValues : null}
                                inputClass="lean-table-filters"
                                extraInputProps = {{
                                    startAdornment: <InputAdornment>
                                        <EventNoteIcon fontSize='small'/>
                                    </InputAdornment>
                                }}
                            />
                        </div>
                    </>
                    ) }
                    </>
                ) : (
                        <>
                            <div className="lean-header-table-page-item lean-header-table-page-item lean-addAppointment-button" >

                                <Button
                                    variant="contained"
                                    disableElevation
                                    className="lean-btn-primary lean-addAppointment-button"
                                // onClick={() => openModal({ name: 'BookingModal', props: room })}
                                >
                                    Book
                            </Button>


                            </div>
                        </>
                    )}


            </div>


        </div>


    );
};
