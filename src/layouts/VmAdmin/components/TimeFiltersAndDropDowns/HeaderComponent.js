import React from 'react';
// import Button from '../Button/Button';
import { DropdownField } from '../InputTextField/DropdownField'
import './HeaderComponent.scss';
import { ModalContext } from "../../utils/ModalContext";
import { Button, InputAdornment } from '@material-ui/core';
import EventNoteIcon from '@material-ui/icons/EventNote';

export const HeaderComponent = props => {
    // const [status, changeStatus] = React.useState("All");
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    const handleNext = () => {
        openModal({ name: 'AddAppointmentInAModal', props: { "isDisabled": false } })
    }

    console.log(props);
    
    function getRemainingContent(){
        if(props.isRequired === "yes"){
            if(props.isStatusRequired === "yes"){
                return(
                    <>
                            <span className="lean-header-table-page-item lean-status-label">Status</span>
    
                            <div className="lean-header-table-page-item lean-status-dropdown">
                                <DropdownField
                                    // label="Status"
                                    id="lean-header-comp-status"
                                    value={props.status}
                                    eventHandler={props.setStatus}
                                    menuItems={props.StatusValues ? props.StatusValues : null}
                                    inputClass="lean-table-filters"
    
                                />
                            </div>
                            <div className="lean-header-table-page-item lean-time-status-dropdown">
                                <DropdownField
                                    // label="Status"
                                    id="lean-header-comp-time-range"
                                    value={props.timeStatus}
                                    eventHandler={props.onTimeRangeChange}
                                    menuItems={props.TimeStatusValues ? props.TimeStatusValues : null}
                                    inputClass="lean-table-filters"
                                    extraInputProps = {{
                                        startAdornment: <InputAdornment>
                                            <EventNoteIcon fontSize='small'/>
                                        </InputAdornment>
                                    }}
                                />
                            </div>
                            {/* <div className="lean-header-table-page-item lean-addAppointment-button" >
                                <Button
                                    variant="contained"
                                    disableElevation
                                    className="lean-header-table-page-item lean-btn-primary lean-addAppointment-button"
                                // onClick={() => openModal({ name: 'BookingModal', props: room })}
                                >
                                    Book
                                </Button>
    
    
                            </div> */}
                        </>
                )
            }
            else if(props.isButtonRequired == "yes"){
                return(
                    <>
                    <div className="lean-header-table-page-item lean-time-status-dropdown">
                        <DropdownField
                            // label="Status"
                            id="lean-header-comp-time-range"
                            value={props.timeStatus}
                            eventHandler={props.onTimeRangeChange}
                            menuItems={props.TimeStatusValues ? props.TimeStatusValues : null}
                            inputClass="lean-table-filters"
                            extraInputProps = {{
                                startAdornment: <InputAdornment>
                                    <EventNoteIcon fontSize='small'/>
                                </InputAdornment>
                            }}
                        />
                    </div>
    
                    <div className="lean-header-table-page-item lean-header-table-page-item lean-addAppointment-button" >
    
                                <Button
                                        style={{marginLeft:"10px"}}
                                        variant="contained"
                                        disableElevation
                                        className="lean-btn-primary lean-addAppointment-button"
                                        onClick={() => props.buttonFunction() }
                                    >
                                        {props.buttonText?props.buttonText:"Button"}
                                </Button>
    
    
                                </div>
                </>
                )
            } else{
                return(
                    <div className="lean-header-table-page-item lean-time-status-dropdown">
                    <DropdownField
                        // label="Status"
                        id="lean-header-comp-time-range"
                        value={props.timeStatus}
                        eventHandler={props.onTimeRangeChange}
                        menuItems={props.TimeStatusValues ? props.TimeStatusValues : null}
                        inputClass="lean-table-filters"
                        extraInputProps = {{
                            startAdornment: <InputAdornment>
                                <EventNoteIcon fontSize='small'/>
                            </InputAdornment>
                        }}
                    />
                </div>
                )
            }
        }
        // else{
        //     return(
        //         <div className="lean-header-table-page-item lean-header-table-page-item lean-addAppointment-button" >
    
        //                             <Button
        //                                 variant="contained"
        //                                 disableElevation
        //                                 className="lean-btn-primary lean-addAppointment-button"
        //                             // onClick={() => openModal({ name: 'BookingModal', props: room })}
        //                             >
        //                                 Book
        //                         </Button>
    
    
        //                         </div>
        //     )
        // } 
    }
    
    
    return (


        <div className="lean-header-table-page">

            <div className="lean-header-table-page-item lean-page-title">
                <div className="lean-header-table-page lean-header-page">{props.titleName}</div>
            </div>
            <div className="lean-header-table-page-item lean-page-content">

                {getRemainingContent()}
            </div>


        </div>


    );
};
