import React from "react";
import TextField from "@material-ui/core/TextField";
import { InputLabel, Chip } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import "./inputTextField.scss";

export const ChipInputField = props => {
  let {
    value,
    eventHandler,
    label,
    placeholder,
    labelClass,
    inputClass,
    ...otherProps
  } = props;
  return (
    <div className="lean-form-element">
      {label ? (
        <InputLabel
          shrink
          className={`lean-form-label ${labelClass ? labelClass : ""}`}
        >
          {label}
        </InputLabel>
      ) : null}
      {/* 
      <ChipInput
        defaultValue={["foo", "bar"]}
        variant='outlined'
        fullWidthInput
        className="lean-form-field"
        onChange={chips => eventHandler(chips)}
        InputProps={{
          className: `lean-form-text-field lean-form-chip ${inputClass ? inputClass : ""}`
        }}
      /> */}

      <Autocomplete
        multiple
        id="tags-filled"
        freeSolo
        renderTags={(value, getTagProps) =>
          value.map((option, index) => {
            console.log("option", option, index, getTagProps, value);
            return (
              <Chip
                className="lean-form-chip"
                variant="outlined"
                label={option}
                {...getTagProps({ index })}
              />
            );
          })
        }
        renderInput={params => {
          if (params.InputProps.className) {
            params.InputProps.className = `${
              params.InputProps.className
            } lean-form-text-field lean-form-chips ${
              inputClass ? inputClass : ""
            }`;
          } 

          return (
            <TextField
              variant="outlined"
              fullWidth
              {...params}
              {...otherProps}
            />
          );
        }}
      />

      {/* <TextField
        placeholder="Placeholder"
        variant="outlined"
        className="lean-form-field"
        value={value}
        onChange={e => eventHandler(e.target.value)}
        placeholder={placeholder}
        InputProps={{
          className: `lean-form-text-field ${inputClass ? inputClass : ""}`
        }}
        {...otherProps}
      /> */}
    </div>
  );
};
