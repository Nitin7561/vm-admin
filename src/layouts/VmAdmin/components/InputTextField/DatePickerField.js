import React from "react";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { InputLabel } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";

import "./inputTextField.scss";



export const DatePickerField = props => {
  let {
    value,
    eventHandler,
    label,
    placeholder,
    labelClass,
    inputClass,
    ...otherProps
  } = props;

  return (
    <div className="lean-form-element">
      {label ? (
        <InputLabel shrink className={`lean-form-label ${labelClass?labelClass:""}`}>
          {label}
        </InputLabel>
      ) : null}

      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          disableToolbar
          autoOk
          disablePast="true"
          // minDate={new Date()}
          variant="inline"
          inputVariant="outlined"
          className="lean-form-field"
          format="dd/MM/yyyy"
          // format="dd MMM yy"

          // maskChar='__/__/____'

          value={value}
          onChange={(date) => eventHandler(date)}
          placeholder={placeholder}
          InputProps={{
            disableUnderline: true,
            className: `lean-form-text-field ${inputClass ? inputClass : ""}`
          }}
          KeyboardButtonProps = {{
            className:'lean-form-picker-button'
          }}

          InputAdornmentProps = {{
            className:'lean-form-picker-adornment'       
          }}
          {...otherProps}
        />
      </MuiPickersUtilsProvider>
    </div>
  );
};
