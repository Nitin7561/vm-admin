import React from "react";
import { InputLabel, Switch } from "@material-ui/core";
import "./inputTextField.scss";

export const ToggleSwitch = props => {
  let {
    value,
    eventHandler,
    label,
    labelClass,
    inputClass,
    ...otherProps
  } = props;


  return (
    <div className="lean-form-element">
      {label ? (
        <InputLabel
          shrink
          className={`lean-form-label ${labelClass ? labelClass : ""}`}
        >
          {label}
        </InputLabel>
      ) : null}

      <Switch

        checked={value}
        onChange={(e) => eventHandler(e.target.checked)}

        className={`${inputClass ? inputClass : ""}`}
        {...otherProps}
      />
    </div>
  );
};
