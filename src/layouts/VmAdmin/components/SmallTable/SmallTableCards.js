import React from 'react'
import { connect } from "react-redux";
// import { getCustomDate, getCustomTime, getCustomTimeDiff } from '../../utils/timeFunctions';
// import DeleteIcon from '../../assets/cancel.svg';
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { ModalContext } from "../../utils/ModalContext";
// import { getUpcomingHostsWithAParticularVisitor } from '../../redux/actions/getUpcomingHostsWithAParticularVisitor';
import { getFromAndTo } from '../../utils/timeFunctions';
import "./tables.scss";
import EmptySVG from '../../../../assets/emptyFav.svg';
import { Grid } from "@material-ui/core";



import {
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Chip,
    TablePagination,
    Button,
    Typography,
    Toolbar,
    Tooltip,
    IconButton,
    TableContainer
} from "@material-ui/core";
import FullPageLoading from '../Loaders/FullPageLoading';

const MenuIcon = ({ handleClick }) => (
    <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
    >
        <MoreVertIcon />
    </IconButton>
);

const MoreMenu = props => {

    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = event => {
        setAnchorEl(event.currentTarget);


    };

    console.log("PROPS FROM PROFILE MODAL", props)

    function openListOfVisitorTableModal(name, visitorName, visitorEmail, visitorId, checkInTime, checkOutTime, date, appointmentId, subject, roomName) {


        console.log("Which Modal to open?", name);
        console.log("Which User?", visitorName);
        console.log("Which email?", visitorEmail);

        if (name === 'RescheduleAppointment') {
            console.log("On click of rescehdule")
            openModal({ name: 'AddAppointmentInAModal', props: { "visitorName": visitorName, "visitorEmail": visitorEmail, "isDisabled": true, "checkInTime": checkInTime, "checkOutTime": checkOutTime, "date": date, "roomName": roomName, "appointmentId": appointmentId, "subject": subject, "classStyles": { paperScrollPaper: 'lean-reschedule-appointment-modal' } } })

        }
        else if (name === 'CancelledAppointment') {
            console.log("Modal Name===>", name);
            openModal({ name: 'CancelAppointmentInAModal', props: { "visitorName": visitorName, "visitorEmail": visitorEmail, "isDisabled": true, "checkInTime": checkInTime, "checkOutTime": checkOutTime, "date": date, "roomName": roomName, "appointmentId": appointmentId, "subject": subject } })
        }
        handleClose();
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <MenuIcon handleClick={handleClick} />
            <Menu
                classes={{ paper: "lean-table-body-menu lean-small-table-popover" }}
                // getContentAnchorEl={null}
                anchorEl={document.getElementById(props.uniqueCellID)}
                anchorOrigin={{
                    horizontal: "left",
                    vertical: "center"
                }}
                getContentAnchorEl={null}
                open={open}
                onClose={handleClose}
            >
                <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("RescheduleAppointment", props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId, props.checkInTime, props.checkOutTime, props.date, props.appointments.appointmentId, props.appointments.purpose, props.appointments.roomName)}>
                    Reschedule
                </MenuItem>
                <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("CancelledAppointment",
                    props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId, props.checkInTime, props.checkOutTime, props.date, props.appointments.appointmentId, props.appointments.purpose, props.appointments.roomName
                )}>
                    Cancel Appointment
                </MenuItem>
            </Menu>
        </>
    );
};

const RecurrentTable = (props) => {


    let tableHeaders = [
        { name: "Meeting Date" },
        { name: "Meeting Subject" },
        { name: "Meeting Time" },
        { name: "Duration" },
        { name: "Action" },
    ];

    console.log("PPPRRRRRRRROOOPPPPPSSS=>", props)

    let statusEnum = {
        approved: "approved",
        registered: "registered",
        blocked: "blocked",
        invited: "invited"
    };

    // React.useEffect(
    //     () => {
    //         console.log("on load")
    //         props.getUpcomingHostsWithAParticularVisitor({
    //             "visitorSub": props.eachVisitorinList.visitorSub
    //         })
    //     }, []
    // );
    let isLoading=false
    let data=[{
        date:1587586748000,
        reportedBy:"Nitin",
        blockedBy:"",
        reason:"He has Corona"
    },{
        date:1587586748000,
        reportedBy:"",
        blockedBy:"Mithun",
        reason:""
    },{
        date:1587586748000,
        reportedBy:"Nitin",
        blockedBy:"",
        reason:"He has double Corona"
    }]
    // let { isLoading, data, error } = props.getUpcomingHostsWithAParticularVisitorReducer;
    // console.log("HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", props.getUpcomingHostsWithAParticularVisitorReducer)


    return (

        <div style={{ position: "relative", minHeight: '300px' }}>
            {isLoading ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading> :
                (
                    <>
                        {

                            data.length == 0 ?
                                (
                                    <>
                                        <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                                            <img src={EmptySVG} />
                                            <span className="lean-table-empty-screen-text">No Report with this Visitor</span>
                                        </Grid>
                                    </>
                                ) :
                                (
                                    <>







                                        {data.map((reportDetail, index) => {

                                           var statedTime=reportDetail.date
                                            var date = new Date(statedTime).toLocaleTimeString();


                                           

                                            return (

                                                <div key={index}
                                                    hover
                                                    className="lean-table-row"
                                                // onMouseOver={e => handleMouseOverRow(index)}
                                                >
                                                    <Grid container className="lean-card lean-table-card">
                                                        <Grid item xs={10}>
                                                            <Grid container>
                                                                <Grid item xs={12} style={{
                                                                    display: 'flex',
                                                                    flexDirection: 'column',
                                                                    padding: "4px 0"
                                                                }}><span className="lean-table-card-details-headline">Date & Time</span><span className="lean-table-card-details-value"> {date}</span></Grid>

                                                                <Grid item xs={12} style={{
                                                                    display: 'flex',
                                                                    flexDirection: 'column',
                                                                    padding: "4px 0"
                                                                }}>
                                                                    <span className="lean-table-card-details-headline"> Reported By</span><span className="lean-table-card-details-value">
                                                                        {reportDetail.reportedBy}
                                                                    </span>
                                                                </Grid>

                                                                <Grid item xs={12} style={{
                                                                    display: 'flex',
                                                                    flexDirection: 'column',
                                                                    padding: "4px 0"
                                                                }}>
                                                                    <span className="lean-table-card-details-headline"> Blocked By</span><span className="lean-table-card-details-value">
                                                                        {reportDetail.blockedBy==""?"-":reportDetail.blockedBy}
                                                                    </span>
                                                                </Grid>

                                                                <Grid item xs={12} style={{
                                                                    display: 'flex',
                                                                    flexDirection: 'column',
                                                                    padding: "4px 0"
                                                                }}>
                                                                    <span className="lean-table-card-details-headline"> Reason</span><span className="lean-table-card-details-value">
                                                                        {reportDetail.reason==""?"NA":reportDetail.reason}
                                                                    </span>
                                                                </Grid>
                                                             
                                                            </Grid>
                                                        </Grid>
                                                        
                                                    </Grid>
                                                </div>


                                            );
                                        })}
                                    </>
                                )


                        }
                    </>
                )}
        </div>

    )
}

const mapStateToProps = state => {
    const {  } = state;
    console.log("Visitors-->", );
    return {  };
};

export default connect(mapStateToProps, {  })(
    RecurrentTable
);


// export default RecurrentTable
