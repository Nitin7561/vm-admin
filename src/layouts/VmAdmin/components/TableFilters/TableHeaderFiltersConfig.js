import React from 'react'
import { TableHeaderFilter } from '../../components/TimeFiltersAndDropDowns/TableHeaderFilters'
import EventNoteIcon from '@material-ui/icons/EventNote';
import { ModalContext } from "../../utils/ModalContext";
import { Grid } from '@material-ui/core';

const TableHeaderFilterConfig = (props) => {

    console.log(props)
    // const { setCurrentModal } = React.useContext(ModalContext);
    // const openModal = ({ name, props }) => setCurrentModal({ name, props });

    return (
        <div>
            <TableHeaderFilter  
            isRequired="yes" 
            buttonText={props.buttonText?props.buttonText:"Button"} 
            isAncestorsNeeded={props.isAncestorsNeeded?props.isAncestorsNeeded:"Button"} 
            isButtonRequired={props.isButtonRequired?props.isButtonRequired:"no"} 
            isAutocompleteNeeded="yes" 
            isStatusRequired="yes" 
            titleName="Log History" 
            ButtonText="Add Appointment" s
            statusValues={props.StatusValues} 
            TimeStatusValues={props.timeStatusValues}  
            status={props.status} 
            setStatus={props.changeStatus} 
            changeModule={props.changeModule} 
            changeSeverity = {props.changeSeverity} 
            timeStatus={props.timeStatus}  
            onTimeRangeChange={props.onTimeRangeChange}  
            fullScreen={props.fullScreen} totalNoOfDocs={10} />
        </div>
    )
}

export default TableHeaderFilterConfig
