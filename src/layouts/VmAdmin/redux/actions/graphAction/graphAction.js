// import {FETCH_REQUEST_GET_ALL_ENUMS, FETCH_FAILURE_GET_ALL_ENUMS,GET_ALL_ENUMS} from './actionsConfig';
// import {getAllEnumsEndPoint} from './apiUrls';
import { GET_VISITORSTATUS_REQ, GET_VISITORSTATUS_SUCCESS, GET_VISITORSTATUS_FAIL } from '../../actionConfig';
import { GET_VISITORCYCLE_REQ, GET_VISITORCYCLE_SUCCESS, GET_VISITORCYCLE_FAIL } from '../../actionConfig';
import { GET_VISITORTABLET_REQ, GET_VISITORTABLET_SUCCESS, GET_VISITORTABLET_FAIL } from '../../actionConfig';
import { GET_GETVISITORSTAYED_REQ, GET_GETVISITORSTAYED_SUCCESS, GET_GETVISITORSTAYED_FAIL } from '../../actionConfig';



import { base_url, base_route, getVisitorStatusCountEP, getVisitorStayedCountEP, getTabletTransactionCountEP, getVisitorCycleCountInOfficeEP } from "../../config";

let months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];          
let hours = ["12am","1am","2am","3am","4am","5am","6am","7am","8am","9am","10am","11am","12pm","1pm","2pm","3pm","4pm","5pm","6pm","7pm","8pm","9pm","10pm","11pm","12pm"]          
let days = [ "Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
//-- Get vm Admin dashboard
export const getVisitorStatusGraph = ({ fromTime, toTime }) => {
    // console.log(reqBody);

    return dispatch => {
        dispatch({ type: GET_VISITORSTATUS_REQ });
        fetch(`${base_url}${base_route}${getVisitorStatusCountEP}?from=${fromTime}&to=${toTime}`, {
            method: "GET",
            headers: { "Content-Type": "application/json" }
        })
            .then(resp => resp.json())
            .then(data => {
                console.log("data", data);
                if (data.status.error) {
                    dispatch({
                        type: GET_VISITORSTATUS_FAIL,
                        payload: { error: data.status }
                    });
                } else {
                    let { data: rxdData,period } = data.data
                    console.log("----------->data",rxdData)


                    let ApprovedArr = [], RegisteredArr = [], InvitedArr = [],BlockedArr=[] ,count = 0, xAxis = []



                    // Date.prototype.getWeek = function () {
                    //     var dt = new Date(this.getFullYear(), 0, 1);
                    //     return Math.ceil((((this - dt) / 86400000) + dt.getDay() + 1) / 7);
                    // };

                    rxdData.map((chartData, index) => {

                        const { Approved, Registered, Invited,Blocked, timestamp } = chartData

                        ApprovedArr.push(Registered); RegisteredArr.push(Approved); InvitedArr.push(Invited);BlockedArr.push(Blocked);

                        const moonLanding = new Date(timestamp );

                        if (period == 'month' || period == '6month') {
                            xAxis.push(months[moonLanding.getMonth()])
                        }
                        // if(period == '6months' ) count++
                        if (period == 'year') xAxis.push(moonLanding.getFullYear())
                        // if(period == 'weeks') xAxis.push(moonLanding.getWeek())     //ToDO Week+index
                        if (period == 'week') xAxis.push('Week' + (index + 1))
                        if (period == 'day') xAxis.push(days[moonLanding.getDay()])
                        if (period == 'hour') xAxis.push(hours[moonLanding.getHours()])          //ToDo custom hours(01AM)

                    })
                    if(period=='hour') period="day"
                    else if(period=='day') period="week"
                    else if(period=='week') period="month"
                    else period="year"
                    let storeObj = {
                        'InvitedUsed': InvitedArr,
                        'ApprovedUsed': RegisteredArr,
                        'RegisteredUsed': ApprovedArr,
                        'BlockedUsed':BlockedArr,
                        'xAxis': xAxis,
                        'period':period
                    }

                    dispatch({
                        type: GET_VISITORSTATUS_SUCCESS,
                        payload: storeObj
                    });
                }
            })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: GET_VISITORSTATUS_FAIL,
                    payload: { error }
                });

            });
    };
};

export const getVisitorStayedGraph = ({ fromTime, toTime }) => {
    // console.log(reqBody);

    return dispatch => {
        dispatch({ type: GET_GETVISITORSTAYED_REQ });
        fetch(`${base_url}${base_route}${getVisitorStayedCountEP}?from=${fromTime}&to=${toTime}`, {
            method: "GET",
            headers: { "Content-Type": "application/json" }
        })
            .then(resp => resp.json())
            .then(data => {
                console.log("data", data);
                if (data.status.error) {
                    dispatch({
                        type: GET_GETVISITORSTAYED_FAIL,
                        payload: { error: data.status }
                    });
                } else {
                      
                    let { data: rxdData,period } = data.data
                    console.log("----------->data",rxdData)


                    let InTimeArr = [], LessTimeArr = [], OverTimeArr = [] ,count = 0, xAxis = []



                    // Date.prototype.getWeek = function () {
                    //     var dt = new Date(this.getFullYear(), 0, 1);
                    //     return Math.ceil((((this - dt) / 86400000) + dt.getDay() + 1) / 7);
                    // };

                    rxdData.map((chartData, index) => {

                        const { InTime, LessTime, OverTime, timestamp } = chartData

                        InTimeArr.push(LessTime); LessTimeArr.push(InTime); OverTimeArr.push(OverTime);

                        const moonLanding = new Date(timestamp );

                        if (period == 'month' || period == '6months') {
                            xAxis.push(months[moonLanding.getMonth()])
                        }
                        // if(period == '6months' ) count++
                        if (period == 'year') xAxis.push(moonLanding.getFullYear())
                        // if(period == 'weeks') xAxis.push(moonLanding.getWeek())     //ToDO Week+index
                        if (period == 'week') xAxis.push('Week' + (index + 1))
                        if (period == 'day') xAxis.push(days[moonLanding.getDay()])
                        if (period == 'hour') xAxis.push(hours[moonLanding.getHours()])          //ToDo custom hours(01AM)

                    })
                    let storeObj = {
                        'OverTime': OverTimeArr,
                        'InTime': LessTimeArr,
                        'LessTime': InTimeArr,
                        'xAxis': xAxis,
                    }
                    dispatch({
                        type: GET_GETVISITORSTAYED_SUCCESS,
                        payload: storeObj
                    });
                }
            })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: GET_GETVISITORSTAYED_FAIL,
                    payload: { error }
                });

            });
    };
};

export const getVisitorCycleGraph = ({ fromTime, toTime }) => {
    console.log("inside action graph",fromTime,toTime);

    return dispatch => {
        dispatch({ type: GET_VISITORCYCLE_REQ });
        fetch(`${base_url}${base_route}${getVisitorCycleCountInOfficeEP}?from=${fromTime}&to=${toTime}`, {
            method: "GET",
            headers: { "Content-Type": "application/json" }
        })
            .then(resp => resp.json())
            .then(data => {
                //
                console.log("data", data);
                if (data.status.error) {
                    dispatch({
                        type: GET_VISITORCYCLE_FAIL,
                        payload: { error: data.status }
                    });
                }
                else {
                    
                    let { data: rxdData,period } = data.data
                    console.log("----------->data",rxdData)


                    let CheckOutArr = [], CheckInArr = [], NotCheckedoutArr = [],ReportedArr=[] ,count = 0, xAxis = []



                    // Date.prototype.getWeek = function () {
                    //     var dt = new Date(this.getFullYear(), 0, 1);
                    //     return Math.ceil((((this - dt) / 86400000) + dt.getDay() + 1) / 7);
                    // };

                    rxdData.map((chartData, index) => {

                        const { CheckIn, CheckOut, NotCheckedout,Reported, timestamp } = chartData

                        CheckOutArr.push(CheckOut); CheckInArr.push(CheckIn); NotCheckedoutArr.push(NotCheckedout);ReportedArr.push(Reported);

                        const moonLanding = new Date(timestamp );

                        if (period == 'month' || period == '6months') {
                            xAxis.push(months[moonLanding.getMonth()])
                        }
                        // if(period == '6months' ) count++
                        if (period == 'year') xAxis.push(moonLanding.getFullYear())
                        // if(period == 'weeks') xAxis.push(moonLanding.getWeek())     //ToDO Week+index
                        if (period == 'week') xAxis.push('Week' + (index + 1))
                        if (period == 'day') xAxis.push(days[moonLanding.getDay()])
                        if (period == 'hour') xAxis.push(hours[moonLanding.getHours()])          //ToDo custom hours(01AM)

                    })
                    let storeObj = {
                        'NotCheckedout': NotCheckedoutArr,
                        'CheckIn': CheckInArr,
                        'CheckOut': CheckOutArr,
                        'Reported':ReportedArr,
                        'xAxis': xAxis,
                    }
                    console.log("inside action graph",storeObj)
                    dispatch({
                        type: GET_VISITORCYCLE_SUCCESS,
                        payload: storeObj
                    });
                }
            })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: GET_VISITORCYCLE_FAIL,
                    payload: { error }
                });

            });
    };
};

export const getVisitorTabletGraph = ({ fromTime, toTime }) => {
    // console.log(reqBody);

    return dispatch => {
        dispatch({ type: GET_VISITORTABLET_REQ });
        fetch(`${base_url}${base_route}${getTabletTransactionCountEP}?from=${fromTime}&to=${toTime}`, {
            method: "GET",
            headers: { "Content-Type": "application/json" }
        })
            .then(resp => resp.json())
            .then(data => {
                console.log("data", data);
                if (data.status.error) {
                    dispatch({
                        type: GET_VISITORTABLET_FAIL,
                        payload: { error: data.status }
                    });
                } else {
                      
                    let { data: rxdData,period } = data.data
                    console.log("----------->data",rxdData)


                    let CheckOutArr = [], CheckInArr = [], NotCheckedoutArr = [],ReportedArr=[] ,count = 0, xAxis = []



                    // Date.prototype.getWeek = function () {
                    //     var dt = new Date(this.getFullYear(), 0, 1);
                    //     return Math.ceil((((this - dt) / 86400000) + dt.getDay() + 1) / 7);
                    // };

                    rxdData.map((chartData, index) => {

                        const { CheckIn, CheckOut, NotCheckedout,Reported, timestamp } = chartData

                        CheckOutArr.push(CheckOut); CheckInArr.push(CheckIn); NotCheckedoutArr.push(NotCheckedout);ReportedArr.push(Reported);

                        const moonLanding = new Date(timestamp );

                        if (period == 'month' || period == '6months') {
                            xAxis.push(months[moonLanding.getMonth()])
                        }
                        // if(period == '6months' ) count++
                        if (period == 'year') xAxis.push(moonLanding.getFullYear())
                        // if(period == 'weeks') xAxis.push(moonLanding.getWeek())     //ToDO Week+index
                        if (period == 'week') xAxis.push('Week' + (index + 1))
                        if (period == 'day') xAxis.push(days[moonLanding.getDay()])
                        if (period == 'hour') xAxis.push(hours[moonLanding.getHours()])          //ToDo custom hours(01AM)

                    })
                    let storeObj = {
                        'NotCheckedout': NotCheckedoutArr,
                        'CheckIn': CheckInArr,
                        'CheckOut': CheckOutArr,
                        'Reported':ReportedArr,
                        'xAxis': xAxis,
                    }
                    dispatch({
                        type: GET_VISITORTABLET_SUCCESS,
                        payload: storeObj
                    });
                }
            })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: GET_VISITORTABLET_FAIL,
                    payload: { error }
                });

            });
    };
};

