// import {FETCH_REQUEST_GET_ALL_ENUMS, FETCH_FAILURE_GET_ALL_ENUMS,GET_ALL_ENUMS} from './actionsConfig';
// import {getAllEnumsEndPoint} from './apiUrls';
import { GET_HOSTCOUNT_REQ, GET_HOSTCOUNT_SUCCESS, GET_HOSTCOUNT_FAIL } from '../../actionConfig';
import { GET_VISCOUNT_REQ, GET_VISCOUNT_SUCCESS, GET_VISCOUNT_FAIL } from '../../actionConfig';
import { GET_OVERSTAYEDCOUNT_REQ, GET_OVERSTAYEDCOUNT_SUCCESS, GET_OVERSTAYEDCOUNT_FAIL } from '../../actionConfig';
import { GET_APPTCOUNT_REQ, GET_APPTCOUNT_SUCCESS, GET_APPTCOUNT_FAIL } from '../../actionConfig';
import { GET_NOTCHECKOUTCOUNT_REQ, GET_NOTCHECKOUTCOUNT_SUCCESS, GET_NOTCHECKOUTCOUNT_FAIL } from '../../actionConfig';
import { GET_VISITORCOUNT_REQ, GET_VISITORCOUNT_SUCCESS, GET_VISITORCOUNT_FAIL } from '../../actionConfig';
import { GET_REGISTEREDCOUNT_REQ, GET_REGISTEREDCOUNT_SUCCESS, GET_REGISTEREDCOUNT_FAIL } from '../../actionConfig';
import { GET_BLOCKEDCOUNT_REQ, GET_BLOCKEDCOUNT_SUCCESS, GET_BLOCKEDCOUNT_FAIL } from '../../actionConfig';


import { base_url, base_route, getTotalHostCountEP, getTotalVisitorCountEP,getOverstayedCountEP, getTotalAppointmentCountEP, getAutoCheckedOutCountEP,getTotalVisitorWithTrendEP,getRegisteredVisitorCountWithTrendEP,getBlockedVisitorCountEP } from "../../config";


//-- Get vm Admin dashboard
export const getTotalHostCount = ({fromTime,toTime}) => {
  // console.log(reqBody);
  
    return dispatch => {
      dispatch({ type: GET_HOSTCOUNT_REQ });
      fetch(`${base_url}${base_route}${getTotalHostCountEP}?from=${fromTime}&to=${toTime}`, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
      })
      .then(resp => resp.json())
      .then(data => {
          console.log("data",data);
          if (data.status.error) {
            dispatch({
                type:GET_HOSTCOUNT_FAIL,
                payload: { error:data.status }
            });
            } else {
          dispatch({
            type:GET_HOSTCOUNT_SUCCESS,
            payload: data.data
          });
        } })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: GET_HOSTCOUNT_FAIL,
          payload: {error}
        });
        
      });
    };
  };

export const getTotalVisitorCount = ({fromTime,toTime}) => {
    // console.log(reqBody);
    
      return dispatch => {
        dispatch({ type: GET_VISCOUNT_REQ });
        fetch(`${base_url}${base_route}${getTotalVisitorCountEP}?from=${fromTime}&to=${toTime}`, {
          method: "GET",
          headers: { "Content-Type": "application/json" }
        })
        .then(resp => resp.json())
        .then(data => {
            console.log("data",data);
            if (data.status.error) {
              dispatch({
                  type:GET_VISCOUNT_FAIL,
                  payload: { error:data.status }
              });
              } else {
            dispatch({
              type:GET_VISCOUNT_SUCCESS,
              payload: data.data
            });
          } })
          .catch(error =>{
            console.log(error)
            dispatch({
            type: GET_VISCOUNT_FAIL,
            payload: {error}
          });
          
        });
      };
    };
export const getTotalOverStayedCount = ({fromTime,toTime}) => {
        // console.log(reqBody);
        
          return dispatch => {
            dispatch({ type: GET_OVERSTAYEDCOUNT_REQ });
            fetch(`${base_url}${base_route}${getOverstayedCountEP}?from=${fromTime}&to=${toTime}`, {
              method: "GET",
              headers: { "Content-Type": "application/json" }
            })
            .then(resp => resp.json())
            .then(data => {
                console.log("data",data);
                if (data.status.error) {
                  dispatch({
                      type:GET_OVERSTAYEDCOUNT_FAIL,
                      payload: { error:data.status }
                  });
                  } else {
                dispatch({
                  type:GET_OVERSTAYEDCOUNT_SUCCESS,
                  payload: data.data
                });
              } })
              .catch(error =>{
                console.log(error)
                dispatch({
                type: GET_OVERSTAYEDCOUNT_FAIL,
                payload: {error}
              });
              
            });
          };
        };

export const getTotalApptCount = ({fromTime,toTime}) => {
        // console.log(reqBody);
        
          return dispatch => {
            dispatch({ type: GET_APPTCOUNT_REQ });
            fetch(`${base_url}${base_route}${getTotalAppointmentCountEP}?from=${fromTime}&to=${toTime}`, {
              method: "GET",
              headers: { "Content-Type": "application/json" }
            })
            .then(resp => resp.json())
            .then(data => {
                console.log("data",data);
                if (data.status.error) {
                  dispatch({
                      type:GET_APPTCOUNT_FAIL,
                      payload: { error:data.status }
                  });
                  } else {
                dispatch({
                  type:GET_APPTCOUNT_SUCCESS,
                  payload: data.data
                });
              } })
              .catch(error =>{
                console.log(error)
                dispatch({
                type: GET_APPTCOUNT_FAIL,
                payload: {error}
              });
              
            });
          };
        };

export const getTotalNotCheckCount = ({fromTime,toTime}) => {
        // console.log(reqBody);
        
          return dispatch => {
            dispatch({ type: GET_NOTCHECKOUTCOUNT_REQ });
            fetch(`${base_url}${base_route}${getAutoCheckedOutCountEP}?from=${fromTime}&to=${toTime}`, {
              method: "GET",
              headers: { "Content-Type": "application/json" }
            })
            .then(resp => resp.json())
            .then(data => {
                console.log("data",data);
                if (data.status.error) {
                  dispatch({
                      type:GET_NOTCHECKOUTCOUNT_FAIL,
                      payload: { error:data.status }
                  });
                  } else {
                dispatch({
                  type:GET_NOTCHECKOUTCOUNT_SUCCESS,
                  payload: data.data
                });
              } })
              .catch(error =>{
                console.log(error)
                dispatch({
                type: GET_NOTCHECKOUTCOUNT_FAIL,
                payload: {error}
              });
              
            });
          };
        };

//or vm Admin Setting 

export const getTotalVisitorTrend = () => {
    // console.log(reqBody);
    
      return dispatch => {
        dispatch({ type: GET_VISITORCOUNT_REQ });
        fetch(`${base_url}${base_route}${getTotalVisitorWithTrendEP}`, {
          method: "GET",
          headers: { "Content-Type": "application/json" }
        })
        .then(resp => resp.json())
        .then(data => {
            console.log("data",data);
            if (data.status.error) {
              dispatch({
                  type:GET_VISITORCOUNT_FAIL,
                  payload: { error:data.status }
              });
              } else {
            dispatch({
              type:GET_VISITORCOUNT_SUCCESS,
              payload: data.data
            });
          } })
          .catch(error =>{
            console.log(error)
            dispatch({
            type: GET_VISITORCOUNT_FAIL,
            payload: {error}
          });
          
        });
      };
    };

export const getTotalRegistered = () => {
        // console.log(reqBody);
        
          return dispatch => {
            dispatch({ type: GET_REGISTEREDCOUNT_REQ });
            fetch(`${base_url}${base_route}${getRegisteredVisitorCountWithTrendEP}`, {
              method: "GET",
              headers: { "Content-Type": "application/json" }
            })
            .then(resp => resp.json())
            .then(data => {
                console.log("data",data);
                if (data.status.error) {
                  dispatch({
                      type:GET_REGISTEREDCOUNT_FAIL,
                      payload: { error:data.status }
                  });
                  } else {
                dispatch({
                  type:GET_REGISTEREDCOUNT_SUCCESS,
                  payload: data.data
                });
              } })
              .catch(error =>{
                console.log(error)
                dispatch({
                type: GET_REGISTEREDCOUNT_FAIL,
                payload: {error}
              });
              
            });
          };
        };

export const getTotalBlocked = () => {
    // console.log(reqBody);
    
        return dispatch => {
        dispatch({ type: GET_BLOCKEDCOUNT_REQ });
        fetch(`${base_url}${base_route}${getBlockedVisitorCountEP}`, {
            method: "GET",
            headers: { "Content-Type": "application/json" }
        })
        .then(resp => resp.json())
        .then(data => {
            console.log("data",data);
            if (data.status.error) {
                dispatch({
                    type:GET_BLOCKEDCOUNT_FAIL,
                    payload: { error:data.status }
                });
                } else {
            dispatch({
                type:GET_BLOCKEDCOUNT_SUCCESS,
                payload: data.data
            });
            } })
            .catch(error =>{
            console.log(error)
            dispatch({
            type: GET_BLOCKEDCOUNT_FAIL,
            payload: {error}
            });
            
        });
        };
    };