// import { GET_GETVISITORSTAYED_REQ, GET_GETVISITORSTAYED_SUCCESS, GET_GETVISITORSTAYED_FAIL } from '../../actionConfig';
import { GET_VISITORCYCLE_REQ, GET_VISITORCYCLE_SUCCESS, GET_VISITORCYCLE_FAIL } from '../../actionConfig';

const initialState = {
    isLoading: false,
    data: {
        "xAxis": [],
        "CheckIn": [],
        "CheckOut": [],
        "NotCheckedout": [],
        "Reported": []
    },
    // message:'',
    error: [],
    isError: false  
}
export default function (state = initialState, action) {
    // console.log("inside visitorCycle12333333",action.payload)
    switch (action.type) {
        case GET_VISITORCYCLE_REQ:
            return { ...state, isLoading: true };      
        case GET_VISITORCYCLE_SUCCESS:            
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                // data: { ...state.data, CheckIn: [12], CheckOut:[2],NotCheckedout:[2], xAxis:["week"],Reported:[4]},
              };     
        case GET_VISITORCYCLE_FAIL:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}