import { GET_GETVISITORSTAYED_REQ, GET_GETVISITORSTAYED_SUCCESS, GET_GETVISITORSTAYED_FAIL } from '../../actionConfig';
import { GET_VISITORTABLET_REQ, GET_VISITORTABLET_SUCCESS, GET_VISITORTABLET_FAIL } from '../../actionConfig';

const initialState = {
    isLoading: false,
    data: {
        "timestamp": 1587034238483,
        "CheckIn": 0,
        "CheckOut": 0,
        "NotCheckedout": 0,
        "Reported": 2
    },
    // message:'',
    error: [],
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_VISITORTABLET_REQ:
            return { ...state, isLoading: true };      
        case GET_VISITORTABLET_SUCCESS:            
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                // data: { ...state.data, fullUsed: newData, lessUsed:false,moreUsed:, xAxis:,avgPercent:},
              };     
        case GET_VISITORTABLET_FAIL:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}