import { GET_GETVISITORSTAYED_REQ, GET_GETVISITORSTAYED_SUCCESS, GET_GETVISITORSTAYED_FAIL } from '../../actionConfig';
import { GET_VISITORSTATUS_REQ, GET_VISITORSTATUS_SUCCESS, GET_VISITORSTATUS_FAIL } from '../../actionConfig';

const initialState = {
    isLoading: false,
    data: {
        'InvitedUsed': [],
        'ApprovedUsed': [],
        'RegisteredUsed': [],
        'BlockedUsed':[],
        'xAxis': [],
        'period':"month"
    },
    // message:'',
    error: [],
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_VISITORSTATUS_REQ:
            return { ...state, isLoading: true };      
        case GET_VISITORSTATUS_SUCCESS:            
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                // data: { ...state.data, fullUsed: newData, lessUsed:false,moreUsed:, xAxis:,avgPercent:},
              };     
        case GET_VISITORSTATUS_FAIL:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}