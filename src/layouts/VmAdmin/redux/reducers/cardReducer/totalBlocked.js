import { GET_BLOCKEDCOUNT_REQ, GET_BLOCKEDCOUNT_SUCCESS, GET_BLOCKEDCOUNT_FAIL } from '../../actionConfig';

const initialState = {
    isLoading: false,
    data: {
        activeCount:0,
        inActiveCount:0
    },
    // message:'',
    error: [],
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_BLOCKEDCOUNT_REQ:
            return { ...state, isLoading: true };      
        case GET_BLOCKEDCOUNT_SUCCESS:            
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                // data: { ...state.data, fullUsed: newData, lessUsed:false,moreUsed:, xAxis:,avgPercent:},
              };     
        case GET_BLOCKEDCOUNT_FAIL:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}