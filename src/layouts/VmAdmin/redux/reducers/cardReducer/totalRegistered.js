import { GET_REGISTEREDCOUNT_REQ, GET_REGISTEREDCOUNT_SUCCESS, GET_REGISTEREDCOUNT_FAIL } from '../../actionConfig';

const initialState = {
    isLoading: false,
    data: {
      count:0
    },
    // message:'',
    error: [],
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_REGISTEREDCOUNT_REQ:
            return { ...state, isLoading: true };      
        case GET_REGISTEREDCOUNT_SUCCESS:            
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                // data: { ...state.data, fullUsed: newData, lessUsed:false,moreUsed:, xAxis:,avgPercent:},
              };     
        case GET_REGISTEREDCOUNT_FAIL:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}