import { GET_VISITORCOUNT_REQ, GET_VISITORCOUNT_SUCCESS, GET_VISITORCOUNT_FAIL } from '../../actionConfig';

const initialState = {
    isLoading: false,
    data: {
      count:0
    },
    // message:'',
    error: [],
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_VISITORCOUNT_REQ:
            return { ...state, isLoading: true };      
        case GET_VISITORCOUNT_SUCCESS:            
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                // data: { ...state.data, fullUsed: newData, lessUsed:false,moreUsed:, xAxis:,avgPercent:},
              };     
        case GET_VISITORCOUNT_FAIL:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}