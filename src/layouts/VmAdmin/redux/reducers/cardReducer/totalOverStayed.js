import { GET_OVERSTAYEDCOUNT_REQ, GET_OVERSTAYEDCOUNT_SUCCESS, GET_OVERSTAYEDCOUNT_FAIL } from '../../actionConfig';

const initialState = {
    isLoading: false,
    data: {
      count:0
    },
    // message:'',
    error: [],
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_OVERSTAYEDCOUNT_REQ:
            return { ...state, isLoading: true };      
        case GET_OVERSTAYEDCOUNT_SUCCESS:            
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                // data: { ...state.data, fullUsed: newData, lessUsed:false,moreUsed:, xAxis:,avgPercent:},
              };     
        case GET_OVERSTAYEDCOUNT_FAIL:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}