import { GET_APPTCOUNT_REQ, GET_APPTCOUNT_SUCCESS, GET_APPTCOUNT_FAIL } from '../../actionConfig';

const initialState = {
    isLoading: false,
    data: {
      count:0,
      arrowDirection:"up"
    },
    // message:'',
    error: [],
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_APPTCOUNT_REQ:
            return { ...state, isLoading: true };      
        case GET_APPTCOUNT_SUCCESS:            
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                // data: { ...state.data, fullUsed: newData, lessUsed:false,moreUsed:, xAxis:,avgPercent:},
              };     
        case GET_APPTCOUNT_FAIL:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}