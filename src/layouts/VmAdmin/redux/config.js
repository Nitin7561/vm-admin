export const base_url = "https://cs.leantron.dev"
// export const vm_port = "300"
// export const lt_port = "3001"
export const base_route = "/vm/api/ver1.2/"


//get
export const getTotalHostCountEP = "admin/getTotalHostCount"
export const getTotalVisitorCountEP = "admin/getTotalVisitorCount"
export const getOverstayedCountEP = "admin/getOverstayedCount"
export const getTotalAppointmentCountEP = "admin/getTotalAppointmentCount"
export const getAutoCheckedOutCountEP = "admin/getAutoCheckedOutCount"
//vm settig card admin
export const getBlockedVisitorCountEP = "admin/getBlockedVisitorCount"
export const getRegisteredVisitorCountWithTrendEP = "admin/getRegisteredVisitorCountWithTrend"
export const getTotalVisitorWithTrendEP = "admin/getTotalVisitorWithTrend"

//vmDashboard graph
export const getVisitorStatusCountEP = "admin/getVisitorStatusCount"
export const getVisitorStayedCountEP = "admin/getVisitorStayedCount"
export const getTabletTransactionCountEP = "admin/getTabletTransactionCount "
export const getVisitorCycleCountInOfficeEP = "admin/getVisitorCycleCountInOffice"



