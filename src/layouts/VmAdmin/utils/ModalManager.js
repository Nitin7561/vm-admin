import React, { useContext } from "react";
import { ModalContext } from "./ModalContext";
import LOSModals from '../layouts/ListOfSupport/Modal/LOSModals'
import LHModal from '../layouts/LogHistory/Modal/LHModal'
import MAModals from '../layouts/MyActions/Modal/MAModals'
import SSModals from '../layouts/SpacesenseSupport/Modal/SSModals';
import DeviceInfoModal from '../layouts/DevicesInfo/Modal/DeviceInfoModal';
import FobMappingModal from '../layouts/DevicesInfo/Modal/FobMappingModal';
import MyProfile from '../components/Modal/myProfile/MyProfile';
import MapModal from '../layouts/DevicesInfo/Modal/MapModal';
import EachDeviceInfo from '../layouts/DevicesInfo/Modal/EachDeviceInfo';
import SuccessFailModals from '../components/StatusModals/StatusModals';
import ChangeUserModal from '../layouts/DevicesInfo/Modal/ChangeUserModal';
import BlockUser from '../VmSetting/Modals/BlockUser';

const Modals = {
  LOSModals,
  LHModal,
  MAModals,
  SSModals,
  DeviceInfoModal,
  FobMappingModal,
  MyProfile,
  MapModal,
  EachDeviceInfo,
  SuccessFailModals,
  ChangeUserModal,
  BlockUser
};

const ModalManager = props => {
  console.log("ModalManager", props)
  const { currentModal, setCurrentModal } = useContext(ModalContext);
  const closeModal = () => setCurrentModal(null);

  if (currentModal) {
    const ModalComponent = Modals[currentModal.name];
    return <ModalComponent closeModal={closeModal} {...currentModal.props} />;
  }

  return null;
};

export default ModalManager;
