export const STATUS_ENUM = {
  0: "Upcoming",
  1: "Cancelled",
  2: "Completed",
  3: "Ongoing",
  4: "All",
  5: "Available",
  6: "Completed",
  7: "Partial",
  8: "Upcoming"
};

export const STATUS_REV_ENUM = {
  "Upcoming": 0,
  "Cancelled": 1,
  "Completed": 2,
  "Ongoing": 3,
  "All": 4,
  "Available": 5
  
};


export const API_FETCH_STATUS = {
  "Success": true,
  "Error": false,
}

export const durationEnum = {
  '8': 'All Day',
  '0.5': '30 Minutes',
  '1': '1 Hour',
  '1.5': '1.5 Hours',
  '2': '2 Hours',
  '2.5': '2.5 Hours',
  '3': '3 Hours',
  '3.5': '3.5 Hours',
  '4': '4 Hours',
  '4.5': '4.5 Hours',
  '5': '5 Hours',
  '5.5': '5.5 Hours',
  '6': '6 Hours',
  '6.5': '6.5 Hours',
  '7': '7 Hours',
  '7.5': '7.5 Hours',
}



