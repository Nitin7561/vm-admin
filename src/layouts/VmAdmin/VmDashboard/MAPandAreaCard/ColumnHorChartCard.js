import React from 'react'
import { Grid, CircularProgress, Hidden } from '@material-ui/core'

import './mapAndAreaCard.scss';
import { connect } from 'react-redux';

import EmptySVG from '../../../../assets/emptyFav.svg'
// import FullPageLoading from '../../../../components/Loaders/FullPageLoading';
import clock from '../../../../assets/admin/clock.svg'
import more from '../../../../assets/admin/more.svg'
import upArrow from '../../../../assets/admin/UpArrow.svg'
import downArrow from '../../../../assets/admin/DownArrow.svg'
import Column from '../Charts/ColumnVertical';
import ColumnHorizontal from '../Charts/ColumnHorizontal';


const ColumnHorChartCard = ({ displayIcon, arrow }) => {

  let arrowBg = arrow
  if (arrow == 'downArrow') arrow = downArrow
  else arrow = upArrow


  React.useEffect(() => {
    // getMyFavorites();
  }, [])



  return (
    <div className="lean-admin-db-areaMap-card" >
      <Grid container className="">

        <Grid item xs={12} >
          <div className='lean-admin-db-columnChart-card-header-div1'>

            <div className='main-div'>

              <div className='div1-header'>
                <div className='header-img'>
                  <img src={clock} />
                </div>

                <div className='header-text'>
                  <span className='header'>Visitor Tablets Report</span>
                  <span className='sub-header'>Area chart</span>
                </div>
              </div>

              <div className='div1-filter1'>
                <div className='header-img-div'>
                  {/* <span className='header'>60%</span> */}
                  {/* <img className={`img ${arrowBg}`} src={arrow} /> */}
                </div>
                {/* <span className='sub-header'>for Last Month Usage</span> */}
              </div>

            </div>

            
            <div className='div1-filter2'>
              {/* <img src={more} /> */}
            </div>
          </div>
          {/* My Favourites {(isLoading || (favoriteRooms.length === 0)) ? "" : `   (${favoriteRooms.length})`} */}
        </Grid>

        <Grid item xs={12} className="" style={{paddingTop: '10px'}}>
          {/* <div className="lean-admin-db-areaMap-card-header-div2"> */}
          <ColumnHorizontal />

          {/* </div> */}
        </Grid>

      </Grid>
    </div>
  )
}
const mapStateToProps = state => {
  let { } = state;
  return {};
};

export default connect(mapStateToProps, {})(ColumnHorChartCard)
