import React from 'react'
import { Grid, CircularProgress, Hidden } from '@material-ui/core'

import './mapAndAreaCard.scss';
import { connect } from 'react-redux';

// import EmptySVG from '../../../assets/emptyFav.svg'
// import FullPageLoading from '../../../components/Loaders/FullPageLoading';
import upArrow from '../../../../assets/admin/UpArrow.svg'
import downArrow from '../../../../assets/admin/DownArrow.svg'
import chair from '../../../../assets/admin/chair.svg'
import more from '../../../../assets/admin/more.svg'
// import clock from '../../../assets/admin/clock.svg'
// import HeatMap from '../Charts/HeatMap';
// import AreaMap from '../Charts/AreaMap';
import ColumnVertical from '../Charts/ColumnVertical';


const ColumnVerChartCard = ({ arrow,chartData }) => {

  let arrowBg = arrow
  if (arrow == 'downArrow') arrow = downArrow
  else arrow = upArrow

  console.log("props.visitorCycle1 dd111",chartData)

  React.useEffect(() => {
    // getMyFavorites();
  }, [])

  let chartFinal = {
          
    series: [{
      name: 'Check In',
      // data: [44, 55, 57, 56, 61]
      data: chartData.CheckIn
    }, {
      name: 'Check Out',
      // data: [76, 85, 101, 98, 87]
      data: chartData.CheckOut
    }, {
      name: "Didn't checkOut",
      // data: [35, 41, 36, 26, 45]
      data: chartData.NotCheckedout
    },{
      name: "Report Visitor",
      // data: [35, 41, 36, 26, 45]
      data: chartData.Reported
    }],
    options: {
      chart: {
        type: 'bar',
        height: 350,
        toolbar:{
          show:false
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '55%',
          // endingShape: 'rounded'
        },
      },
      legend:{
        show:true,
        position: 'top',
        horizontalAlign: 'center', 
      },
      dataLabels: {
        enabled: false
      },
      colors:['#00d499','#FF7486','#5fa3fa','#808080'],
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      xaxis: {
        // categories: ['Week1', 'Week2', 'Week3', 'Week4', 'Week5'],
        categories: chartData.xAxis,
      },
      yaxis: {
        title: {
          text: 'No of Seats'
        }
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return val
          }
        },          
      }
    },
  
  
  };



  return (
    <div className="lean-admin-db-areaMap-card" >
      <Grid container className="">

        <Grid item xs={12} >
          <div className='lean-admin-db-columnChart-card-header-div1'>
            <div className='main-div'>
              <div className='div1-header'>

                <div className='header-img'>
                  <img src={chair} />
                </div>

                <div className='header-text'>
                  <span className='header'>Visitor Utilisation</span>
                  <span className='sub-header'>Area chart</span>
                </div>

              </div>

              <div className='div1-filter1'>
                <div className='header-img-div'>
                  {/* <span className='header'>{`${chartData.avgPercent}%`}</span> */}
                  {/* <span className='header'>123</span> */}
                  {/* <img className={`img ${arrowBg}`} src={arrow} /> */}
                </div>
                {/* <span className='sub-header'>for Last Month Usage</span> */}
              </div>
            </div>


            <div className='div1-filter2'>
              <img src={more} />
            </div>
          </div>
          {/* My Favourites {(isLoading || (favoriteRooms.length === 0)) ? "" : `   (${favoriteRooms.length})`} */}
        </Grid>

        <Grid item xs={12} className="" style={{paddingTop: '10px'}}>
          {/* <div className="lean-admin-db-areaMap-card-header-div2"> */}
          {/* <ColumnVertical chartData={} /> */}
          <ColumnVertical  chartFinal={chartFinal}/>

          {/* </div> */}
        </Grid>

      </Grid>
    </div>
  )
}
const mapStateToProps = state => {
  let { } = state;
  return {};
};

export default connect(mapStateToProps, {})(ColumnVerChartCard)
