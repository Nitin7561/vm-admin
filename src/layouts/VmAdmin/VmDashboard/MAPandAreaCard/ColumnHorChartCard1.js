import React from 'react'
import { Grid, CircularProgress, Hidden } from '@material-ui/core'

import './mapAndAreaCard.scss';
import { connect } from 'react-redux';

import EmptySVG from '../../../../assets/emptyFav.svg'
// import FullPageLoading from '../../../../components/Loaders/FullPageLoading';
import clock from '../../../../assets/admin/clock.svg'
import more from '../../../../assets/admin/more.svg'
import upArrow from '../../../../assets/admin/UpArrow.svg'
import downArrow from '../../../../assets/admin/DownArrow.svg'
import Column from '../Charts/ColumnVertical';
import ColumnHorizontal1 from '../Charts/ColumnHorizontal1';


const ColumnHorChartCard1 = ({ arrow,chartData }) => {

  let arrowBg = arrow
  if (arrow == 'downArrow') arrow = downArrow
  else arrow = upArrow
  console.log("props.visitorsTAYED dd111",chartData)


  React.useEffect(() => {
    // getMyFavorites();
  }, [])
  
 
  let chartFinal = {
          
    series: [{
      name: 'Scheduled Time',
      // data: [44, 55, 57, 56, 61],        
      data: chartData.InTime        
    }, {
      name: 'Over Time',
      // data: [76, 85, 101, 98, 87]
      data: chartData.OverTime    
    }, {
      name: 'Less Time',
      // data: [35, 41, 36, 26, 45]
      data: chartData.LessTime    
    }],
    
    options: {
      chart: {
        type: 'bar',
        height: 350,
        stacked:true,
        toolbar:{
          show:false
        }
      },
      zoom: {
        enabled: true
      },
      plotOptions: {
        bar: {
          position: 'bottom',
          offsetX: -10,
          offsetY: 0
          // horizontal: true,
          // columnWidth: '55%',
          // // endingShape: 'rounded'
        },
      },
      colors:['#00D499','#FF7486','#FFA879'],
      legend:{
        show:true,
        position: 'top',
        horizontalAlign: 'center', 
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      xaxis: {
        categories: chartData.xAxis,   

      },
      yaxis: {
        title: {
          text: ''
        }
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return val
          }
        }
      }
    },
  
  
  };





  return (
    <div className="lean-admin-db-areaMap-card" >
      <Grid container className="">

        <Grid item xs={12} >
          <div className='lean-admin-db-columnChart-card-header-div1'>

            <div className='main-div'>

              <div className='div1-header'>
                <div className='header-img'>
                  <img src={clock} />
                </div>

                <div className='header-text'>
                  <span className='header'>Visitor Stayed Duration Report</span>
                  <span className='sub-header'>Area chart</span>
                </div>
              </div>

              <div className='div1-filter1'>
                <div className='header-img-div'>
                  {/* <span className='header'>60%</span> */}
                  {/* <img className={`img ${arrowBg}`} src={arrow} /> */}
                </div>
                {/* <span className='sub-header'>for Last Month Usage</span> */}
              </div>

            </div>

            
            <div className='div1-filter2'>
              {/* <img src={more} /> */}
            </div>
          </div>
          {/* My Favourites {(isLoading || (favoriteRooms.length === 0)) ? "" : `   (${favoriteRooms.length})`} */}
        </Grid>

        <Grid item xs={12} className="" style={{paddingTop: '10px'}}>
          {/* <div className="lean-admin-db-areaMap-card-header-div2"> */}
          <ColumnHorizontal1 chartFinal={chartFinal}/>

          {/* </div> */}
        </Grid>

      </Grid>
    </div>
  )
}
const mapStateToProps = state => {
  let { } = state;
  return {};
};

export default connect(mapStateToProps, {})(ColumnHorChartCard1)
