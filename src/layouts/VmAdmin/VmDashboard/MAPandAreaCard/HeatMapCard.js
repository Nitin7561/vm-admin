import React from 'react'
import { Grid, CircularProgress, Hidden } from '@material-ui/core'

import './mapAndAreaCard.scss';
import { connect } from 'react-redux';

import EmptySVG from '../../../../assets/emptyFav.svg'
import FullPageLoading from '../../../../components/Loaders/FullPageLoading';

import heatMap from '../../../../assets/admin/heatMap.svg'
import more from '../../../../assets/admin/more.svg'
import HeatMap from '../Charts/HeatMap';
import AreaMap from '../Charts/AreaMap';


const HeatMapCard = ({ displayIcon, displayArrow }) => {
  


  React.useEffect(() => {
    // getMyFavorites();
  }, [])



  return (
    <div className="lean-admin-db-areaMap-card" >
      <Grid container className="">

        <Grid item xs={12} >
          <div className='lean-admin-db-areaMap-card-header-div1'>
            <div className='div1-header'>

              <div className='header-img'>
                <img src={heatMap} />
              </div>

              <div className='header-text'>
                <span className='header'>Room Transactions Over Time</span>
                <span className='sub-header'>Area chart</span>
              </div>

            </div>
            
            <div className='div1-filter1'>
              {/* <img src={cardIcon} /> */}
            </div>
            <div className='div1-filter2'>
              <img src={more} />
            </div>
          </div>
          {/* My Favourites {(isLoading || (favoriteRooms.length === 0)) ? "" : `   (${favoriteRooms.length})`} */}
        </Grid>

        <Grid item xs={12} className="" style={{paddingTop: '10px'}}>
          {/* <div className="lean-admin-db-areaMap-card-header-div2"> */}
            <HeatMap/>
            {/* <AreaMap/> */}
          {/* </div> */}
        </Grid>

      </Grid>
    </div>
  )
}
const mapStateToProps = state => {
  let { } = state;
  return {};
};

export default connect(mapStateToProps, {})(HeatMapCard)
