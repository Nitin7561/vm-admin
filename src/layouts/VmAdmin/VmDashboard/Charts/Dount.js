
import React from "react";

import Chart from "react-apexcharts";

// import moment from "react-moment";

class Donut extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
          
      series: [44, 55],
      
      options: {
        chart: {
          id: 'apexchart-example',
          type: 'donut',          
          // labels:{
          //   show: true,            
          //   name: {
          //     show: true,
          //     fontSize: '22px',
          //     fontFamily: 'Helvetica, Arial, sans-serif',
          //     color: "#000000",
          //     offsetY: -10
          //   },
          //   value: {
          //     show: true,
          //     fontSize: '16px',
          //     fontFamily: 'Helvetica, Arial, sans-serif',
          //     color: "#000000",
          //     offsetY: 16,
          //     formatter: function (val) {
          //       return val
          //     }
          //   },
          //   total: {
          //     show: true,
          //     label: 'Total',
          //     color: '#373d3f',
          //     formatter: function (w) {
          //       return w.globals.seriesTotals.reduce((a, b) => {
          //         return a + b
          //       }, 0)
          //     }
          //   }
          // }
          
        },
        labels: ['Booked', 'Not used'],
        colors:['#b5d435','#f55b46'],
        dataLabels: {
          enabled: false          
        },  
        plotOptions: {
          pie: {
            customScale: 1,
            offsetX: 0,
            offsetY: 0,
            expandOnClick: true,
            dataLabels: {
                offset:0,
                minAngleToShowLabel: 10
            }, 
            donut: {
              size: '75%',
              background: 'transparent',
              labels: {
                show: true,
                name: {
                  show: true,
                  fontSize: '8px',
                  fontFamily: 'Helvetica, Arial, sans-serif',
                  fontWeight: 600,
                  color: '#1A2F5A',
                  offsetY: 16
                },
                value: {
                  show: true,
                  fontSize: '14px',
                  fontFamily: 'Helvetica, Arial, sans-serif',
                  fontWeight: 600,
                  color: '#1A2F5A',
                  offsetY: -19,
                  formatter: function (val) {
                    return val
                  }
                },
                total: {
                  show: true,
                  showAlways: false,
                  label: 'Total Booking',
                  fontSize: '9px',
                  fontFamily: 'Helvetica, Arial, sans-serif',
                  fontWeight: 600,
                  color: '#1A2F5A',
                  offsetY: 6,
                  formatter: function (w) {
                    return w.globals.seriesTotals.reduce((a, b) => {
                      return a + b 
                    }, 0)
                  }
                }
              }
            },      
          }
        },
        legend:{
          // show:false,
          // position:'top',
          // horizontalAlign: 'center', 
          fontSize: '10px',
          fontWeight: 600,
          offsetX: 0,
          offsetY: 0,
          formatter: function(seriesName, opts) {
            return [seriesName, "-", opts.w.globals.series[opts.seriesIndex],"%"]
        },
           
        }  
       
      },
    
    
    };
  
  }

  render() {
    return (
      <Chart
        options={this.state.options}
        series={this.state.series}
        // plotOptions={{expandOnClick: false}}
        // chartOptions={this.state.options}
        type="donut"
        // width={600}
        // height={400}

        height={130}
        // className="lean-apex-donut-legend-pos"

      />
    );
  }
}



export default Donut;

// export default function ApexDonutChart(props) {

//   console.log("ApexDonutChart=", props)

//   let { cancelled, confirmed, unused, completed } = props.donut
//   let options = {
//     chart: {
//       width: 380,          
//       type: 'donut',
//     },
//     dataLabels: {
//       enabled: true
//     },
//     fill: {
//       // type: 'gradient',
//     },
//     legend: {
//       formatter: function (val, opts) {
//         return val + " (" + opts.w.globals.series[opts.seriesIndex] + ")"
//       }
//     },
//     // responsive: [{
//     //   breakpoint: 480,
//     //   options: {
//     //     chart: {
//     //       width: 200
//     //     },
//     //     legend: {
//     //       position: 'bottom'
//     //     }
//     //   }
//     // }],
//     labels: ['Confirmed', 'Cancelled', 'Unused', 'Completed'],
//     colors: ['#89d84c', '#ff6666', '#f7b500', '#969696']
//   }
//   // let series = [10, 20, 30, 40]
//   let series = [confirmed, cancelled, unused, completed]


//   return (
//     <div style={{ display: 'flex' }}>
//       <Chart
//         options={options}
//         series={series}
//         // chartOptions={this.state.chartOptions}
//         type="donut"
//         // width={600}
//         // height={400}

//         height={450}
//         className="lean-apex-donut-legend-pos"

//       />
//     </div>
//   )

// }



