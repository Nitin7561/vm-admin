
import React from "react";

import Chart from "react-apexcharts";
import GaugeChart from 'react-gauge-chart'
// import moment from "react-moment";

// class SemiCircleGuage extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {

//       series: [76],
//       options: {
//         chart: {
//           type: 'radialBar',
//           offsetY: -20,
//           sparkline: {
//             enabled: true
//           }
//         },
//         plotOptions: {
//           radialBar: {
//             startAngle: -90,
//             endAngle: 90,
//             track: {
//               background: "#e7e7e7",
//               strokeWidth: '97%',
//               margin: 5, 
//               dropShadow: {
//                 enabled: true,
//                 top: 2,
//                 left: 0,
//                 color: '#999',
//                 opacity: 1,
//                 blur: 2
//               }
//             },
//             dataLabels: {
//               name: {
//                 show: false
//               },
//               value: {
//                 offsetY: -2,
//                 fontSize: '22px'
//               }
//             }
//           }
//         },
//         grid: {
//           padding: {
//             top: -10
//           }
//         },
//         fill: {
//           type: 'gradient',
//           gradient: {
//             shade: 'light',
//             shadeIntensity: 0.4,
//             inverseColors: false,
//             opacityFrom: 1,
//             opacityTo: 1,
//             stops: [0, 50, 53, 91]
//           },
//         },
//         labels: ['Average Results'],

//       },


//     };

//   }

//   render() {
//     return (
//       <Chart
//         options={this.state.options}
//         series={this.state.series}        
//         type="radialBar"        
//         height={250}
//       />
//     );
//   }
// }

// export default SemiCircleGuage;

export default function SemiCircleGuage(props) {

  // console.log("ApexDonutChart=", props)

  // let { cancelled, confirmed, unused, completed } = props.donut
  let value= 0.90
  const chartStyle = {
    height: 365,
  }

  
  return (
    // <div style={{ display: 'flex',height:'220px' }}>
      <GaugeChart id="gauge-chart1"
        nrOfLevels={3}
        colors={["#f2ae2e", "#f55b46",'#b5d435']}
        arcWidth={0.15}
        arcPadding={0.0}
        percent={value}
        arcsLength={[0.5, 0.5, 0.5]}
        formatTextValue	={value=>value +'%'}
        textColor={"#1A2F5A"}
        style={chartStyle}
        cornerRadius={1}
      />
    // </div>
  )

}



