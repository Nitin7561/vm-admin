import React from "react";

import Chart from "react-apexcharts";

// import moment from "react-moment";

class AreaMap extends React.Component {
  constructor(props) {
    super(props);

    
  }

  render() {
    return (
      <Chart
        options={this.props.chartFinal.options}
        series={this.props.chartFinal.series}
        // chartOptions={this.state.chartOptions}
        type="area"
        // width={600}
        // height={400}

        height={275}
        // className="lean-apex-donut-legend-pos"

      />
    );
  }
}


// function generateDayWiseTimeSeries(s, count) {
//   var values = [[
//     4,3,10,9,29,19,25,9,12,7,19,5,13,9,17,2,7,5
//   ], [
//     2,3,8,7,22,16,23,7,11,5,12,5,10,4,15,2,6,2
//   ]];
//   var i = 0;
//   var series = [];
//   var x = new Date("11 Nov 2012").getTime();
//   while (i < count) {
//     series.push([x, values[s][i]]);
//     x += 86400000;
//     i++;
//   }
//   return series;
// }
function generateDayWiseTimeSeries(baseval, count, yrange) {
  var i = 0;
  var series = [];
  while (i < count) {
    var x = baseval;
    var y =
      Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

    series.push([x, y]);
    baseval += 86400000;
    i++;
  }
  return series;
}
export default AreaMap;