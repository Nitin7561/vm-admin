
import React from "react";

import Chart from "react-apexcharts";

// import moment from "react-moment";

class ColumnHorizontal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
          
      series: [{
        name: 'Tablet1',
        data: [44, 55, 57, 56, 61],                
      }, {
        name: 'Tablet2',
        data: [76, 85, 101, 98, 87]
      }, {
        name: 'Tablet3',
        data: [35, 41, 36, 26, 45]
      }],
      
      options: {
        chart: {
          type: 'bar',
          height: 350,
          stacked:true,
          toolbar:{
            show:false
          }
        },
        zoom: {
          enabled: true
        },
        plotOptions: {
          bar: {
            position: 'bottom',
            offsetX: -10,
            offsetY: 0
            // horizontal: true,
            // columnWidth: '55%',
            // // endingShape: 'rounded'
          },
        },
        colors:['#97C9F8','#0790F7','#2579B9'],
        legend:{
          show:true,
          position: 'top',
          horizontalAlign: 'center', 
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['Week1', 'Week2', 'Week3', 'Week4', 'Week5'],
        },
        yaxis: {
          title: {
            text: ''
          }
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val
            }
          }
        }
      },
    
    
    };
  
  }

  render() {
    return (
      <Chart
        options={this.state.options}
        series={this.state.series}
        // chartOptions={this.state.chartOptions}
        type="bar"
        // width={600}
        // height={400}

        height={275}
        // className="lean-apex-donut-legend-pos"

      />
    );
  }
}



export default ColumnHorizontal;

// export default function ApexDonutChart(props) {

//   console.log("ApexDonutChart=", props)

//   let { cancelled, confirmed, unused, completed } = props.donut
//   let options = {
//     chart: {
//       width: 380,          
//       type: 'donut',
//     },
//     dataLabels: {
//       enabled: true
//     },
//     fill: {
//       // type: 'gradient',
//     },
//     legend: {
//       formatter: function (val, opts) {
//         return val + " (" + opts.w.globals.series[opts.seriesIndex] + ")"
//       }
//     },
//     // responsive: [{
//     //   breakpoint: 480,
//     //   options: {
//     //     chart: {
//     //       width: 200
//     //     },
//     //     legend: {
//     //       position: 'bottom'
//     //     }
//     //   }
//     // }],
//     labels: ['Confirmed', 'Cancelled', 'Unused', 'Completed'],
//     colors: ['#89d84c', '#ff6666', '#f7b500', '#969696']
//   }
//   // let series = [10, 20, 30, 40]
//   let series = [confirmed, cancelled, unused, completed]


//   return (
//     <div style={{ display: 'flex' }}>
//       <Chart
//         options={options}
//         series={series}
//         // chartOptions={this.state.chartOptions}
//         type="donut"
//         // width={600}
//         // height={400}

//         height={450}
//         className="lean-apex-donut-legend-pos"

//       />
//     </div>
//   )

// }



