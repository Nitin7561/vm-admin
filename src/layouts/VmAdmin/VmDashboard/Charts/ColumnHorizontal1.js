
import React from "react";

import Chart from "react-apexcharts";

// import moment from "react-moment";

class ColumnHorizontal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Chart
        options={this.props.chartFinal.options}
        series={this.props.chartFinal.series}
        // chartOptions={this.state.chartOptions}
        type="bar"
        // width={600}
        // height={400}

        height={275}
        // className="lean-apex-donut-legend-pos"

      />
    );
  }
}



export default ColumnHorizontal;

// export default function ApexDonutChart(props) {

//   console.log("ApexDonutChart=", props)

//   let { cancelled, confirmed, unused, completed } = props.donut
//   let options = {
//     chart: {
//       width: 380,          
//       type: 'donut',
//     },
//     dataLabels: {
//       enabled: true
//     },
//     fill: {
//       // type: 'gradient',
//     },
//     legend: {
//       formatter: function (val, opts) {
//         return val + " (" + opts.w.globals.series[opts.seriesIndex] + ")"
//       }
//     },
//     // responsive: [{
//     //   breakpoint: 480,
//     //   options: {
//     //     chart: {
//     //       width: 200
//     //     },
//     //     legend: {
//     //       position: 'bottom'
//     //     }
//     //   }
//     // }],
//     labels: ['Confirmed', 'Cancelled', 'Unused', 'Completed'],
//     colors: ['#89d84c', '#ff6666', '#f7b500', '#969696']
//   }
//   // let series = [10, 20, 30, 40]
//   let series = [confirmed, cancelled, unused, completed]


//   return (
//     <div style={{ display: 'flex' }}>
//       <Chart
//         options={options}
//         series={series}
//         // chartOptions={this.state.chartOptions}
//         type="donut"
//         // width={600}
//         // height={400}

//         height={450}
//         className="lean-apex-donut-legend-pos"

//       />
//     </div>
//   )

// }



