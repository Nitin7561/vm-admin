
import React from "react";

import Chart from "react-apexcharts";

// import moment from "react-moment";

class HeatMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
          
      series: [{
          name: 'Jan',
          data: generateData(20, {
            min: -30,
            max: 55
          })
        },
        {
          name: 'Feb',
          data: generateData(20, {
            min: -30,
            max: 55
          })
        },
        {
          name: 'Mar',
          data: generateData(20, {
            min: -30,
            max: 55
          })
        },
        {
          name: 'Apr',
          data: generateData(20, {
            min: -30,
            max: 55
          })
        },
        {
          name: 'May',
          data: generateData(20, {
            min: -30,
            max: 55
          })
        },
        {
          name: 'Jun',
          data: generateData(20, {
            min: -30,
            max: 55
          })
        },
        {
          name: 'Jul',
          data: generateData(20, {
            min: -30,
            max: 55
          })
        },
        {
          name: 'Aug',
          data: generateData(20, {
            min: -30,
            max: 55
          })
        },
        {
          name: 'Sep',
          data: generateData(20, {
            min: -30,
            max: 55
          })
        }
      ],
      options: {
        chart: {
          height: 350,
          type: 'heatmap',
          toolbar:{
            show:false
          }
        },
        plotOptions: {
          heatmap: {
            shadeIntensity: 0.5,
            radius: 0,
            useFillColorAsStroke: true,
            colorScale: {
              ranges: [{
                  from: -30,
                  to: 5,
                  name: 'low',
                  color: '#00A100'
                },
                {
                  from: 6,
                  to: 20,
                  name: 'medium',
                  color: '#128FD9'
                },
                {
                  from: 21,
                  to: 45,
                  name: 'high',
                  color: '#FFB200'
                },
                {
                  from: 46,
                  to: 55,
                  name: 'extreme',
                  color: '#FF0000'
                }
              ]
            }
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: 1
        },
        title: {
          // text: 'HeatMap Chart with Color Range'
        },
      },
    
    
    };
  
  }

  render() {
    return (
      <Chart
        options={this.state.options}
        series={this.state.series}
        // chartOptions={this.state.chartOptions}
        type="heatmap"
        // width={600}
        // height={400}

        height={275}
        // className="lean-apex-donut-legend-pos"

      />
    );
  }
}


function generateData(count, yrange) {
  var i = 0;
  var series = [];
  while (i < count) {
    var x = (i + 1).toString();
    var y =
      Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

    series.push({
      x: x,
      y: y
    });
    i++;
  }
  return series;
}
export default HeatMap;

// export default function ApexDonutChart(props) {

//   console.log("ApexDonutChart=", props)

//   let { cancelled, confirmed, unused, completed } = props.donut
//   let options = {
//     chart: {
//       width: 380,          
//       type: 'donut',
//     },
//     dataLabels: {
//       enabled: true
//     },
//     fill: {
//       // type: 'gradient',
//     },
//     legend: {
//       formatter: function (val, opts) {
//         return val + " (" + opts.w.globals.series[opts.seriesIndex] + ")"
//       }
//     },
//     // responsive: [{
//     //   breakpoint: 480,
//     //   options: {
//     //     chart: {
//     //       width: 200
//     //     },
//     //     legend: {
//     //       position: 'bottom'
//     //     }
//     //   }
//     // }],
//     labels: ['Confirmed', 'Cancelled', 'Unused', 'Completed'],
//     colors: ['#89d84c', '#ff6666', '#f7b500', '#969696']
//   }
//   // let series = [10, 20, 30, 40]
//   let series = [confirmed, cancelled, unused, completed]


//   return (
//     <div style={{ display: 'flex' }}>
//       <Chart
//         options={options}
//         series={series}
//         // chartOptions={this.state.chartOptions}
//         type="donut"
//         // width={600}
//         // height={400}

//         height={450}
//         className="lean-apex-donut-legend-pos"

//       />
//     </div>
//   )

// }



