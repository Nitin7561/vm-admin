import React from 'react'
import { Grid, CircularProgress, Hidden } from '@material-ui/core'

// import './mapAndAreaCard.scss';
import { connect } from 'react-redux';
// import RoomInformation from '../BoxCard/RoomInformation';
// import RoomChart from './BoxCard/RoomChart';
import HeatMapCard from './MAPandAreaCard/HeatMapCard';
import AreaMapCard from './MAPandAreaCard/AreaMapCard';
import ColumnVerChartCard from './MAPandAreaCard/ColumnVerChartCard';
import ColumnHorChartCard from './MAPandAreaCard/ColumnHorChartCard';
import ColumnHorChartCard1 from './MAPandAreaCard/ColumnHorChartCard1';
// import Ancestor from '../../../components/Ancestor/AdminAncestor';
import { HeaderComponent } from './HeaderComponent/HeaderComponent';
import { openToday, openLastWeek, openLastMonth, openLastSixMonths, openLastYear } from './HeaderComponent/DateFilters'
import HeaderStatCards from './HeaderStatCards/HeaderStatCards';
import DescriptionIcon from '@material-ui/icons/Description';
import './vmDashboard.scss'
import { getTotalHostCount,getTotalVisitorCount,getTotalOverStayedCount,getTotalApptCount,getTotalNotCheckCount } from '../redux/actions/cardAction/cardAction';
import {getVisitorCycleGraph ,getVisitorStatusGraph,getVisitorStayedGraph,getVisitorTabletGraph } from '../redux/actions/graphAction/graphAction';

// import Head
import { ModalContext } from "../../../utils/ModalContext";
// import { adminSeatUtilizationAction } from '../redux/action/dashboard';
// import { getAncestor, getAncestorFromFilters } from '../redux/action/adminAncestor';

const VmDashboard = (props) => {

  let {adminSeatUtilization, adminAncestorReducer} = props
  let {getAncestor,getAncestorFromFilters, adminSeatUtilizationAction} = props
  const { setCurrentModal } = React.useContext(ModalContext);
  const openModalCustomDate = ({ name, props }) => setCurrentModal({ name, props });

  const [timestamp, setTimestamp] = React.useState({    
    fromTime: new Date().setHours("00", "00"),
    toTime: new Date().setHours("23", "59")
  })
  const [period,setPeriod]=React.useState("")
  React.useEffect(() => {
      console.log("props.visitorCycle",props.visitorCycle)
  }, [props.visitorCycle])

  React.useEffect(() => {
    var fromTime =Math.round(timestamp.fromTime / 1000) , toTime = Math.round(timestamp.toTime / 1000)
    props.getTotalHostCount(timestamp)  
    props.getTotalApptCount(timestamp)  
    props.getTotalNotCheckCount(timestamp)  
    props.getTotalOverStayedCount(timestamp)  
    props.getTotalVisitorCount(timestamp)  
    props.getVisitorCycleGraph(timestamp)  
    props.getVisitorStayedGraph(timestamp)  
    props.getVisitorStatusGraph(timestamp)  
  
    
    
  }, [timestamp])
  console.log("props.visitorCycle1 dd",props.visitorCycle)

  let [adminFilters, setAdminFilters] = React.useState({
    campusName: 'Pragathi',
    buildingName: 'Pragathi Concepts',
    floorName: 'First Floor',
    zoneName: 'z1',
  })

  let filters = {
    campusName: 'Pragathi',
    buildingName: 'Pragathi Concepts',
    floorName: 'First Floor',
    zoneName: 'z1',
    // roomName: 'All Room',
  }

  React.useEffect(()=>{
    // console.log()
    // getAncestorFromFilters(filters)
  },[])

  

  let ancestorsReducer = {

    // roomName: roomName,
    campusName: ['Pragathi'],
    buildingName: ['Pragathi Concepts'],
    floorName: ['First Floor', 'Second Floor'],
    zoneName: ['z0', 'z1'],
    roomName: ['All Room', 'Cabin-06']
  }


  const onAncestorChange = (ancestorType, ancestorValue) => { //Ex: name=campusName,value=Pragathi
    setAdminFilters({...adminFilters, [ancestorType]:ancestorValue})
    let ancestorTemp = {}
    let nextAncestor = null;


    let {
      campusName,
      buildingName,
      floorName,
      zoneName,
    } = filters;

    switch (ancestorType) {
      case "campusName": ancestorTemp = { buildingName: "", floorName: "", zoneName: "" }; nextAncestor = "buildingName"; break;
      case "buildingName": ancestorTemp = { campusName, floorName: "", zoneName: "" }; nextAncestor = "floorName"; break;
      case "floorName": ancestorTemp = { campusName, buildingName, zoneName: "" }; nextAncestor = "zoneName"; break;
      case "zoneName": ancestorTemp = { campusName, buildingName, floorName }; nextAncestor = null; break;
    }

    // changeLocationFilters({ [ancestorType]: ancestorValue, ...ancestorTemp });
    console.log("asdf", nextAncestor)
    if (nextAncestor) {
      getAncestor({ [ancestorType]: ancestorValue, ...ancestorTemp }, nextAncestor);
    }
    else {
      var newValue = { "campusName": campusName, "buildingName": buildingName, "floorName": floorName, "zoneName": ancestorValue }
      let reqBody = { 'type': 'room', 'parent': '', 'child': 'location', 'stateVal': '', 'value': newValue, 'settingType': 'ancestorLoc' }
      console.log('updateRoomSettingAction', reqBody)
      // updateRoomSettingAction(reqBody)
    }
  }

  //-------------------

  function openCD(fromTime, toTime) {
    console.log("Ntitit", fromTime, toTime);
    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });

  }
  function openCustomDateModal() {
    console.log("On click of custom date");
    openModalCustomDate({ name: 'CustomDate', props: { openCD } })
  }

  const statusValues = [
    { value: "All", name: "All" },
  ]
  
  async function dateSelector(action) {
    console.log("dateSelector",action);
    let result = {}
    switch (action) {
      case 'today':
        result =  await openToday()
        console.log("dateSelector :",result)
        setTimestamp({ ...timestamp, 'fromTime': result.fromTime, 'toTime': result.toTime })
        break;
      case 'lastweek':
        result = await openLastWeek()
        console.log("dateSelector :",result)
        setTimestamp({ ...timestamp, 'fromTime': result.fromTime, 'toTime': result.toTime })
        break;
      case 'lastmonth':
        result = await openLastMonth()
        setTimestamp({ ...timestamp, 'fromTime': result.fromTime, 'toTime': result.toTime })
        break;
      case 'last6month':
        result = await openLastSixMonths()
        setTimestamp({ ...timestamp, 'fromTime': result.fromTime, 'toTime': result.toTime })
        break;
      case 'lastyear':
        result = await openLastYear()
        setTimestamp({ ...timestamp, 'fromTime': result.fromTime, 'toTime': result.toTime })
      break;      
    }
    
  }


  const timeStatusValues = [
    { value: "Today", name: <div className="lean-calendar-image" onClick={() => { dateSelector('today') }}> <span className=" lean-calendar-image-text">Today</span></div> },
    { value: "Last Week", name: <div className="lean-calendar-image" onClick={() => { dateSelector('lastweek') }}> <span className="lean-calendar-image lean-calendar-image-text">Last Week</span></div> },
    { value: "Last Month", name: <div className="lean-calendar-image" onClick={() => { dateSelector('lastmonth') }}> <span className="lean-calendar-image lean-calendar-image-text">Last Month</span></div> },
    { value: "Last 6 Months", name: <div className="lean-calendar-image" onClick={() => { dateSelector('last6month') }}><span className="lean-calendar-image lean-calendar-image-text"> Last 6 Months</span></div> },
    { value: "Last Year", name: <div className="lean-calendar-image" onClick={() => { dateSelector('lastyear') }}><span className="lean-calendar-image lean-calendar-image-text">Last Year</span></div> },
    { value: "Custom Date", name: <div className="lean-calendar-image" onClick={() => { openCustomDateModal() }}><span className="lean-calendar-image lean-calendar-image-text">Custom Date</span></div> }
  ]
  var statText=""
  if(props.visitorStatus.data !=undefined){
    statText="last "+ props.visitorStatus.data.period+ " Usage"
    console.log("statText--->",statText)
    // setPeriod(statText)
  }
  var statFOBData=
            [{
              arrowType:["none","none"],
              color:"Type_D",
              statName:["Total Host","Total Visitor"],
              value:[props.totalHost.data.count,props.totalVisitor.data.count],
              type:"dual",
              statTimeText:[statText,statText]
            },
            {
                arrowType:"none",
                color:"Type_C",
                statName:"Overstayed Total",
                value:props.totalOverStayed.data.count,
                type:"single",
                statTimeText:statText
            },
            
            {
                arrowType:props.totalAppt.data.arrowDirection,
                color:"Type_B",
                statName:"Total Appointments",
                value:props.totalAppt.data.count,
                type:"single",
                statTimeText:statText
            },
            
            {
              arrowType:"none",
              color:"Type_Y",
              statName:"Didn't CheckOut",
              value:props.totalNotCheckOut.data.count,
              type:"single",
              statTimeText:statText
            }
                ]
                // console.log("inside vmdashboard",props.visitorCycle)


  return (
    <div style={{ height: '100%', padding: "5px 20px", backgroundColor: '#EAEFF9',overflowY:"scroll",overflowX:"hidden" }} >

      <Grid direction="row" justify="flex-end" className="lean-card-filters-ancestors-container">   
              <div style={{display:"flex"}}>
            <button className="exportXLSButton"><DescriptionIcon/> </button>
           <HeaderComponent isRequired="yes" titleName="" ButtonText="Add Appointment" TimeStatusValues={timeStatusValues} StatusValues={statusValues} />
            </div>

      </Grid>

     
        <Grid item xs={12} >
        <HeaderStatCards data={statFOBData} cardType="primary"/>
          {/* <RoomInformation /> */}
        </Grid>
    

      <Grid container spacing={4} style={{ padding: '20px 0px 10px' }}>
        <Grid item xs={12} sm={6} className="">
          <AreaMapCard chartData={props.visitorStatus.data} />
          {/* <AreaMapCard /> */}
        </Grid>
        <Grid item xs={12} sm={6} className="">
          {/* <ColumnHorChartCard arrow='downArrow'/> */}
          <ColumnVerChartCard  chartData={props.visitorCycle.data}/>

        </Grid>
      </Grid>

      <Grid container spacing={4} style={{ padding: '20px 0px 10px' }}>
        <Grid item xs={12} sm={6} className="">
          {/* <ColumnVerChartCard arrow='upArrow' chartData={ancestorsReducer}/>} */}
          <ColumnHorChartCard arrow='downArrow'/>
        </Grid>
        <Grid item xs={12} sm={6} className="">
          <ColumnHorChartCard1 chartData={props.visitorStayed.data}/>
        </Grid>
      </Grid>


      {/* <Grid container className="" spacing={4}>
        <Grid item xs={12} className="">
          <AreaMapCard />
        </Grid> */}
       

      {/* </Grid> */}
    </div>
  )
}
const mapStateToProps = state => {
  let {totalHost,totalAppt,totalVisitor,totalNotCheckOut,totalOverStayed ,adminAncestorReducer,visitorCycle,visitorStatus,visitorStayed,visitorTablet } = state;
  return {totalHost,totalAppt,totalVisitor,totalNotCheckOut,totalOverStayed, adminAncestorReducer,visitorCycle,visitorStatus,visitorStayed,visitorTablet};
};

export default connect(mapStateToProps, {getTotalHostCount,getTotalVisitorCount,getTotalOverStayedCount,getTotalApptCount,getTotalNotCheckCount,getVisitorCycleGraph,getVisitorStatusGraph,getVisitorStayedGraph,getVisitorTabletGraph})(VmDashboard)
