var lastday = function (y, m) {
  return new Date(y, m + 1, 0).getDate();
}
async function openToday() {
  var newDate = new Date();
  // var fromTime = newDate.setHours("00", "00");  
  // var toTime = newDate.setHours("23", "59");
  var fromTime = new Date().setHours(0, 0, 0, 0, 0)
  var toTime = new Date(new Date().getTime() + 24 * 60 * 60 * 1000).setHours(0, 0, 0, 0, 0)
  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
  // console.log("New Date", fromTime, toTime);
  // console.log("New Time", timestamp);
  console.log("open Today", fromTime, toTime)
  return ({'fromTime':fromTime,'toTime':toTime})
}

async function openLastWeek() {
  var curr = new Date();
  var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay() - 6));
  var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
  var fromTime = firstday.setHours("00", "00");
  var toTime = lastday.setHours("23", "59");
  // var toTime = lastday.setHours("00","00");
  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
  // console.log("New Time", firstday, lastday);
  console.log("open last Week",  fromTime, toTime)
  return ({'fromTime':fromTime,'toTime':toTime})
}


async function openLastMonth() {
  var lastMonth = new Date().getMonth() - 1;  //return number
  var setLastMonthDate = new Date().setMonth(lastMonth)  //set month
  var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
  var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
  var lastDayOfMonth = lastday(new Date(fromTime).getFullYear(), lastMonth)
  var temp = new Date(setLastMonthDate).setDate(lastDayOfMonth);
  var toTime = new Date(temp).setHours("23", "59")

  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })

  console.log("open LastMonth->",  fromTime, toTime);
  return ({'fromTime':fromTime,'toTime':toTime})
}


async function openLastSixMonths() {
  var lastSixMonth = new Date().getMonth() - 6;  //return number
  var setLastMonthDate = new Date().setMonth(lastSixMonth)  //set month
  var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
  var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
  var lastMonth = (new Date().getMonth() - 1)
  var lastMonthDate = new Date().setMonth(lastMonth)
  // console.log(lastMonth,lastMonthDate)
  var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
  var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
  var toTime = new Date(temp).setHours("23", "59");
  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });

  console.log("open LastSixMonths->", fromTime,toTime)
  return ({'fromTime':fromTime,'toTime':toTime})
}
async function openLastYear() {
  //Check last year
  var lastElevenMonth = new Date().getMonth() - 12;  //return number
  var setLastMonthDate = new Date().setMonth(lastElevenMonth)  //set month
  var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
  var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")

  console.log("From time ===>" + fromTime);
  var lastMonth = (new Date().getMonth() - 1)
  var lastMonthDate = new Date().setMonth(lastMonth)
  console.log(lastMonth, lastMonthDate)
  var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
  var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
  var toTime = new Date(temp).setHours("23", "59");
  console.log("open LastYear->", fromTime,toTime)
  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });
  return ({'fromTime':fromTime,'toTime':toTime})

}



module.exports = {
  openToday: async ()=>{
    return await openToday()
  },
  openLastWeek: async()=>{
    return openLastWeek()
  },
  openLastMonth:async ()=>{
    return await openLastMonth()
  },
  openLastSixMonths: async()=>{
    return await openLastSixMonths()
  },
  openLastYear: async()=>{
    return await openLastYear()
  },
  
}