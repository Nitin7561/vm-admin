import React,{useEffect, useState} from 'react';


import './HeaderStatCards.scss';
import StatCard from './StatCard/StatCard';

function HeaderStatCards(props){
    return(
        <div className={props.cardType=="primary"?"HSCOuterContainer":"HSCOuterContainer2"}>
            <StatCard 
             arrowType={props.data?props.data[0].arrowType:"up"}
             color={props.data?props.data[0].color:"Type_A"}
             statName={props.data?props.data[0].statName:"Total Issues"} 
             value = {props.data?props.data[0].value:"0"}
             statTimeText={props.data?props.data[0].statTimeText:"since last month"}
             type= {props.data?props.data[0].type:"single"}      
             cardType = {props.cardType=="secondary"?props.cardType:"primary"}        
            />
            <StatCard
            arrowType={props.data?props.data[1].arrowType:"up"}
            color={props.data?props.data[1].color:"Type_C"}
            statName={props.data?props.data[1].statName:"Total Issues"} 
            value = {props.data?props.data[1].value:"0"}
            statTimeText={props.data?props.data[1].statTimeText:"since last month"}
            type= {props.data?props.data[1].type:"single"}    
            cardType = {props.cardType=="secondary"?props.cardType:"primary"}
            />
            <StatCard 
             arrowType={props.data?props.data[2].arrowType:"up"}
             color={props.data?props.data[2].color:"Type_B"}
             statName={props.data?props.data[2].statName:"Total Issues"} 
             value = {props.data?props.data[2].value:"0"}
             statTimeText={props.data?props.data[2].statTimeText:"since last month"}
             type= {props.data?props.data[2].type:"single"}   
             cardType = {props.cardType=="secondary"?props.cardType:"primary"}
            />
            <StatCard 
             arrowType={props.data?props.data[3].arrowType:"up"}
             color={props.data?props.data[3].color:"Type_D"}
             statName={props.data?props.data[3].statName:"Total Issues"} 
             value = {props.data?props.data[3].value:"0"}
             statTimeText={props.data?props.data[3].statTimeText:"since last month"}
             type= {props.data?props.data[3].type:"single"}   
             cardType = {props.cardType=="secondary"?props.cardType:"primary"}
            />
        </div>
    )
}


export default HeaderStatCards;