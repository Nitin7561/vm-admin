import React,{useState,useEffect} from 'react';

import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { makeStyles } from "@material-ui/core/styles";

// import Button from '../Button/Button'
import {connect} from 'react-redux';

import { CircularProgress } from '@material-ui/core'

import { ModalContext } from "../../../../utils/ModalContext";
import {clearVMSubmitStatus} from '../Redux/actions/clearSubmitStatus';

import './Modal.scss'

const useStyles = makeStyles({
    input: {
      height: 44,
      width: "265px"
    },
    support:{
      height: 108,
      width: "450px",
    },
    dd:{
      height: 108,
      width: "680px",
    }
  });

function VMSLoaderModal(props){
    const classes = useStyles();
 
    let [action, changeAction] = React.useState("")
    let [issue, changeIssue] = React.useState("")
    let [description, changeDescription] = React.useState("")
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({name, props}) => setCurrentModal({ name, props });
    const closeModal = () => setCurrentModal(null);



if( props.vmsetting.submitStatus === 1000){
      return (
        <div>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className="modalU"
          open={true}
          onClose={props.closeModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={true}>
            <div className={"contentPaper"}>
                 <CircularProgress disableShrink />
            </div>
          </Fade>
        </Modal>
      </div>
      )
    }

  else if(props.vmsetting.submitStatus  === 200){  
    return(
      <>
          {getContent(props.modalTypeSuccess?props.modalTypeSuccess:"submitSuccess")}
      </>
    )
    

  }else if(props.vmsetting.submitStatus  === 500){
    return(<>
    {getContent(props.modalTypeFail?props.modalTypeFail:"deletesubmitFail")}
    </>)
      
  }

  function getContent(type){
    openModal({name:'StatusModals', props:{type:type,closeModal:closeModal}})
    props.clearVMSubmitStatus();
  }

}

const mapStateToProps = state => {
  let { vmsetting } = state;

  return { vmsetting };
};

export default connect(mapStateToProps, {clearVMSubmitStatus})(VMSLoaderModal);