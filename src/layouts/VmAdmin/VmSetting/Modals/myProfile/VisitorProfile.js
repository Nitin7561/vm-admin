import React from "react";
// import StatusModal from '../StatusModal'
import FunctionalModal from '../FunctionalModal'
// import bridgeUX from '../../../assets/images/bridgeUX.svg'
// import myProfile from '../../../assets/images/myProfile.svg'
import myProfile from '../../../../../assets/images/myProfile.svg'


import './myProfile.scss'
import { Grid, useTheme, useMediaQuery } from "@material-ui/core";
import { CircularProgress,Switch  } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
// import { getUserInfoAction } from '../../../redux/actions/user';
import { connect } from 'react-redux';
import SmallTable from '../../../components/SmallTable/RecurrentTable';
import SmallTableCards from "../../../components/SmallTable/SmallTableCards";
// import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor';
import {getVisitorHistory} from '../../Redux/actions/getVisitorHistory';

const VisitorProfile = (props) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

  React.useEffect(()=>{
    props.getVisitorHistory(props.userInfo.visitorSub)
  },[])

  function mapUserData() {
    // let myProfileData = [
    //   { title: 'Contact Number', value: props.eachVisitorinList.Mobile },
    //   { title: 'Email ID', value: props.eachVisitorinList.email },
    //   { title: 'Location', value: props.eachVisitorinList.currentCompany },
    //   { title: 'Visitor ID', value: props.eachVisitorinList.visitorSub },
    // ]
    let myProfileData = [
      { title: 'Contact Number', value:props.userInfo?props.userInfo.Mobile:"8904959412" },
      { title: 'Email ID', value:props.userInfo?props.userInfo.email:"agrawalnitin7561@gmail.com" },
      { title: 'Location', value: props.userInfo?props.userInfo.currentCompany:"Leanovate Solution/ Bangalore" },
      { title: 'Visitor ID', value:props.userInfo?props.userInfo.visitorId:"something" },
    ]

    return myProfileData
  }



  const [open, setOpen] = React.useState(true);
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  return (
    <div>
      {/* <FunctionalModal title={props.eachVisitorinList.name} isAppointment="true" status={props.eachVisitorinList.status} children={modalContent(mapUserData(), props, fullScreen)} maxWidth='md' handleClose={props.closeModal} visitorSub={props.eachVisitorinList.visitorSub} classStyles={{ root: 'lean-each-visitor-host-meetings-list' }} /> */}
      <FunctionalModal title={props.userInfo?props.userInfo.name:"Nitin Agrawal"}  children={modalContent(mapUserData(), props, fullScreen)} maxWidth='md' handleClose={props.closeModal} visitorSub={"something"} classStyles={{ root: 'lean-each-visitor-host-meetings-list' }} />
    </div>
  )
}
function modalContent(myProfileData, props, fullScreen) {
  return (
    <>

      <Grid container classes={{ root: 'lean-my-profile-vm-app-visitor-appointment-info' }}>

        <Grid item xs={12} sm={4} >
          {/* <img className='my-profile-image-from-list-of-appointments' src={`data:image/png;base64,${props.eachVisitorinList.thumbnailImage}`}  ></img> */}
          <img className='my-profile-image-from-list-of-appointments' style ={{height:150,borderRadius:8, width:150,marginLeft:20,marginTop:30}} src={props.userInfo?`data:image/png;base64,${props.userInfo.thumbnailImage}`:myProfile}  ></img>
        </Grid>

        <Grid item xs={12} sm={8}>

          {myProfileData.map((data) => {
            return (

              <Grid container style={{ minHeight: '50px' }} >
                <Grid item xs={4} >
                  <div className='lean-myprofile-title'>{data.title}</div>
                </Grid>
                <Grid item xs={8} >
                  <div className='lean-myprofile-description '>{data.value}</div>
                </Grid>
              </Grid>
            )
          }
               
          )}
           <Grid item xs={4} >
                  <div className='lean-myprofile-title'>Block Visitor</div>
                </Grid>
                <Grid item xs={8} >
                  <div  style={{marginLeft: "149px" ,marginTop: "-29px"}} ><span ><Switch inputProps={{ 'aria-label': 'primary checkbox' }} /></span></div>
             </Grid>


        </Grid>

        <Grid item xs={12}>
          <p className='lean-table-header-appointment-details-visitor'>Report and Block History</p>

          {fullScreen ?
            <SmallTableCards eachVisitorinList={props.eachVisitorinList} /> :
            <SmallTable isLoading={props.vmsetting.isLoading} visitorHistory={props.vmsetting.visitorHistory} />}
        </Grid>



      </Grid>
    </>
  )
}

// export default connect(null, { getDetailForVisitor: getDetailForVisitor })(
//   MyProfile
// );


const mapStateToProps = state => {
  let { vmsetting} = state;
  return {vmsetting};
};

export default connect(mapStateToProps, {getVisitorHistory})(VisitorProfile)



// export default MyProfile