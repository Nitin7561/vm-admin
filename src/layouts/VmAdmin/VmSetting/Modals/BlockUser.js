import React,{useState,useEffect} from 'react';

import InputTextField from '../../components/InputTextField/MLInputTextField'
// import DropDownField from '../../../components/InputTextField/DropdownField'
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from '@material-ui/icons/Close';
import IconButton from "@material-ui/core/IconButton";
// import Button from '../../components/Button/Button'
import {connect} from 'react-redux'
import { ModalContext } from "../../../../utils/ModalContext";
import Button from '../../components/Button/Button'
import InfoStaticText from '../../components/InputTextField/InfoStaticText'
import {blockVisitor} from '../Redux/actions/blockVisitor'
import './Modal.scss'

const useStyles = makeStyles({
    input: {
      height: 44,
      width: "265px"
    },
    dd:{
      height: 98,
      width: "470px",
    }
  });

function BlockUser(props){
    const classes = useStyles();
 
    let [action, changeAction] = React.useState("")
    let [description, changeDescription] = React.useState("")
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({name, props}) => setCurrentModal({ name, props });
    const closeModal = () => setCurrentModal(null);

   
      return(
        <div>
       <Modal
         aria-labelledby="transition-modal-title"
         aria-describedby="transition-modal-description"
         className="modalU"
         open={true}
         onClose={props.closeModal}
         closeAfterTransition
         BackdropComponent={Backdrop}
         BackdropProps={{
           timeout: 500
         }}
       >
         <Fade in={true}>
           <div className="LOScontentPaper7">
             <div className = "supportHeader">
               <p>Block User</p>
               <span>
                 <IconButton aria-label="delete" onClick = {() =>{props.closeModal?props.closeModal():console.log("Modal close")}}>
                   <CloseIcon fontSize="large"  style={{  color: "#030240"}} />
                 </IconButton>    
               </span>
             </div>
             <hr className="horizontalLine" />
             <div className = "inputFields">
             <div className="">
               <InfoStaticText label="Visitor Name" >{props.visitorInfo?props.visitorInfo.name:"Yeray Rosales"}</InfoStaticText>
               {/* <InfoStaticText label="Contact Number" >98765 23456</InfoStaticText> */}
               {/* <InfoStaticText label="Department" >Human Resources</InfoStaticText> */}
               
             </div>  
           

             <div >
             <InputTextField
               value = {description}
               eventHandler={e => {
                 changeDescription(e)
               }}
               className="RegisterTextField"
               label="Reason (Optional)"
               placeholder="Type Description"
               InputProps={{
                   className: classes.dd,
                   
                 }}
               inputProps={{
                 maxLength:150
               }}

             />              
               </div>
            
             {/* <div style={{alignSelf:"flex-start"}}> */}
             <Button style={{backgroundColor:"linear-gradient(0deg, #002647 0%, #2E79B1 100%)",color:"white"}} onClick={()=>{
               let reqbody={
                 visitorId:props.visitorInfo.visitorId
               }
               props.blockVisitor(reqbody)
               openModal({name:'VMSLoaderModal', props:{modalTypeSuccess:"submitSuccess",modalTypeFail:"blockSubmitFail",closeModal:closeModal}})
             }} size="big-btn"  text="Submit"   />
             {/* </div> */}
           </div>
           </div>
         </Fade>
       </Modal>
     </div>
   )
    }
    
    

    const mapStateToProps = state => {
      let { vmsetting} = state;
      return {vmsetting};
    };
    
    export default connect(mapStateToProps, {blockVisitor})(BlockUser)
    