import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import "./Modal.scss";
import { Grid, Hidden, useMediaQuery } from "@material-ui/core";
// import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor';
// import {fireDrawer} from '../../redux/actions/chatAction/fireDrawer';
import { connect } from "react-redux";




const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

const FunctionalModal = ({ title, status, children, maxWidth, handleClose, isAppointment, classStyles,getDetailForVisitor,visitorSub,fireDrawerNow }) => {

  const classes = useStyles();


  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  console.log("classStyles", classStyles);


  if (!classStyles) {
    classStyles = {};
  }

  
  return (
    <div>
      <Dialog fullScreen={fullScreen? fullScreen: false} onClose={handleClose} open={true} classes={classStyles}>
        <DialogTitle disableTypography className="lean-modal-header">
          {isAppointment ? (
            <>
              <div className="lean-text-header lean-profile-visitor-name">
              <div className="lean-modal-header-text">{title}</div>
                {status ? (
                    <Chip
                      label={status}
                      className={`lean-modal-header-status lean-room-${status} lean-status`}
                      
                    />
                  ) : null}
              </div>
              <div className="lean-modal-header-right">
              
                <IconButton onClick={handleClose}>
                  <CloseIcon fontSize="large" />
                </IconButton>
              </div>
            </>
          ) : (
              <>
                {/* <div className="lean-text-header">{title}</div> */}
                <div className="lean-modal-header-text">{title}</div>
                <div className="lean-modal-header-right">
                 
                  <IconButton onClick={handleClose}>
                    <CloseIcon fontSize="large" />
                  </IconButton>
                </div>
              </>

            )}

        </DialogTitle>
        <DialogContent dividers style={{overflowX: "hidden"}}>{children}</DialogContent>
      </Dialog>
    </div>
  );
};

// export default connect(null, { getDetailForVisitor: getDetailForVisitor,fireDrawerNow:fireDrawer })(
//   FunctionalModal
// );
export default connect(null, { })(
  FunctionalModal
);
// export default FunctionalModal;
