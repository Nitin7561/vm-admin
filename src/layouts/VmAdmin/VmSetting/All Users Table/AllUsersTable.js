import React, { useContext } from "react";
import { connect } from "react-redux";
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Chip,
  TablePagination,
  Button,
  Typography,
  Toolbar,
  Tooltip,
  Grid
} from "@material-ui/core";
import Avatar from '@material-ui/core/Avatar';

import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";

// import EmptySVG from '../../../assets/emptyFav.svg'
import EmptySVG from '../../../../assets/admin/emptyFav.svg'

import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

import "./tables.scss";
// import { ModalContext } from "../../utils/ModalContext";
import { ModalContext } from "../../../../utils/ModalContext";
import { STATUS_ENUM } from "../../utils/Enums";
// import FullPageLoading from "../../components/Loaders/FullPageLoading";
import FullPageLoading from "../../components/Loaders/FullPageLoading";
import {unblockVisitor} from '../Redux/actions/unblockVisitor';


const MenuIcon = ({ handleClick, iconClass }) => (
  <IconButton
    aria-label="more"
    aria-controls="long-menu"
    aria-haspopup="true"
    onClick={handleClick}

  >
    <MoreVertIcon className={iconClass} />
  </IconButton>
);

var colorMapping={
  "Invited":"#F0910C",
  "Registered":"#EAAB00",
  "Blocked":"#556080",
  "Approved":"#2D7C31"
}
var backgroundColorMapping={
  "Invited":"rgba(240, 145, 12, 0.2)",
  "Registered":"rgba(234, 172, 0, 0.2)",
  "Blocked":"#5560802a",
  "Approved":"rgba(45, 124, 49, 0.2)"
}
const MoreMenu = ({ menuItems, modalOpener,visitorInfo,unblockVisitor }) => {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const { setCurrentModal } = useContext(ModalContext);
  const openModal = ({name, props}) => setCurrentModal({ name, props });
  const closeModal = () => setCurrentModal(null);


  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event) => {
    setAnchorEl(null);
  };

  function onFunctionalMenuClick(menuOption,e)
  {
    console.log("something got clicked")
    if(menuOption === "blockVisitor")
    {
        openModal({name:'BlockUser', props:{type:'support',visitorInfo:visitorInfo,closeModal:closeModal}})
         console.log("blocked clicked bithc")
        handleClose();

    }

    else if(menuOption === "unBlockVisitor")
    {

          openModal({name:'StatusModals', props:{type:'unBlockUserSuccess',val:e,closeModal:closeModal}})

        console.log("unblocked clicked bithc")
        handleClose();

    }
    else if (menuOption === "deleteVisitor")
    {
      openModal({name:'StatusModals', props:{type:'areYouSureUser',visitorInfo:visitorInfo,closeModal:closeModal}})

      handleClose();
    }
    else if (menuOption === "unblockVisitor")
    {
      let reqBody = {
        visitorId:visitorInfo.visitorId,     
      }
      unblockVisitor(reqBody)
      openModal({name:'VMSLoaderModal', props:{modalTypeSuccess:"unBlockUserSuccess",modalTypeFail:"unBlockSubmitFail",closeModal:closeModal}})

      handleClose();
    }
  }

  return (
    <>
      <MenuIcon handleClick={handleClick} iconClass="lean-table-body-menu-icon" />
      <Menu
        classes={{ paper: "lean-table-body-menu" }}
        anchorEl={anchorEl}
        anchorOrigin={{
          horizontal: "right",
          vertical: "bottom"
        }}
        getContentAnchorEl={null}
        open={open}
        disablePortal
        onClose={handleClose}
      >

        <MenuItem className="lean-table-body-menu-item" onClick={(event) => {onFunctionalMenuClick("blockVisitor",event)}}>Block Visitor</MenuItem>
        <MenuItem className="lean-table-body-menu-item" onClick={(event) => {onFunctionalMenuClick("deleteVisitor",event)}}>Delete Visitor</MenuItem>
        <MenuItem className="lean-table-body-menu-item" onClick={(event) => {onFunctionalMenuClick("unblockVisitor",event)}}>Unblock Visitor</MenuItem>
      </Menu>
    </>
  );
};

const TableActions = ({ page, rowsPerPage, count, pagination, onChangePage }) => {
  let noOfPages = Math.ceil(count / rowsPerPage);

  return (
    <div className="lean-table-pagination-action">
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, 1, pagination)}
      >
        First
      </Button>
      <Button
        className="lean-table-pagination-btn lean-table-pagination-btn-prev"
        disabled={page === 1}
        onClick={() => onChangePage({}, page - 1, pagination)}
      >
        <ChevronLeftIcon />
      </Button>
      <Button
        disabled={page === noOfPages}
        className="lean-table-pagination-btn lean-table-pagination-btn-next"
        onClick={() => onChangePage({}, page + 1, pagination)}
      >
        <ChevronRightIcon />
      </Button>
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, noOfPages, pagination)}
      >
        Last
      </Button>
    </div>
  );
};

const AllUsersTable = ({ page, setPage, onClickSort,visitors,isLoading, rowsPerPage, unblockVisitor ,setRowsPerPage, handleChangePage, handleChangeRowsPerPage, onChangeRX, sortColumnIndex, sortOrder }) => {

  const { setCurrentModal } = useContext(ModalContext);

  const openModal = ({name, props}) => setCurrentModal({ name, props });
  const closeModal = () => setCurrentModal(null);
  console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",visitors)
  // const isLoading = isTableLoading;

  let pagination = 5;
  // let { singleBookings, recurrantBookings, pagination, isLoading, rxdBatchIds } = myBookings;

  // React.useEffect(() => {
  //   // if (!rxdBatchIds) {
  //   //   onChangeRX([])
  //   // } else {
  //   //   onChangeRX(rxdBatchIds);
  //   // }
  // }, [])

  // if (!singleBookings) {
  //   singleBookings = [];
  // }

  // if (!recurrantBookings) {
  //   recurrantBookings = [];
  // }


  function getCustomDate(date) {
    return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`;
  }

  function getCustomTime(time) {
    let ampm = "am";

    var h = time.getHours();
    var m = time.getMinutes();
    // add a zero in front of numbers<10
    if (h >= 12) {
      ampm = "pm";
      h = h % 12 === 0 ? h : h % 12;
    }

    h = checkTime(h);
    m = checkTime(m);

    return `${h}:${m} ${ampm}`;
  }

  function getCustomTimeDiff(from, to) {
    let diff = Math.abs(to - from);

    let hours = Math.floor(diff / (60 * 60));
    let h = checkTime(hours);
    let mins = diff / 60 - hours * 60;
    let m = checkTime(mins);
    return `${h}:${m}`;
  }

  function checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }

  let months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  const handleTableHeaderClick = event => {

  };

  const handleMouseOverRow = index => {
    console.log("handleMouseOverRow", index);
  };

  // const handleChangePage = (event, newPage) => {
  //   setPage(newPage);

  //   onLoad({
  //     fromTime: Math.ceil((((new Date()).getTime()) / 1000) - 2 * 365 * 24 * 60 * 60),
  //     toTime: Math.ceil((((new Date()).getTime()) / 1000) + 2 * 365 * 24 * 60 * 60),
  //     rowsPerPage: rowsPerPage,
  //     actualCount: pagination ? pagination.pages[newPage].actualCount : 0,
  //     sortColumnIndex: 3,
  //     sortOrder: -1,
  //     statusFilter: 4
  //   });
  // };

  // const handleChangeRowsPerPage = event => {
  //   setRowsPerPage(parseInt(event.target.value, 10));
  //   setPage(0);
  //   onLoad({
  //     fromTime: Math.ceil((((new Date()).getTime()) / 1000) - 2 * 365 * 24 * 60 * 60),
  //     toTime: Math.ceil((((new Date()).getTime()) / 1000) + 2 * 365 * 24 * 60 * 60),
  //     rowsPerPage: event.target.value,
  //     actualCount: 0,
  //     sortColumnIndex: 3,
  //     sortOrder: -1,
  //     statusFilter: 4
  //   });
  // };

  let tableHeaders = [
    null,
    { name: "Name", sortIndex: 0 },
    { name: "Mobile Number", sortIndex: null },
    { name: "Email", sortIndex: 1 },
    { name: "Company Name / Location ", sortIndex: null },
    { name: "Last Appoointment Date", sortIndex: null },
    { name: "Total Appointment", sortIndex: null },
    { name: "Active / Inactive", sortIndex: null },
    { name: "Status", sortIndex: null },
    { name: "Action", sortIndex: null },
  ];


  function getSeverityColor(a){   
    if(a=="Critical")
    return "#FF6503"
    else if(a=="Error")
    return "#FF0000"
    else if(a=="Warning")
    return "#e8c40e"
    else
    return "black"
}

    function getStatusColor(a){    
        if(a=="New")
        return "#3399FF"
        else if(a=="Resolved")
        return "#388E3C"
        else if(a=="WIP")
        return "#E52323"
        else
        return "black"
    }

  function onRowClick(event,userInfo)
  {
    if(event.target.tagName === "TD")
    {
      openModal({name:'VisitorProfile', props:{userInfo:userInfo}})
    }
    else
    {
      event.persist();
      event.preventDefault();
      event.stopPropagation();
      event.nativeEvent.stopImmediatePropagation();
    }
  }
  return (
    <div>
      {(isLoading) ? <FullPageLoading mainClassNames="lean-loaders-full-page">Getting visitors...</FullPageLoading> :

          visitors.length !== 0 ? (

          <Table>
            <TableHead classes={{ root: "lean-table-header" }}>
              <TableRow>
                {tableHeaders.map(tableHeader => (
                  <TableCell>
                    {tableHeader ? (

                      tableHeader.sortIndex !== null ? (
                        <Button
                          disableRipple
                          name={tableHeader ? tableHeader.sortIndex : null
                          }
                          onClick={() => onClickSort(tableHeader.sortIndex)}
                        >
                          {tableHeader.name}
                          {sortColumnIndex === tableHeader.sortIndex ? sortOrder === 1 ? <i className="lean-icon-arrow_drop_down" /> : <i className="lean-icon-arrow_drop_up" /> : null}
                        </Button>
                      ) : (
                          <span>
                            {tableHeader.name}
                          </span>
                        )) : null
                    }

                  </TableCell>
                ))}
              </TableRow>
            </TableHead>

            <TableBody classes={{ root: "lean-table-body" }}>
              {visitors.map((eachSupportIssue, index) => {
              
                return (
                  <TableRow
                    key={index}
                    hover
                    className="lean-table-row"
                    onClick={(e) => {onRowClick(e,eachSupportIssue)}}
                    
                  >
                    <TableCell>
                      <Avatar src={`data:image/png;base64,${eachSupportIssue.thumbnailImage}`}></Avatar>
                    </TableCell>
                 
                    <TableCell>
                      {eachSupportIssue.name}
                    </TableCell>

                    <TableCell className="lean-table-body-subj-cell">
                      {eachSupportIssue.Mobile}
                    </TableCell>

                    <TableCell>{eachSupportIssue.email}</TableCell>
                    
                    <TableCell>{eachSupportIssue.currentCompany}</TableCell>
                    
                    <TableCell>{eachSupportIssue.LastApptDate}</TableCell>

                    <TableCell>{eachSupportIssue.totalAppointment}</TableCell>

                    <TableCell>{eachSupportIssue.active==true?"true":"false"}</TableCell>                
                  <TableCell><span style={{color:colorMapping[eachSupportIssue.status],padding:"6px",}} ><span  style={{width:"60px",borderRadius:"5px",padding:"9px",backgroundColor:backgroundColorMapping[eachSupportIssue.status]}}>{eachSupportIssue.status}</span></span></TableCell>
                    
                    
                    <TableCell>
                      <MoreMenu visitorInfo={eachSupportIssue} unblockVisitor={unblockVisitor}/>
                    </TableCell>
                  
                  
                  </TableRow>
                );
              })}
            </TableBody>


            <TableCell
              colspan="3"
              className="lean-table-pagination lean-table-pagination-left-heading"
            >
              <Toolbar className="lean-table-pagination-left-heading-tb">
                <div></div>
                <Typography
                  variant="caption"
                  className="lean-table-pagination-heading"
                >
                Showing {(page - 1) * rowsPerPage + 1}-
                {Math.min(
                  rowsPerPage * (page),
                  pagination ? (pagination <= 5 ? visitors.length : pagination) : 0
                )}{" "}
                  out of {pagination ? (pagination <= 5 ? visitors.length : pagination) : 0} entries
                </Typography>
              </Toolbar>
            </TableCell>


            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              count={pagination ? (pagination <= 5 ? visitors.length : pagination) : 0}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={(e, np) => handleChangePage(e, np, pagination)}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              colspan="100"
              align="center"
              labelDisplayedRows={({ from, to, count }) => {
                console.log("labelDisplayedRows", from, to, count)
                return `${page} of ${Math.ceil(count / rowsPerPage)}`
              }

              }
              classes={{
                select: "lean-table-pagination-select",
                spacer: "lean-table-pagination-spacer",
                caption: "lean-table-pagination-heading",
                root: "lean-table-pagination lean-table-pagination-left-heading",
                toolbar: "lean-table-pagination-left-heading-tb",
                selectRoot: "lean-table-pagination-select-root"
              }}
              // SelectProps = {{
              //   className:'lean-table-pagination-select'
              // }}
              ActionsComponent={() => (
                <TableActions
                  rowsPerPage={rowsPerPage}
                  page={page}
                  count={pagination ? pagination.uniqueDocs : 0}
                  pagination={pagination}
                  onChangePage={(e, np) => handleChangePage(e, np, pagination)}
                />
              )}
            />
          </Table>) : (
            <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
              <img src={EmptySVG} />
              <span className="lean-table-empty-screen-text">No Bookings</span>
            </Grid>
          )
      }
    </div >
  );
};

const mapStateToProps = state => {
  let { vmsetting } = state;

  return { vmsetting };
};

export default connect(mapStateToProps, {unblockVisitor})(AllUsersTable);