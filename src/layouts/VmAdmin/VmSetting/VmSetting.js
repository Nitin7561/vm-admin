import React from 'react'
import { Grid, Hidden, TextField } from '@material-ui/core'

// import './mapAndAreaCard.scss';
import { connect } from 'react-redux';
import HeaderStatCards from './HeaderStatCards/HeaderStatCards';
import DescriptionIcon from '@material-ui/icons/Description';
import { HeaderComponent } from './HeaderComponent/HeaderComponent';

// import TableFilters from '../TableFilters/TableFilters';
import TableHeaderFilterConfig from './../components/TableFilters/TableHeaderFiltersConfig';
import AllUsersTable from './All Users Table/AllUsersTable';
// import { ModalContext } from "../../utils/ModalContext";

import { getTotalVisitorTrend,getTotalRegistered,getTotalBlocked } from '../redux/actions/cardAction/cardAction';
import {getAllVisitors} from './Redux/actions/getAllVisitors';
import './VmSetting.scss'

// import Head
// import { ModalContext } from "../../../utils/ModalContext";
// import { adminSeatUtilizationAction } from '../redux/action/dashboard';
// import { getAncestor, getAncestorFromFilters } from '../redux/action/adminAncestor';

const VmSetting = (props) => {

    const [page, setPage] = React.useState(1);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [sortColumnIndex, setSortIndex] = React.useState(3);
    const [sortOrder, setSortOrder] = React.useState(-1);
    const [status, changeStatus] = React.useState("All");
    const [timeStatus, changeTimeStatus] = React.useState({from:1583330352000,to:1587716005000});
    const [timeText,changeTimeText] = React.useState("");
    
  React.useEffect(() => {
    // console.log("React.useEffect -- >",timestamp)
    // adminSeatUtilizationAction(fromTime,toTime)  
    props.getTotalVisitorTrend()  
    props.getTotalRegistered()  
    props.getTotalBlocked()
  }, [])
//   const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  React.useEffect(()=>{
    props.getAllVisitors(page,rowsPerPage,"totalAppointment",status,"desc");
  },[rowsPerPage,status])

function updateTimeDuration(a){
  changeTimeText(a);
  if(a == "All"){
      openAll()
  }else if(a == "Today"){
      openToday()
  }else if(a == "Last Week"){
      openLastWeek()
  }else if(a == "Last Month"){
      openLastMonth()
  }else if(a == "Last 6 Months"){
      openLastSixMonths()
  }else if(a == "Last Year"){
      openLastYear()
  }

  
}

function openAll() {



  var fromTime = null
  var toTime = null

  changeTimeStatus({from:fromTime, to:toTime})
  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
  // console.log("New Date", fromTime, toTime);
  // console.log("New Time", timestamp);
}

function openToday() {
  var newDate = new Date();
  var fromTime = newDate.setHours("00", "00");
  var toTime = newDate.setHours("23", "59");
  console.log("INOPEN TODAY")
  changeTimeStatus({from:fromTime, to:toTime})
  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
  // console.log("New Date", fromTime, toTime);
  // console.log("New Time", timestamp);
}

function openLastWeek() {
  var curr = new Date();
  var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay() - 6));
  var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
  var fromTime = firstday.setHours("00", "00");
  var toTime = lastday.setHours("23", "59");

  changeTimeStatus({from:fromTime, to:toTime})

  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
  console.log("New Time", firstday, lastday);
}
var lastday = function (y, m) {
  return new Date(y, m + 1, 0).getDate();
}


function openLastMonth() {
  var lastMonth = new Date().getMonth() - 1;  //return number
  var setLastMonthDate = new Date().setMonth(lastMonth)  //set month
  var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
  var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
  var lastDayOfMonth = lastday(new Date(fromTime).getFullYear(), lastMonth)
  var temp = new Date(setLastMonthDate).setDate(lastDayOfMonth);
  var toTime = new Date(temp).setHours("23", "59")

  changeTimeStatus({from:fromTime, to:toTime})


  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })

  console.log("openLastMonth->", fromTime, toTime);
}

function openLastSixMonths() {
  var lastSixMonth = new Date().getMonth() - 6;  //return number
  var setLastMonthDate = new Date().setMonth(lastSixMonth)  //set month
  var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
  var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
  var lastMonth = (new Date().getMonth() - 1)
  var lastMonthDate = new Date().setMonth(lastMonth)
  console.log(lastMonth, lastMonthDate)
  var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
  var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
  var toTime = new Date(temp).setHours("23", "59");

  changeTimeStatus({from:fromTime, to:toTime})

  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });

  console.log("openLastSixMonths->", toTime)
}

function openLastYear() {
  //Check last year
  var lastElevenMonth = new Date().getMonth() - 12;  //return number
  var setLastMonthDate = new Date().setMonth(lastElevenMonth)  //set month
  var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
  var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")

  console.log("From time ===>" + fromTime);
  var lastMonth = (new Date().getMonth() - 1)
  var lastMonthDate = new Date().setMonth(lastMonth)
  console.log(lastMonth, lastMonthDate)
  var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
  var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
  var toTime = new Date(temp).setHours("23", "59");
  console.log("openLastYear->", fromTime, toTime)

  changeTimeStatus({from:fromTime, to:toTime})

  // setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });
}


  var statFOBData=
            [{
                arrowType:"none",
                color:"Type_D",
                statName:"Total Visitor",
                value:props.totalVisitorTrend.data.count,
                type:"single",
                statTimeText:""
            },
            {
                arrowType:"none",
                color:"Type_C",
                statName:"Registered Visitor",
                value:props.totalRegistered.data.count,
                type:"single",
                statTimeText:""
            },
            
            {
                arrowType:"none",
                color:"Type_B",
                statName:"Active Visitor",
                value:props.totalBlocked.data.activeCount,
                type:"single",
                statTimeText:""
            },
            
            {
              arrowType:"none",
              color:"Type_Y",
              statName:"InActive Visitor",
              value:props.totalBlocked.data.inActiveCount,
              type:"single",
              statTimeText:""
            }
                ]
                const timeStatusValues = [
                  { value: "All", name: <div className="lean-calendar-image" > <span className="lean-calendar-image lean-calendar-image-text">All</span></div> },
                  { value: "Today", name: <div className="lean-calendar-image" > <span className="lean-calendar-image lean-calendar-image-text">Today</span></div> },
                  { value: "Last Week", name: <div className="lean-calendar-image" > <span className="lean-calendar-image lean-calendar-image-text">Last Week</span></div> },
                  { value: "Last Month", name: <div className="lean-calendar-image"> <span className="lean-calendar-image lean-calendar-image-text">Last Month</span></div> },
                  { value: "Last 6 Months", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text"> Last 6 Months</span></div> },
                  { value: "Last Year", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Last Year</span></div> },
                  { value: "Custom Date", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Custom Date</span></div> }
              ]
          
    // const onChangeStatus = (newStatus) => {

    //     // console.log("onChangeStatus", newStatus, JSON.stringify({
    //     //     fromTime: Math.floor(timing.fromTime / 1000),
    //     //     toTime: Math.floor(timing.toTime / 1000),
    //     //     rowsPerPage: rowsPerPage,
    //     //     pageNumber: 1,
    //     //     sortColumnIndex,
    //     //     sortOrder,
    //     //     statusFilter: STATUS_REV_ENUM[newStatus],
    //     //     rxdBatchIds: [],
    //     // }))
    //     setStatus(newStatus);
    //     setPage(1)

    //     // changeBookings([])
    //     // loadMyBookings({
    //     //     fromTime: Math.floor(timing.fromTime / 1000),
    //     //     toTime: Math.floor(timing.toTime / 1000),
    //     //     rowsPerPage: rowsPerPage,
    //     //     pageNumber: 1,
    //     //     sortColumnIndex,
    //     //     sortOrder,
    //     //     statusFilter: STATUS_REV_ENUM[newStatus],
    //     //     rxdBatchIds: []
    //     // });
    // }

    let onClickSort = (newSI) => {

        if (newSI === null) {
            return;
        }
        let newSortOrder = sortColumnIndex;
        if (newSI === sortColumnIndex) {
            if (sortOrder === -1) {
                newSortOrder = 1;
            } else {
                newSortOrder = -1
            }
        } else {
            newSortOrder = 1
        }

        setSortIndex(newSI)
        setSortOrder(newSortOrder)

        // loadMyBookings({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: rowsPerPage,
        //     pageNumber: 1,
        //     sortColumnIndex: newSI,
        //     sortOrder: newSortOrder,
        //     statusFilter: STATUS_REV_ENUM[status],
        //     rxdBatchIds: []
        // });

    }

    const handleChangePage = (event, newPage, pagination) => {
        setPage(newPage);

        // console.log("newRx", newPage, JSON.stringify({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: rowsPerPage,
        //     pageNumber: newPage,
        //     sortColumnIndex,
        //     sortOrder,
        //     statusFilter: STATUS_REV_ENUM[status],
        //     rxdBatchIds,
        // }));

        // loadMyBookings({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: rowsPerPage,
        //     pageNumber: newPage,
        //     sortColumnIndex,
        //     sortOrder,
        //     statusFilter: STATUS_REV_ENUM[status],
        //     rxdBatchIds,
        // });

        // onLoad({
        //   fromTime: Math.ceil((((new Date()).getTime())/1000) - 2 * 365 * 24 * 60 * 60 ),
        //   toTime: Math.ceil((((new Date()).getTime())/1000) + 2 * 365 * 24 * 60 * 60 ),
        //   rowsPerPage: rowsPerPage,
        //   actualCount: pagination ? pagination.pages[newPage].actualCount : 0,
        //   sortColumnIndex: 1,
        //   sortOrder: 1,
        //   statusFilter: 4
        // });
    };

    // const

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(1);

        // loadMyBookings({
        //     fromTime: Math.floor(timing.fromTime / 1000),
        //     toTime: Math.floor(timing.toTime / 1000),
        //     rowsPerPage: event.target.value,
        //     pageNumber: 1,
        //     sortColumnIndex,
        //     sortOrder,
        //     statusFilter: STATUS_REV_ENUM[status],
        //     rxdBatchIds: []
        // });

        // onLoad({
        //   fromTime: Math.ceil((((new Date()).getTime())/1000) - 2 * 365 * 24 * 60 * 60 ),
        //   toTime: Math.ceil((((new Date()).getTime())/1000) + 2 * 365 * 24 * 60 * 60 ),
        //   rowsPerPage: ,
        //   actualCount: 0,
        //   sortColumnIndex: 3,
        //   sortOrder: 1,
        //   statusFilter: 4
        // });
    };
var tableValue=[{
    icon:"",
    name:"Nitin Agrawal",
    Mobile:8904959412,
    email:"agrawalnitin23@gmail.com",
    company:"Leanovate",
    lastAppt:1587538176000,
    totalAppt:10,
    active:true,
    status:"Approved"
},{}]

const statusValues = [
   { value: "All", name: "All"},
   {value: "Invited", name: "Invited"},
   {value: "Registered", name: "Registered"},
   {value: "Approved", name: "Approved"} ,
   {value: "Blocked", name: "Blocked"} ,
  ]
  return (
    <div style={{height: "100%",overflowY:"auto", paddingLeft:30, paddingRight:30}} > 
    <Grid container className="lean-book-table" >
        <Grid item xs={12} >
        <HeaderComponent 
            isRequired="no" titleName="List of Visitor" ButtonText="Add Appointment" TimeStatusValues={timeStatusValues} StatusValues={statusValues} />
        <HeaderStatCards data={statFOBData} cardType="primary"/>
        </Grid>

        <Grid item xs={12} >
            {/* <TextField className="lean-search-text" variant="outlined"> </TextField>
            <div style={{display:"flex",float:"right"}}>
            <button className="exportXLSButton"><DescriptionIcon/> </button> */}
         
            {/* </div> */}
        </Grid>
    

        <Grid item xs={12} >
            <TableHeaderFilterConfig 
            StatusValues={statusValues} 
            changeStatus = {changeStatus}
            status={status}
            timeStatusValues={timeStatusValues}  
            timeStatus={timeText}
            onTimeRangeChange={updateTimeDuration}
            totalNoOfDocs={10} />
        </Grid>
        <Grid item xs={12}>
                    <Hidden xsDown implementation="css">
                        <AllUsersTable visitors={props.vmsetting.allVisitors} isLoading={props.vmsetting.isTableLoading} page={page} setPage={setPage} rowsPerPage={rowsPerPage} setRowsPerPage={setRowsPerPage} handleChangePage={handleChangePage} handleChangeRowsPerPage={handleChangeRowsPerPage}
                             onClickSort={onClickSort} sortColumnIndex={sortColumnIndex} sortOrder={sortOrder}
                        />
                    </Hidden>

        </Grid>

    

      <Grid container spacing={4} style={{ padding: '20px 0px 10px' }}>
      
      </Grid>


      {/* <Grid container className="" spacing={4}>
        <Grid item xs={12} className="">
          <AreaMapCard />
        </Grid> */}
       

      {/* </Grid> */}
      </Grid>
    </div>
  )
}
const mapStateToProps = state => {
  let { totalVisitorTrend,totalBlocked,totalRegistered,vmsetting} = state;
  return {totalVisitorTrend,totalBlocked,totalRegistered,vmsetting};
};

export default connect(mapStateToProps, {getTotalVisitorTrend,getTotalRegistered,getTotalBlocked,getAllVisitors})(VmSetting)
