import React from 'react';
// import Button from '../Button/Button';
import {DropdownField} from '../../../../components/InputTextField/DropdownField';
// import AddAppointmentInAModal from '../../layouts/AddAppointment/AddAppointmentInAModal'
import { Grid } from "@material-ui/core";
import './HeaderComponent.scss';
import { ModalContext } from "../../../../utils/ModalContext";
import InputAdornment from '@material-ui/core/InputAdornment';
import EventNoteIcon from '@material-ui/icons/EventNote';
import { Button } from '@material-ui/core';
    

export const HeaderComponent = props => {
    const [status, changeStatus] = React.useState("All");
    const [timeStatus, changeTimeStatus] = React.useState("All");
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    const handleNext = () => {
        openModal({ name: 'AddAppointmentInAModal', props: { "isDisabled": false } })
    }

    return (


        <div className="lean-header-table-page">

            <div className="lean-header-table-page-item lean-page-title">
                    <div style={{width:200, paddingTop:50, paddingBottom:50,marginLeft:"-10px"}} className="lean-header-table-page lean-header-page">{props.titleName ? props.titleName:"List Of Appointments"}</div>
            </div>
            <div className="lean-header-table-page-item lean-page-content">

                {props.isRequired === "yes" ? (
                    <>
                        {/* <span className="lean-header-table-page-item lean-status-label">Status</span>

                            <div className="lean-header-table-page-item lean-status-dropdown">
                                <DropdownField
                                        // label="Status"
                                        value={status}
                                        eventHandler={changeStatus}
                                        menuItems={props.StatusValues ? props.StatusValues : null}
                                    />
                            </div> */}
                        <div className="lean-header-table-page-item lean-time-status-dropdown" >

                            <DropdownField
                                // label="Status"
                                value={timeStatus}
                                eventHandler={changeTimeStatus}
                                menuItems={props.TimeStatusValues ? props.TimeStatusValues : null}
                                // className=''

                                // menuClass="lean-dashboard-header-component-select"
                                menuItemsClass="lean-dashboard-header-component-text"

                                // otherMenuProps = {{
                                //     MenuProps: {
                                //         anchorOrigin: {
                                //             vertical: 'bottom',
                                //             horizontal: 'left',
                                //         }
                                //     }
                                // }}

                                
                                otherInputProps={{
                                    startAdornment:
                                        (<InputAdornment position="start">
                                            <EventNoteIcon fontSize="small" />
                                        </InputAdornment>)

                                }}

                                inputClass="lean-dashboard-header-component"


                            />
                        </div>
                        {/* <div className="lean-header-table-page-item lean-addAppointment-button" >
                            
                                <Button
                                    text={props.ButtonText ? props.ButtonText : "Add Appointment"}
                                    size="small-btn"
                                    variant="contained"
                                    color="primary"
                                    onClick={handleNext}
                                />
                            
                            </div> */}
                    </>

                ) : (
                        <>
                            {/* <div className="lean-header-table-page-item lean-addAppointment-button" >
                            
                                <Button
                                    text={props.ButtonText ? props.ButtonText : "Add Appointment"}
                                    size="small-btn"
                                    variant="contained"
                                    color="primary"
                                    onClick={handleNext}                                    
                                    
                                />
                            
                            </div> */}
                        </>
                    )}


            </div>


        </div>


    );
};
