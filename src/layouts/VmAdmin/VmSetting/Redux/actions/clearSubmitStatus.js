
import {CLEAR_VISITOR_SUBMIT_STATUS} from './actionConfig';

export const clearVMSubmitStatus = () => {
    return dispatch => {
        dispatch({type:CLEAR_VISITOR_SUBMIT_STATUS});
        console.log("store dispatched");
    }
}