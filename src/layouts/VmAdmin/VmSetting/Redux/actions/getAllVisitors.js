import APIs from '../apiCollection';
import { GET_ALL_VISITORS_REQUEST,GET_ALL_VISITORS_RECEIVE,GET_ALL_VISITORS_ERROR} from './actionConfig';

export const getAllVisitors = (page,offset,sortBy,status,order) => {  
    return dispatch => {
      dispatch({type: GET_ALL_VISITORS_REQUEST});
      let url = new URL(APIs.getAllVisitors.url);

      url.search = new URLSearchParams({        
        page:page,
        offset:offset,
        sortBy:sortBy,
        status:status,
        order:order
      })

      fetch(url, {
        method: APIs.getAllVisitors.type,
        headers: { "Content-Type": "application/json" },      
      })
        .then(data1 => data1.json())
        .then(data => {
          console.log("get all visitors", data);
          dispatch({
            type: GET_ALL_VISITORS_RECEIVE,
            payload: APIs.getAllVisitors.parser(data)
          });
        })
        .catch(error =>
          dispatch({
            type: GET_ALL_VISITORS_ERROR,
            payload: error.toString()
          })
        );
    };
  };