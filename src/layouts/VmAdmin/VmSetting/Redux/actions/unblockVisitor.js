import APIs from '../apiCollection';
import { UNBLOCK_VISITOR_REQUEST,UNBLOCK_VISITOR_RECEIVE,UNBLOCK_VISITOR_ERROR} from './actionConfig';

export const unblockVisitor = (reqbody) => {  
    return dispatch => {
      dispatch({type: UNBLOCK_VISITOR_REQUEST});
  

      fetch(APIs.unblockVisitor.url, {
        method: APIs.unblockVisitor.type,
        headers: { "Content-Type": "application/json" },   
        body:JSON.stringify(reqbody)   
      })
        .then(data1 => data1.json())
        .then(data => {
          console.log("resolve issue", data);
          dispatch({
            type: UNBLOCK_VISITOR_RECEIVE,
            payload: APIs.unblockVisitor.parser(data)
          });
        })
        .catch(error =>
          dispatch({
            type: UNBLOCK_VISITOR_ERROR,
            payload: error.toString()
          })
        );
    };
  };