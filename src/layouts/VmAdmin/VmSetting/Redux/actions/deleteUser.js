import APIs from '../apiCollection';
import { DELETE_USER_REQUEST,DELETE_USER_RECEIVE,DELETE_USER_ERROR} from './actionConfig';

export const deleteUser = (reqbody) => {  
    return dispatch => {
      dispatch({type: DELETE_USER_REQUEST});
  

      fetch(APIs.deleteUser.url, {
        method: APIs.deleteUser.type,
        headers: { "Content-Type": "application/json" },   
        body:JSON.stringify(reqbody)   
      })
        .then(data1 => data1.json())
        .then(data => {
          console.log("submit support", data);
          dispatch({
            type: DELETE_USER_RECEIVE,
            payload: APIs.deleteUser.parser(data)
          });
        })
        .catch(error =>
          dispatch({
            type: DELETE_USER_ERROR,
            payload: error.toString()
          })
        );
    };
  };