import APIs from '../apiCollection';
import { BLOCK_VISITOR_REQUEST,BLOCK_VISITOR_RECEIVE,BLOCK_VISITOR_ERROR} from './actionConfig';

export const blockVisitor = (reqbody) => {  
    return dispatch => {
      dispatch({type: BLOCK_VISITOR_REQUEST});
  

      fetch(APIs.blockVisitor.url, {
        method: APIs.blockVisitor.type,
        headers: { "Content-Type": "application/json" },   
        body:JSON.stringify(reqbody)   
      })
        .then(data1 => data1.json())
        .then(data => {
          console.log("resolve issue", data);
          dispatch({
            type: BLOCK_VISITOR_RECEIVE,
            payload: APIs.blockVisitor.parser(data)
          });
        })
        .catch(error =>
          dispatch({
            type: BLOCK_VISITOR_ERROR,
            payload: error.toString()
          })
        );
    };
  };