import APIs from '../apiCollection';
import { GET_VISITOR_HISTORY_REQUEST,GET_VISITOR_HISTORY_RECEIVE,GET_VISITOR_HISTORY_ERROR} from './actionConfig';

export const getVisitorHistory = (visitorSub) => {  
    return dispatch => {
      dispatch({type: GET_VISITOR_HISTORY_REQUEST});
      let url = new URL(APIs.getVisitorHistory.url);

      url.search = new URLSearchParams({
      visitorSub:visitorSub
      })

      fetch(url, {
        method: APIs.getVisitorHistory.type,
        headers: { "Content-Type": "application/json" },      
      })
        .then(data1 => data1.json())
        .then(data => {
          console.log("get Issue Details", data);
          dispatch({
            type: GET_VISITOR_HISTORY_RECEIVE,
            payload: APIs.getVisitorHistory.parser(data)
          });
        })
        .catch(error =>
          dispatch({
            type: GET_VISITOR_HISTORY_ERROR,
            payload: error.toString()
          })
        );
    };
  };