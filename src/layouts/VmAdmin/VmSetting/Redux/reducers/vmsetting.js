import {
    GET_ALL_VISITORS_REQUEST,
    GET_ALL_VISITORS_RECEIVE,
    GET_ALL_VISITORS_ERROR,
    GET_VISITOR_HISTORY_REQUEST,
    GET_VISITOR_HISTORY_RECEIVE,
    GET_VISITOR_HISTORY_ERROR,
    CLEAR_ERROR_STATUS,
    CLEAR_VMSETTING_STORE_STORE,
    CLEAR_VISITOR_SUBMIT_STATUS,
    BLOCK_VISITOR_ERROR,
    BLOCK_VISITOR_RECEIVE,
    BLOCK_VISITOR_REQUEST,
    UNBLOCK_VISITOR_ERROR,
    UNBLOCK_VISITOR_RECEIVE,
    UNBLOCK_VISITOR_REQUEST,
    DELETE_USER_ERROR,
    DELETE_USER_REQUEST,
    DELETE_USER_RECEIVE
    

} from '../actions/actionConfig';

const initialState = {
    isLoading: false,
    isTableLoading:false,    
    supportStaticData:[],
    submitStatus:1000,
    allVisitors:[],
    visitorDetails:[],
    visitorHistory:[],
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_ALL_VISITORS_REQUEST:
            return { ...state, isTableLoading: true };

        case GET_ALL_VISITORS_RECEIVE:
            return { ...state, isTableLoading: false, isError: false, ...action.payload };

        case GET_ALL_VISITORS_ERROR:
            return { ...state, isTableLoading: false, isError: true, ...action.payload };

        case GET_VISITOR_HISTORY_REQUEST:
            return { ...state, isLoading: true };

        case GET_VISITOR_HISTORY_RECEIVE:
            return { ...state, isLoading: false, isError: false, ...action.payload };

        case GET_VISITOR_HISTORY_ERROR:
            return { ...state, isLoading: false, isError: true, ...action.payload };

        case BLOCK_VISITOR_REQUEST:
            return { ...state, isLoading: true };

        case BLOCK_VISITOR_RECEIVE:
            return { ...state, isLoading: false, isError: false, ...action.payload };

        case BLOCK_VISITOR_ERROR:
            return { ...state, isLoading: false, isError: true, ...action.payload };

        case UNBLOCK_VISITOR_REQUEST:
            return { ...state, isLoading: true };

        case UNBLOCK_VISITOR_RECEIVE:
            return { ...state, isLoading: false, isError: false, ...action.payload };

        case UNBLOCK_VISITOR_ERROR:
            return { ...state, isLoading: false, isError: true, ...action.payload };

        case DELETE_USER_REQUEST:
            return { ...state, isLoading: true };

        case DELETE_USER_RECEIVE:
            return { ...state, isLoading: false, isError: false, ...action.payload };

        case DELETE_USER_ERROR:
            return { ...state, isLoading: false, isError: true, ...action.payload };

        case CLEAR_VMSETTING_STORE_STORE:
            return { ...initialState };

        case CLEAR_ERROR_STATUS:
            { return { ...state, errorStatus: 200 }}


        case CLEAR_VISITOR_SUBMIT_STATUS:
            { return { ...state, submitStatus: 1000 }}

        default:
            return state;
    }
}