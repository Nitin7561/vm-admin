var server = "cs";

var APIs = {
    getAllVisitors:{
        url:"https://cs.leantron.dev/vm/api/ver1.2/admin/getListOfVisitorsByAdmin",
        type:"GET",
        sampleRequest:{},
        sampleResponse:{},
        parser: function(payload){
            console.log(payload);
            if(payload.status.code != 200){
                return { getVisitorStaus:500 }
            }else 
            return {allVisitors:payload.data.data};
        }                    
    },
    deleteUser:{
        url:"https://cs.leantron.dev/vm/api/ver1.2/visitor/deleteVisitor",
        type:"POST",
        sampleRequest:{},
        sampleResponse:{},
        parser: function(payload){
            console.log(payload);
            if(payload.status.code != 200){
                return { submitStatus:500 }
            }else 
            return {submitStatus:200};
        }                    
    },
    blockVisitor:{
        url:"https://cs.leantron.dev/vm/api/ver1.2/visitor/blacklistVisitor",
        type:"POST",
        sampleRequest:{},
        sampleResponse:{},
        parser: function(payload){
            console.log(payload);
            if(payload.status.code != 200){
                return { submitStatus:500 }
            }else 
            return {submitStatus:200};
        }                    
    },
    getVisitorHistory:{
        url:"https://cs.leantron.dev/vm/api/ver1.2/admin/getReportedAndBlockedHistory",
        type:"GET",
        sampleRequest:{},
        sampleResponse:{},
        parser: function(payload){
            console.log(payload);
            if(payload.status.code != 200){
                return { errorStatus:500 }
            }else 
            return {visitorHistory:payload.data.history};
        }                    
    },
    unblockVisitor:{
        url:"https://cs.leantron.dev/vm/api/ver1.2/visitor/unBlacklistVisitor",
        type:"POST",
        sampleRequest:{},
        sampleResponse:{},
        parser: function(payload){
            console.log(payload);
            if(payload.status.code != 200){
                return { submitStatus:500 }
            }else 
            return {submitStatus:200};
        }                    
    }

}


export default APIs;