import React,{useEffect,useState} from 'react';
import './StatCard.scss'
import upArrow from './up.svg';
import downArrow from './down.svg';

function StatCard(props){
    
    function cardClassName(){
            if(props.color){
                if(props.cardType == "secondary")
                return "SCOuterContainer secondaryCard "+props.color;
                else
                return "SCOuterContainer "+props.color;  
            }
            else{
                return "SCOuterContainer";
            }
    }

    if(props.type === "dual"){
        return(
            <div className={cardClassName()}>
                <div className = "SCInnerContainer" >
                    <p className = "SCStatName">{props.statName?props.statName[0]:"Assigned"}</p>
                    <div className="SCStatArrowContainer">
                        <p className = "SCStatvalue">{props.value?props.value[0]:0}</p>
                        <img className="SCArrowIcon" src = {props.arrowType[0] === "up"?upArrow:props.arrowType[0] === "down"?downArrow:""}/>
                    </div>
                    <p className="SCTime">{props.statTimeText?props.statTimeText[0]:"updated now"}</p>
                </div>

                <div className = "SCInnerContainer" style={{marginLeft:"20px"}} >
                    <p className = "SCStatName">{props.statName?props.statName[1]:"Unassigned"}</p>
                    <div className="SCStatArrowContainer">
                        <p className = "SCStatvalue">{props.value?props.value[1]:0}</p>
                        <img className="SCArrowIcon" src = {props.arrowType[1] === "up"?upArrow:props.arrowType[1] === "down"?downArrow:""}/>
                        {/* <img className="SCLogo" src={props.logoSrc}/> */}
                    </div>
                    <p className="SCTime">{props.statTimeText?props.statTimeText[1]:"updated now"}</p>
                </div>
            </div>
        )
    }
    else{
        return(
            <div className={cardClassName()}>
                <div className = "SCInnerContainer" >
                    <p className = "SCStatName">{props.statName?props.statName:"Total Issues"}</p>
                    <div className="SCStatArrowContainer">
                        <p className = "SCStatvalue">{props.value?props.value:0}</p>
                        <img className="SCArrowIcon" src = {props.arrowType === "up"?upArrow:props.arrowType === "down"?downArrow:""}/>
                        <img className="SCLogo" src={upArrow}/>
            
                    </div>
                    <p className="SCTime">{props.statTimeText?props.statTimeText:"updated now"}</p>
                </div>
            </div>
        )
    }
}


export default StatCard;