import React from "react";
import Modal from '../../components/Modal/FunctionalModal';
import RejectVisitor from '../RejectVisitor/RejectVis';

export default function AddAppointmentInAModal({closeModal,...props}) {
    // let {visitorName,visitorEmail,isDisabled} =props
    // console.log('Name', visitorName);
    // console.log('Email',visitorEmail);
    // console.log('isDisabled',isDisabled);
    // console.log("Visi ID==>",props.visitorId)
    return (
       <>
        <Modal title="Reject Visitor" handleClose={closeModal} children={<RejectVisitor visitorId={props.visitorId} appointmentId={props.appointmentId} registrationRejection={props.registrationRejection} />}/>
       </>
    );
}
