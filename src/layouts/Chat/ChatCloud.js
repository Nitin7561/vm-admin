import React, { useEffect } from "react";
import { InfoStaticText } from "../../components/InputTextField/InfoStaticText";
import { Grid, ListItem, List, ListItemAvatar, ListItemText, Avatar } from '@material-ui/core'
import userImage from '../../assets/userImg.svg'
import User from '../../assets/User.png'
import Loader from 'react-loader-spinner'
import "./chatCloud.scss";
import EmptySVG from '../../assets/emptyFav.svg'




function timeDifference(current, previous) {
    // console.log("curent",current)
    // console.log("previos",previous)
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;
    var elapsed = current - previous;
    if (elapsed < 2 * msPerMinute) {
         return getCustomTime(new Date(previous));   
    }
    else if (elapsed < msPerHour) {
        //  return getCustomTime(new Date(message.timestamp));   
         return Math.round(elapsed/msPerMinute) + ' minutes ago';   
    }
    else if (elapsed < msPerDay ) {
         return Math.round(elapsed/msPerHour ) + ' hours ago';   
    }
    else if (elapsed < msPerMonth) {
        return  Math.round(elapsed/msPerDay) + ' days ago';   
    }
    else if (elapsed < msPerYear) {
        return 'approximately ' + Math.round(elapsed/msPerMonth) + ' months ago';   
    }
    else {
        return 'approximately ' + Math.round(elapsed/msPerYear ) + ' years ago';   
    }
}


function checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }


function getCustomTime(time) {
    let ampm = "am";
    // console.log(time    )
    var h = time.getHours();
    var m = time.getMinutes();
    var d =time.getDay()
    // add a zero in front of numbers<10
    if (h >= 12) {
      ampm = "pm";
      h = h % 12 === 0 ? h : h % 12;
    }
    h = checkTime(h);
    m = checkTime(m);
    return `${h}:${m} ${ampm}`;
  }


const InComing = ({ message,visIcon }) => (
    <ListItem >
        <ListItemAvatar>
            <Avatar></Avatar>
        </ListItemAvatar>
        {/* <ListItemText className="cloud1" primary={message.message} secondary={getCustomTime(new Date(message.timestamp))} /> */}
        {/* <ListItem   Text secondary={message[1]} /> */}
         <ListItemText className="cloud1" primary={message.message} secondary={timeDifference(Date.now(),message.timestamp)} />
    </ListItem>);



const OutGoing = ({ message,visIcon }) => (
    <ListItem >
        <ListItemText className="cloud2" primary={message.message} secondary={timeDifference(Date.now(),message.timestamp)} />
        <ListItemAvatar>
            <Avatar src={visIcon}></Avatar>
        </ListItemAvatar>
    </ListItem>);



const ChatCloud = (props) => {
    
    const [duration, changeDuration] = React.useState("b");
    const [description, changeDescription] = React.useState("");

    useEffect(() => {
        var ref = document.getElementById("hii");
        ref.scrollTop = ref.scrollHeight
    })
    
    return (
        <>
            <div id="messagesList" className="main">
                <List id="hii" className="message-list">
                   {props.messages.length === 0 || props.messages === "" ? 
                   (<>

                        
                        <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                            <img src={EmptySVG} />
                            <span className="lean-table-empty-screen-text">No Messages</span>
                        </Grid>
                                   
                
                   </>):
                   (<>
                     {props.messages.length==0?<div><Loader type="TailSpin" height={100} width={100} color="#3F51B5" style={{marginTop:"235px",marginLeft:"134px"}} /></div>:props.messages.map((message, index) => {
                        return (
                            <div>          
                                {message.sender =="host"? <InComing visIcon={props.visIcon} message={message} /> : <OutGoing visIcon={props.visIcon} message={message} />}
                            </div>
                        )
                    })}
                   </>) }
                </List>
            </div>
        </>
    );
};
export default ChatCloud;