import React, { useEffect } from "react";
import { Button, TextField, IconButton, Drawer } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import { connect } from "react-redux";

import ChatBox from './ChatBox'
import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor'



// import "./chatCloud.scss"

const ChatButton = (props) => {
    const [state, setState] = React.useState({ "vis1": false, "vis1": false });
    // const [visSub, setVisSub] = React.useState('')
    // const [trigger, setTrigger] = React.useState(false)


    return (
        <>  <div style={{ display: "flex", flexDirection: "column", alignContent: "center" }}>
            <div style={{ flex: 1 }}>
                <Button variant="contained" color="primary" onClick={(e) => {
                     props.getDetailForVisitor({
                        "visitorSub": "LTVI1575540362000"   
                    })
                    setState({ ...state, "vis1": true, "vis2": false })
                    // setVisSub("LTVI1575540362000")
                    // setTrigger(true)
                   

                }}>
                    MOTU </Button>
            </div>
            <br />
            <div style={{ flex: 1 }}>

                <Button variant="contained" color="secondary" 
                    onClick={(e) => {
                        // setTrigger(true)
                        // setVisSub("LTVI157554036000")
                        props.getDetailForVisitor({
                            "visitorSub": "LTVI157554036000"   
                        })
                        setState({ ...state, "vis1": true, "vis2": true })
                    }}>PATLU</Button>
            </div>

        </div>


            <Drawer variant="persistent" anchor="right" open={state.vis1} >
                <IconButton style={{ position: "absolute", right: 0, top: 0 }} color="inherit" aria-label="open drawer" onClick={(e) => setState({ ...state, "vis1": false, "vis2": false })}  > <CloseIcon /> </IconButton>
                   <ChatBox/> 
            </Drawer>
          

        </>

    )


};

const mapStateToProps = state => {
    const {  getDetailForVisitorReducer } = state;
    console.log("Message-->", getDetailForVisitorReducer);
    return {  visitorDetails: getDetailForVisitorReducer };
};
export default connect(mapStateToProps, { getDetailForVisitor: getDetailForVisitor })(
    ChatButton
);

// export default ChatButton

