import React, { useEffect } from "react";
import {  TextField, IconButton } from '@material-ui/core'
import ChatHeader from "./ChatHeader"
import SendSharpIcon from '@material-ui/icons/SendSharp';
import ChatCloud from "./ChatCloud.js";
import { messaging } from "../../init-fcm";
import { connect } from "react-redux";
import { getAllMessage, sendMessage, receiveMessage } from '../../redux/actions/chatAction/getAllMessage';
import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor'
import "./chatCloud.scss"
import FullPageLoading from '../../components/Loaders/FullPageLoading';
import { replyNotificationAction } from '../../layouts/LeanSidebarWithNoti/LeanSideBarContainer/actions/notifications';



const ChatBox = ({ messageList, visitorDetails, onLoad, sendMessage, receiveMessage,replyNotificationActionNow }) => {

    
    const [message, changeMessage] = React.useState("");

    let { loading: isLoadingMessageList, data: dataMessageList, error: isErrorMessageList } = messageList

    let { loading: isLoadingGetVisitorDetails, data: dataGetVisitorDetails, error: isErrorGetVisitorDetails } = visitorDetails

    let { loading: isLoadingGetReply, data: dataGetReply, error: isErrorGetReply } = replyNotificationActionNow

    console.log("Heyy reply notification-->",dataGetReply);


    useEffect(() => {

        console.log('ChatBox in chatBox', dataGetVisitorDetails)

        if(!isLoadingGetVisitorDetails){
            console.log('ChatBox not null', dataGetVisitorDetails)
                onLoad({
                    "id": dataGetVisitorDetails.sub,
                    // "hostSub": "60240705"
                })
            }
            else{
                console.log('ChatBox null', dataGetVisitorDetails)
            }
        },[dataGetVisitorDetails,dataGetReply])

    



    console.log('ChatBox', dataMessageList)

    console.log("Printing message from crap",message);

    console.log("Printing message from crap 1",{message});



    return (
        <div className="main-root">
            
            <div className="chat-header">
                {isLoadingGetVisitorDetails ? null : 
                (
                    <>
                        <div > <ChatHeader visitorDetails={dataGetVisitorDetails} /></div>
                    </>
                )}
            </div>
            
            <div className="cloud-flex">
              {isLoadingGetVisitorDetails ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading> : ( <>
                {isLoadingMessageList ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading> : (<>
                    <div> <ChatCloud visIcon={dataGetVisitorDetails.photo == undefined ? " " : dataGetVisitorDetails.photo} messages={dataMessageList === 0 ? [""] : dataMessageList} /></div>
                </>)}</>)}

                {/* <div> <ChatCloud visIcon={dataGetVisitorDetails.photo == undefined ? " " : dataGetVisitorDetails.photo} messages={dataMessageList === 0 ? [""] : dataMessageList} /></div> */}

            </div>

            <div className="send-flex">
                <TextField
                    classes={{root:'my-adorned-button'}}
                    variant="outlined"
                    id="sendButton"
                    style={{width: "100%"}}
                    placeholder="Type Something"
                    onKeyPress={(ev) => {
                        console.log(`Pressed keyCode ${ev.key}`);
                        if (ev.key === 'Enter') {
                            console.log("entered presses",ev.target.value)
                            sendMessage(ev.target.value, { "visitorSub": dataGetVisitorDetails.sub, "message": ev.target.value });
                            document.getElementById("sendButton").value=""
                            ev.preventDefault();
                        }
                        changeMessage(ev.target.value)
                      }}
                    
                    InputProps={{
                        endAdornment: (
                            
                            <IconButton  id="iconButton" onClick={(e) => {
                                sendMessage(message, { "visitorSub": dataGetVisitorDetails.sub, "message": message });
                                document.getElementById("sendButton").value=""
                            }}>
                                <SendSharpIcon className="sharpButton" fontSize='large' />
                            </IconButton>
                        )
                    }}
                />
            </div>
        </div>
    );

};
const mapStateToProps = state => {
    const { getAllMessageReducer, getDetailForVisitorReducer,replyNotification } = state;
    console.log("Message-->", getAllMessageReducer);
    return { messageList: getAllMessageReducer, visitorDetails: getDetailForVisitorReducer,replyNotificationActionNow : replyNotification };
};
export default connect(mapStateToProps, { onLoad: getAllMessage, sendMessage: sendMessage, receiveMessage: receiveMessage, getDetailForVisitor: getDetailForVisitor, })(
    ChatBox
);