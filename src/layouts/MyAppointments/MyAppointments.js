import React from 'react'
import { Grid, CircularProgress, Hidden, useTheme, useMediaQuery } from '@material-ui/core'
import TableFilters from '../TableFilters/TableFilters'
import BookingTable from '../../components/Tables/BookingTable'

import './myBookings.scss';
import { loadMyBookings } from '../../actions/myBookings';
import { connect } from 'react-redux';
import { STATUS_REV_ENUM } from '../../utils/Enums';
import TableCard from '../../components/Tables/TableCard';


const MyAppointments = ({ refresher, myBookings, loadMyBookings }) => {

    const [page, setPage] = React.useState(1);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [sortColumnIndex, setSortIndex] = React.useState(3);
    const [sortOrder, setSortOrder] = React.useState(-1);

    const [bookings, changeBookings] = React.useState([]);

    let { isLoading, singleBookings, pagination, isError, error } = myBookings;

    let totalNoOfPages = pagination ? (pagination.uniqueDocs <= rowsPerPage ? 1 : Math.ceil((pagination.uniqueDocs) / rowsPerPage)) : 0;

    let totalNoOfDocs = pagination ? (pagination.uniqueDocs <= rowsPerPage ? singleBookings.length : pagination.uniqueDocs) : 0;

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    let rxdBatchIds = [];

    let onChangeRX = (newRX) => {
        console.log("newRX", newRX);
        rxdBatchIds = [...newRX]
    }

    const [status, setStatus] = React.useState("All");
    const [timing, setTiming] = React.useState({ fromTime: Math.ceil((((new Date()).getTime())) - 2 * 365 * 24 * 60 * 60 * 1000), toTime: Math.ceil((((new Date()).getTime())) + 2 * 365 * 24 * 60 * 60 * 1000) });

    const onChangeStatus = (newStatus) => {
        console.log("onChangeStatus", newStatus, JSON.stringify({
            fromTime: Math.floor(timing.fromTime / 1000),
            toTime: Math.floor(timing.toTime / 1000),
            rowsPerPage: rowsPerPage,
            pageNumber: 1,
            sortColumnIndex,
            sortOrder,
            statusFilter: STATUS_REV_ENUM[newStatus],
            rxdBatchIds: [],
        }))
        setStatus(newStatus);
        setPage(1)
        changeBookings([])
        loadMyBookings({
            fromTime: Math.floor(timing.fromTime / 1000),
            toTime: Math.floor(timing.toTime / 1000),
            rowsPerPage: rowsPerPage,
            pageNumber: 1,
            sortColumnIndex,
            sortOrder,
            statusFilter: STATUS_REV_ENUM[newStatus],
            rxdBatchIds: []
        });
    }

    let onClickSort = (newSI) => {

        if (newSI === null) {
            return;
        }
        let newSortOrder = sortColumnIndex;
        if (newSI === sortColumnIndex) {
            if (sortOrder === -1) {
                newSortOrder = 1;
            } else {
                newSortOrder = -1
            }
        } else {
            newSortOrder = 1
        }

        setSortIndex(newSI)
        setSortOrder(newSortOrder)

        loadMyBookings({
            fromTime: Math.floor(timing.fromTime / 1000),
            toTime: Math.floor(timing.toTime / 1000),
            rowsPerPage: rowsPerPage,
            pageNumber: 1,
            sortColumnIndex: newSI,
            sortOrder: newSortOrder,
            statusFilter: STATUS_REV_ENUM[status],
            rxdBatchIds: []
        });

    }

    const handleChangePage = (event, newPage, pagination) => {
        setPage(newPage);

        console.log("newRx", newPage, JSON.stringify({
            fromTime: Math.floor(timing.fromTime / 1000),
            toTime: Math.floor(timing.toTime / 1000),
            rowsPerPage: rowsPerPage,
            pageNumber: newPage,
            sortColumnIndex,
            sortOrder,
            statusFilter: STATUS_REV_ENUM[status],
            rxdBatchIds,
        }));

        loadMyBookings({
            fromTime: Math.floor(timing.fromTime / 1000),
            toTime: Math.floor(timing.toTime / 1000),
            rowsPerPage: rowsPerPage,
            pageNumber: newPage,
            sortColumnIndex,
            sortOrder,
            statusFilter: STATUS_REV_ENUM[status],
            rxdBatchIds,
        });

        // onLoad({
        //   fromTime: Math.ceil((((new Date()).getTime())/1000) - 2 * 365 * 24 * 60 * 60 ),
        //   toTime: Math.ceil((((new Date()).getTime())/1000) + 2 * 365 * 24 * 60 * 60 ),
        //   rowsPerPage: rowsPerPage,
        //   actualCount: pagination ? pagination.pages[newPage].actualCount : 0,
        //   sortColumnIndex: 1,
        //   sortOrder: 1,
        //   statusFilter: 4
        // });
    };

    // const

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(1);

        loadMyBookings({
            fromTime: Math.floor(timing.fromTime / 1000),
            toTime: Math.floor(timing.toTime / 1000),
            rowsPerPage: event.target.value,
            pageNumber: 1,
            sortColumnIndex,
            sortOrder,
            statusFilter: STATUS_REV_ENUM[status],
            rxdBatchIds: []
        });

        // onLoad({
        //   fromTime: Math.ceil((((new Date()).getTime())/1000) - 2 * 365 * 24 * 60 * 60 ),
        //   toTime: Math.ceil((((new Date()).getTime())/1000) + 2 * 365 * 24 * 60 * 60 ),
        //   rowsPerPage: ,
        //   actualCount: 0,
        //   sortColumnIndex: 3,
        //   sortOrder: 1,
        //   statusFilter: 4
        // });
    };

    const filterMyBookings = (fromTime, toTime) => {

        console.log("filterMyBookings", fromTime, toTime);
        setTiming({ fromTime, toTime });
        setPage(1);
        changeBookings([]);
        loadMyBookings({
            fromTime: Math.floor(fromTime / 1000),
            toTime: Math.floor(toTime / 1000),
            rowsPerPage: rowsPerPage,
            pageNumber: 1,
            sortColumnIndex,
            sortOrder,
            statusFilter: STATUS_REV_ENUM[status],
            rxdBatchIds: []
        });
    }



    React.useEffect(() => {
        loadMyBookings({
            fromTime: Math.floor(timing.fromTime / 1000),
            toTime: Math.floor(timing.toTime / 1000),
            rowsPerPage: rowsPerPage,
            pageNumber: page,
            sortColumnIndex,
            sortOrder,
            statusFilter: STATUS_REV_ENUM[status],
            rxdBatchIds,
        });
    }, [refresher.myBookings]);


    React.useEffect(() => {
        if (fullScreen) {
            console.log("bookings", singleBookings, bookings);
            changeBookings([...bookings, ...singleBookings])
        }
    }, [singleBookings]);


    const handleScroll = (e) => {

        if (!fullScreen) return;

        if (isLoading || isError || (totalNoOfPages < (page + 1))) return;

        if (e.currentTarget.offsetHeight + e.currentTarget.scrollTop >= e.currentTarget.scrollHeight - 50) {

            console.log("LAZY LOAD", e.currentTarget.offsetHeight, e.currentTarget.scrollTop, e.currentTarget.scrollHeight, e.currentTarget.offsetHeight + e.currentTarget.scrollTop >= e.currentTarget.scrollHeight - 50)

            handleChangePage(null, page + 1, null);
            console.log('SCROLL eop')
        }
    }

    return (
        <div style={{ height: "100%", overflowY: "auto", overflowX: 'hidden' }} onScroll={(e) => handleScroll(e)}>
            <Grid container className="lean-book-table" >


                <Grid item xs={12} className="lean-grid-item">
                    <TableFilters filterMyBookings={filterMyBookings} status={status} setStatus={onChangeStatus} fullScreen={fullScreen} totalNoOfDocs={totalNoOfDocs} />
                </Grid>

                <Grid item xs={12} className="lean-grid-item">
                    <Hidden smUp implementation="css">
                        <TableCard myBookings={{ ...myBookings, singleBookings: bookings }} page={page} setPage={setPage} rowsPerPage={rowsPerPage} setRowsPerPage={setRowsPerPage} handleChangePage={handleChangePage} handleChangeRowsPerPage={handleChangeRowsPerPage}
                            totalNoOfDocs={totalNoOfDocs}
                            onChangeRX={onChangeRX} onClickSort={onClickSort} sortColumnIndex={sortColumnIndex} sortOrder={sortOrder} />
                    </Hidden>

                    <Hidden xsDown implementation="css">
                        <BookingTable myBookings={myBookings} page={page} setPage={setPage} rowsPerPage={rowsPerPage} setRowsPerPage={setRowsPerPage} handleChangePage={handleChangePage} handleChangeRowsPerPage={handleChangeRowsPerPage}
                            onChangeRX={onChangeRX} onClickSort={onClickSort} sortColumnIndex={sortColumnIndex} sortOrder={sortOrder}
                        />
                    </Hidden>
                </Grid>
            </Grid>
        </div>
    )
}

const mapStateToProps = state => {
    let { refresher, myBookings } = state;
    console.log("MyAppointments", refresher);
    return { refresher, myBookings };
};

export default connect(mapStateToProps, { loadMyBookings })(MyAppointments);
