import React from "react";
import Modal from '../../components/Modal/FunctionalModal';
import ReportVisitor from '../Report/ReportUser';


export default function AddAppointmentInAModal({closeModal,...props}) {
    // let {visitorName,visitorEmail,isDisabled} =props
    // console.log('Name', visitorName);
    // console.log('Email',visitorEmail);
    // console.log('isDisabled',isDisabled);
    return (
       <>
        <Modal title="Report Visitor" handleClose={closeModal} children={<ReportVisitor visitorId={props.visitorId}/>}/>
       </>
    );
}
