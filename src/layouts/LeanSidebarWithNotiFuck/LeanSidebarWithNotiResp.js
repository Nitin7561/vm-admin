import React from 'react'

import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import SettingsIcon from "@material-ui/icons/Settings";
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from "@material-ui/icons/Notifications";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";

import "./leanSidebar.scss";

// import { messaging } from "../../fcm/initFCM";


import CompanyIcon from "../../assets/BridgeUX_SVG.svg";
import UserImage from "../../assets/user.png";
import HelpIcon from "../../assets/help.svg";
import AboutIcon from "../../assets/About.png";
import SupportIcon from "../../assets/Support.svg";
import LogoutIcon from "../../assets/Logout.svg";
import PersonIcon from "../../assets/person.svg";
import SettingIcon from "../../assets/setting.svg";

import HomeIcon from "../../assets/home.svg";
import WFIcon from "../../assets/wayfinding.svg";
import RBIcon from "../../assets/rb.svg";
import VMIcon from "../../assets/vm.svg";
import DeskIcon from "../../assets/desk.svg";
import LeanovateIcon from "../../assets/leanovate.svg";

// import MyProfile from '../modal/myProfile/MyProfile'
import {
    ListItemAvatar,
    Avatar,
    ListItemSecondaryAction,
    Popover
} from "@material-ui/core";
import { Link, Redirect } from "react-router-dom";
/// *********
import { ModalContext } from "../../utils/ModalContext";
/// *********
import PopUpNotificationPage from "./notificationPopUp/PopUpNotificationPage";
import { getAllNotificationsAction, addNotificationAction } from './LeanSideBarContainer/actions/notifications';
import { getApplicationsAction } from './LeanSideBarContainer/actions/homeStaticData';
import { getUserInfoAction } from './LeanSideBarContainer/actions/user';
import { CircularProgress } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { connect } from 'react-redux';
import "firebase/messaging";
import Cookies from 'universal-cookie';
import Plain from './common/snackBar/notifications/PlainToast';
import Confirm from './common/snackBar/notifications/ConfirmToast';
import Select from './common/snackBar/notifications/SelectToast';
import Chat from './common/snackBar/notifications/ChatToast';
import fcmClient from './library/fcmClient/sharangClient'
import fcmConfig from './library/fcmClient/sampleConfig'
import { homePageId, rbPageId, applicationToPages, settingsApp } from './constants';
const useStyles = makeStyles(theme => ({
    drawer: {
        width: 300,
        flexShrink: 0,
        whiteSpace: "nowrap",
        zIndex: 2
    },
    drawerOpen: {
        width: 300,
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    drawerClose: {
        width: 300,
        // transition: theme.transitions.create("width", {
        //     easing: theme.transitions.easing.sharp,
        //     duration: 1000
        // }),
        // overflowX: "hidden",
        // width: theme.spacing(7) + 1

        // [theme.breakpoints.up("sm")]: {
        //   width: theme.spacing(9) + 1
        // }
    },

    toolbar: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar
    },
    menuButton: {
        // marginRight: theme.spacing(2)
    }
}));

const LeanSidebarWithNotiResp = (props) => {

    let { hover, userReducer, getApplicationReducer, getAllNotificationsReducer, getTenantReducer } = props
    let { getUserInfoAction, getApplicationsAction, getAllNotificationsAction, addNotificationAction } = props;


    let { isLoading: isLoadingUser, data: UserData, error: errorUser, isError: isErrorUser } = userReducer
    let { isLoading: isLoadingApplications, data: ApplicationsData, error: errorApplications, isError: isErrorApplications } = getApplicationReducer
    let { isLoading: isLoadingNotifications, data: notificationsData, error: errorNotifications, isError: isErrorNotifications, count: reduxCount } = getAllNotificationsReducer;



    const [openSB, setOpenSB] = React.useState(true);

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSB(false);
    };
    function loadSnackBar(message) {
        return (
            <Snackbar open={openSB} autoHideDuration={6000}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    {/* This is a success message!                 */}
                    {message}
                </Alert>
            </Snackbar>
        )
    }

    function onMessageSample(message) {
        // alert("function 1 alerted")
        console.log("FCM CLIENT 1 LIBRARY alerted")


    }
    function onMessageSample2(message) {
        // alert("function 2 alerted")
        // console.log("FCM CLIENT 2 LIBRARY alerted", message)
        console.log("onMessage firebase", message)

        let { type } = message.data['firebase-messaging-msg-data'].data

        console.log("type*****0", type)
        if (type == 'plain') {
            setopenToast({ ...openToast, state: true, message: message.data['firebase-messaging-msg-data'].data, type: 'plain' })
        }
        if (type == 'confirm') {
            setopenToast({ ...openToast, state: true, message: message.data['firebase-messaging-msg-data'].data, type: 'confirm' })
        }
        if (type == 'select') {
            setopenToast({ ...openToast, state: true, message: message.data['firebase-messaging-msg-data'].data, type: 'select' })
        }
        if (type == 'string') {
            setopenToast({ ...openToast, state: true, message: message.data['firebase-messaging-msg-data'].data, type: 'string' })
        }
        addNotificationAction(message.data['firebase-messaging-msg-data'].data)
    }
    //--------------------------------------------------  
    // console.log("count****",count)
    //----------------- Firebase Init ------------------
    // const [notificationCount, setNotificationCount] = React.useState(reduxCount);
    const [openToast, setopenToast] = React.useState({ 'state': false, 'message': '', 'type': '' });
    console.log("Rendered rendered")


    const [userMenuAnchorEl, setUserMenuAnchorEl] = React.useState(null);
    const userMenuOpen = Boolean(userMenuAnchorEl);

    const [
        notificationMenuAnchorEl,
        setNotificationMenuAnchorEl
    ] = React.useState(null);



    const notificationMenuOpen = Boolean(notificationMenuAnchorEl);

    function handleNotificationMenu(event) {

        //TODO: Take to notifications page
        window.location.href = "https://cs.leantron.dev/spaceSense/web/pages/notifications"
        // setNotificationMenuAnchorEl(event.currentTarget);
    }

    function handleNotificationMenuClose() {
        setNotificationMenuAnchorEl(null);
    }

    function handleUserMenu(event) {
        console.log("handleUserMenu", event.currentTarget);
        setUserMenuAnchorEl(event.currentTarget);
    }

    function handleUserMenuClose() {
        setUserMenuAnchorEl(null);
    }

    //   const toggleDrawerOpen = () => {
    //     if (hover) {
    //       setOpen(!open);
    //     }
    //   };

    //   const handleDrawerOpen = () => {
    //     if (hover) {
    //       setOpen(true);
    //     }
    //   };

    //   const handleDrawerClose = () => {
    //     if (hover) {
    //       setOpen(false);
    //     }
    //   };


    const closeSession = () => {
        const cookies = new Cookies();

        cookies.remove('lean.sid', { domain: '' });
        cookies.remove('rb.sid', { domain: '' });
        cookies.remove('vm.sid', { domain: '' });

        window.location.href = '/spaceSense/web/pages/home';

        // console.log("closeSession", cookies.get('lean-username'))
        // if (cookies.get('lean-username')) {
        //   cookies.remove('lean-username')
        //   window.location.href = '/login';
        // }
        // window.location.href = '/login';
    };

    const handleToastReset = () => {
        console.log("handleToastReset")
        setopenToast({ ...openToast, state: false, message: '' })
    };
    const callToastHandler = () => {
        console.log("callToastHandler", openToast)
        if (openToast.type == 'plain') return <Plain state={true} data={openToast.message} closeToast={handleToastReset} />
        if (openToast.type == 'select') return <Select state={true} data={openToast.message} closeToast={handleToastReset} />

        if (openToast.type == 'confirm') return <Confirm state={true} data={openToast.message} closeToast={handleToastReset} />
        if (openToast.type == 'string') return <Chat state={true} data={openToast.message} closeToast={handleToastReset} />
    };





    // let { isLoading: isLoadingApplications, data: ApplicationsData, error: errorApplications, isError: isErrorApplications } = getApplicationReducer


    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [appSelected, changeAppSelected] = React.useState(homePageId);

    React.useEffect(() => {
        console.log(rbPageId);
        changeAppSelected(rbPageId)
    }, [])



    let allData = [...ApplicationsData, ...settingsApp]

    React.useEffect(() => {
        // getApplicationsAction()
        // getUserInfoAction();
        getAllNotificationsAction()


        var motherAppFcmClient = new fcmClient("VMAP12345876564")

        motherAppFcmClient.initalize(fcmConfig, onMessageSample2)
        // motherAppFcmClient.registerReceiever(onMessageSample2)

    }, []);

    const toggleDrawerOpen = () => {
        setOpen(o => !o);
        changeAppSelected(rbPageId);
    };

    console.log(" ApplicationsData", ApplicationsData);
    return (
        <div>
            {openToast.state ? callToastHandler() : null}
            <AppBar position="fixed" className="lean-appbar">
                <Toolbar className="lean-appbar-toolbar">
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={toggleDrawerOpen}
                        edge="start"
                        disableRipple
                        className="lean-appbar-toolbar-drawer-icon"
                    >
                        <MenuIcon />
                    </IconButton>
                    <Link to="/home">
                        <div
                            edge="start"
                            color="white"
                            aria-label="logo"
                            className="lean-appbar-toolbar-logo-mob"
                        >
                            <img src={getTenantReducer.data.tenantLogo} height={40} alt={getTenantReducer.data.tenantName} />
                        </div>
                    </Link>

                    <div className="lean-appbar-toolbar-title" />
                    <div className="lean-appbar-toolbar-right">
                        <List className="lean-appbar-toolbar-list">


                            <ListItem button button onClick={handleNotificationMenu}
                            >
                                <Badge badgeContent={1} color="secondary">
                                    <NotificationsIcon />
                                </Badge>
                            </ListItem>

                            <ListItem button onClick={handleUserMenu}
                            >
                                <ListItemAvatar src={UserImage}>
                                    <Avatar src={UserImage} />
                                </ListItemAvatar>
                                <ListItemSecondaryAction>
                                    <ArrowDropDownIcon />
                                </ListItemSecondaryAction>
                            </ListItem>
                        </List>
                    </div>


                    <Popover
                        className="lean-appbar-profile"
                        getContentAnchorEl={null}
                        anchorOrigin={{
                            vertical: "bottom"
                            // horizontal: "right"
                        }}
                        disablePortal
                        keepMounted
                        anchorEl={userMenuAnchorEl}
                        open={userMenuOpen}
                        onClose={handleUserMenuClose}
                    >
                        <List className="lean-appbar-profile-list">
                            <ListItem button onClick={() => { handleUserMenuClose(); openModal({ name: "MyProfile", props: "" }) }}>
                                <ListItemIcon>
                                    <img src={PersonIcon} />
                                    {/* <InboxIcon /> */}
                                </ListItemIcon>
                                <ListItemText primary="My Profile" />
                            </ListItem>
                            {/* <Link to="/home/myProfile">
                <ListItem button>
                  <ListItemIcon>
                    <img src={PersonIcon} />                    
                  </ListItemIcon>
                  <ListItemText primary="My Profile" />
                </ListItem>
              </Link> */}
                            <ListItem button onClick={() => {
                                handleUserMenuClose();
                                window.location.href = "https://cs.leantron.dev/spaceSense/web/pages/faq";
                            }}>
                                <ListItemIcon>
                                    <img src={HelpIcon} />
                                </ListItemIcon>
                                <ListItemText primary="Help & FAQ" />
                            </ListItem>

                            <ListItem button onClick={() => { handleUserMenuClose(); window.location.href = "https://cs.leantron.dev/spaceSense/web/pages/supportFeedback"; }}>
                                <ListItemIcon>
                                    <img src={SupportIcon} />
                                </ListItemIcon>
                                <ListItemText primary="Support & Feedback" />
                            </ListItem>

                            <ListItem button onClick={() => {
                                handleUserMenuClose();
                                window.location.href = "https://cs.leantron.dev/spaceSense/web/pages/aboutUs";
                            }}>
                                <ListItemIcon>
                                    <img src={AboutIcon} />

                                    {/* <InboxIcon /> */}
                                </ListItemIcon>
                                <ListItemText primary="About" />
                            </ListItem>

                            <ListItem button onClick={() => { handleUserMenuClose(); closeSession() }}>
                                <ListItemIcon>
                                    <img src={LogoutIcon} />
                                </ListItemIcon>
                                <ListItemText primary="Logout" />
                            </ListItem>
                        </List>
                    </Popover>

                    <Popover
                        getContentAnchorEl={null}
                        anchorOrigin={{
                            vertical: "bottom",
                            horizontal: "center"
                        }}
                        disablePortal={true}
                        keepMounted
                        anchorEl={notificationMenuAnchorEl}
                        open={notificationMenuOpen}
                        onClose={handleNotificationMenuClose}
                    >
                        {/* <Link to="/notifications" style={{ textDecoration: 'none' }}> */}
                        <PopUpNotificationPage closeNotificationPopUp={handleNotificationMenuClose} />
                        {/* </Link> */}
                    </Popover>




                </Toolbar>
            </AppBar>


            <Drawer
                // onMouseEnter={handleDrawerOpen}
                // onMouseLeave={handleDrawerClose}
                // onMouseOver = {handleDrawerOpen}
                // variant="persistent"
                // variant="permanent"
                open={open}
                transitionDuration={450}
                onClose={(e) => { setOpen(false); }}
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open
                })}
                classes={{
                    paper: clsx("lean-sidebar-paper", {
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open
                    })
                }}
            >
                {/* <div className={classes.toolbar}></div> */}
                <List>
                    <ListItem button style={{ paddingLeft: 20 }}
                    >
                        <ListItemAvatar src={UserImage} >
                            {/* <Avatar src={UserImage} />   */}
                            <i className="lean-icon-powered-by" style={{ fontSize: 32 }} />

                        </ListItemAvatar>
                        <ListItemText className="lean-appbar-toolbar-list-user">
                            SpaceSense
                        </ListItemText>

                    </ListItem>

                </List>
                <Divider />

                <div className={`lean-sidebar-list-mob ${appSelected === homePageId ? "" : "lean-sidebar-list-mob-hide"}`}>


                    <div>


                        <List>
                            {allData.map((text, index) => {
                                let { instance, applicationId } = text;
                                let url = "https://google.com"
                                if (instance.isRunning === "running") {
                                    let { protocol, host, port } = instance;
                                    url = `${protocol}://${host}:${port}${text.applicationHomePath}`;
                                    // `${protocol}://${host}:${port}`
                                }
                                return (
                                    <ListItem
                                        button
                                        onClick={(e) => {
                                            if (applicationToPages[applicationId].length === 0) {

                                                console.log('list down no')
                                                window.location.href = url
                                            } else {
                                                console.log('list down yes')
                                                changeAppSelected(applicationId);
                                            }
                                        }}
                                        key={applicationId}
                                        className={`lean-sidebar-list-item ${
                                            index === 1 ? "lean-sidebar-list-item-active" : ""
                                            }`}
                                    >
                                        <ListItemIcon className="lean-sidebar-list-item-icon">
                                            {/* <img src={text.applicationIcon} /> */}
                                            {/* <img src={temporaryIcon[index].icon} /> */}
                                            {/* <i className="lean-icon-ma-way-finder" /> */}
                                            <i className={text.applicationIcon} />
                                        </ListItemIcon>
                                        <ListItemText
                                            className="lean-sidebar-list-item-text"
                                            // primary={text.applicationName}
                                            primary={

                                                (
                                                    <div className="lean-sidebar-list-item-text-item">
                                                        <span>
                                                            {text.applicationName == "SpaceSense" ? "Home" : text.applicationName}
                                                        </span>

                                                        {applicationToPages[applicationId].length !== 0 ? <i className="lean-icon-chevron_right" /> : null}

                                                    </div>
                                                )}

                                        />

                                        {/* {
                                            applicationToPages[applicationId].length !== 0 ?
                                                <ListItemSecondaryAction>
                                                    
                                                    <i className="lean-icon-chevron_right" />
                                                </ListItemSecondaryAction> :
                                                null
                                        } */}


                                    </ListItem>
                                )
                            })}
                        </List>
                    </div>



                    {/* <div>
                        <List>
                            <ListItem
                                button
                                onClick={(e) => {

                                    changeAppSelected("settings");
                                }}
                                key={"settings"}
                                className={`lean-sidebar-list-item`}
                            >
                                <ListItemIcon className="lean-sidebar-list-item-icon">
                                    
                                </ListItemIcon>
                                <ListItemText
                                    className="lean-sidebar-list-item-text"
                                    primary="Settings"
                                />
                            </ListItem>

                        </List>
                    </div> */}

                </div>




                {allData.map((text, index) => {
                    let { instance, applicationId, applicationName, applicationIcon } = text;
                    console.log("text", applicationId);
                    let url = "https://google.com"
                    if (instance.isRunning === "running") {
                        let { protocol, host, port } = instance;
                        url = `${protocol}://${host}:${port}${text.applicationHomePath}`;
                        // `${protocol}://${host}:${port}`
                    }

                    console.log(appSelected)

                    return (


                        <div className={`lean-sidebar-list-app-mob ${(appSelected !== homePageId) && (appSelected === applicationId) ? "lean-sidebar-list-app-mob-show" : ""}`}>
                            <List>
                                <ListItem className="lean-sidebar-list-item " button key={applicationName} onClick={() => {
                                    changeAppSelected(homePageId)
                                }}>

                                    <ListItemIcon className="lean-sidebar-list-item-icon lean-sidebar-list-item-back">
                                        <i className="lean-icon-chevron_left" />
                                    </ListItemIcon>

                                    <ListItemText
                                        className="lean-sidebar-list-item-text lean-sidebar-list-item-back"
                                        primary="Back"
                                    />

                                </ListItem>

                                <ListItem className="lean-sidebar-list-item " button key={applicationName}
                                    // onClick={() => {
                                    //     changeAppSelected(homePageId)
                                    // }}
                                >

                                    {/* <ListItemIcon className="le an-sidebar-list-item-icon">
                                        <i className={applicationIcon} />
                                    </ListItemIcon> */}

                                    <ListItemText
                                        className="lean-sidebar-list-item-text"
                                        primary={applicationName}
                                    />

                                </ListItem>


                                {applicationToPages[applicationId].map((pages, idx) => {

                                    console.log("LIST", pages);
                                    return (
                                        <ListItem className="lean-sidebar-list-item" button key={pages.name} onClick={() => {
                                            window.location.href = pages.link
                                        }}>
                                            <ListItemIcon className="lean-sidebar-list-item-icon">
                                                <i className={pages.icon} />
                                            </ListItemIcon>

                                            <ListItemText
                                                className="lean-sidebar-list-item-text"
                                                primary={pages.name}
                                            />

                                        </ListItem>
                                    )
                                })}

                            </List>
                        </div>
                    )
                })}




            </Drawer>
        </div >
    )
}

const mapStateToProps = (state) => {
    let { userReducer, getAllNotificationsReducer, getApplicationReducer, getTenantReducer } = state;
    return { userReducer, getAllNotificationsReducer, getApplicationReducer, getTenantReducer };
    // return { userReducer };
}


// export default connect(mapStateToProps,getUserInfoAction,getApplicationsAction,getAllNotificationsAction)(LeanSidebarWithNoti)
export default connect(mapStateToProps, {
    getUserInfoAction, getApplicationsAction, getAllNotificationsAction,
    addNotificationAction
})(LeanSidebarWithNotiResp)
