import { FETCH_REQ_FEEDBACK_SUBMIT, FETCH_FAIL_FEEDBACK_SUBMIT, SUBMIT_FEEDBACK } from '../../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_FEEDBACK_SUBMIT:
            return { ...state, isLoading: true };       
        case SUBMIT_FEEDBACK:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };    
        case FETCH_FAIL_FEEDBACK_SUBMIT:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };     

        default:
            return state;
    }
}