import { FETCH_REQ_FAQ, FETCH_FAIL_FAQ, GET_FAQ } from '../../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_FAQ:
            return { ...state, isLoading: true };   
        case GET_FAQ:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };    
        case FETCH_FAIL_FAQ:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}