import { FETCH_REQ_ABOUT, FETCH_FAIL_ABOUT, GET_ABOUT_US } from '../../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: {
        'aboutDescription':''
    },
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_ABOUT:
            return { ...state, isLoading: true };
        case GET_ABOUT_US:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: {...state.data,aboutDescription:action.payload},
              };
        case FETCH_FAIL_ABOUT:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };

        default:
            return state;
    }
}