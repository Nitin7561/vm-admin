import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import SettingsIcon from "@material-ui/icons/Settings";
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from "@material-ui/icons/Notifications";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";

import "./leanSidebar.scss";

// import { messaging } from "../../fcm/initFCM";


import CompanyIcon from "../../assets/BridgeUX_SVG.svg";
import UserImage from "../../assets/User.png";
import HelpIcon from "../../assets/help.svg";
import AboutIcon from "../../assets/About.png";
import SupportIcon from "../../assets/Support.svg";
import LogoutIcon from "../../assets/Logout.svg";
import PersonIcon from "../../assets/person.svg";
import SettingIcon from "../../assets/setting.svg";

import HomeIcon from "../../assets/home.svg";
import WFIcon from "../../assets/wayfinding.svg";
import RBIcon from "../../assets/rb.svg";
import VMIcon from "../../assets/vm.svg";
import DeskIcon from "../../assets/desk.svg";
import LeanovateIcon from "../../assets/leanovate.svg";

// import MyProfile from '../modal/myProfile/MyProfile'
import {
  ListItemAvatar,
  Avatar,
  ListItemSecondaryAction,
  Popover
} from "@material-ui/core";
import { Link } from "react-router-dom";
/// *********
import { ModalContext } from "../../utils/ModalContext";
/// *********
import PopUpNotificationPage from "./notificationPopUp/PopUpNotificationPage";
import { getAllNotificationsAction, addNotificationAction } from './LeanSideBarContainer/actions/notifications';
import { getApplicationsAction } from './LeanSideBarContainer/actions/homeStaticData';
import { getUserInfoAction } from './LeanSideBarContainer/actions/user';
import { CircularProgress } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { connect } from 'react-redux';
import "firebase/messaging";
import Cookies from 'universal-cookie';
import Plain from './common/snackBar/notifications/PlainToast';
import Confirm from './common/snackBar/notifications/ConfirmToast';
import Select from './common/snackBar/notifications/SelectToast';
import Chat from './common/snackBar/notifications/ChatToast';
import fcmClient from './library/fcmClient/sharangClient'
import fcmConfig from './library/fcmClient/sampleConfig'


const useStyles = makeStyles(theme => ({
  drawer: {
    width: 300,
    flexShrink: 0,
    whiteSpace: "nowrap",
    zIndex: 2
  },
  drawerOpen: {
    width: 300,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    // width: theme.spacing(7) + 1
    width: 72

    // [theme.breakpoints.up("sm")]: {
    //   width: theme.spacing(9) + 1
    // }
  },

  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar
  },
  menuButton: {
    // marginRight: theme.spacing(2)
  }
}));

// function LeanSidebar({ hover,userReducer,getAllNotificationsReducer,getApplicationReducer,
//     getUserInfoAction,getApplicationsAction,getAllNotificationsAction }) {
// function LeanSidebar({ hover,userReducer,getApplicationReducer}) {
function LeanSidebarWithNoti(props) {

  let { hover, userReducer, getApplicationReducer, getAllNotificationsReducer } = props
  let { getUserInfoAction, getApplicationsAction, getAllNotificationsAction, addNotificationAction, getTenantReducer } = props
  // console.log("LeanSidebarWithNoti --", props)  

  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const { setCurrentModal } = React.useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });



  React.useEffect(() => {
    if (!hover) {
      if (!open) {
        setOpen(true)
      }
    }
    else {
      if (open) {
        setOpen(false)
      }
    }
  }, [hover]);




  //-------------- MotherApp Get User Name from reducer --------------
  // let { userReducer, getAllNotificationsReducer } = props
  console.log("LeanSidebarWithNoti user==", userReducer)
  let { isLoading: isLoadingUser, data: UserData, error: errorUser, isError: isErrorUser } = userReducer
  let { isLoading: isLoadingApplications, data: ApplicationsData, error: errorApplications, isError: isErrorApplications } = getApplicationReducer
  let { isLoading: isLoadingNotifications, data: notificationsData, error: errorNotifications, isError: isErrorNotifications, count: reduxCount } = getAllNotificationsReducer
  console.log("ApplicationsData ***", ApplicationsData)

  //--------------- SnackBar Open for displaying errro messages-----------------

  const [openSB, setOpenSB] = React.useState(true);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSB(false);
  };
  function loadSnackBar(message) {
    return (
      <Snackbar open={openSB} autoHideDuration={6000}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {/* This is a success message!                 */}
          {message}
        </Alert>
      </Snackbar>
    )
  }

  function onMessageSample(message) {
    // alert("function 1 alerted")
    console.log("FCM CLIENT 1 LIBRARY alerted")


  }
  function onMessageSample2(message) {
    // alert("function 2 alerted")
    // console.log("FCM CLIENT 2 LIBRARY alerted", message)
    console.log("onMessage firebase", message)

    let { type } = message.data['firebase-messaging-msg-data'].data

    console.log("type*****0", type)
    if (type == 'plain') {
      setopenToast({ ...openToast, state: true, message: message.data['firebase-messaging-msg-data'].data, type: 'plain' })
    }
    if (type == 'confirm') {
      setopenToast({ ...openToast, state: true, message: message.data['firebase-messaging-msg-data'].data, type: 'confirm' })
    }
    if (type == 'select') {
      setopenToast({ ...openToast, state: true, message: message.data['firebase-messaging-msg-data'].data, type: 'select' })
    }
    if (type == 'string') {
      setopenToast({ ...openToast, state: true, message: message.data['firebase-messaging-msg-data'].data, type: 'string' })
    }
    addNotificationAction(message.data['firebase-messaging-msg-data'].data)
  }
  //--------------------------------------------------  
  // console.log("count****",count)
  //----------------- Firebase Init ------------------
  // const [notificationCount, setNotificationCount] = React.useState(reduxCount);

  React.useEffect(() => {
    // getApplicationsAction()
    // getUserInfoAction();
    // console.log("service worker 1111", JSON.stringify(onMessageSample2));
    console.log("service worker", JSON.stringify(onMessageSample2));

    var motherAppFcmClient = new fcmClient("VMAP12345876564")
    motherAppFcmClient.initalize(fcmConfig, onMessageSample2)
    motherAppFcmClient.registerReceiever(onMessageSample2)

    getAllNotificationsAction()

  }, []);

  const [openToast, setopenToast] = React.useState({ 'state': false, 'message': '', 'type': '' });
  console.log("Rendered rendered")


  const [userMenuAnchorEl, setUserMenuAnchorEl] = React.useState(null);
  const userMenuOpen = Boolean(userMenuAnchorEl);

  const [
    notificationMenuAnchorEl,
    setNotificationMenuAnchorEl
  ] = React.useState(null);



  const notificationMenuOpen = Boolean(notificationMenuAnchorEl);

  function handleNotificationMenu(event) {
    setNotificationMenuAnchorEl(event.currentTarget);
  }

  function handleNotificationMenuClose() {    
    setNotificationMenuAnchorEl(null);
  }

  function handleUserMenu(event) {
    setUserMenuAnchorEl(event.currentTarget);
  }

  function handleUserMenuClose() {
    setUserMenuAnchorEl(null);
  }

  const toggleDrawerOpen = () => {
    if (hover) {
      setOpen(!open);
    }
  };

  const handleDrawerOpen = () => {
    if (hover) {
      setOpen(true);
    }
  };

  const handleDrawerClose = () => {
    if (hover) {
      setOpen(false);
    }
  };


  const closeSession = () => {
    const cookies = new Cookies();

    cookies.remove('lean.sid', { domain: '' });
    cookies.remove('rb.sid', { domain: '' });
    cookies.remove('vm.sid', { domain: '' });

    window.location.href = '/spaceSense/web/pages/home';    
  };

  const handleToastReset = () => {
    console.log("handleToastReset")
    setopenToast({ ...openToast, state: false, message: '' })
  };
  const callToastHandler = () => {
    console.log("callToastHandler", openToast)
    if (openToast.type == 'plain') return (<Plain state={true} data={openToast.message} closeToast={handleToastReset} />)
    if (openToast.type == 'select') return (<Select state={true} data={openToast.message} closeToast={handleToastReset} />)

    if (openToast.type == 'confirm') return (<Confirm state={true} data={openToast.message} closeToast={handleToastReset} />)
    if (openToast.type == 'string') return (<Chat state={true} data={openToast.message} closeToast={handleToastReset} />)
  };


  // let modulesAdded = [
  //   { name: "Home", icon: HomeIcon },
  //   { name: "Room Booking", icon: RBIcon },
  //   { name: "Way Finder", icon: WFIcon },
  //   { name: "Visitor Management", icon: VMIcon },
  //   { name: "Desk Booking", icon: DeskIcon }
  // ];

  let temporaryIcon = [
    { icon: HomeIcon },
    { icon: RBIcon },
    { icon: WFIcon },
    { icon: VMIcon },
    { icon: DeskIcon }
  ]

  let modulesAdded = isLoadingApplications ? [] : ApplicationsData

  return (

    <div className={classes.root}>
      {openToast.state ? callToastHandler() : null}
      <CssBaseline />
      <AppBar position="fixed" className="lean-appbar">
        <Toolbar className="lean-appbar-toolbar">
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawerOpen}
            edge="start"
            disableRipple
            className="lean-appbar-toolbar-drawer-icon"
          >
            <MenuIcon />
          </IconButton>
          <Link to="/home">
            <div
              edge="start"
              color="white"
              aria-label="logo"
              className="lean-appbar-toolbar-logo"              
            >

              <img src={getTenantReducer.data.tenantLogo} height={40} alt={getTenantReducer.data.tenantName} />
            </div>
          </Link>

          <div className="lean-appbar-toolbar-title" />
          <div className="lean-appbar-toolbar-right">
            <List className="lean-appbar-toolbar-list">
              {/* <Link to="/settings/general"> */}
              <ListItem button onClick={() => {
                // window.location.href = "https://cs.leantron.dev/spaceSense/web/pages/settings/general"
                window.location.href = "/spaceSense/web/pages/settings/general"
              }}>
                <img src={SettingIcon} />
              </ListItem>
              {/* </Link> */}

              <ListItem button onClick={handleNotificationMenu}>
                {/* <Badge badgeContent={notificationCount} color="secondary"> */}
                <Badge badgeContent={reduxCount} color="secondary">
                  <NotificationsIcon />
                </Badge>
                {/* <NotificationsIcon /> */}
              </ListItem>

              <ListItem button onClick={handleUserMenu}>
                <ListItemAvatar src={UserImage}>
                  <Avatar src={UserImage} />
                </ListItemAvatar>
                <ListItemText className="lean-appbar-toolbar-list-user">
                  {/* {isLoadingUser ? <CircularProgress /> : ''} */}
                  {isErrorUser ? loadSnackBar('View All Notifications  : ' + errorUser) : ` ${UserData.name}`}
                </ListItemText>
                <ArrowDropDownIcon />
                {/* <ListItemSecondaryAction>
                  <ArrowDropDownIcon />
                </ListItemSecondaryAction> */}
              </ListItem>
              
            </List>
            
          </div>

          <Popover
            className="lean-appbar-profile"
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: "bottom"
              // horizontal: "right"
            }}
            disablePortal
            keepMounted
            anchorEl={userMenuAnchorEl}
            open={userMenuOpen}
            onClose={handleUserMenuClose}
          >
            <List className="lean-appbar-profile-list">
              <ListItem button onClick={() => { handleUserMenuClose(); openModal({ name: "MyProfile", props: "" }) }}>
                <ListItemIcon>
                  <img src={PersonIcon} />
                  {/* <InboxIcon /> */}
                </ListItemIcon>
                <ListItemText primary="My Profile" />
              </ListItem>
              
              <Link to="/faq">
                <ListItem button onClick={() => {
                  handleUserMenuClose();
                  // window.location.href = "https://cs.leantron.dev/spaceSense/web/pages/faq";                  
                }}>
                  <ListItemIcon>
                    <img src={HelpIcon} />
                  </ListItemIcon>
                  <ListItemText primary="Help & FAQ" />
                </ListItem>
              </Link>

              <Link to="/supportFeedback" >
                {/* <ListItem button onClick={() => { handleUserMenuClose(); window.location.href = "https://cs.leantron.dev/spaceSense/web/pages/supportFeedback"; }}> */}
                <ListItem button onClick={() => { handleUserMenuClose();
                  //  window.location.href = "/spaceSense/web/pages/supportFeedback"; 
                   }}>
                  <ListItemIcon>
                    <img src={SupportIcon} />
                  </ListItemIcon>
                  <ListItemText primary="Support & Feedback" />
                </ListItem>
              </Link>

              <Link to="/aboutUs">
                <ListItem button onClick={() => {
                  handleUserMenuClose();
                  // window.location.href = "https://cs.leantron.dev/spaceSense/web/pages/aboutUs";
                  // window.location.href = "/spaceSense/web/pages/aboutUs";
                }}>
                  <ListItemIcon>
                    <img src={AboutIcon} />

                    {/* <InboxIcon /> */}
                  </ListItemIcon>
                  <ListItemText primary="About" />
                </ListItem>
              </Link>

              <ListItem button onClick={() => { handleUserMenuClose(); closeSession() }}>
                <ListItemIcon>
                  <img src={LogoutIcon} />
                </ListItemIcon>
                <ListItemText primary="Logout" />
              </ListItem>
            </List>
          </Popover>

          <Popover
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center"
            }}
            disablePortal={true}
            keepMounted
            anchorEl={notificationMenuAnchorEl}
            open={notificationMenuOpen}
            onClose={handleNotificationMenuClose}
          >
            {/* <Link to="/notifications" style={{ textDecoration: 'none' }}> */}
            <PopUpNotificationPage closeNotificationPopUp={handleNotificationMenuClose} />
            {/* </Link> */}
          </Popover>
        </Toolbar>
      </AppBar>
      <Drawer
        onMouseEnter={handleDrawerOpen}
        onMouseLeave={handleDrawerClose}
        // onMouseOver = {handleDrawerOpen}
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open
        })}
        classes={{
          paper: clsx("lean-sidebar-paper", {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open
          })
        }}
      >
        <div className={classes.toolbar}></div>
        <Divider />
        <List>
          

          {modulesAdded.map((text, index) => {
            let { instance } = text;
            let url = "https://google.com"
            if (instance.isRunning === "running") {
              let { protocol, host, port } = instance;
              url = `${protocol}://${host}:${port}${text.applicationHomePath}`;
              // `${protocol}://${host}:${port}`
            }
            return (
              <ListItem
                button
                onClick={(e) => { window.location.href = url }}
                key={text.applicationName == "SpaceSense" ? "Home" : text.applicationName}
                className={`lean-sidebar-list-item ${
                  index === 1 ? "lean-sidebar-list-item-active" : ""
                  }`}
              >
                <ListItemIcon className="lean-sidebar-list-item-icon">
                  {/* <img src={text.applicationIcon} /> */}
                  {/* <img src={temporaryIcon[index].icon} /> */}
                  {/* <i className="lean-icon-ma-way-finder" /> */}
                  <i className={text.applicationIcon} />
                </ListItemIcon>
                <ListItemText
                  className="lean-sidebar-list-item-text"
                  // primary={text.applicationName}
                  primary={text.applicationName == "SpaceSense" ? "Home" : text.applicationName}
                />
              </ListItem>
            )
          })}
        </List>

        <div className="lean-sidebar-powered">
          {open ? (
            <>
              <div className="lean-sidebar-powered-subtext">Powered By</div>
              <div className="lean-sidebar-powered-logo">
                <img src={LeanovateIcon} />
              </div>
              <div className="lean-sidebar-powered-subtext">
                App Version 1.0
              </div>
            </>
          ) : (
              <>
                <div className="lean-sidebar-powered-logo">
                  <img src={LeanovateIcon} />
                </div>
              </>
            )}
        </div>
      </Drawer>
    </div>
  );
}

const mapStateToProps = (state) => {
  let { userReducer, getAllNotificationsReducer, getApplicationReducer, getTenantReducer } = state;
  return { userReducer, getAllNotificationsReducer, getApplicationReducer, getTenantReducer };
  // return { userReducer };
}


// export default connect(mapStateToProps,getUserInfoAction,getApplicationsAction,getAllNotificationsAction)(LeanSidebarWithNoti)
export default connect(mapStateToProps, {
  getUserInfoAction, getApplicationsAction, getAllNotificationsAction,
  addNotificationAction
})(LeanSidebarWithNoti)
