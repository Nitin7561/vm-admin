import React from 'react'
// import Notification_image from "../common/snackBar/notifications/assets/notification_image.svg";
import Notification_image from "../common/snackBar/notifications/assets/myProfile.svg";
import './notificationPopUp.scss'

export default function ConfirmPopUp(props) {
  
  const { seen, replied } = props.value
  let { notificationId, content, type, typespec } = props.value.data
  content = content.content
  // console.log("verify123",content)

  let replyMessage = 'APPROVE'

  //-- Message seen?
  const isMessgSeen = seen ? {} : { backgroundColor: "#ffe5e5" }

  const clickspan1 = (event) => {
    props.seenHandler(event, notificationId)
  }

  return (
    <div className="lean-notification-box-root" style={isMessgSeen}>
      <div className='lean-notification-box-flex'>
        <div className='div-1'>
          <div className="div-logo">
            <img className="logo-visitor" src={Notification_image} alt='loading icon..' />
          </div>
        </div>
        <div className='div-2'>
          <div className='message'>
            {/* <div className="div-cgp-header"> */}
            <span className="message-content" onClick={clickspan1}>{content}</span>
            {/* </div> */}
          </div>
          <div className='input'>
            {!replied ?
              <>
                <button className='input-button button-1-text' onClick={(event) => props.replyHandler(event, notificationId, true, type, replied)}> {typespec['yesText']}</button>
                <button className='input-button button-2-text' onClick={(event) => props.replyHandler(event, notificationId, false, type, replied)}>{typespec['noText']}</button>
              </>
              : <>
                <span className=' greyedOut'>Message Replied : </span>
                <span className=' greyedOut-message'>{replyMessage} </span>
              </>
            }

            {/* <button className='input-button button-1-text' onClick={(event) => props.replyHandler(event,notificationId,true,type, replied)}> {yesText } </button>
              <button className='input-button-2 button-2-text' onClick={(event) => props.replyHandler(event,notificationId,false,type, replied)}>{ noText }</button> */}
            {/* <span className='span-time'>{props.time}</span> */}
          </div>
        </div>

        <div className='div-3'>
          <span className='span-time-2'>{props.time}</span>
        </div>
      </div>

    </div>

  );
}