export const homePageId = "LTAP1111111111"
export const rbPageId = "LTAP1580370721"
export const vmPageId = "LTAP1580370719"

export const settingsApp = [
  {
    "applicationName": "Settings",
    "applicationNameShort": "LTS",
    "applicationId": "LTAP0000000000",
    "applicationIcon": "lean-icon-settings",
    "vendorName": "Leanovate",
    "version": "v1.0.6",
    "verifyPath": "/spaceSense/auth/profile",
    "applicationHomePath": "/spaceSense/web/pages/settings/general",
    "cookieName": "lean.sid",
    "loginPath": "/spaceSense/auth/login",
    "instance": {
      "protocol": "https",
      "host": "cs.leantron.dev",
      "port": 443,
      "isRunning": "running"
    }
  },
]


export const applicationToPages = {
  "LTAP1111111111": [],
  "LTAP1580370721": [
    {
      name: "Book a Room",
      // icon: BookARoomNavIcon,
      icon: 'lean-icon-book-a-room',
      link: "https://cs.leantron.dev/rb/web/pages/bookaroom",
      padding: "3px"
    },
    {
      name: "My Bookings",
      // icon: MyBookingsNavIcon,
      icon: 'lean-icon-my-bookings',
      link: "https://cs.leantron.dev/rb/web/pages/mybookings",
      padding: "4px"
    },
    {
      name: "My Favourites",
      // icon: MyFavouritesNavIcon,
      icon: 'lean-icon-my-favourites',
      link: "https://cs.leantron.dev/rb/web/pages/myfavourites",
      padding: "2px 1px"
    }
  ],

  "LTAP1580370719": [
    {
      name: "My Appointments",
      icon: 'lean-icon-book-a-room',
      link: "https://cs.leantron.dev/vm/web/pages/MyAppointments",
      padding: "3px"
    },
    {
      name: "List Of Visitors",
      icon: 'lean-icon-my-bookings',
      link: "https://cs.leantron.dev/vm/web/pages/VisitorTable",
      padding: "4px"
    },
    {
      name: "Frequent Visitors",
      icon: 'lean-icon-my-favourites',
      link: "https://cs.leantron.dev/vm/web/pages/MyFrequentVisitor",
      padding: "2px 1px"
    },
  ],


  "LTAP0000000000": [
    {
      name: "General Settings",
      icon: 'lean-icon-book-a-room',
      link: "https://cs.leantron.dev/spaceSense/web/pages/settings/general",
      padding: "3px"
    },
    {
      name: "Room Settings",
      icon: 'lean-icon-my-bookings',
      link: "https://cs.leantron.dev/spaceSense/web/pages/settings/rb",
      padding: "4px"
    },
  ],

}