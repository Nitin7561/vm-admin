module.exports = {
    "type":"fcm",
    "server":{
        "protocol":"https",
        "host":"cs.leantron.dev",
        "port":"443",
        "endpoints":{
            "registerDevice":{
                "path":"/spaceSense/fe/registerDevice",
                "method":"POST"
            },
            "deleteDevice":{
                "path":"/spaceSense/fe/deleteDevice",
                "method":"POST"
            }
        }
    },
    "config":{
        "publicVapidKey": "BJXDOJn46AI-xDBlZh6tUpYJbsj12MO6PkJ1szzKfP7oFrGVrTBv880LW79y2xn0q0zrDGSVpvYF1m5yJIs17gk",
        "apiKey": "AIzaSyBiYOHgCRU1nHLUYeMFQSzn201WSleGNa4",
        "authDomain": "chat-app-23529.firebaseapp.com",
        "databaseURL": "https://chat-app-23529.firebaseio.com",
        "projectId": "chat-app-23529",
        "storageBucket": "chat-app-23529.appspot.com",
        "messagingSenderId": "1088102145471",
        "measurementId": "1088102145471",
        "appId": "1:1088102145471:web:3725852aa40df9c29d150a"
    }
}
