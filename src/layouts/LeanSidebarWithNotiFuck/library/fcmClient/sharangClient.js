import firebase from 'firebase';
function getUserAgent() {
    return navigator.userAgent
}
function getUserType(userAgent) { //web, android, ios
    var mobile = ['iphone', 'ipad', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) return "Mobile";
    return "web";
}
function sendTokenToServer(token, serverConfig) {
    return new Promise(async function (resolve, reject) {
        console.log("inside sendToken", token)
        var serverUrl = serverConfig["protocol"] + "://" + serverConfig["host"] + ":" + serverConfig["port"] + serverConfig["endpoints"]["registerDevice"]["path"];
        var serverMethod = serverConfig["endpoints"]["registerDevice"]["method"];
        var userAgent = getUserAgent();
        var userType = getUserType(userAgent);
        var params = {
            "userAgent": userAgent,
            "type": "web",
            "token": token
        }
        var request = require('request');
        var options = {
            'method': serverMethod,
            'url': serverUrl,
            'headers': {
                'Content-Type': 'application/json'
            },
        };
        if (serverMethod == "POST") {
            options['body'] = JSON.stringify(params);
        } else {
            options['qs'] = params;
        }
        request(options, function (error, response, body) {
            try {
                if (error) {
                    throw new Error(error)
                }
                //TODO: If server Down, handle responseCode "undefind"
                var responseCode = response.statusCode;
                console.log("responseCode", responseCode, response)
                if (responseCode < 199 || responseCode > 299) {
                    throw "restApiFailError"
                }
                if (responseCode != undefined)
                    body = JSON.parse(body);
            } catch (err) {
                console.log("ERRROR inREST", err);
                return reject(err);
            }
            return resolve(body);
        });
    });
}
function deleteTokenFromServer(token, serverConfig) {
    return new Promise(async function (resolve, reject) {
        var serverUrl = serverConfig["protocol"] + "://" + serverConfig["host"] + ":" + serverConfig["port"] + serverConfig["endpoints"]["deleteDevice"]["path"];
        var serverMethod = serverConfig["endpoints"]["deleteDevice"]["method"];
        var userAgent = getUserAgent();
        var userType = getUserType(userAgent);
        var params = {
            "userAgent": userAgent,
            "type": "web",
            "token": token
        }
        var request = require('request');
        var options = {
            'method': serverMethod,
            'url': serverUrl,
            'headers': {
                'Content-Type': 'application/json'
            },
        };
        if (serverMethod == "POST") {
            options['body'] = JSON.stringify(params);
        } else {
            options['qs'] = params;
        }
        request(options, function (error, response, body) {
            try {
                if (error) {
                    throw new Error(error)
                }
                var responseCode = response.statusCode;
                if (responseCode < 199 || responseCode > 299) {
                    throw "restApiFailError"
                }
                body = JSON.parse(body);
            } catch (err) {
                console.log("ERRROR inREST", err);
                return reject(err);
            }
            return resolve(body);
        });
    });
}
function removeFromArray(middlewareFunctions, middlewareFunction) {
    //TODO
    const index = middlewareFunctions.indexOf(middlewareFunction);
    if (index > -1) {
        middlewareFunctions.splice(index, 1);
    }
    return middlewareFunctions;
}
function generateUUIDMicros(obj) {
    var data = Date.now()
    var random = obj + data.toString()
    return random
}
function isArray(obj) {
    return Object.prototype.toString.call(obj) === '[object Array]';
}
function isObject(obj) {
    return typeof obj === 'object' && obj !== null && !(obj instanceof Array) && !(obj instanceof Date);
}
function isString(word) {
    if (typeof word === 'string' || word instanceof String) {
        return true
    }
    else {
        return false
    }
}
function isFunction(obj) {
    console.log("isFunction", obj);
    return obj && {}.toString.call(obj) === '[object Function]';
}
let fcmClient = class fcmClient {
    middlewareFunctions = [];
    type = null;
    messaging = null;
    constructor(recieverId) {
        this.type = recieverId;
    }
    initalize(config, defaultNotifShowFunction) {
        var allfuns = this.middlewareFunctions;
        return new Promise(async function (resolve, reject) {
            // if (!isObject(config) && !config.hawOwnProperty('type')) {
            if (!isObject(config)) {
                return reject("failedToObtainToken")
            }
            config.type = config.type.toLowerCase();
            if (config.type == "fcm") {
                var serverConfig = config['server'];
                var firebaseConfig = config['config'];
                var token = null;
                // if (!config.hawOwnProperty('server') || !isObject(config['server'])) {
                if (!isObject(config['server'])) {
                    return reject("badServerConfiguration")
                }

                if (!isFunction(defaultNotifShowFunction)) {
                    throw new Error("notAFunction");
                }
                if (!firebase.apps.length) {
                    console.log("its okay 1")
                    firebase.initializeApp(
                        // taken from your project settings --> cloud messaging
                        firebaseConfig
                        //TODO should wwe send more.. PLEASE READ FULLY BRO
                    );
                } else {
                    console.log("its okay 2", firebase.apps)
                }

                var messaging = null
                try {

                    messaging = firebase.messaging();

                    messaging.usePublicVapidKey(firebaseConfig['publicVapidKey']);

                    var permission = await messaging.requestPermission();


                    var registration;
                    let nR = await navigator.serviceWorker.getRegistrations();

                    if(nR.length === 0){
                        console.log("service worker NR length registeration", nR.length)
                        window.addEventListener('load', function() {
                            registration = navigator.serviceWorker.register('/spaceSense/web/firebase-messaging-sw.js', { scope: '/' }).then(function(registration){
                                console.log("Service is registered successfully for this application (VM-USER-WEB) !!!",registration);
                                messaging.useServiceWorker(registration)
                            },function(err){
                                console.log('ServiceWorker registration failed: ', err);
                            });
                        })
                    }
                    else
                    {
                        messaging.useServiceWorker(nR[0]);
                    }


                    // if ('serviceWorker' in navigator) {
                    //     window.addEventListener('load', function() {
                    //       navigator.serviceWorker.register('/rb/web/firebase-messaging-sw.js').then(function(registration) {
                    //         // Registration was successful
                    //         console.log('ServiceWorker registration successful with scope: ', registration.scope);
                    //       }, function(err) {
                    //         // registration failed :(
                    //         console.log('ServiceWorker registration failed: ', err);
                    //       });
                    //     });
                    //   }
                    // else{
                    //     console.log("Broswer does not support service worker")
                    // }


                    try {
                        var token = await messaging.getToken();
                        console.log('token do usuário:', token);
                    }
                    catch (err) {
                        console.log("errrrrrrr not able regiser service worker===>", err)
                        throw "failedToObtainToken";
                    }
                    // if (!token) {
                    // }
                    messaging.onTokenRefresh(() => {
                        var refreshedToken = messaging.getToken()
                        console.log("refreshed token", refreshedToken)
                        deleteTokenFromServer(token, serverConfig);
                        sendTokenToServer(refreshedToken, serverConfig);
                    });
                } catch (error) {
                    //TODO errorhandling
                    console.error("error in intialse", error);
                }
                try {
                    console.log("sendingTOken", token)
                    sendTokenToServer(token, serverConfig);
                } catch (error) {
                    //TODO errorhandling
                    // i think to stop app
                    console.error(error);
                }
                navigator.serviceWorker.addEventListener("message", (payload) => {
                    // messaging.onMessage(async function (payload) {
                    console.log("********************PAYLOAD", payload, allfuns);
                    var middlewareLength = allfuns.length;
                    var content = payload.data['firebase-messaging-msg-data'].data.content
                    var quotes = content.replace(/'/g, '"');
                    console.log(quotes)
                    console.log(JSON.parse(quotes))
                    var contentObject = JSON.parse(quotes)              // console.log("from sharang client",typeof content)
                    console.log(contentObject.sender.appId)

                    // var temp = {'content':'test 122323','sender':{'isPlatform':false,'appId':'LTAP1580370719'}

                    // console.log("from sharang client",this.type)
                    // var senderType=payload.data['firebase-messaging-msg-data'].data.co
                    if (isObject(payload) && payload.hasOwnProperty('sender') && contentObject.sender.appId == this.type) {
                        for (var i = 0; i < middlewareLength; i++) {
                            var middlewareObj = allfuns[i];
                            try {
                                var middlewareFunction = middlewareObj['onMessage'];
                                var middlewareParams = middlewareObj['params'];
                                middlewareFunction(payload, middlewareParams);
                            } catch (error) {
                                //TODO errorhandling
                                console.error(error);
                            }
                        }
                    } else {
                        //TODO make notification 
                        const notificationTitle = 'Background Message Title';
                        const notificationOptions = {
                            body: 'Background Message body.',
                            icon: ''
                        };
                        console.log("This is not my modules. May be somether module")
                        // self.registration.showNotification(notificationTitle, notificationOptions);
                        // ORRRR
                        defaultNotifShowFunction(payload);
                        // TODO santosh
                        // defaultNotifShowFunction should 1. show snackbar, 2. Do reply when replied
                    }
                });
                // ONLY IF NOTIFICATION SETTINGS ARE NOT SET IN PAYLOAD
                // messaging.setBackgroundMessageHandler(function (payload) {
                //     console.log('[firebase-messaging-sw.js] Received background message ', payload);
                //     //TODO make notification 
                //     const notificationTitle = 'Background Message Title';
                //     const notificationOptions = {
                //         body: 'Background Message body.',
                //         icon: ''
                //     };
                //     // return defaultNotifShowFunction(... payload);
                //     // ORRRR
                //     return self.registration.showNotification(notificationTitle, notificationOptions);
                // });
                return resolve(true);
            } else {
                return reject("ONLY FCM SUPPORTED");
            }
        });
    }
    registerReceiever(onMessage, params) {
        if (!isFunction(onMessage)) {
            console.log("notAFunction")
            throw new Error("notAFunction");

        }
        var recieverId = generateUUIDMicros("SHRC");
        this.middlewareFunctions.push({ onMessage, params, recieverId });
        return recieverId
    }
    unregisterReciever(recieverId) {
        this.middlewareFunctions.forEach(middlewareFunction => {
            if (middlewareFunction['recieverId'] == recieverId) {
                removeFromArray(this.middlewareFunctions, middlewareFunction);

            }
        });
    }
    stopNotifications() {
        //TODO delete this object and this.middlewareFunctions
        this.middlewareFunctions = []
    }
}
export default fcmClient;
//https://firebase.google.com/docs/cloud-messaging/js/client
//https://firebase.google.com/docs/cloud-messaging/js/receive
//https://medium.com/commencis/web-push-notifications-with-service-workers-cf6ec8005a6c
// var a = 5;
// var that = this.state;
// vmReciever.registerReceiever(function (payload, myOldParams){
//     var trueVar = (this == vmReciever); // true
//     console.log(a, that ); // 5, {<your state>}
// }, {
//     myState: this.state, b: "asdasdsad"
// })