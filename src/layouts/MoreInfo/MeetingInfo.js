import React from "react";
import { Grid, Button } from "@material-ui/core";

import { DatePickerField } from "../../components/InputTextField/DatePickerField";
import { TimePickerField } from "../../components/InputTextField/TimePickerField";
import { DropdownField } from "../../components/InputTextField/DropdownField";
import { InfoStaticText } from "../../components/InputTextField/InfoStaticText";

import MapIcon from "../../assets/map.svg";
import OfficeChairIcon from "../../assets/office-chair.svg";

import "./moreInfo.scss";
import Amenties from "../../components/Amenties/Amenties";

const MeetingInfo = () => {
  const [meetingDate, changeMeetingDate] = React.useState(new Date());
  const [startTime, changeStartTime] = React.useState(new Date());
  const [duration, changeDuration] = React.useState("b");
  return (
    <div>
      <Grid container>
        <Grid className="lean-grid-item" item xs={12}>
          <InfoStaticText inputClass="lean-section-text-header-small">
            Room Info
          </InfoStaticText>
        </Grid>

        <Grid className="lean-grid-item" item xs={12}>
          <InfoStaticText label="Room Name">Meeting Room One</InfoStaticText>
        </Grid>

        <Grid className="lean-grid-item" item xs={12} sm={8}>
          <InfoStaticText label="Room Located In">
            <img src={MapIcon} /> EGL Tech Park / Wipro / 1 Floor / Zone_1
          </InfoStaticText>
        </Grid>

        <Grid className="lean-grid-item" item xs={12} sm={4}>
          <InfoStaticText label="Capacity">
            <img src={OfficeChairIcon} /> 10 Seats
          </InfoStaticText>
        </Grid>

        <Grid className="lean-grid-item" item xs={6} sm={4}>
          <DatePickerField
            value={meetingDate}
            eventHandler={changeMeetingDate}
            label="Meeting Date"
          />
        </Grid>

        <Grid className="lean-grid-item" item xs={6} sm={4}>
          <TimePickerField
            value={startTime}
            eventHandler={changeStartTime}
            label="Start Time"
          />
        </Grid>

        <Grid className="lean-grid-item" item xs={6} sm={4}>
          <DropdownField
            label="Duration"
            value={duration}
            eventHandler={changeDuration}
            menuItems={[
              { value: "a", name: "a" },
              { value: "b", name: "b" }
            ]}
          />
        </Grid>

        <Grid className="lean-grid-item" item xs={12}>
          <InfoStaticText inputClass="lean-section-text-header-small">
            Amenties
          </InfoStaticText>

          <Amenties />
        </Grid>

        <Grid className="lean-grid-item lean-action" item xs={12}>
          <Button variant="contained" className="lean-btn-primary">
            Book
          </Button>
        </Grid>
        {/* <Grid className="lean-grid-item lean-action" item xs={12}>
          <Button variant="contained" className="lean-btn-primary">
            Reschedule
          </Button>
          <Button className="lean-btn-primary">Cancel Meeting</Button>
        </Grid> */}

        {/* <Grid className="lean-grid-item lean-action" item xs={6}>
          <InfoStaticText label="Meeting Start Time and End Time">
            2:00 pm to 3:00 pm
          </InfoStaticText>
        </Grid>

        <Grid className="lean-grid-item lean-action" item xs={6}>
          <InfoStaticText label="No of Occupants">05</InfoStaticText>
        </Grid> */}

        {/* <Grid className="lean-grid-item lean-action" item xs={12}>
          <InfoStaticText label="Cancel Reason">
            Client is not Available
          </InfoStaticText>
        </Grid> */}
      </Grid>
    </div>
  );
};

export default MeetingInfo;
