import React from 'react'
import Image360 from './Image360';
import MeetingInfo from './MeetingInfo';

import { Grid } from '@material-ui/core';

const MoreInfo = () => {
    return (
        <div>
            <Grid container>
                <Grid className="lean-grid-item" item xs={6}>
                    <Image360 />
                </Grid>

                <Grid className="lean-grid-item" item xs={6}>
                    <MeetingInfo />
                </Grid>
            </Grid>
        </div>
    )
}

export default MoreInfo
