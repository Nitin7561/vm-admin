import React from "react";
import UserInfoCard from '../UserAppointmentsModal/UserInfoCard';
import FunctionalModal from '../../components/Modal/FunctionalModal';
import './UserAppointmentsModal.scss'

export default function UserAppointmentsModal() {

    return (
       <>
            <FunctionalModal title="Ramnarayan" status="Approved" isAppointment="true" children={<UserInfoCard/>}/>
       </>
    );
}
