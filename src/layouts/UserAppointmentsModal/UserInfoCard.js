import React from "react";
import './UserAppointmentsModal.scss';
import People from '../../assets/people.png';
import MyAppointments from '../../components/Tables/MyAppointmentsTable';

export default function UserInfoCard() {

    return (
       <>
            <div style={{display:"inline-flex"}}>
                <img src = {People} style={{width:"40%", height:"20%"}}/>
                <div style={{paddingLeft:"20px"}}>
                    <div>
                        <p>Contact Number <span>9686073950</span></p>
                    </div>
                    <div>
                        <p>Email ID <span>killerex62@gmail.com</span></p>
                    </div>
                    <div>
                        <p>Location <span>ABC Company,Bengaluru</span></p>
                    </div>
                    <div>
                        <p>Employee ID<span>M1029263</span></p>
                    </div>
                </div>
            </div>
            {/* <MyAppointments/> */}
            {/* <Grid container>

            </Grid> */}
       </>
    );
}
