import React from "react";
import { Divider, Radio, RadioGroup, FormControl, FormLabel, Button, FormControlLabel, useTheme, useMediaQuery } from '@material-ui/core'
import { MLInputTextField } from "../../components/InputTextField/MLInputTextField.js"
import { connect } from "react-redux";
import { cancelAppt } from "../../redux/actions/cancelAppt";
import { getAllEnums } from "../../redux/actions/getAllEnums";
import { ModalContext } from "../../utils/ModalContext";
import "./cancelAppt.scss"
import EmptySVG from '../../assets/emptyFav.svg';
import FullPageLoading from "../../components/Loaders/FullPageLoading";
import { Grid } from "@material-ui/core";
import {refresher} from '../../redux/actions/refresher';

const CancelAppointment = (props) => {

    console.log("PROPS",props)
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));
    React.useEffect(
        () => {
            console.log("Cancel Appt -> on load")
            props.getTheEnumsNow();
        },[]
    );

    const [description, changeDescription] = React.useState("");
    const [value, setValue] = React.useState('');
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => {console.log(name); setCurrentModal({ name, props })};

    const handleChange = event => {
        setValue(event.target.value);
        console.log(value)
    };

   

    const handleClick = e => {
        if (value == "") {
            alert("Please select any reason")
        }

        console.log("APPT ID==>",props.appointmentId)
        console.log("PPRRRROPPPPS",props)
        console.log("ROOM NAME NOW",props.roomName)

        if(props.roomName)
        {
            openModal({ name: 'MoreInfoModal', props: { 
                "type":"ConfrimRoomCancel","appointmentId":props.appointmentId,"reason":value,"description":description } })
        }
        else
        {
            props.cancelApptNow({
                "appointmentId":props.appointmentId,
                "doRoomCancel":false,
                "reason":props.reason,
                "description":props.description
              })
            //   setCurrentModal(null);
            //   props.refresher("cancelledAppointment");
            openModal({ name: 'CancelledWithoutRoom', props: { }})
        }
        

        // props.cancelApptNow({
        //     "appointmentId":"someId",
	    //     "doRoomCancel":false
        // })

        // openModal({name:'CancelledAppointmentModal',props:{}})
    }


    let { isLoading, data, isError } = props.getAllEnumsReducer;

    console.log("DAAAATTEEEEEEEEEEE=>",data)
    console.log("GET ALL ENUMS",props.getAllEnumsReducer)
    
    return (
        
        <div style={{ position: 'relative', minWidth:fullScreen?'unset':'400px', minHeight:'350px' }}>
        {isLoading ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading> : (
            <>
            {data["cancelApptEnum"].length == 0 ?
            (
                
                <>
                    <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                    <img src={EmptySVG} />
                    <span className="lean-table-empty-screen-text">No Visitors</span>
                    </Grid>
                </> 
            )
                  
            :
            (
                <div className="mainBody">
                    <div className="radioDiv">
                        <FormControl component="fieldset" >
                            <FormLabel component="legend">Reason</FormLabel>
                            <RadioGroup aria-label="position" name="position" value={value} onChange={handleChange} row>
                            {data["cancelApptEnum"].map(( eachItemInArray =>{
                        
                            return(
                                        <FormControlLabel
                                            value= {eachItemInArray}
                                            control={<Radio />}
                                            label= {eachItemInArray}
                                            labelPlacement="end"
                                        />
                                )
                                
                            }))}
                            </RadioGroup>
                        </FormControl>
                    </div>

                    

                    <div className="descriptionDiv" >
                        <MLInputTextField
                            label="Description (optional)"
                            value={description}
                            eventHandler={changeDescription}
                            inputProps={{ maxLength: 150 }}
                        />
                    </div>

                    <div style={{ flex: 1 }}>
                        <Button classes={{ root: "cancel-appointment-button" }} onClick={(e) => handleClick(e)} >
                            Submit
                        </Button>
                    </div>

                </div>
            )
            }
            </>

        )}
        </div>
    );
};

const mapStateToProps = state => {
  const { getAllEnumsReducer,refresherReducer } = state;
  return { getAllEnumsReducer,refresherReducer };
};

export default connect(mapStateToProps,{cancelApptNow:cancelAppt,getTheEnumsNow:getAllEnums,refresher})(CancelAppointment);
