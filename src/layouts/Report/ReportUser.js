import React  from "react";
import { InfoStaticText } from "../../components/InputTextField/InfoStaticText";
import { Divider, Radio, RadioGroup, FormControl, FormLabel, Button, FormControlLabel, Grid, ListItem, List, ListItemAvatar, ListItemText, Avatar, useTheme, useMediaQuery } from '@material-ui/core'
import { MLInputTextField } from "../../components/InputTextField/MLInputTextField.js"
import { connect } from "react-redux";
import { getAllEnums } from "../../redux/actions/getAllEnums";
import { reportUser } from '../../redux/actions/reportUser'
import { ModalContext } from "../../utils/ModalContext";
import EmptySVG from '../../assets/emptyFav.svg';
import FullPageLoading from "../../components/Loaders/FullPageLoading";
import "./report.scss"


const ReportUser = (props) => {


    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

    const [description, changeDescription] = React.useState("");
    const [value, setValue] = React.useState('');
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => {console.log(name); setCurrentModal({ name, props })};

    const handleChange = event => {
        setValue(event.target.value);
        console.log(value)
    };

    React.useEffect(
        () => {
            console.log("Report Appt -> on load")
            props.getTheEnumsNow();
        },[]
    );

    const handleClick = e => {

        if (value == "") {
            alert("please select any reason")
        }

        console.log("VISITOR ID NOW==>",props.visitorId)

        props.reportUserNow({
            "visitorId":props.visitorId,
            "reason":value,
            "description":description
        })

        openModal({name:'ReportedVisitor',props:{}})

    }

    let { isLoading, data, isError } = props.getAllEnumsReducer;


    return (
        <div style={{ position: 'relative', minWidth:fullScreen?'unset':'400px',minHeight:'350px' }}>
        {isLoading ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading> : (
            <>
            {data["reportUserEnum"].length == 0 ?
            (
                
                <>
                    <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                    <img src={EmptySVG} />
                    <span className="lean-table-empty-screen-text">No Visitors</span>
                    </Grid>
                </> 
            )
                  
            :
            (

            <div className="mainBody">
                <div className="radioDiv">
                    <FormControl component="fieldset" >
                        <FormLabel component="legend">Reason</FormLabel>
                        <RadioGroup aria-label="position" name="position" value={value} onChange={handleChange} row>
                            {data["reportUserEnum"].map(( eachItemInArray =>{
                        
                            return(
                                        <FormControlLabel
                                            value= {eachItemInArray}
                                            control={<Radio />}
                                            label= {eachItemInArray}
                                            labelPlacement="end"
                                        />
                                )
                                
                            }))}
                        </RadioGroup>
                    </FormControl>
                </div>

                <div className="descriptionDiv" >
                    <MLInputTextField
                        label="Description (optional)"
                        value={description}
                        eventHandler={changeDescription}
                        inputProps={{ maxLength: 150 }}
                    />
                </div>

                <div style={{ flex: 1 }}>
                    <Button classes={{ root: "report-button" }} onClick={(e) => handleClick(e)} >
                        Submit
                    </Button>
                </div>

            </div>
                )
            }
            </>

        )}
        </div>
    );
};

const mapStateToProps = state => {
  const { reportUserReducer,getAllEnumsReducer } = state;
  return { reportUserReducer,getAllEnumsReducer };
};


export default connect(mapStateToProps, {reportUserNow:reportUser,getTheEnumsNow:getAllEnums})(ReportUser);

