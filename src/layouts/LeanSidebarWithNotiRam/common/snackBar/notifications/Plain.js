import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import Grid from '@material-ui/core/Grid';
import Notification_bell from "./assets/Notification_bell.png";
import { ModalContext } from "../../../../../utils/ModalContext";
import './notificationsSnackBar.scss';




const Plain = ({ state, data,closeToast }) => {

  const [open, setOpen] = React.useState(state);
  const [modal, setModal] = React.useState(null);
  const { setCurrentModal } = React.useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });
  
  console.log('Plain props',state, data);

  let {subject,content,type,redirects,notificationId} = data
  var quotes= content.replace(/'/g, '"');
  var contentObject=JSON.parse(quotes) 
  content = contentObject.content
 
  

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (e, reason) => {
    
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
    closeToast()
  };

 
  const handleReplyClick = (e,id, newValue,type,isReplied) => {  

    console.log("event.target.tagName",e.target.tagName)

    if(e.target.tagName === "svg" || e.target.tagName === "path"  )
    {
      setOpen(false);
      closeToast()
    }
    else if (e.target.tagName === "DIV")
    {
      if(contentObject.sender.appId === "LTAP1580370719")
      {
        var redirectsWeb = JSON.parse(redirects)
        console.log("Redirects for Web ==>",redirectsWeb);
        let {onClickWeb} = redirectsWeb;
        console.log("Redirects for Web Url ==>",onClickWeb);
      
        var splitVisIdandUrl = onClickWeb.split("?");
        var getEncryptId = splitVisIdandUrl[1].split("=");
        
        console.log("Content Sender==>",contentObject.sender)

        console.log("getEncryptId",getEncryptId[1])

        console.log("getEncryptId",JSON.stringify(getEncryptId[1]))


        console.log("Its VM App");
        openModal({name:'VisitorInfoModal',props:{"visitorId":getEncryptId[1]}})
      }
    }
    else if (e.target.tagName === "INPUT")
    {
      e.persist();
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();

    }      
  }

  return (
    <div>
      <Snackbar className='lean-snack-notf-root'
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={() => handleClose()}        
        message={
          <div style={{backgroundColor: '#444f65'}} >            
              <Grid container spacing={1}>
                <Grid item>                  
                  <img  className='lean-snack-notif-logo-bell' src={Notification_bell} />                  
                </Grid>
                <Grid item xs={12} sm container>
                  <Grid item xs container direction="column" spacing={0}>
                    <Grid item xs>                      
                      <span className='lean-snack-notif-header'>
                      {subject}
                      </span>                    
                    </Grid>
                    <Grid item xs>
                    <span className='lean-snack-notif-app'>
                      {content}
                      </span>
                      </Grid>
                    <Grid item>
                                                                           
                    </Grid>
                  </Grid>                  
                </Grid>
                <Grid item>                  
                <IconButton size="small" aria-label="close" color="inherit" onClick={(event) => handleClose(event)}>
              <CloseIcon fontSize="small" />
            </IconButton>                 
                </Grid>
              </Grid>            
          </div>
        }
        onClick={(event) => handleReplyClick(event)}
        // action={
          
        //     <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
        //       <CloseIcon fontSize="small" />
        //     </IconButton>
         
        // }
      />
      {/* <Snackbar open={open} autoHideDuration={6000} 
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          This is a success message!                
        </Alert>
      </Snackbar> */}
    </div>
  );
}

export default Plain
