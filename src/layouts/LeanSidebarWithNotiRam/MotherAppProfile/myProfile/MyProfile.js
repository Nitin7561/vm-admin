import React from "react";
import StatusModal from '../../../../components/Modal/StatusModal';
import FunctionalModal from '../../../../components/Modal/FunctionalModal';
import bridgeUX from '../../../../assets/BridgeUX_SVG.svg'
import myProfile from '../../../../assets/myProfile.svg'
import './myProfile.scss'
import { Grid, CircularProgress } from "@material-ui/core";
import { getUserInfoAction } from '../../LeanSideBarContainer/actions/user.js';
import { connect } from 'react-redux';
// import EmptySVG from '../../../../assets/emptyFav.svg'
import { EmptySVG } from '../../../../assets/emptyFav.svg';
import FullPageLoading from '../../../../components/Loaders/FullPageLoading';
// import { withSnackbar } from 'notistack';
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

const MyProfile = (props) => {

  let { userReducer, enqueueSnackbar, closeSnackbar,getTenantReducer } = props  

  React.useEffect(() => {
    getUserInfoAction();
  }, [])

  let { isLoading: isLoadingUser, data: userData, error: errorUser, isError: isErrorUser } = userReducer
  // let {aboutDescription} = aboutData
  // let myProfileData = mapUserData(userData)

  function mapUserData(data) {
    let myProfileData = [
      // { title: 'Job Title', value: data.title },
      { title: 'Department', value: data.department },
      { title: 'Name', value: data.name },
      { title: 'Employee ID', value: data.sub },
      { title: 'Contact Number', value: data.phone },
      { title: 'Email ID', value: data.email }
    ]

    return myProfileData
  }

  // //--------------- SnackBar Open for displaying errro messages-----------------
  // //-- Handle notistack snackbar
  // const handleToast = (type, message) => {
  //   console.log("handleToast", type, message)
  //   let key = enqueueSnackbar(message, {
  //     preventDuplicate: true,
  //     variant: type,
  //     action: (
  //       <IconButton
  //         aria-label="Close notification"
  //         color="inherit"
  //         onClick={() => closeSnackbar(key)}
  //       >
  //         <CloseIcon fontSize="small" />
  //       </IconButton>
  //     ),
  //   });
  //   // setToast({...toast,state:false,message:'',type:''})
  // };
  // //--------------------------------------------------

  return (
    // <StatusModal icon={icon} heading='My Profile' message='hell0..' buttonMessage='CLick'/>
    <div>

      <div style={{ position: "relative" }}>
        {/* {isLoadingUser ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader" >Loading</FullPageLoading>  */}
        {isLoadingUser ? <CircularProgress size={15}/> 
        // :  isErrorUser ? handleToast('error',errorUser) 
        // :  Object.keys(userData).length == 0 ? handleToast('warning',"Empty profile, try later!")           
        :  <FunctionalModal title='My Profile' children={modalContent(mapUserData(userData),getTenantReducer)} maxWidth='md' handleClose={props.closeModal} />
        }
      </div>
    </div>

  )
}
function modalContent(myProfileData,getTenantReducer) {
  return (
    <div >

      <Grid container >

        <Grid item xs={12} sm={9} >

          {myProfileData.map((data) => {
            return (

              <Grid container style={{ minHeight: '40px' }} >
                <Grid item xs={4} >
                  <div className='lean-myprofile-title'>{data.title}</div>
                </Grid>
                <Grid item xs={8} >
                  <div className='lean-myprofile-description '>{data.value}</div>
                </Grid>
              </Grid>
            )
          })}

        </Grid>

        <Grid item xs={12} sm={3} >
          <Grid container justify={"center"}  >
            <Grid item >
              <img src={myProfile}  ></img>
            </Grid>
            <Grid item >
              <img src={getTenantReducer.data.tenantLogo} height={40} width={150} alt={getTenantReducer.data.tenantName} />
            </Grid>
          </Grid>
        </Grid>

      </Grid>
    </div>
  )
}

const mapStateToProps = (state) => {
  let { userReducer,getTenantReducer } = state;
  return { userReducer,getTenantReducer };
}

export default connect(mapStateToProps, { getUserInfoAction })(MyProfile)