import {
    FETCH_REQ_ALL_NOT, FETCH_FAIL_ALL_NOT, GET_ALL_NOTIFICATIONS,
    FETCH_REQ_SEE_NOT, FETCH_FAIL_SEE_NOT, SEE_NOTIFICATION,
    FETCH_REQ_DEL_NOT, FETCH_FAIL_DEL_NOT, DELETE_NOTIFICATION,
    FETCH_REQ_DEL_NOT_ALL, FETCH_FAIL_DEL_NOT_ALL, DELETE_NOTIFICATION_ALL,
    APPEND_NOTIFICATIONS
} from '../../actions/actionsConfig';

const initialState = {
    isLoading: true,
    data: [],
    count:0,
    error: '',
    isError: false
}
export default function (state = initialState, action) {
    // console.log("APPEND_NOTIFICATIONS ****",state,action)
    switch (action.type) {
        case FETCH_REQ_ALL_NOT:
            return { ...state, isLoading: true };
        case APPEND_NOTIFICATIONS:
            // console.log("APPEND_NOTIFICATIONS --",action)
            return {
                ...state,                
                data: action.payload,
                count: action.count
            };
        case GET_ALL_NOTIFICATIONS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                count: action.count
            };
        case FETCH_FAIL_ALL_NOT:
            return {
                ...state,
                isError: true,
                isLoading: false,
                error: action.payload
            };

        case FETCH_REQ_SEE_NOT:
            return { ...state, isLoading: true };

        case SEE_NOTIFICATION:            
            let storeData = [...state.data]
            var index = storeData.findIndex(r => r.data.id == action.payload[0])
            if (index > -1) {
                storeData[index].seen = true
                return {
                    ...state,
                    isLoading: false,
                    isError: false,
                    data: storeData,
                }
            }
            else
                return {
                    ...state,
                    isLoading: false,
                    isError: false,
                   
                };
        case FETCH_FAIL_SEE_NOT:
            return {
                ...state,
                isError: true,
                isLoading: false,
                error: action.payload
            };

        case FETCH_REQ_DEL_NOT:
            return { ...state, isLoading: true };

        case DELETE_NOTIFICATION:
            // console.log("Testing///->",state) 
            let storeDelData = [...state.data]
            var indexDel = storeDelData.findIndex(r => r.data.notificationId == action.payload[0])
            // console.log("Testing///->",indexDel) 
            if (indexDel > -1) {               
                return { ...state, isLoading: false,  isError: false,  
                    data: state.data.filter(item => item.data.notificationId != action.payload[0]),
                    count: state.count===0?state.count:state.count-1
                }
            }
            else
                return { ...state, isLoading: false, isError: false };
            
        case FETCH_FAIL_DEL_NOT:
            return {
                ...state, isError: true,  isLoading: false,  error: action.payload
            };

            case FETCH_REQ_DEL_NOT_ALL:
                return { ...state, isLoading: true };
    
            case DELETE_NOTIFICATION_ALL:
                return {
                    ...state,
                    isError: false,
                    isLoading: false,
                    data: [],
                    count:0
                };
            case FETCH_FAIL_DEL_NOT_ALL:
                return {
                    ...state, isError: true, isLoading: false,
                    error: action.payload
                };

        default:
            return state;
    }
}

// function markAsRead(dataObject, payload) {
//     console.log("dataObject", dataObject)
//     let arrayIndex = null
//     let { data: storeData } = dataObject
//     storeData.map((store, index) => {

//         let { seen, data } = store
//         let { id: storeId } = data
//         payload.map((seenId) => {
//             if (seenId == storeId) {
//                 console.log("store", seen, seenId, store)
//                 // store.seen =true
//                 // seen=true
//                 arrayIndex = index
//                 console.log("store", index, seen, seenId, store)
//             }
//         })

//     })
//     return arrayIndex

// }