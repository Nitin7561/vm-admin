import { FETCH_REQ_REPLY_NOT, FETCH_FAIL_REPLY_NOT, REPLY_NOTIFICATION } from '../../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_REPLY_NOT:
            return { ...state, isLoading: true };      
        case REPLY_NOTIFICATION:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };     
        case FETCH_FAIL_REPLY_NOT:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}