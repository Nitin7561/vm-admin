import { FETCH_REQUEST_USER, FETCH_FAILURE_USER, GET_USER_INFO } from '../actions/actionsConfig';

const initialState = {
    isLoading: true,
    data: [],
    error: '',
    isError: false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_USER:
            console.log('rerender initialState User', state);
            return { ...state, isLoading: true };   

        case GET_USER_INFO:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };        

        case FETCH_FAILURE_USER:
            // console.log("user error-->",action.payload)
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };

        default:
            return state;
    }
}