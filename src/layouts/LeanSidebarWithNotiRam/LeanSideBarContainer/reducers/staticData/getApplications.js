import { FETCH_REQ_APPL, FETCH_FAIL_APPL, GET_APPLICATIONS } from '../../actions/actionsConfig';

const initialState = {
    isLoading: true,
    data: [],
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_APPL:
            return { ...state, isLoading: true };    
        case GET_APPLICATIONS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };   
        case FETCH_FAIL_APPL:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };   

        default:
            return state;
    }
}