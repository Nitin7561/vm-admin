import { FETCH_REQ_TENANT, FETCH_FAIL_TENANT, GET_TENANT } from '../../actions/actionsConfig';

const initialState = {
  isLoading: false,
  data: {},
  error: '',
  isError: false
}
export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_REQ_TENANT:
      return { ...state, isLoading: true };

      case GET_TENANT:
        return {
          ...state,
          error: '',
          isError: false,
          isLoading: false,
          data: action.payload,        
        };
        
    case FETCH_FAIL_TENANT:
      return {
        ...state,
        isError: true,
        isLoading: false,
        error: action.payload
      };

    default:
      return state;
  }
}