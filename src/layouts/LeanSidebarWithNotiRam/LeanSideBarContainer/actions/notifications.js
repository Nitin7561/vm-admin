import {
  FETCH_REQ_ALL_NOT, FETCH_FAIL_ALL_NOT, GET_ALL_NOTIFICATIONS,
  FETCH_REQ_SEE_NOT, FETCH_FAIL_SEE_NOT, SEE_NOTIFICATION,
  FETCH_REQ_DEL_NOT, FETCH_FAIL_DEL_NOT, DELETE_NOTIFICATION,
  FETCH_REQ_REPLY_NOT, FETCH_FAIL_REPLY_NOT, REPLY_NOTIFICATION,
  FETCH_REQ_DEL_NOT_ALL, FETCH_FAIL_DEL_NOT_ALL, DELETE_NOTIFICATION_ALL,
  APPEND_NOTIFICATIONS
} from './actionsConfig';

import {
  URL_GetAllNotifications, URL_ReplyNotification,
  URL_SeeNotification, URL_DeleteNotification
} from "./config";

//-- Get Room settings
export const getAllNotificationsAction = () => {
  return dispatch => {
    dispatch({ type: FETCH_REQ_ALL_NOT });
    fetch(URL_GetAllNotifications, {
      method: "GET",
    }).then(resp => resp.json())
      .then(data => {
        const { status, data: notificationsData } = data
        if (!status.error) {
          //Badge count loading logic          
          var count = 0
          notificationsData.map((messg) => {
            const { seen, replied, timestamp } = messg
            if (!seen) count++
          })

          dispatch({
            type: GET_ALL_NOTIFICATIONS,
            payload: data.data,
            'count': count
          });
        }
        else {
          dispatch({
            type: FETCH_FAIL_ALL_NOT,
            payload: status.message,
            count: 0
          });
        }
      })
      .catch(error => {
        console.log("error", error)
        dispatch({
          type: FETCH_FAIL_ALL_NOT,
          payload:  error,
          count: 0
        });

      });
  };
};

export const addNotificationAction = (message) => {
  console.log("addNotificationAction before", message)
  
  let { notificationId, subject, content, contentType, priority, icon, type, typespec, redirects, timestamp } = message
  var quotes= content.replace(/'/g, '"'); 
  var contentObject=JSON.parse(quotes) 

  var newMessg = {
    "timestamp": parseInt(timestamp),
    "seen": false,
    "replied": false,
    "data": {
      "notificationId": notificationId,
      "subject": subject,
      // "content": content,
      "content": contentObject,
      "contentType": contentType,
      "priority": priority,
      "icon": icon,
      "type": type,
      "typespec": JSON.parse(typespec),
      "redirects": redirects
    }
  }
  delete message.timestamp
  delete message.__proto__
  console.log("addNotificationAction after****", newMessg)
  return (dispatch, getState) => {

    let { getAllNotificationsReducer } = getState();
    let { count, data } = getAllNotificationsReducer
    console.log("addNotificationAction redux",data)
    if(data.length == 0){
      dispatch({
        type: APPEND_NOTIFICATIONS,
        payload: [newMessg],
        count: count + 1
      });
    }
    else{
      dispatch({
        type: APPEND_NOTIFICATIONS,
        payload: [newMessg, ...data],
        'count': count + 1
      });
    }
    
  }
}

//
export const seeNotificationAction = (reqBody,isBulkSeen) => {
  // if ( !isBulkSeen) 
      // dispatch({ type: FETCH_REQ_SEE_NOT });
    // else
      // dispatch({ type: FETCH_REQ_SEE_NOT_ALL });

      console.log("handleSeenClick",reqBody)
      

      // return

  return dispatch => {
    // dispatch({
    //   type: FETCH_FAIL_SEE_NOT,
    //   payload: "(Something went wrong )=> "
    // });
    dispatch({ type: FETCH_REQ_SEE_NOT });
    fetch(URL_SeeNotification, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(reqBody)
    })
      .then(resp => resp.json())
      .then(data => {
        console.log("reqBody", reqBody.notifications)
        const { status } = data
        if (!status.error) {
          dispatch({
            type: SEE_NOTIFICATION,
            payload: reqBody.notifications // Sending id of the message
            // payload: data.data // Sending id of the message
          });
        }
        else {
          dispatch({
            type: FETCH_FAIL_SEE_NOT,
            payload: status.message
          });
        }
      })
      .catch(error => {
        console.log(error)
        dispatch({
          type: FETCH_FAIL_SEE_NOT,
          payload: "(Something went wrong )=> " + error
        });

      });
  };
};

//
export const replyNotificationAction = (reqBody) => {
  console.log("replyNotificationAction", reqBody)
  // return ''
  return dispatch => {
    dispatch({ type: FETCH_REQ_REPLY_NOT });
    fetch(URL_ReplyNotification, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(reqBody)
    })
      .then(resp => resp.json())
      .then(data => {
        console.log("Data from repy notif",data)
        const { status } = data
        if (!status.error) {
          dispatch({
            type: REPLY_NOTIFICATION,
            payload: data.data
          });
        }
        else {
          dispatch({
            type: FETCH_FAIL_REPLY_NOT,
            payload: status.message
          });
        }
      })
      .catch(error => {
        console.log("Data from repy notif error",error)
        console.log(error)
        dispatch({
          type: FETCH_FAIL_REPLY_NOT,
          payload: error
        });

      });
  };
};

//
export const deleteNotificationAction = (reqBody,isBulkDelete) => {
  return dispatch => {
    
    if ( !isBulkDelete)
      dispatch({ type: FETCH_REQ_DEL_NOT });
    else
      dispatch({ type: FETCH_REQ_DEL_NOT_ALL });

    console.log("deleteNotificationAction",reqBody)

    fetch(URL_DeleteNotification, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(reqBody)
    })
      .then(resp => resp.json())
      .then(data => {
        console.log("deleteNotificationAction",data)
        const { status } = data
        if (!status.error) {
          if (!isBulkDelete)
            dispatch({
              type: DELETE_NOTIFICATION,
              payload: reqBody.notifications
            });
          else
            dispatch({
              type: DELETE_NOTIFICATION_ALL,
              payload: []
            });
        }
        else {          
          if (!isBulkDelete)
            dispatch({
              type: FETCH_FAIL_DEL_NOT,
              payload: status.message
            });
          else
            dispatch({
              type: FETCH_FAIL_DEL_NOT_ALL,
              payload: status.message
            });
        }
      })
      .catch(error => {
        console.log(error)
        if (reqBody.notifications.length == 1)
          dispatch({
            type: FETCH_FAIL_DEL_NOT,
            payload: "(something went wrong) : " + error
          });
        else
          dispatch({
            type: FETCH_FAIL_DEL_NOT_ALL,
            payload: "(something went wrong) : " + error
          });

      });
  };
};
