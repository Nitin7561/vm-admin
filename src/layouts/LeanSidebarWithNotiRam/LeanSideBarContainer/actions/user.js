import { FETCH_REQUEST_USER, FETCH_FAILURE_USER, GET_USER_INFO,GET_USER_INFO_ERROR,
   } from './actionsConfig';

import { URL_GetUserDetails } from "./config";

//-- Get Pin 
export const getUserInfoAction = () => {
  return dispatch => {
    dispatch({ type: FETCH_REQUEST_USER });
    console.log("getUserInfoAction",URL_GetUserDetails)
    fetch( URL_GetUserDetails, { method: "GET",      
    // fetch( 'https://cs.leantron.dev/spaceSense/auth/getUser', { 
      method: "GET", 
      credentials: 'include'     
    }).then(async function(resp){
      // console.log("lookie", await resp.text());
      return resp.json()
    })
      .then(data => {        
        console.log("getUserInfoAction",data)
        const {  status } = data     
        if (!status.error) { 
          // console.log("GET_USER_INFO->", data);          
          dispatch({
            type: GET_USER_INFO,
            payload: data.data
          });
        }
        else{       
          dispatch({
            type: GET_USER_INFO_ERROR,
            payload: status.message
          });
        }
      })
      .catch(error =>{
        console.log("getUserInfoAction",error)
        dispatch({
        type: FETCH_FAILURE_USER,
        payload: error.message
      });
      
    });
  };
};
