import React from 'react'
// import Settings from "../../assets/images/Settings.png";
// import Notification from "../../assets/images/Notification.png";
import Notification_image from "../common/snackBar/notifications/assets/notification_image.svg";
import './notificationPopUp.scss'
import {

  Avatar,
  
} from "@material-ui/core";

export default function NotificationVisitorApproval(props){

  // const { seen, replied, timestamp } = props.value  
  // const { notificationId, subject, content, type, redirects, icon, typespec } = props.value.data  
  // console.log("verify123",props)
  const { seen } = props.value  
  let { notificationId, content, type, typespec } = props.value.data
  // var jsonContent = JSON.parse(content)
  console.log("verify123 **",content)
  // var jsonContent = JSON.parse(content)
  console.log("verify123 ***",content.content)
  content = content.content
  // content = jsonContent.content
  // const  notificationId='123', content='hi', type='confirm', typespec ={}
  // console.log("verify123 **",{ notificationId, content, type, typespec })

  let {yesText,noText} = typespec  
  // console.log("verify123",typespec,yesText,noText)
  
  
  //-- Message seen?
    const  isMessgSeen =  seen ? {} : {backgroundColor:"#ffe5e5"}

    const clickspan1 = (event) => {    
      // if(!seen)
        props.seenHandler(event,notificationId)
    }

    return (
      <div className="lean-notification-box-root" style={isMessgSeen}>
        <div className='lean-notification-box-flex'>
          <div className='div-1'>
            <div className="div-logo">
              <Avatar></Avatar>
              {/* <img className="logo-visitor" alt='loading icon..'/> */}
            </div>  
          </div>
          <div className='div-2'>
            <div className='message'>
            {/* <div className="div-cgp-header"> */}
              <span className="message-content" onClick={clickspan1}>{'content'}</span>
            {/* </div> */}
            </div>
            <div className='input'>
              <button className='input-button button-1-text' onClick={(event) => props.replyHandler(event,notificationId,true,type)}> {yesText } </button>
              <button className='input-button-2 button-2-text' onClick={(event) => props.replyHandler(event,notificationId,false,type)}>{ noText }</button>
              {/* <span className='span-time'>{props.time}</span> */}
            </div>            
          </div>  

          <div className='div-3'>
            <span className='span-time-2'>{props.time}</span>
          </div>        
        </div>        

      </div>
      
    );
  }