import {
    FETCH_REQ_ALL_NOT, FETCH_FAIL_ALL_NOT, GET_ALL_NOTIFICATIONS,
    FETCH_REQ_SEE_NOT, FETCH_FAIL_SEE_NOT, SEE_NOTIFICATION,
    FETCH_REQ_DEL_NOT, FETCH_FAIL_DEL_NOT, DELETE_NOTIFICATION,
    FETCH_REQ_DEL_NOT_ALL, FETCH_FAIL_DEL_NOT_ALL, DELETE_NOTIFICATION_ALL, DELETE_NOTIFICATION_STATUS,
    APPEND_NOTIFICATIONS, UPDATE_NOTIFICAION_REPLIED,
    REPLY_NOTIFICATION, REPLY_NOTIFICATION_STATUS
} from '../../actions/actionsConfig';
const initialState = {
    isLoading: true,
    data: [],
    count: 0,
    error: '',
    isError: false,
    deleteStatus: '',
    replyStatus: ''
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_ALL_NOT:
            return { ...state, isLoading: true };
        case APPEND_NOTIFICATIONS:
            return {
                ...state,
                data: action.payload,
                count: action.count
            };
        case GET_ALL_NOTIFICATIONS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
                count: action.count
            };
        case FETCH_FAIL_ALL_NOT:
            return {
                ...state,
                isError: true,
                isLoading: false,
                error: action.payload
            };
        case FETCH_REQ_SEE_NOT:
            return { ...state, isLoading: true };
        case SEE_NOTIFICATION:
            let storeData = [...state.data], tempMap = new Map(), seenCount=0
            action.payload.map((item) => {
                tempMap.set(item, 0)
            })
            storeData.map((messg, index) => {
                if (tempMap.has(messg.data.notificationId)) {
                    storeData[index].seen = true
                    // storeData[index].replied = true
                    seenCount++
                }
            })
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: storeData,
                count: state.count>seenCount ? state.count - seenCount : 0
            }
        case UPDATE_NOTIFICAION_REPLIED:
            let storeDataReplied = [...state.data] , repliedCount =0
            var index = storeDataReplied.findIndex(r => r.data.notificationId == action.payload.notificationId)
            // console.log("Testing///->",storeDataReplied[index]) 
            if (index > -1) {
                storeDataReplied[index].replied = true
                storeDataReplied[index]['reply'] = action.payload.reply               
                storeDataReplied[index]['seen'] = true
                repliedCount++
            }            
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: storeDataReplied, replyStatus:'Thanks for your reply',
                // count: storeDataReplied[index].seen == false ? state.count - 1 : state.count
                count:  state.count - repliedCount
            }
        case FETCH_FAIL_SEE_NOT:
            return {
                ...state,
                isError: true,
                isLoading: false,
                error: action.payload
            };
        case FETCH_REQ_DEL_NOT:
            return { ...state, isLoading: true };
        case DELETE_NOTIFICATION:
            let storeDelData = [...state.data]
            var indexDel = storeDelData.findIndex(r => r.data.notificationId == action.payload[0])
            console.log("Testing/// DELETE_NOTIFICATION->",storeDelData[indexDel]) 
            if (indexDel > -1) {
                return {
                    ...state, isLoading: false, isError: false, deleteStatus:"Message deleted",
                    data: state.data.filter(item => item.data.notificationId != action.payload[0]),
                    count: storeDelData[indexDel].seen == false ? state.count - 1 : state.count
                }
            }
            else
                return { ...state, isLoading: false, isError: false };
        case FETCH_FAIL_DEL_NOT:
            return {
                ...state, isError: true, isLoading: false, error: action.payload
            };
        case FETCH_REQ_DEL_NOT_ALL:
            return { ...state, isLoading: true };
        case DELETE_NOTIFICATION_ALL:
            return {
                ...state, deleteStatus:"All Message deleted",
                isError: false,
                isLoading: false,
                data: [],
                count: 0
            };
        case FETCH_FAIL_DEL_NOT_ALL:
            return {
                ...state, isError: true, isLoading: false,
                error: action.payload
            };
        case DELETE_NOTIFICATION_STATUS:
            return{...state, deleteStatus:''}
        case REPLY_NOTIFICATION:
            return{...state, replyStatus:action.payload}
        case REPLY_NOTIFICATION_STATUS:
            return{...state, replyStatus:''}
        default:
            return state;
    }
}