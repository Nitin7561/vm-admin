import { FETCH_REQ_SUPPORT_REQUEST, FETCH_FAIL_SUPPORT_REQUEST, SUBMIT_SUPPORT_REQUEST } from '../../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_SUPPORT_REQUEST:
            return { ...state, isLoading: true };    
        case SUBMIT_SUPPORT_REQUEST:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };    
        case FETCH_FAIL_SUPPORT_REQUEST:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };     

        default:
            return state;
    }
}