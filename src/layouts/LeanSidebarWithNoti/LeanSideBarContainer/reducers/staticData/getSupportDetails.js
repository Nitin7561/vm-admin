import { FETCH_REQ_SUPPORT, FETCH_FAIL_SUPPORT, GET_SUPPORT_DETAILS } from '../../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: {
        'phone':[],
        'email':[],
        'supportMessage':''
    },
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_SUPPORT:
            return { ...state, isLoading: true };     
        case GET_SUPPORT_DETAILS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };    
        case FETCH_FAIL_SUPPORT:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };     

        default:
            return state;
    }
}