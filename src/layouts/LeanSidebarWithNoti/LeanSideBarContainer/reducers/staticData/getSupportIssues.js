import { FETCH_REQ_SUPPORT_ISSUE, FETCH_FAIL_SUPPORT_ISSUE, GET_SUPPORT_ISSUE } from '../../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_SUPPORT_ISSUE:
            return { ...state, isLoading: true };      
        case GET_SUPPORT_ISSUE:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };     
        case FETCH_FAIL_SUPPORT_ISSUE:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}