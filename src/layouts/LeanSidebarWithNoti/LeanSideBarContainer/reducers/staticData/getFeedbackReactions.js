import { FETCH_REQ_FEEDBACK, FETCH_FAIL_FEEDBACK, GET_FEEDBACK_REACTIONS } from '../../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQ_FEEDBACK:
            return { ...state, isLoading: true };     
        case GET_FEEDBACK_REACTIONS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };    
        case FETCH_FAIL_FEEDBACK:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}