export const URL_GetTenantFromUserName = 'https://cs.leantron.dev/global/getTenantFromUsername?username='
export const URL_GetTenant = 'https://cs.leantron.dev/spaceSense/fe/getTenant'

export const URL_CheckTandC = 'https://cs.leantron.dev/spaceSense/fe/checkTandC'
export const URL_AcceptTandC = 'https://cs.leantron.dev/spaceSense/fe/updateTandC'
export const URL_GetTermsAndConditions = 'https://cs.leantron.dev/spaceSense/fe/getTandC'

export const URL_GetApplications = 'https://cs.leantron.dev/spaceSense/fe/getApplications'
export const URL_GetUserDetails = 'https://cs.leantron.dev/spaceSense/auth/getUser'

// export const URL_GetUpcomingEvents = 'https://cs.leantron.dev/spaceSense/fe/getWSMUpcomingEvents'
// export const URL_GetRoomBarChartData = 'https://cs.leantron.dev/spaceSense/fe/roomsUsageStatistics?fromTime='
// export const URL_GetRoomDonutChartData = 'https://cs.leantron.dev/spaceSense/fe/roomsUsageAnalytics?startTime='
// export const URL_GetRoomUsageBoxData = 'https://cs.leantron.dev/spaceSense/fe/getUserRoomUtilizationData?startTime='
// export const URL_GetVMLineChartData = 'https://cs.leantron.dev/spaceSense/fe/visitorSummaryStatistics?from='


// export const URL_GetRBAncestorLocation = 'https://cs.leantron.dev/spaceSense/be/getAncestorLocation'



export const URL_GetAboutInfo = 'https://cs.leantron.dev/spaceSense/fe/getAboutInfo'
export const URL_GetFAQ = 'https://cs.leantron.dev/spaceSense/fe/getHelpArticles?application='

// export const URL_GetSupportIssuesList = 'https://cs.leantron.dev/spaceSense/fe/getSupportIssues?key='
// export const URL_GetSupportDetailsList = 'https://cs.leantron.dev/spaceSense/fe/getSupportDetails?key='
// export const URL_SubmitSupportForm = 'https://cs.leantron.dev/spaceSense/fe/submitSupportRequest'
// export const URL_SubmitFeedbackReaction = 'https://cs.leantron.dev/spaceSense/fe/submitFeedback'

export const URL_GetUserNotificationSettings = 'https://cs.leantron.dev/spaceSense/fe/getDefaultNotificationSettings'
export const URL_UpdateNotificationSettings = 'https://cs.leantron.dev/spaceSense/fe/changeNotificationSetting?'
export const URL_GetDefaultUserRoomSettings = 'https://cs.leantron.dev/spaceSense/fe/getUserRoomSettings'
export const URL_UpdateUserRoomSettings = 'https://cs.leantron.dev/spaceSense/fe/updateUserRoomSettings'
export const URL_RegeneratePIN = 'https://cs.leantron.dev/spaceSense/auth/regenPin'

export const URL_GetAllNotifications = 'https://cs.leantron.dev/spaceSense/sharang/getAllNotifications'
export const URL_ReplyNotification = 'https://cs.leantron.dev/spaceSense/sharang/replyNotification'
export const URL_SeeNotification = 'https://cs.leantron.dev/spaceSense/sharang/seeNotification'
export const URL_DeleteNotification = 'https://cs.leantron.dev/spaceSense/sharang/deleteNotification'
