import {
  // FETCH_REQ_SUPPORT, FETCH_FAIL_SUPPORT, GET_SUPPORT_DETAILS,
  // FETCH_REQ_TC, FETCH_FAIL_TC, GET_TERMS_CONDITION,
  FETCH_REQ_APPL, FETCH_FAIL_APPL, GET_APPLICATIONS,
  // FETCH_REQ_ABOUT, FETCH_FAIL_ABOUT, GET_ABOUT_US,
  // FETCH_REQ_SUPPORT_ISSUE, FETCH_FAIL_SUPPORT_ISSUE, GET_SUPPORT_ISSUE,
  // FETCH_REQ_FEEDBACK, FETCH_FAIL_FEEDBACK, GET_FEEDBACK_REACTIONS,
  // FETCH_REQ_FEEDBACK_SUBMIT, FETCH_FAIL_FEEDBACK_SUBMIT, SUBMIT_FEEDBACK,
  // FETCH_REQ_FAQ, FETCH_FAIL_FAQ, GET_FAQ,
  // FETCH_REQ_SUPPORT_REQUEST, FETCH_FAIL_SUPPORT_REQUEST, SUBMIT_SUPPORT_REQUEST,
  FETCH_REQ_TENANT, FETCH_FAIL_TENANT, GET_TENANT
} from './actionsConfig';
import {
  URL_GetApplications,
  // URL_GetAboutInfo, URL_GetFAQ, URL_GetSupportIssuesList,
  // URL_GetSupportDetailsList, URL_SubmitSupportForm, URL_SubmitFeedbackReaction,
  URL_GetTenant
} from "./config";

//-- Get Terms & Condition
// export const getTermsConditionAction = () => {
//   return dispatch => {
//     dispatch({ type: FETCH_REQ_TC });
//     fetch("https://fe.leantron.dev:3001/getTandC", {
//       method: "GET",      
//     })
//       .then(resp => resp.json())
//       .then(data => {
//         // console.log("GET_TENANT->", data);
//         // dispatch({
//         //   type: GET_TERMS_CONDITION,
//         //   payload: data.data
//         // });
//         // console.log("TESTING*****->", data);
//         if (data.status === 'success') {          
//           dispatch({
//             type: GET_TERMS_CONDITION,
//             payload: data.data
//           });
//         }
//         else {
//           dispatch({
//             type: FETCH_FAIL_TC,
//             payload: data.message 
//           });
//         }      
//       })
//       .catch(error =>{
//         console.log(error)
//         dispatch({
//         type: FETCH_FAIL_TC,
//         payload: error.message
//       });

//     });
//   };
// };

//-- Get Side-Nav Bar applications to display
export const getApplicationsAction = () => {
  return dispatch => {
    dispatch({ type: FETCH_REQ_APPL });
    fetch(URL_GetApplications, {
      method: "GET",
    }).then(resp => resp.json())
      .then(data => {
        const { status } = data
        if (!status.error) {
          dispatch({
            type: GET_APPLICATIONS,
            payload: data.data
          });
        }
        else {
          dispatch({
            type: FETCH_FAIL_APPL,
            payload: status.message
          });
        }
      })
      .catch(error => {
        console.log(error)
        dispatch({
          type: FETCH_FAIL_APPL,
          payload: error
        });

      });
  };
};

//-- Get About Us data
// export const getAboutUsAction = () => {
//   return dispatch => {
//     dispatch({ type: FETCH_REQ_ABOUT });
//     fetch(URL_GetAboutInfo, {
//       method: "GET",
//     }).then(resp => resp.json())
//       .then(data => {
//         const { status } = data
//         if (!status.error) {
//           dispatch({
//             type: GET_ABOUT_US,
//             payload: data.data.aboutDescription
//           });
//         }
//         else {
//           dispatch({
//             type: FETCH_FAIL_ABOUT,
//             payload: status.message
//           });
//         }
//       })
//       .catch(error => {
//         console.log(error)
//         dispatch({
//           type: FETCH_FAIL_ABOUT,
//           payload: error.message
//         });

//       });
//   };
// };

// //-- Get list of  Support Request
// export const getSupportIssuesAction = (appName) => {
//   return dispatch => {
//     dispatch({ type: FETCH_REQ_SUPPORT_ISSUE });
//     fetch(URL_GetSupportIssuesList + appName, {
//       method: "GET",
//     }).then(resp => resp.json())
//       .then(data => {
//         const { status } = data
//         if (!status.error) {
//           console.log("Support Issues app data", data.data.issues)
//           dispatch({
//             type: GET_SUPPORT_ISSUE,
//             payload: data.data.issues
//           });
//         }
//         else {
//           dispatch({
//             type: FETCH_FAIL_SUPPORT_ISSUE,
//             payload: status.message
//           });
//         }
//       })
//       .catch(error => {
//         console.log(error)
//         dispatch({
//           type: FETCH_FAIL_SUPPORT_ISSUE,
//           payload: error
//         });

//       });
//   };
// };

// //-- Get list of  Support Details
// export const getSupportDetailsAction = (appName) => {
//   return dispatch => {
//     dispatch({ type: FETCH_REQ_SUPPORT });
//     fetch(URL_GetSupportDetailsList + appName, {
//       method: "GET",
//     }).then(resp => resp.json())
//       .then(data => {
//         const { status } = data
//         if (!status.error) {
//           dispatch({
//             type: GET_SUPPORT_DETAILS,
//             payload: data.data
//           });
//         }
//         else {
//           dispatch({
//             type: FETCH_FAIL_SUPPORT,
//             payload: status.message
//           });
//         }
//       })
//       .catch(error => {
//         console.log(error)
//         dispatch({
//           type: FETCH_FAIL_SUPPORT,
//           payload: error
//         });

//       });
//   };
// };

// //-- Get list of  Support Issues
// export const getFeedbackReactionsAction = () => {
//   return dispatch => {
//     dispatch({ type: FETCH_REQ_FEEDBACK });
//     fetch("https://fe.leantron.dev:3001/getFeedbackReactions", {
//       method: "GET",
//     })
//       .then(resp => resp.json())
//       .then(data => {
//         // console.log("GET_TENANT->", data);
//         // dispatch({
//         //   type: GET_FEEDBACK_REACTIONS,
//         //   payload: data.data
//         // });
//         if (data.status === 'success') {
//           let { favouriteLocation, amenityInfo, roomPreferences } = data.result;
//           dispatch({
//             type: GET_FEEDBACK_REACTIONS,
//             payload: data.data
//           });
//         }
//         else {
//           dispatch({
//             type: FETCH_FAIL_FEEDBACK,
//             payload: data.message
//           });
//         }
//       })
//       .catch(error => {
//         console.log(error)
//         dispatch({
//           type: FETCH_FAIL_FEEDBACK,
//           payload: error
//         });

//       });
//   };
// };

// //-- Submit  Support Issues
// export const submitSupportRequestAction = (reqBody) => {
//   return dispatch => {
//     dispatch({ type: FETCH_REQ_SUPPORT_REQUEST });
//     fetch(URL_SubmitSupportForm, {
//       method: "POST",
//       headers: { "Content-Type": "application/json" },
//       body: JSON.stringify(reqBody)
//     })
//       .then(resp => resp.json())
//       .then(data => {
//         const { status } = data
//         if (!status.error) {
//           dispatch({
//             type: SUBMIT_SUPPORT_REQUEST,
//             payload: data.data
//           });
//         }
//         else {
//           dispatch({
//             type: FETCH_FAIL_SUPPORT_REQUEST,
//             payload: status.message
//           });
//         }
//       })
//       .catch(error => {
//         console.log(error)
//         dispatch({
//           type: FETCH_FAIL_SUPPORT_REQUEST,
//           payload: error
//         });

//       });
//   };
// };

// //-- Submit  Feedback
// export const submitFeedbackAction = (reqBody) => {
//   return dispatch => {
//     console.log("submitFeedbackAction", reqBody)
//     dispatch({ type: FETCH_REQ_FEEDBACK_SUBMIT });
//     fetch(URL_SubmitFeedbackReaction, {
//       method: "POST",
//       headers: { "Content-Type": "application/json" },
//       body: JSON.stringify(reqBody)
//     })
//       .then(resp => resp.json())
//       .then(data => {
//         const { status } = data
//         console.log("submitFeedbackAction", data)
//         if (!status.error) {
//           dispatch({
//             type: SUBMIT_FEEDBACK,
//             payload: data.data
//           });
//         }
//         else {
//           dispatch({
//             type: FETCH_FAIL_FEEDBACK_SUBMIT,
//             payload: status.message
//           });
//         }
//       })
//       .catch(error => {
//         console.log("submitFeedbackAction", error)
//         dispatch({
//           type: FETCH_FAIL_FEEDBACK_SUBMIT,
//           payload: error
//         });

//       });
//   };
// };

// //-- Get list of  Help & FAQ
// export const getFAQAction = (appName) => {
//   // console.log("FAQ Testing",appName)
//   return dispatch => {
//     dispatch({ type: FETCH_REQ_FAQ });
//     fetch(URL_GetFAQ + appName, {
//       method: "GET",
//     }).then(resp => resp.json())
//       .then(data => {
//         // console.log("FAQ Testing data",data)
//         const { status } = data
//         if (!status.error) {
//           dispatch({
//             type: GET_FAQ,
//             payload: data.data
//           });
//         }
//         else {
//           dispatch({
//             type: FETCH_FAIL_FAQ,
//             payload: status.message
//           });
//         }
//       })
//       .catch(error => {
//         // console.log("FAQ Testing error",error)
//         dispatch({
//           type: FETCH_FAIL_FAQ,
//           payload: error
//         });

//       });
//   };
// };


export const getTenantAction = () => {
  return dispatch => {
    dispatch({ type: FETCH_REQ_TENANT });

    fetch(URL_GetTenant, {
      method: "GET",
    }).then(resp => resp.json())
      .then(data => {
        console.log("getTenantReducer suc", data)
        const { status } = data
        if (!status.error) {
          dispatch({
            type: GET_TENANT,
            payload: data.data
          });
        }
        else {
          console.log("About else")
          dispatch({
            type: FETCH_FAIL_TENANT,
            payload: status.message
          });
        }
      })
      .catch(error => {
        console.log("getTenantReducer err", error)
        dispatch({
          type: FETCH_FAIL_TENANT,
          payload: error
        });

      });
  };
};
