import { homePageId, rbPageId, vmPageId, settingsPageId } from "./constants"

export const whichAppIsThis = (location) => {
    if (location.includes('vm')) {
        return vmPageId
    }

    if (location.includes('spaceSense')) {
        if (location.includes('settings')) {
            return settingsPageId
        } else if (location.includes('home')) {
            return homePageId
        }
        return homePageId
    }

    if (location.includes('rb')) {

        return rbPageId
    }

}