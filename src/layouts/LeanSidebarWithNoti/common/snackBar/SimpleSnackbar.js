import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import { makeStyles } from '@material-ui/core/styles';
import Notification_bell from "../../../assets/images/notification_image.svg";
import './snackbar.scss'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: 500,
    backgroundColor: '#444f65',    
  },
  image: {
    width: 128,
    height: 128,
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
}));

const SimpleSnackbar = ({ message }) => {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  const handleReplyClick = (e,id, newValue,type,isReplied) => {        
    console.log('handleReplyClick ==>',id,newValue,type,isReplied)    
    const reqBody = {
      "notificationId":  id ,
      'reply' : newValue,
      'replyType' : type
    }
    // if(!isReplied) ''
      // replyNotificationAction(reqBody)
  }

  return (
    <div>
      <Button onClick={handleClick}>Open simple snackbar</Button>
      <Snackbar className='lean-snack-root3'
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={600000}
        onClose={handleClose}        
        message={
          <div style={{backgroundColor: '#444f65'}} >            
              <Grid container spacing={1}>
                <Grid item>                  
                  <img  src={Notification_bell} />                  
                </Grid>
                <Grid item xs={12} sm container>
                  <Grid item xs container direction="column" spacing={0}>
                    <Grid item xs>                      
                      <span className='lean-snack-notification-header'>
                      Joshua D need approval for meeting
                      </span>                    
                    </Grid>
                    <Grid item xs>
                    <span className='lean-snack-notification-app'>
                      Visitor Management
                      </span>
                      </Grid>
                    <Grid item>
                    <button className='lean-snack-notification-button-1-text' onClick={(event) => handleReplyClick(event)}> Approve </button>
                    <button className='lean-snack-notification-button-2-text' onClick={(event) => handleReplyClick(event)}>Decline</button>                   
                    <button className='lean-snack-notification-button-2-text' onClick={(event) => handleReplyClick(event)}>Decline</button>                   
                    <button className='lean-snack-notification-button-2-text' onClick={(event) => handleReplyClick(event)}>Decline</button>                   
                    </Grid>
                  </Grid>                  
                </Grid>
                <Grid item>                  
                <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
              <CloseIcon fontSize="small" />
            </IconButton>                 
                </Grid>
              </Grid>            
          </div>
        }
        // action={
          
        //     <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
        //       <CloseIcon fontSize="small" />
        //     </IconButton>
         
        // }
      />
      {/* <Snackbar open={open} autoHideDuration={6000} 
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          This is a success message!                
        </Alert>
      </Snackbar> */}
    </div>
  );
}

export default SimpleSnackbar
