import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import Grid from '@material-ui/core/Grid';
import Notification_bell from "./assets/notification_image.svg";
import './notificationsSnackBar.scss'
import { replyNotificationAction} from '../../../LeanSideBarContainer/actions/notifications';
import { connect } from 'react-redux';
import {
  Avatar
} from "@material-ui/core";
import { rbPageId, vmPageId, homePageId} from '../../../constants'

const ConfirmToast = ({ state, data,closeToast,replyNotificationReducer,replyNotificationAction }) => {
  
  // console.log('typespec Confirm props',state, data)
  let {subject,content,type,typespec,notificationId} = data
  typespec = JSON.parse(typespec)
  var quotes= content.replace(/'/g, '"'); 
  var contentObject=JSON.parse(quotes) 
  content = contentObject.content

  setTimeout(closeToast,10000)

  let {yesText,noText} = typespec
  // console.log("typespec",typespec,yesText,noText)
  
  const [openNot, setOpenNot] = React.useState(state);

  //--------------- SnackBar Open for displaying error messages -----------------
  const [open, setOpen] = React.useState(true);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
  // function loadSnackBar(message) {
  //   return (
  //     <Snackbar open={open} autoHideDuration={6000}
  //       anchorOrigin={{
  //         vertical: 'bottom',
  //         horizontal: 'left',
  //       }}
  //       onClose={handleClose}>
  //       <Alert onClose={handleClose} severity="error">
  //         {message}
  //       </Alert>
  //     </Snackbar>
  //   )
  // }
  //--------------------------------------------------
  const handleClick = () => {
    setOpen(true);
  };

  const handleCloseNot = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenNot(false);
    closeToast()
  };

 
  const handleReplyClick = (e,newValue) => {            
      console.log('handleReplyClick ==>',notificationId,newValue,type)    
      const reqBody = {
        "notificationId":  notificationId ,
        'reply' : newValue,
        'replyType' : type
      }
        replyNotificationAction(reqBody)
        setOpenNot(false);
        closeToast()    
  }

  return (
    <div>
      {/* { errorReplyNotif ? loadSnackBar('Error replying to notification..') : null} */}
      <Button onClick={handleClick}>Open simple snackbar</Button>
      <Snackbar className='lean-snack-notf-root'
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openNot}
        autoHideDuration={6000}
        onClose={handleClose}        
        message={
          <div style={{backgroundColor: '#444f65'}} >            
              <Grid container spacing={1}>
                <Grid item>                  
                  <Avatar/>     
                </Grid>
                <Grid item xs={12} sm container>
                  <Grid item xs container direction="column" spacing={0}>
                    <Grid item xs>                      
                      <span className='lean-snack-notif-header'>
                      {subject}
                      </span>                    
                    </Grid>
                    <Grid item xs>
                    <span className='lean-snack-notif-app'>
                      {content}
                      </span>
                      </Grid>
                    <Grid item>                                       
                    <button className='lean-snack-notif-button-1-text' onClick={(event) => handleReplyClick(event,true)}> {yesText} </button>
                    <button className='lean-snack-notif-button-2-text' onClick={(event) => handleReplyClick(event,false)}> {noText} </button>                                                        
                    </Grid>
                  </Grid>                  
                </Grid>
                <Grid item>                  
                <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseNot}>
              <CloseIcon fontSize="small" />
            </IconButton>                 
                </Grid>
              </Grid>            
          </div>
        }        
      />

    </div>
  );
}


const mapStateToProps = (state) => {
  let { replyNotificationReducer } = state;
  return { replyNotificationReducer };
}


export default connect(mapStateToProps, {
   replyNotificationAction
})(ConfirmToast)
