import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import Grid from '@material-ui/core/Grid';
import Notification_bell from "./assets/Notification_room.png";
import './notificationsSnackBar.scss'
import { replyNotificationAction } from '../../../LeanSideBarContainer/actions/notifications';
import { connect } from 'react-redux';
import TextField from "@material-ui/core/TextField";
import send from "./assets/send.svg";
// import { sendMessageJustOnChatCloud } from '../../../../../redux/actions/chatAction/getAllMessage';
import {fireDrawer} from '../../../../../redux/actions/chatAction/fireDrawer';
import { getDetailForVisitor } from '../../../../../redux/actions/chatAction/getDetailForVisitor'




const ChatToast = ({ state, data, closeToast, replyNotificationReducer, replyNotificationAction,fireDrawer,getDetailForVisitor,fireDrawerReducer,getDetailForVisitorReducer }) => {

  // console.log('Chat props', state, data)
  let { subject, content, type, typespec, notificationId } = data
  var quotes = content.replace(/'/g, '"');
  var contentObject = JSON.parse(quotes)
  content = contentObject.content

  setTimeout(closeToast,20000)

  var newContent = content.split("$$");

  let { loading: isLoadingGetVisitorDetails, data: dataGetVisitorDetails, error: isErrorGetVisitorDetails } = getDetailForVisitorReducer;

  const [openNot, setOpenNot] = React.useState(state);

  //--------------- SnackBar Open for displaying errro messages-----------------
  const [open, setOpen] = React.useState(true);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }


  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };


  // function loadSnackBar(message) {
  //   return (
  //     <Snackbar open={setOpenNot} autoHideDuration={6000}
  //       anchorOrigin={{
  //         vertical: 'bottom',
  //         horizontal: 'left',
  //       }}
  //       onClose={handleClose}>
  //       <Alert onClose={handleClose} severity="error">
  //         {/* This is a success message!                 */}
  //         {message}``
  //       </Alert>
  //     </Snackbar>
  //   )
  // }

  const [value, setValue] = React.useState('');

  const handleChange = event => {
    setValue(event.target.value);
  };

  const handleClick = () => {
    setOpen(true);
  };

  const handleCloseNot = (e) => {
    console.log("event.target.tagName", e.target.tagName)
    if (e.target.tagName === "svg" || e.target.tagName === "path") {
      setOpen(false);
      closeToast()
    }
    else {
      e.persist();
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
    }
  };

  const handleKeyPress = event => {
    if (event.key == 'Enter') {
      handleReplyClick(event, value, notificationId, type);
    }
  };


  const handleReplyClick = (e, newValue, notificationId, type) => {

    console.log("event.target.tagName in reply click", e.target.tagName)
    if (contentObject.sender.appId === "LTAP1580370719") {
      console.log('handleReplyClick ==>', notificationId, newValue, type)

      const reqBody = {
        "notificationId": notificationId,
        'reply': newValue,
        'replyType': type
      }

      replyNotificationAction(reqBody)
      setOpenNot(false);
      closeToast()
    }

  }

  const handleRedirectDrawer = (e,newValue) => {

    console.log("event.target.tagName on whole click",e.target.tagName)

    if(contentObject.sender.appId === "LTAP1580370719")
    {
      if (e.target.tagName === "DIV")
      {
          console.log("Clicking on the chat notification to open particular chat drawer");
          getDetailForVisitor({
            "visitorSub": newContent[1]
          });
          if(fireDrawerReducer.fireDrawer === true)
          {
            fireDrawer("fireDrawer");
            fireDrawer("fireDrawer");
            setOpen(false);
            closeToast()
          }
          else
          {
            fireDrawer("fireDrawer");
            setOpen(false);
            closeToast()
          }
          
      }
      else
      {
        e.persist();
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
      }
    }
  }


  return (
    <div>
      {/* <Button onClick={handleClick}>Open simple snackbar</Button> */}
      <Snackbar className='lean-snack-notf-root'
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={openNot}
        autoHideDuration={6000}
        onClose={handleClose}
        message={
          <div style={{ backgroundColor: '#444f65' }} >
            <Grid container spacing={1}>
              <Grid item>
                <img className='lean-snack-notif-logo-bell' src={Notification_bell} />
              </Grid>
              <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={0}>
                  <Grid item xs>
                    <span className='lean-snack-notif-header'>
                      {subject}
                    </span>
                  </Grid>
                  <Grid item xs>
                    <span className='lean-snack-notif-app'>
                      {newContent[0]}
                    </span>
                  </Grid>
                  <Grid item>
                    {/* <div className='input'> */}
                    <TextField id="outlined-basic" value={value} onChange={handleChange} onKeyPress={handleKeyPress}
                      InputProps={{
                        className: "lean-form-text-field lean-visitorChat-text-field",
                        endAdornment: <IconButton aria-label="delete" InputProps={{ className: 'lean-form-text-field-adornedEnd' }} size="small"
                          onClick={(event) => handleReplyClick(event, value, notificationId, type)}
                        >
                          <img className="logo-send" src={send} />
                        </IconButton>
                      }}
                      // multiline rowsMax="4" 
                      variant="outlined"
                    />
                    {/* <RadioButton customClass={true} label='Mark as read' value={seen} notnotificationId={notificationId} seenHandler={props.seenHandler}/>  */}
                    {/* </div> */}
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <IconButton size="small" aria-label="close" color="inherit" onClick={(event) => handleCloseNot(event)}>
                  <CloseIcon fontSize="small" />
                </IconButton>
              </Grid>
            </Grid>
          </div>
        }
        onClick={(event) => handleRedirectDrawer(event, value)}
      />

    </div>
  );
}


const mapStateToProps = (state) => {
  let { replyNotificationReducer,getDetailForVisitorReducer,fireDrawerReducer } = state;
  return { replyNotificationReducer,getDetailForVisitorReducer,fireDrawerReducer };
}

export default connect(mapStateToProps, { replyNotificationAction,fireDrawer,getDetailForVisitor })(ChatToast)