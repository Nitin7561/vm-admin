import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import Grid from '@material-ui/core/Grid';
import Notification_bell from "./assets/notification_image.svg";
import './notificationsSnackBar.scss'
import { replyNotificationAction} from '../../../LeanSideBarContainer/actions/notifications';
import { connect } from 'react-redux';


const SelectToast = ({ state, data,closeToast,replyNotificationReducer,replyNotificationAction }) => {

  // console.log('Select props',state, data)
  let {subject,content,type,typespec,notificationId} = data
  typespec = JSON.parse(typespec)
  var quotes= content.replace(/'/g, '"'); 
  var contentObject=JSON.parse(quotes) 
  content = contentObject.content

  setTimeout(closeToast,8000)

  const [openNot, setOpenNot] = React.useState(state);

//--------------- SnackBar Open for displaying errro messages-----------------
const [open, setOpen] = React.useState(true);

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const handleClose = (event, reason) => {
  if (reason === 'clickaway') {
    return;
  }
  setOpen(false);
};
// function loadSnackBar(message) {
//   return (
//     <Snackbar open={setOpenNot} autoHideDuration={6000}
//       anchorOrigin={{
//         vertical: 'bottom',
//         horizontal: 'left',
//       }}
//       onClose={handleClose}>
//       <Alert onClose={handleClose} severity="error">
//         {/* This is a success message!                 */}
//         {message}
//       </Alert>
//     </Snackbar>
//   )
// }
//--------------------------------------------------
// let { isLoading: isLoadingReplyNotif, data: ReplyNotifData, error: errorReplyNotif, isError: isErrorReplyNotif } = replyNotificationReducer
  

  const handleClick = () => {
    setOpen(true);
  };

  const handleCloseNot = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
    closeToast()
  };


  const handleReplyClick = (e,  newValue) => {
    console.log('handleReplyClick ==>', notificationId, newValue, type)
    const reqBody = {
      "notificationId": notificationId,
      'reply': newValue,
      'replyType': type
    }
    // if(!isReplied) ''
    replyNotificationAction(reqBody)
    setOpenNot(false);
    closeToast()
  }

  return (
    <div>
      {/* <Button onClick={handleClick}>Open simple snackbar</Button> */}
      <Snackbar className='lean-snack-notf-root'
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={openNot}
        autoHideDuration={6000}
        onClose={handleClose}
        message={
          <div style={{ backgroundColor: '#444f65' }} >
            <Grid container spacing={1}>
              <Grid item>
                <img src={Notification_bell} />
              </Grid>
              <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={0}>
                  <Grid item xs>
                    <span className='lean-snack-notif-header'>
                      {subject}
                      </span>
                  </Grid>
                  <Grid item xs>
                    <span className='lean-snack-notif-app'>
                      {content}
                      </span>
                  </Grid>
                  <Grid item>
                    {/* <button className='lean-snack-notif-button-select' onClick={(event) => handleReplyClick(event)}> Approve </button>
                    <button className='lean-snack-notif-button-select' onClick={(event) => handleReplyClick(event)}>Decline</button>                                                        
                    <button className='lean-snack-notif-button-select' onClick={(event) => handleReplyClick(event)}>Decline</button>                                                        
                    <button className='lean-snack-notif-button-select' onClick={(event) => handleReplyClick(event)}>Decline</button>                                                         */}
                    {typespec.enums.map((enumVal) => {
                      // return <button className='input-button button-1-text' onClick={(event) => props.replyHandler(event, notificationId, enumVal, type, replied)} > {enumVal} </button>
                      return <button className='lean-snack-notif-button-select' onClick={(event) => handleReplyClick(event,enumVal)}> {enumVal} </button> 
                    })}
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseNot}>
                  <CloseIcon fontSize="small" />
                </IconButton>
              </Grid>
            </Grid>
          </div>
        }
      // action={

      //     <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
      //       <CloseIcon fontSize="small" />
      //     </IconButton>

      // }
      />
      {/* <Snackbar open={open} autoHideDuration={6000} 
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          This is a success message!                
        </Alert>
      </Snackbar> */}
    </div>
  );
}


const mapStateToProps = (state) => {
  let { replyNotificationReducer } = state;
  return { replyNotificationReducer };
}

export default connect(mapStateToProps, {
   replyNotificationAction
})(SelectToast)

