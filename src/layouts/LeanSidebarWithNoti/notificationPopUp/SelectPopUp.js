import React from 'react'
import Notification_bell from "../common/snackBar/notifications/assets/Notification_bell.png";
import './notificationPopUp.scss'

export default function SelectPopUp(props){

  // const { seen, replied, timestamp } = props.value  
  // const { notificationId, subject, content, type, redirects, icon, typespec } = props.value.data
  // console.log("NotificationVisitorApproval",props.value.data)
  const { seen, replied } = props.value  
  let { notificationId, content, type, typespec } = props.value.data
  // var jsonContent = JSON.parse(content)
  // content = jsonContent.content
  content = content.content
  if(typespec == undefined) typespec["enums"] = []

  const clickspan1 = (event) => {    
    // if(!seen)
       props.seenHandler(event,notificationId)
  }

  const  isMessgSeen =  seen ? {} : {backgroundColor:"#ffe5e5"}
 
    return (
      <div className="lean-notification-box-root" style={isMessgSeen}>
        <div className='lean-notification-box-flex'>
          <div className='div-1'>
            <div className="div-logo">
              <img className="logo-bell" src={Notification_bell} alt='loading icon..'/>
            </div>  
          </div>
          <div className='div-2'>
            <div className='message'>
            {/* <div className="div-cgp-header"> */}
              <span className="message-content" onClick={clickspan1}>{ content}</span>
            {/* </div> */}
            </div>
            <div className='input'>
            {typespec.enums.map((enumVal)=>{
              return <button className='input-button button-1-text' onClick={(event) => props.replyHandler(event,notificationId,enumVal,type,replied)} > {enumVal} </button>
            })}
              {/* <span className='span-time'>{props.time}</span> */}
            </div>
          </div>   

          <div className='div-3'>
            <span className='span-time-2'>{props.time}</span>
          </div>       
        </div>        
      </div>
      
    );
  }
