import React from 'react'
import Notification_bell from "../common/snackBar/notifications/assets/Notification_bell.png";
import './notificationPopUp.scss'


import { rbPageId, vmPageId, homePageId, vmRedirect } from '../constants'



export default function PlanePopUp(props) {
  // const { seen, replied, timestamp } = props.value  
  // const { notificationId, subject, content, redirects, icon, typespec } = props.value.data
  const { seen } = props.value    
  let { notificationId, subject,content,redirects } = props.value.data

  // console.log("NotificationMessage ",props.value)
  // console.log("NotificationMessage **",content)

  const clickspan1 = (event) => {    
    // if(!seen)
      //  if(redirects !== null)
      //  {
        if(content){
          if (content.sender.appId === vmPageId) {
            
            // var redirectsWeb = JSON.parse(redirects)
            console.log("Redirects for Web ==>", redirects);
            let { onClickWeb } = redirects;
            var splitVisIdandUrl = onClickWeb.split("?");
            var getEncryptId = splitVisIdandUrl[1].split("=");
            window.location.href = `${vmRedirect}?id=${getEncryptId[1]}`
        
          }
          if (content.sender.appId === rbPageId) {
          }
          if (content.sender.appId === homePageId) {
          }
        }

        props.seenHandler(event,notificationId,seen)

      //  }
  }

  const  isMessgSeen =  seen ? {} : {backgroundColor:'#ffe5e5'}  
  // isMessgSeen = content.sender.appId === vmPageId ? {cursor:'pointer'} : {}
    return (

      <div  onClick={clickspan1} className={content.sender.appId === vmPageId && redirects !== null? 'lean-notification-box-root cursor' : 'lean-notification-box-root'} style={isMessgSeen}>
        <div className='lean-notification-box-flex'>
          <div className='div-1'>
            <div className="div-logo">
              <img className="logo-bell" src={Notification_bell} alt='loading icon..'/>
            </div>
          </div>
          <div className='div-2'>
            <div className='message'>              
              <span className="message-content">{subject}</span>              
            </div>
            <div className='input'>
              <span className='span-date'>{props.date}</span>
              {/* <span className='span-time-2'>{props.time}</span> */}
            </div>
          </div>
          <div className='div-3'>
            <span className='span-time-2'>{props.time}</span>
          </div>
        </div>
        
      </div>

    );
  }
