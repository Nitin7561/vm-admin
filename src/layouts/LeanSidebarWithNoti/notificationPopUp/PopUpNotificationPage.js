import React from 'react'
import { Redirect } from 'react-router-dom'
import NotificationVisitorApproval from "./ConfirmPopUp";
import NotificationMessage from "./PlanePopUp";
import NotificationUserInput from "./SelectPopUp";
import NotificationChat from "./ChatPopUp";
import './notificationPopUp.scss'
// import { getAllNotificationsAction,seeNotificationAction, replyNotificationAction } from '../../redux/actions/notifications';
import {
  getAllNotificationsAction, seeNotificationAction, replyNotificationAction,
  resetReplyStatusAction
} from '../LeanSideBarContainer/actions/notifications';
import { connect } from 'react-redux';
import { CircularProgress, Link } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
// import { withSnackbar } from 'notistack';
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

function PopUpNotificationPage(props) {

  console.log("Props from Popup Blahc",props)

  let { getAllNotificationsAction, seeNotificationAction, replyNotificationAction, resetReplyStatusAction } = props
  let { getAllNotificationsReducer } = props
  let { closeNotificationPopUp } = props

  // console.log("rxdRotifications",rxdRotifications)

  React.useEffect(() => {
    // getAllNotificationsAction()
  }, [])

  let [redirect, setRedirect] = React.useState(false)

  //-- Handle notistack snackbar
  const handleToast = (type, message) => {
    console.log("handleToast", type, message)
    // let key = enqueueSnackbar(message, {
    //   preventDuplicate: true,
    //   variant: type,
    //   action: (
    //     <IconButton
    //       aria-label="Close notification"
    //       color="inherit"
    //       onClick={() => closeSnackbar(key)}
    //     >
    //       <CloseIcon fontSize="small" />
    //     </IconButton>
    //   ),
    // });
    // setEableToast(enableToast==false)
    // setToast({...toast,state:false,message:'',type:''})
  };
  //--------------------------------------------------

  let { isLoading: isLoadingNotifications, data: notificationsData, error: errorNotifications, isError: isErrorNotifications, replyStatus } = getAllNotificationsReducer

  const resetReplyStatusRedux = () => {
    resetReplyStatusAction()
  }


  const handleReplyClick = (e, id, newValue, type, isReplied) => {
    console.log('handleReplyClick popup==>', id, newValue, type, isReplied)
    const reqBody = {
      "notificationId": id,
      'reply': newValue,
      'replyType': type
    }
    if (!isReplied) {
      replyNotificationAction(reqBody)
    }
    else {
      console.log("handleReplyClick loadSnackBar")
      handleToast('error', "Alraedy notification replied!!")
    }
  };

  //--------------- Handle delete Request ---------------
  const handleSeenClick = (e, id, isSeen) => {
    console.log('handleSeenClick ==>', id, isSeen)
    // e.preventDefault()    
    const reqBody = {
      "notifications": [id]
    }
    if (!isSeen)
      seeNotificationAction(reqBody)
  };


  const markAsRead = () => {
    console.log('handleSeenClick ==>')
    var reqBody, tempArray = [], isbulkSeen = false, count = 0

    for (var i = 0; i < notificationsData.length; i++) {
      let item = notificationsData[i]
      const { seen } = item
      const { type, notificationId } = item.data
      if ((type == 'plain' || type == 'string') && !seen) {
        tempArray.push(notificationId)
        count++
      }
      if ((type == 'confirm' || type == 'select') && !seen) {
        // tempArray.push(notificationId) 
        count++
      }
      if (count >= 5) break
    }
    // console.log("tempArray-->", tempArray.length)
    isbulkSeen = true
    reqBody = { "notifications": tempArray }
    if (tempArray.length > 0)
      seeNotificationAction(reqBody, isbulkSeen)

    window.location.href = 'https://cs.leantron.dev/spaceSense/web/pages/notifications';

    closeNotificationPopUp()

  };




  return (
    <div className="lean-notification-card">
      {replyStatus != "" ? (handleToast('info', replyStatus), resetReplyStatusRedux()) : null}

      <div >
        {isLoadingNotifications ? <CircularProgress /> : isErrorNotifications ? handleToast('error', errorNotifications) :
          mapNotifications(notificationsData, handleSeenClick, handleReplyClick)}
      </div>


      <div className='div-viewAll' >
        {/* <Link to='/spaceSense/web/pages/notifications'> */}
        <span className='text' onClick={() => { markAsRead() }}>View All</span>
        {/* </Link> */}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  let { getAllNotificationsReducer, replyNotificationReducer } = state;
  return { getAllNotificationsReducer, replyNotificationReducer };
}


export default connect(mapStateToProps, {
  getAllNotificationsAction, seeNotificationAction, replyNotificationAction, resetReplyStatusAction
})(PopUpNotificationPage)


function mapNotifications(rxdMessg, handleSeenClick, handleReplyClick) {
  var messgList = []
  if (rxdMessg.length > 0) {
    // rxdMessg.slice(0, 5).map((data) => {
    rxdMessg.map((data) => {
      // const { seen, replied, timestamp } = data      
      // const { id, subject, content, type, redirects, icon, typespec } = data.data  
      const { timestamp, seen } = data
      const { type } = data.data
      if (!seen) {
        var date1 = new Date();
        var date2 = new Date(timestamp);
        var displayTime, displayDate = getCustomDateTime(new Date(timestamp))
        // To calculate the time difference of two dates 
        var Difference_In_Time = date2.getTime() - date1.getTime();
        // To calculate the no. of days between two dates 
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        if (Difference_In_Days > 2)
          displayTime = getCustomDate(date2)
        else if (Difference_In_Days == 1)
          displayTime = 'yesterday'
        else
          displayTime = getCustomTimeDiff(date2.getTime() / 1000, date1.getTime() / 1000)
        if (type == 'plain')
          messgList.push(<NotificationMessage value={data} time={displayTime} date={displayDate} seenHandler={handleSeenClick} />)
        else if (type == 'select')
          messgList.push(<NotificationUserInput value={data} time={displayTime} replyHandler={handleReplyClick} seenHandler={handleSeenClick} />)
        else if (type == 'confirm')
          // messgList.push(<VisitorApproval value={seen, replied, id, subject, content, type, redirects, icon, typespec, timestamp} />)
          messgList.push(<NotificationVisitorApproval value={data} time={displayTime} replyHandler={handleReplyClick} seenHandler={handleSeenClick} />)
        else if (type == 'string')
          messgList.push(<NotificationChat value={data} time={displayTime} replyHandler={handleReplyClick} seenHandler={handleSeenClick} />)
      }
    })
    if (messgList.length == 0) return <div style={{ padding: "30px 0", display: 'flex', justifyContent: 'center', fontSize: '18px', fontWeight: 600}}> No new notifications</div>
    else return messgList.slice(0, 5)
  }
  return <div style={{ padding: "30px 0", display: 'flex', justifyContent: 'center', fontSize: '18px', fontWeight: 600}}> No new notifications</div>
}

//-------------- Display Date & Time Logic -------------------
let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
function checkTime(i) {
  if (i < 10) { i = "0" + i; }
  return i;
}
function getCustomDate(date) {
  return `${date.getDate()}-${months[date.getMonth()]}-${date.getFullYear()}`;
}

function getCustomDateTime(date) {
  return `${date.getDate()}-${months[date.getMonth()]}-${date.getFullYear()} 
    ${checkTime(date.getHours())}:${checkTime(date.getMinutes())}:${checkTime(date.getSeconds())}`;
}

function getCustomTimeDiff(from, to) {
  let diff = Math.abs(to - from);
  let hours = Math.floor(diff / (60 * 60));
  let h = checkTime(hours);
  let mins = diff / 60 - hours * 60;
  let m = checkTime(mins);

  if (h == 0)
    return `${Math.round(m)} Min`
  else
    return `${h} hr ${Math.round(m)} Min`;
}