import React from 'react'
import Notification_room from "../common/snackBar/notifications/assets/Notification_room.png";
import send from "../common/snackBar/notifications/assets/send.svg";
import TextField from "@material-ui/core/TextField";
import IconButton from '@material-ui/core/IconButton';
import './notificationPopUp.scss'

export default function ChatPopUp(props) {
  const { seen, replied } = props.value
  let { notificationId, content, type } = props.value.data
  console.log("ChatPopUp", content)
  content = content.content
  var newContent = content.split("$$");
  const isMessgSeen = seen ? {} : { backgroundColor: "#FFE5E5" }
  const [value, setValue] = React.useState('');
  const handleChange = event => {
      setValue(event.target.value);
  };
  const clickspan1 = (event) => {
      // if(!seen)
      props.seenHandler(event, notificationId)
  }
  return (
      <div className='lean-notification-box-root' style={isMessgSeen}>
          <div className='lean-notification-box-flex'>
              <div className='div-1'>
                  <div className="div-logo">
                      <img className="logo-bell" src={Notification_room} alt='loading icon..' />
                  </div>
              </div>
              <div className='div-2'>
                  <div className='message'>
                      <span className="message-content" onClick={clickspan1}>{newContent[0]}</span>
                  </div>
                  <div className='input'>
                      <TextField id="outlined-basic" value={value} onChange={handleChange}
                          InputProps={{
                              className: "lean-form-text-field-small-chat",
                              endAdornment: <IconButton aria-label="delete" InputProps={{ className: 'lean-form-text-field-adornedEnd' }} size="small"
                                  onClick={(event) => props.replyHandler(event, notificationId, value, type, replied)}
                              >
                                  <img className="logo-send" src={send} alt='loading icon..' />
                              </IconButton>
                          }}
                          // multiline rowsMax="4" 
                          variant="outlined"
                      />
                      {/* <span className='span-time-3'>{props.time}</span> */}
                  </div>
              </div>
              <div className='div-3'>
                  <span className='span-time-2'>{props.time}</span>
              </div>
          </div>
      </div>
  );
}