import React from "react";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { connect } from "react-redux";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Button } from "@material-ui/core";
import { InputTextField } from "../../components/InputTextField/InputTextField";
import { InputTextFieldForNewVisitors } from "../../components/InputTextField/InputTextFieldForNewVisitors"
import { MLInputTextField } from "../../components/InputTextField/MLInputTextField";
import { DatePickerField } from "../../components/InputTextField/DatePickerField";
import { TimePickerField } from "../../components/InputTextField/TimePickerField";
import { DropdownField } from "../../components/InputTextField/DropdownField";
// import { InputAutoField } from "../../components/InputTextField/DropdownField";
import CancelIcon from '@material-ui/icons/Cancel';
import { getAddAppointment } from '../../redux/actions/addAppointment';
import { getAllAvailableMeetingRooms } from '../../redux/actions/getAllAvailableMeetingRooms';
import { getAllBookedRooms } from '../../redux/actions/getAllBookedRooms';
import { rescheduleAppt } from '../../redux/actions/rescheduleAppointment';
import { CircularProgress } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import AddAppointmentBookingModal from '../../components/Modal/AddAppointmentBookingModal';
import { ModalContext } from "../../utils/ModalContext";
import { getFromAndTo } from '../../utils/timeFunctions';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
// import {refresher} from '../../redux/actions/refresher';


import "./addAppointment.scss";
import { InputAutoField } from "../../components/InputTextField/InputAutoField";

const AntSwitch = withStyles(theme => ({
  root: {
    width: 28,
    height: 16,
    padding: 0,
    display: 'flex',
  },
  switchBase: {
    padding: 2,
    color: theme.palette.grey[500],
    '&$checked': {
      transform: 'translateX(12px)',
      color: theme.palette.common.white,
      '& + $track': {
        opacity: 1,
        backgroundColor: theme.palette.primary.main,
        borderColor: theme.palette.primary.main,
      },
    },
  },
  thumb: {
    width: 12,
    height: 12,
    boxShadow: 'none',
  },
  track: {
    border: `1px solid ${theme.palette.grey[500]}`,
    borderRadius: 16 / 2,
    opacity: 1,
    backgroundColor: theme.palette.common.white,
  },
  checked: {},
}))(Switch);

const AddAppointmentForm = props => {
  const [subject, changeSubject] = React.useState("");
  const [visitorList, changeVisitorList] = React.useState({ [Math.random()]: { name: "", email: "" } });
  const [meetingDate, changeMeetingDate] = React.useState(new Date());
  const [startTime, changeStartTime] = React.useState(new Date((new Date()).getTime() + 1 * 60 * 60 * 1000));
  const [duration, changeDuration] = React.useState("0.5");
  const [bookARoom, changeBookARoom] = React.useState("Dont Book a Room for this Appointment");
  const [allocateRoom, changeAllocateRoom] = React.useState("Select a Room");
  const [description, changeDescription] = React.useState("");
  const { setCurrentModal } = React.useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });

  // let { getAddAppointmentReducer } = props

  let { isLoading: isLoadingAppointment, data: AppointmentData, error: errorAppointment, isError: isErrorAppointment } = props.getAddAppointmentReducer;

  const [state, setState] = React.useState(false);
  const handleChange = event => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const durationMenuItems = [
    { name: 'All Day', value: 8 },
    { name: '30 Minutes', value: 0.5 },
    { name: '1 Hour', value: 1 },
    { name: '1.5 Hours', value: 1.5 },
    { name: '2 Hours', value: 2 },
    { name: '2.5 Hours', value: 2.5 },
    { name: '3 Hours', value: 3 },
    { name: '3.5 Hours', value: 3.5 },
    { name: '4 Hours', value: 4 },
    { name: '4.5 Hours', value: 4.5 },
    { name: '5 Hours', value: 5 },
    { name: '5.5 Hours', value: 5.5 },
    { name: '6 Hours', value: 6 },
    { name: '6.5 Hours', value: 6.5 },
    { name: '7 Hours', value: 7 },
    { name: '7.5 Hours', value: 7.5 },
  ]

  var getFromandToFromFn = getFromAndTo(meetingDate, startTime, Number(duration));
  console.log("Get From and To==>", getFromandToFromFn.from, getFromandToFromFn.to)
  console.log("Duration==>", duration)

  let { isLoading: isLoadingAvailableRooms, data: dataAvailableRooms, isError: isErrorAvailableRoomsIssues } = props.getAllAvailableRoomsReducer

  let { isLoading: isLoadingBookedRooms, data: dataBookedRooms, isError: isErrorBookedRoomsIssues } = props.getAllBookedRoomsReducer


  function getRoomDetails(roomName) {
    for (var i = 0; i < dataAvailableRooms.data.finalRoomDetails.length; i++) {
      if (roomName === dataAvailableRooms.data.finalRoomDetails[i].shortName) {
        console.log("Room matched");
        var campusName = dataAvailableRooms.data.finalRoomDetails[i].location.campusName;
        var buildingName = dataAvailableRooms.data.finalRoomDetails[i].location.buildingName;
        var floorName = dataAvailableRooms.data.finalRoomDetails[i].location.floorName;
        var zoneName = dataAvailableRooms.data.finalRoomDetails[i].location.zoneName;
        var roomId = dataAvailableRooms.data.finalRoomDetails[i].roomId;
        return { campusName, buildingName, floorName, zoneName, roomId }
      }
      else {
        console.log("Not matched");
      }
    }

  }

  React.useEffect(
    () => {

      console.log("On Load ")

      console.log("COMING HERE FOR AVAIL ROOMS");


      props.getAllAvailableMeetingRooms({
        "sub": "60240705",
        "fromTime": getFromandToFromFn.from * 1000,
        "toTime": getFromandToFromFn.to * 1000
      })

      console.log("COMING HERE FOR AVAIL ROOMS")

      // props.getAllBookedRooms({
      //   "sub": "60240705",
      //   "fromTime": getFromandToFromFn.from * 1000,
      //   "toTime": getFromandToFromFn.to * 1000
      // })

    }, [meetingDate, startTime, duration]
  );

  const submit = (isDisabled) => {

    console.log("When Submit Clicked!!");

    var meetingDateNew = meetingDate.toDateString();
    var startTimeNew = startTime.toTimeString();

    var dateNow = meetingDateNew.split(" ");
    var timeNow = startTimeNew.split(" ");

    var newDateTimeString = dateNow[1] + " " + dateNow[2] + "," + " " + dateNow[3] + " " + timeNow[0];

    console.log("Meeting Date-->", meetingDateNew);
    console.log("Meeting Start Time-->", startTimeNew);
    console.log("Meeting  Time-->", newDateTimeString);

    var getEpoch = new Date(newDateTimeString).getTime();

    const keys = Object.values(visitorList)
    var visitorEmails = [];
    var visitorNames = [];

    for (var i = 0; i < keys.length; i++) {
      visitorNames.push(keys[i].name);
      visitorEmails.push(keys[i].email);
    }

    console.log("VM Names", visitorNames);
    console.log("VM Emails", visitorEmails);
    console.log("Get Epoch", getEpoch);



    var getAncestorDetails = getRoomDetails(allocateRoom)
    console.log(getAncestorDetails)

    if (isDisabled) {
      {
        bookARoom === "Dont Book a Room for this Appointment" ?

          props.rescheduleAppt({
            "arrivalTime": getEpoch,
            "duration": (duration * 60 * 60 * 1000),
            "doRoomCancel": state,
            "doRoomBooking": false,
            "appointmentId": props.appointmentId
          })

          :

          props.rescheduleAppt({

            "arrivalTime": getEpoch,
            "duration": (duration * 60 * 60 * 1000),
            "doRoomCancel": state,
            "doRoomBooking": true,
            "appointmentId": props.appointmentId,
            "bookingDetails": {

              "campusName": getAncestorDetails.campusName,
              "buildingName": getAncestorDetails.buildingName,
              "floorName": getAncestorDetails.floorName,
              "zoneName": getAncestorDetails.zoneName,
              "componentId": getAncestorDetails.roomId,
              "type": "meeting",
              "participants": visitorEmails.length,
              "fromTime": getFromandToFromFn.from,
              "toTime": getFromandToFromFn.to,
              "calender": "false",
              "subject": subject,
              "description": description,
              "attendeesMailId": [visitorEmails]

            }

          })
      }
      openModal({ name: 'RescheduledAppointmentBooked', props: { "isDisabled": isDisabled } })
    }
    else if (!props.addAppointmentToParticularVisitor) {
      {
        bookARoom === "Dont Book a Room for this Appointment" ?

          props.getAddAppointment({
            "purpose": subject,
            "authType": "leantron",
            "visitorNames": visitorNames,
            "emails": visitorEmails,
            "arrivalTime": getEpoch,
            "duration": (duration * 60 * 60 * 1000),
            "doRoomBooking": false
          })

          :

          props.getAddAppointment({
            "purpose": subject,
            "authType": "leantron",
            "visitorNames": visitorNames,
            "emails": visitorEmails,
            "arrivalTime": getEpoch,
            "duration": (duration * 60 * 60 * 1000),
            "doRoomBooking": true,
            "bookingDetails": {

              "campusName": getAncestorDetails.campusName,
              "buildingName": getAncestorDetails.buildingName,
              "floorName": getAncestorDetails.floorName,
              "zoneName": getAncestorDetails.zoneName,
              "componentId": getAncestorDetails.roomId,
              "type": "meeting",
              "participants": visitorEmails.length,
              "fromTime": getFromandToFromFn.from,
              "toTime": getFromandToFromFn.to,
              "calender": "false",
              "subject": subject,
              "description": description,
              // "attendeesMailId": [visitorEmails]
            }

          })

        openModal({ name: 'AddAppointmentBookingModal', props: { "isDisabled": isDisabled } })
      }
    }
    else if (props.addAppointmentToParticularVisitor) {


      console.log("VISITOR LIST FROM DYNA ADD", visitorNames, visitorEmails)
      visitorNames[0] = (props.visitorName);
      visitorEmails[0] = (props.visitorEmail);
      console.log("AFTER VISITOR LIST FROM DYNA ADD", visitorNames, visitorEmails)

      {
        bookARoom === "Dont Book a Room for this Appointment" ?

          props.getAddAppointment({
            "purpose": subject,
            "authType": "leantron",
            "visitorNames": visitorNames,
            "emails": visitorEmails,
            "arrivalTime": getEpoch,
            "duration": (duration * 60 * 60 * 1000),
            "doRoomBooking": false
          })

          :

          props.getAddAppointment({
            "purpose": subject,
            "authType": "leantron",
            "visitorNames": visitorNames,
            "emails": visitorEmails,
            "arrivalTime": getEpoch,
            "duration": (duration * 60 * 60 * 1000),
            "doRoomBooking": true,
            "bookingDetails": {

              "campusName": getAncestorDetails.campusName,
              "buildingName": getAncestorDetails.buildingName,
              "floorName": getAncestorDetails.floorName,
              "zoneName": getAncestorDetails.zoneName,
              "componentId": getAncestorDetails.roomId,
              "type": "meeting",
              "participants": visitorEmails.length,
              "fromTime": getFromandToFromFn.from,
              "toTime": getFromandToFromFn.to,
              "calender": "false",
              "subject": subject,
              "description": description,
              "attendeesMailId": [visitorEmails]
            }

          })

        openModal({ name: 'AddAppointmentBookingModal', props: { "isDisabled": isDisabled } })
      }
    }
  }

  const onAddVisitor = () => {

    let newVisitor = {
      [Math.random()]: { name: "", email: "" }
    }
    changeVisitorList({ ...visitorList, ...newVisitor })
  }


  const onDeleteVisitor = (e) => {
    console.log(e.currentTarget.name);
    let key = e.currentTarget.name;
    console.log("Key mtf", key)
    let tempList = { ...visitorList }
    delete tempList[key]
    changeVisitorList(tempList);
  }

  const changeHandler = (e) => {

    let key = e.target.name.split('_')[1];

    let test = {
      ...visitorList[key],
      [e.target.name.split('_')[0]]: e.target.value,
    }

    changeVisitorList({ ...visitorList, [key]: test })
  }



  var hour = null;
  var newDate = null;
  var newHour = null;
  var newNewNewTms = null;
  var newMinutes = null;

  var hour1 = null;
  var newDate1 = null;
  var newHour1 = null;
  var newNewNewTms1 = null;
  var newMinutes1 = null;

  // var timeConversion = null;
  var timeDiff = null;

  const convertTime12to24 = (time12h) => {

    console.log("SPLIITTTT===>", time12h)
    const [time, modifier] = time12h.split(' ');

    let [hours, minutes] = time.split(':');

    if (hours === '12') {
      hours = '00';
    }

    if (modifier === 'PM') {
      hours = parseInt(hours, 10) + 12;
    }

    return `${hours}:${minutes}`;
  }

  function timeConversion(millisec) {

    var seconds = (millisec / 1000).toFixed(1);

    var minutes = (millisec / (1000 * 60)).toFixed(1);

    var hours = (millisec / (1000 * 60 * 60)).toFixed(0);

    var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);

    if (seconds < 60) {
      return seconds + " Sec";
    } else if (minutes < 60) {
      return minutes;
    } else if (hours < 24) {
      return hours;
    } else {
      return days
    }
  }

  if (props.isDisabled) {

    console.log("Get Checkin", props.checkInTime)
    console.log("Get Checkout", props.checkOutTime)
    console.log("Get Date", props.date)

    hour = convertTime12to24(props.checkInTime)
    newDate = new Date(props.date);
    newNewNewTms = hour.split(':')
    newHour = newDate.setHours(newNewNewTms[0])
    newMinutes = newDate.setMinutes(newNewNewTms[1])
    console.log("NEW NEW DATE==>", newDate.getTime())

    hour1 = convertTime12to24(props.checkOutTime)
    newDate1 = new Date(props.date);
    newNewNewTms1 = hour1.split(':')
    newHour1 = newDate1.setHours(newNewNewTms1[0])
    newMinutes1 = newDate1.setMinutes(newNewNewTms1[1])
    console.log("NEW NEW DATE 1==>", newDate1.getTime())

    timeDiff = newDate1.getTime() - newDate.getTime()
    console.log("Time diff==>", timeConversion(timeDiff))
  }


  console.log("Props that came", props)

  console.log("Rooms Info", dataAvailableRooms)

  console.log("dataAvailableRooms.finalRoomDetails", props.getAllAvailableRoomsReducer)

  console.log("isLoading", isLoadingAvailableRooms)


  return (
    <>
      <Grid container>
        {/* <Grid className="lean-grid-item" item xs={12}>
          <div className="lean-section-text-header">Add Appointment</div>
        </Grid> */}
        {/* <HeaderComponent/> */}
        <Grid className="lean-grid-item" item xs={11}>
          <InputTextField
            // disabled = "true"
            inputClass={props.isDisabled ? "lean-reschedule-appointment-disable-input-text-field" : null}
            disabled={props.isDisabled ? "true" : null}
            value={props.isDisabled ? props.subject : null}
            eventHandler={changeSubject}
            label="Subject"
            placeholder="Please enter meeting Subject" />
        </Grid>

        <Grid className="lean-grid-item" item xs={12}>
          <div>
            <List>

              {Object.keys(visitorList).map(eachKey => {

                let { email, name } = visitorList[eachKey]
                return (
                  <ListItem disableGutters="true">
                    <Grid container>
                      <Grid item xs={11}>
                        <Grid container>
                          <Grid className="lean-grid-item-visitor-email" item xs={12} sm={6}>
                            <InputTextFieldForNewVisitors
                              inputClass={props.isDisabled ? "lean-reschedule-appointment-disable-input-text-field" : null}
                              disabled={props.isDisabled ? "true" : null}
                              name={`email_${eachKey}`}
                              label="Visitor Email"
                              value={props.visitorEmail != undefined ? props.visitorEmail : email}
                              eventHandler={changeHandler}
                              placeholder="Enter the visitor's email"
                            />
                          </Grid>
                          <Grid className="lean-grid-item" item xs={12} sm={6}>
                            <InputTextFieldForNewVisitors
                              inputClass={props.isDisabled ? "lean-reschedule-appointment-disable-input-text-field" : null}
                              disabled={props.isDisabled ? "true" : null}
                              name={`name_${eachKey}`}
                              label="Visitor Name"
                              value={props.visitorName != undefined ? props.visitorName : name}
                              eventHandler={changeHandler}
                              placeholder="Enter the visitor's name"
                            />
                          </Grid>
                        </Grid>
                      </Grid>

                      <Grid item xs={1} className="lean-deleteButton-item">

                        {props.isDisabled || props.addAppointmentToParticularVisitor ? (
                          null
                        ) :
                          (
                            <>
                              {Object.keys(visitorList).length > 1 ? (
                                <IconButton className="deleteButton" name={eachKey} onClick={onDeleteVisitor}>
                                  <CancelIcon className="deleteButton" />
                                </IconButton>) : null}

                            </>
                          )}
                      </Grid>

                    </Grid>

                    {/* <Grid container>
                      <Grid className="lean-grid-item-visitor-email" item xs={6} sm={5}>
                        <InputTextFieldForNewVisitors
                          inputClass={props.isDisabled ? "lean-reschedule-appointment-disable-input-text-field" : null}
                          disabled={props.isDisabled ? "true" : null}
                          name={`email_${eachKey}`}
                          label="Visitor Email"
                          value={props.visitorEmail != undefined ? props.visitorEmail : email}
                          eventHandler={changeHandler}
                          placeholder="Enter the visitor's email"
                        />
                      </Grid>
                      <Grid className="lean-grid-item" item xs={6} sm={5}>
                        <InputTextFieldForNewVisitors
                          inputClass={props.isDisabled ? "lean-reschedule-appointment-disable-input-text-field" : null}
                          disabled={props.isDisabled ? "true" : null}
                          name={`name_${eachKey}`}
                          label="Visitor Name"
                          value={props.visitorName != undefined ? props.visitorName : name}
                          eventHandler={changeHandler}
                          placeholder="Enter the visitor's name"
                        />
                      </Grid>
                      <Grid className="lean-deleteButton-item" item xs={6} sm={1}>
                        {props.isDisabled || props.addAppointmentToParticularVisitor ? (
                          null
                        ) :
                          (
                            <>
                              {Object.keys(visitorList).length > 1 ? (
                                <IconButton className="deleteButton" name={eachKey} onClick={onDeleteVisitor}>
                                  <CancelIcon className="deleteButton" />
                                </IconButton>) : null}

                            </>
                          )}
                      </Grid>
                    </Grid> */}
                  </ListItem>
                )
              })}

            </List>
          </div>
        </Grid>

        <Grid className="lean-grid-item" item xs={12}>
          {props.isDisabled || props.addAppointmentToParticularVisitor ? (
            null
          )
            :
            <>
              <div className="lean-fab-button">
                <AddCircleIcon />
                <label className="fab-label"> <span className="fab-bold" onClick={onAddVisitor}>Add another</span></label>
              </div>
            </>
          }

        </Grid>

        <Grid className="lean-grid-item" item xs={12} sm={4}>
          <DatePickerField
            value={meetingDate}
            eventHandler={changeMeetingDate}
            label="Meeting Date"
          />
        </Grid>

        <Grid className="lean-grid-item" item xs={12} sm={4}>
          <TimePickerField
            value={startTime}
            eventHandler={changeStartTime}
            label="Start Time"
          />
        </Grid>

        <Grid className="lean-grid-item" item xs={12} sm={4}>
          <DropdownField
            label="Duration"
            value={duration}
            eventHandler={changeDuration}
            menuItems={durationMenuItems}
          />
          {/* <InputAutoField
                type="time"
                label="Duration"
                value="00:30"
                eventHandler={(v) => {
                //  changeDuration(v);
                }}
                menuItems={durationMenuItems}
                name="duration"
                onEveryChange={true}
                // inputClass="lean-form-text-field-disabled"
          /> */}
        </Grid>

        <Grid className="lean-grid-item" item xs={12}>
          {props.isDisabled ? props.roomName.length > 0 ?
            (<>
              <Typography component="div">
                <Grid component="label" container alignItems="center" spacing={1}>
                  <Grid item className='lean-reschedule-modal-text'>Want to cancel the Room Booked with the previous appointment?</Grid>
                  <Grid item>
                    <AntSwitch checked={state.checked} onChange={handleChange} name="checked" />
                  </Grid>
                  {/* <Grid item>On</Grid> */}
                </Grid>
              </Typography>
            </>)
            : null

            : null
          }
        </Grid>

        <Grid className="lean-grid-item" item xs={12} sm={6}>
          <DropdownField
            inputClass={props.isDisabled ? "lean-reschedule-appointment-disable-input-text-field" : null}
            disabled={props.isDisabled ? "true" : null}
            label="Need to Book a Room for this Appointment?"
            value={bookARoom}
            eventHandler={changeBookARoom}
            menuItems={[
              { 'value': "Dont Book a Room for this Appointment", 'name': "Dont Book a Room for this Appointment" },
              { 'value': "Book a Room for this Appointment", 'name': "Book a Room for this Appointment" },
            ]}
          // isAddAppointment="true"
          />
        </Grid>


        <Grid className="lean-grid-item" item xs={12} sm={6}>
          <DropdownField
            inputClass={props.isDisabled || bookARoom == "Dont Book a Room for this Appointment" ? "lean-reschedule-appointment-disable-input-text-field" : null}
            disabled={props.isDisabled || bookARoom == "Dont Book a Room for this Appointment" ? "true" : null}
            label="Select a Meeting Room"
            value={allocateRoom}
            eventHandler={changeAllocateRoom}
            menuItems={isLoadingAvailableRooms ? null : dataAvailableRooms.data.finalRoomDetails.map((item) => ({ 'value': item.shortName, 'name': item.shortName }))}
            isAddAppointment="true"
          />
        </Grid>

        <Grid className="lean-grid-item" item xs={12}>
          <MLInputTextField
            inputClass={props.isDisabled ? "lean-reschedule-appointment-disable-input-text-field" : null}
            disabled={props.isDisabled ? "true" : null}
            label="Description (Optional)"
            value={description}
            eventHandler={changeDescription}
            inputProps={{ maxLength: 150 }}
          />
        </Grid>

        <Grid className="lean-grid-item" item xs={12}>
          <Button variant="contained" className="lean-btn-primary" onClick={() => submit(props.isDisabled)}>
            Submit
          </Button>
        </Grid>

      </Grid>
    </>
  );
};

const mapStateToProps = (state) => {
  let { getAddAppointmentReducer, getAllAvailableRoomsReducer, getAllBookedRoomsReducer, rescheduleAppointmentReducer, refresherReducer } = state;
  return { getAddAppointmentReducer, getAllAvailableRoomsReducer, getAllBookedRoomsReducer, rescheduleAppointmentReducer, refresherReducer };
}
export default connect(mapStateToProps, { getAddAppointment, getAllAvailableMeetingRooms, getAllBookedRooms, rescheduleAppt })(AddAppointmentForm);