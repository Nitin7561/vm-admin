import React from "react";
import AddAppointment from "./AddAppointment";
import Modal from '../../components/Modal/FunctionalModal';

export default function AddAppointmentInAModal({closeModal,...props}) {
    let {visitorName,visitorEmail,isDisabled, classStyles,addAppointmentToParticularVisitor,checkInTime,checkOutTime,date,roomName,appointmentId,subject} =props
    console.log('Name', visitorName);
    console.log('Email',visitorEmail);
    console.log('isDisabled',isDisabled);
    console.log('checkInTime',checkInTime);
    console.log('checkOutTime',checkOutTime);
    console.log('date',date);
    console.log('subject',subject);



    return (
       <>
        <Modal title={isDisabled ? "Reschedule" : "Add Appointment"} handleClose={closeModal} classStyles={classStyles} children={<AddAppointment visitorName={visitorName} visitorEmail={visitorEmail} isDisabled={isDisabled} addAppointmentToParticularVisitor={addAppointmentToParticularVisitor} checkInTime = {checkInTime} checkOutTime={checkOutTime} date={date} roomName={roomName} appointmentId={appointmentId} subject={subject} />} />
       </>
    );
}
