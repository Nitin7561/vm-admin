/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'lean-icons\'">' + entity + '</span>' + html;
	}
	var icons = {
		'lean-icon-ac-large': '&#xe937;',
		'lean-icon-blinds': '&#xe938;',
		'lean-icon-googlecalender': '&#xe902;',
		'lean-icon-heater': '&#xe939;',
		'lean-icon-hospitality': '&#xe93a;',
		'lean-icon-light': '&#xe93b;',
		'lean-icon-my-favourites-side': '&#xe93c;',
		'lean-icon-phone-system': '&#xe93d;',
		'lean-icon-pointer-laser': '&#xe93e;',
		'lean-icon-projector-screen': '&#xe93f;',
		'lean-icon-video-conference': '&#xe940;',
		'lean-icon-view-map': '&#xe941;',
		'lean-icon-writing-pad': '&#xe944;',
		'lean-icon-dashboard-send-message': '&#xe943;',
		'lean-icon-dashboard-close': '&#xe950;',
		'lean-icon-dashboard-open': '&#xe951;',
		'lean-icon-dashboard-delete-filled': '&#xe952;',
		'lean-icon-dashboard-delete': '&#xe953;',
		'lean-icon-Admin-Dashboard-1': '&#xe954;',
		'lean-icon-dashboard-seat': '&#xe955;',
		'lean-icon-dashboard-clock': '&#xe956;',
		'lean-icon-dashboard-trophy': '&#xe957;',
		'lean-icon-dashboard-down-arrow': '&#xe958;',
		'lean-icon-dashboard-up-arrow': '&#xe959;',
		'lean-icon-dashboard-meeting': '&#xe95a;',
		'lean-icon-dashboard-contact': '&#xe95b;',
		'lean-icon-arrow_drop_down-copy1': '&#xe964;',
		'lean-icon-logout': '&#xe965;',
		'lean-icon-person': '&#xe966;',
		'lean-icon-support-feedback': '&#xe967;',
		'lean-icon-help-faq': '&#xe968;',
		'lean-icon-radio-btn': '&#xe969;',
		'lean-icon-vm-user': '&#xe96a;',
		'lean-icon-chat': '&#xe96b;',
		'lean-icon-radio-btn-checked': '&#xe96c;',
		'lean-icon-ac': '&#xe903;',
		'lean-icon-arrow_drop_down-copy': '&#xe904;',
		'lean-icon-arrow_drop_down': '&#xe905;',
		'lean-icon-arrow_drop_up': '&#xe906;',
		'lean-icon-book-a-room': '&#xe907;',
		'lean-icon-cancel': '&#xe908;',
		'lean-icon-capacity': '&#xe909;',
		'lean-icon-check': '&#xe90a;',
		'lean-icon-check_box': '&#xe90b;',
		'lean-icon-chevron_left': '&#xe90c;',
		'lean-icon-chevron_right': '&#xe90d;',
		'lean-icon-close': '&#xe90e;',
		'lean-icon-delete': '&#xe900;',
		'lean-icon-desk-booking': '&#xe90f;',
		'lean-icon-error-outline': '&#xe910;',
		'lean-icon-error': '&#xe911;',
		'lean-icon-expand_less': '&#xe912;',
		'lean-icon-expand_more': '&#xe913;',
		'lean-icon-fav': '&#xe914;',
		'lean-icon-floor': '&#xe915;',
		'lean-icon-grid': '&#xe916;',
		'lean-icon-home': '&#xe917;',
		'lean-icon-list': '&#xe901;',
		'lean-icon-map': '&#xe918;',
		'lean-icon-map1': '&#xe919;',
		'lean-icon-menu': '&#xe91a;',
		'lean-icon-more': '&#xe91b;',
		'lean-icon-my-bookings': '&#xe91c;',
		'lean-icon-my-favourites': '&#xe91d;',
		'lean-icon-navigation': '&#xe91e;',
		'lean-icon-notification': '&#xe91f;',
		'lean-icon-powered-by': '&#xe920;',
		'lean-icon-projector': '&#xe921;',
		'lean-icon-ancestor': '&#xe922;',
		'lean-icon-bell': '&#xe923;',
		'lean-icon-refresh': '&#xe924;',
		'lean-icon-room-booking': '&#xe925;',
		'lean-icon-screen': '&#xe926;',
		'lean-icon-seat-copy': '&#xe927;',
		'lean-icon-seat': '&#xe928;',
		'lean-icon-security-camera': '&#xe929;',
		'lean-icon-settings': '&#xe92a;',
		'lean-icon-shape': '&#xe92b;',
		'lean-icon-timer': '&#xe92c;',
		'lean-icon-tv': '&#xe92d;',
		'lean-icon-un-check': '&#xe92e;',
		'lean-icon-visitor-management': '&#xe930;',
		'lean-icon-warning': '&#xe931;',
		'lean-icon-way-finder': '&#xe932;',
		'lean-icon-wheel-chair': '&#xe933;',
		'lean-icon-wifi': '&#xe934;',
		'lean-icon-zone': '&#xe935;',
		'lean-icon-desk-booking-1': '&#xe936;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/lean-icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
