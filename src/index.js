import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
//-- add Redux functionality to app
import { Provider } from "react-redux";
import configureStore from "./redux/configureStore";
//-- Function Call: creates Store with enahncers(middlewares)
const store = configureStore();
// ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(
  //-- Pass Store object to react-redux Provider component.This ensures that any time we connect to Redux in our app via react-redux connect, the store is available to our components.
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
