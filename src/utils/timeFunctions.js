export function getFromAndTo(date, time, duration) {
    let dateISO = date.toISOString().split("T")[0];
    let timeISO = new Date(time.setSeconds(0)).toISOString().split("T")[1];
    let fromDateTimeEpoch = new Date(dateISO + "T" + timeISO).getTime();
    let toDateTimeEpoch = fromDateTimeEpoch + (duration * 60 * 60 * 1000)
    console.log("TO DATE TIME EPOCH",toDateTimeEpoch);
    return {
      from: Math.ceil(fromDateTimeEpoch / 1000),
      to: Math.ceil(toDateTimeEpoch / 1000)
    }
  }
  export function getCustomDay(otherDate) {
    var d = new Date()
    let bool = false;
    console.log("getCustomDay", bool)
    bool = (d.toDateString() === otherDate.toDateString());
    console.log("getCustomDay", bool)
    if (bool) {
      return "Today"
    }
    console.log("getCustomDay", bool)
    bool = ((new Date(d.getTime() + 24 * 60 * 60
      * 1000)).toDateString() === otherDate.toDateString());
    console.log("getCustomDay", bool)
    if (bool) {
      return "Tom"
    }
    console.log("getCustomDay", bool)
    return getCustomDateUM(new Date(otherDate.getTime()))
  }
  export function getDateTimeDurationInDT(from, to) {
    let fromTime = new Date(from * 1000);
    let toTime = new Date(to * 1000);
    let date = fromTime;
    let time = fromTime;
    let duration = (to - from) / (60 * 60)
    return {
      date, time, duration
    }
  }
  export function getDateTimeDuration(from, to) {
    let fromTime = new Date(from * 1000);
    let toTime = new Date(to * 1000);
    let date = getCustomDate(fromTime);
    let time = getCustomTime(fromTime);
    let duration = getCustomTimeDiff(from, to)
    return {
      date, time, duration
    }
  }
  export function getCustomDate(date) {
    return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`;
  }
  function addPrefixZero(number) {
    if (number < 10) {
      return `0${number}`
    }
    return `${number}`
  }
  export function getCustomDateUM(date) {
    if (date.getDate() > 9) {
    }
    return `${addPrefixZero(date.getDate())}/${addPrefixZero(date.getMonth() + 1)}`;
  }
  export function getCustomTime(time) {
    let ampm = "am";
    var h = time.getHours();
    var m = time.getMinutes();
    // add a zero in front of numbers<10
    if (h >= 12) {
      ampm = "pm";
      h = h % 12 === 0 ? h : h % 12;
    }
    h = checkTime(h);
    m = checkTime(m);
    return `${h}:${m} ${ampm}`;
  }
  export function getCustomTimeDiff(from, to) {
    let diff = Math.abs(to - from);
    let hours = Math.floor(diff / (60 * 60));
    let h = checkTime(hours);
    let mins = diff / 60 - hours * 60;
    let m = checkTime(mins);
    return `${h}:${m}`;
  }
  function checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }
  let months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];