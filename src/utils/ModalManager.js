import React, { useContext } from "react";
import { ModalContext } from "./ModalContext";
import AddAppointmentInAModal from '../layouts/AddAppointment/AddAppointmentInAModal';
import AddAppointmentBookingModal from '../components/Modal/AddAppointmentBookingModal';
import MoreInfoModal from '../components/Modal/MoreInfoModal';
import CustomDate from '../components/Modal/CustomDate';
import CancelAppointmentInAModal from '../layouts/CancelAppointmentInAModal/CancelAppointmentInAModal';
import CancelledAppointmentModal from '../components/Modal/CancelledAppointmentModal';
import CancelledWithoutRoom from '../components/Modal/CancelledWithoutRoom';
import RejectVisitor from '../layouts/RejectVisitorInAModal/RejectVisitorInAModal';
import RejectedVisitor from '../components/Modal/RejectedVisitor';
import ReportVisitor from '../layouts/ReportVisitorInAModal/ReportVisitorInAModal';
import ReportedVisitor from '../components/Modal/ReportedVisitor';
import BlockedVisitor from '../components/Modal/BlockedVisitor';
import RemoveAsFrequentVisitor from '../components/Modal/RemoveAsFrequentVisitor';
import EachVisitorModal from '../components/Modal/myProfile/MyProfile';
import ApprovedVisitorModal from '../components/Modal/ApprovedVisitorModal';
import ApprovedVisitorRegistrationModal from '../components/Modal/ApproveVisitorRegistrationModal';
import RejectedVisitorRegistrationModal from '../components/Modal/RejectedVisitorRegistration';
import RescheduledAppointmentBooked from '../components/Modal/RescheduledAppointmentBooked';
import AddAsFreqVis from '../components/Modal/AddAsFrequentVisitor';
import RemoveAsFreq from '../components/Modal/RemoveAsFrequentVisitor';
import UnblockedVisitor from '../components/Modal/UnblockedVisitor';
import MyProfile from '../layouts/LeanSidebarWithNoti/MotherAppProfile/myProfile/MyProfile';
import VisitorInfoModal from '../components/Modal/visitor-reg-approve-reject/VisitorApproveReject';
import BlockUser from '../layouts/VmAdmin/VmSetting/Modals/BlockUser'
import StatusModals from "../layouts/VmAdmin/components/StatusModals/StatusModals"
import VisitorProfile from "../layouts/VmAdmin/VmSetting/Modals/myProfile/VisitorProfile"
import VMSLoaderModal from '../layouts/VmAdmin/VmSetting/Modals/VMSLoaderModal'

const Modals = {
  AddAppointmentInAModal,
  AddAppointmentBookingModal,
  MoreInfoModal,
  CustomDate,
  CancelAppointmentInAModal,
  CancelledAppointmentModal,
  ReportVisitor,
  RejectVisitor,
  RejectedVisitor,
  ReportedVisitor,
  BlockedVisitor,
  RemoveAsFrequentVisitor,
  EachVisitorModal,
  ApprovedVisitorModal,
  ApprovedVisitorRegistrationModal,
  RejectedVisitorRegistrationModal,
  RescheduledAppointmentBooked,
  AddAsFreqVis,
  RemoveAsFreq,
  UnblockedVisitor,
  CancelledWithoutRoom,
  MyProfile,
  VisitorInfoModal,
  BlockUser,
  StatusModals,
  VisitorProfile,
  VMSLoaderModal

};

const ModalManager = props => {
  console.log("ModalManager", props)
  const { currentModal, setCurrentModal } = useContext(ModalContext);
  const closeModal = () => setCurrentModal(null);

  if (currentModal) {
    const ModalComponent = Modals[currentModal.name];
    return <ModalComponent closeModal={closeModal} {...currentModal.props} />;
  }

  return null;
};

export default ModalManager;
