import { FETCH_REQUEST_CANCEL_APPT, FETCH_FAILURE_CANCEL_APPT,CANCEL_APPT } from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    isSuccess: true,
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_CANCEL_APPT:
            return { ...state, isLoading: true }; 
    
        case CANCEL_APPT:
            // console.log(state, aci);
            return {

                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
            };

       

        case FETCH_FAILURE_CANCEL_APPT:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
            };

        default:
            return state;
    }
}