import { FETCH_REQUEST_GET_ALL_MESSAGE, FETCH_FAILURE_GET_ALL_MESSAGE, GET_ALL_MESSAGE } from '../../actions/actionsConfig';


const initialState = {
    loading: true,
    data: {
        status:{},
        data: []
    },
    error: ''
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_GET_ALL_MESSAGE:
            return { ...state, loading: true };

        case 'SEND_MESSAGE':
            console.log("SEND MESSAGE ACTION",action)
            return {

                loading: false,
                data: [...state.data, action.payload],
                error: ''
            };

        case 'RECEIVE_MESSAGE':
            console.log("RECEIVE_MESSAGE ACTION",action)
            return {
                loading:false,
                data:[...state.data,action.payload],
                error:""
            }

        case GET_ALL_MESSAGE:
            console.log("GET_ALL_MESSAGE ACTION",action)
            return {

                loading: false,
                data: action.payload,
                error: ''
            };

        case FETCH_FAILURE_GET_ALL_MESSAGE:
            return {
                loading: false,
                data: [],
                error: action.payload
            };

        default:
            return state;
    }
}