import { FETCH_REQUEST_GET_VISITOR_DETAILS, FETCH_FAILURE_GET_VISITOR_DETAILS, GET_VISITOR_DETAILS } from '../../actions/actionsConfig';

// import { FETCH_REQUEST, FETCH_FAILURE  } from '../../actions/actionsConfig';

const initialState = {
    loading: true,
    data: {
        status:{},
        data:{}
    },
    error: ''
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_GET_VISITOR_DETAILS:
            return { ...state, loading: true };
        case GET_VISITOR_DETAILS:
            console.log("okokokokokokokoko",action.payload)
            if(action.payload.found==false){
                return {
                    loading: false,
                    data: [],
                    error: action.payload
                };
            }
            return{
                loading: false,
                data: action.payload,
                error: ""
            }

        case FETCH_FAILURE_GET_VISITOR_DETAILS:
            return {
                loading: false,
                data: [],
                error: action.payload
            };

        default:
            return state;
    }
}