import {
    FIRE_DRAWER
  } from "../../actions/actionsConfig";
  
  let initialState = {
    fireDrawer: false,
  };
  
  export default function(state = initialState, action) {
    switch (action.type) {
  
      case FIRE_DRAWER:
        return { ...state, ...action.payload };
     
      default:
        return state;
    }
  }
  