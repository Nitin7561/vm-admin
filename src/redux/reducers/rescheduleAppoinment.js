import {FETCH_REQUEST_RESCHEDULE_APPT, FETCH_FAILURE_RESCHEDULE_APPT,RESCHEDULE_APPT} from '../actions/actionsConfig';


const initialState = {
    isLoading: false,
    data:{
        status:{},
        data:[]
    },
    isSuccess: true,
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_RESCHEDULE_APPT:
            return { ...state, isLoading: true };    
        case RESCHEDULE_APPT:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };   
        case FETCH_FAILURE_RESCHEDULE_APPT:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };   

        default:
            return state;
    }
}