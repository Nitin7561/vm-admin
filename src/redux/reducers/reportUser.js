import { FETCH_REQUEST_REPORT_USER,FETCH_FAILURE_REPORT_USER,REPORT_USER} from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    isSuccess: true,
    error: '',
    isError: false  
}
 
export default function (state = initialState, action) {
    console.log(action)
    switch (action.type) {
        case FETCH_REQUEST_REPORT_USER:
            return { ...state, isLoading: true };          
        case REPORT_USER:
            return {
                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
              };     
        case FETCH_FAILURE_REPORT_USER:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}