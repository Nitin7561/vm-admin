import { combineReducers } from 'redux';
import getAllMessage from "./chatReducer/getAllMessage"
import getDetailForVisitor from "./chatReducer/getDetailForVisitor"
import reportUser from "./reportUser"
import rejectUser from "./rejectUser"
import cancelAppt from "./cancelAppt"
import getVisitorsListTable from './vmTable/getVisitorsListTable';
import getListOfAppointments from './vmTable/getListOfAppointments';
import getMyFrequentVisitors from './vmTable/getMyFrequestVisitors';
import addAppointment from './addAppointment';
import getAllAvailableRooms from './getAllAvailableRoomsReducer';
import getAllBookedRooms from './getAllBookedRooms';
import verifyOtp from './mobileVerification/verifyOtp'
import sendOtp from './mobileVerification/sendOtp'
import getAllUpcomingEventsFromSidebarApp from './upcomingEventsSidebarApp';
import getVisitorChatListSideBar from './getVisitorChatList';
import getUpcomingHostsWithAParticularVisitor from './getUpcomingHostsWithAParticularVisitor';
// import getApplications from './getApplications';
// import getAllNotifications from './notifications/getAllNotifications';
import rescheduleAppointment from './rescheduleAppoinment';
// import user from './user';
import refresher from './refresher';
import getAllEnums from './getAllEnums'
import approveVisitorAppointment from './approveVisitorAppointment';
import approveVisitorRegistration from './approveVisitorRegistration';
import rejectUseRegistration from './rejectUserRegistration';
import blockUser from './blockUser';
import unblockUser from './unblockUser';
import addToFrequentVisitor from './addToFrequentVisitors';
import removeFrequentVisitor from './removeFrequentVisitors'
import replyNotification from '../../layouts/LeanSidebarWithNoti/LeanSideBarContainer/reducers/notifications/replyNotification';
import getAllNotificationsReducer from '../../layouts/LeanSidebarWithNoti/LeanSideBarContainer/reducers/notifications/getAllNotifications';
import getApplicationReducer from '../../layouts/LeanSidebarWithNoti/LeanSideBarContainer/reducers/staticData/getApplications';
import userReducer from '../../layouts/LeanSidebarWithNoti/LeanSideBarContainer/reducers/user';
import fireDrawer from '../reducers/chatReducer/fireDrawer';
import appCookieRecv from '../reducers/appCookieRecv';
import getParticularVisitorInfo from './getParticularVisitorInfo';
import getTenantReducer from '../../layouts/LeanSidebarWithNoti/LeanSideBarContainer/reducers/staticData/getTenantInfo';


//VM ADMIN PART
import totalHost from '../../layouts/VmAdmin/redux/reducers/cardReducer/totalHost'
import totalAppt from '../../layouts/VmAdmin/redux/reducers/cardReducer/totalAppt'
import totalVisitor from '../../layouts/VmAdmin/redux/reducers/cardReducer/totalVisitor'
import totalNotCheckOut from '../../layouts/VmAdmin/redux/reducers/cardReducer/totalNotCheckOut'
import totalOverStayed from '../../layouts/VmAdmin/redux/reducers/cardReducer/totalOverStayed'
import totalVisitorTrend from '../../layouts/VmAdmin/redux/reducers/cardReducer/totalVisitorTrend'
import totalBlocked from '../../layouts/VmAdmin/redux/reducers/cardReducer/totalBlocked'
import totalRegistered from '../../layouts/VmAdmin/redux/reducers/cardReducer/totalRegistered'
import visitorCycle from '../../layouts/VmAdmin/redux/reducers/graphReducer/visitorCycle'
import visitorStatus from '../../layouts/VmAdmin/redux/reducers/graphReducer/visitorStatus'
import visitorStayed from '../../layouts/VmAdmin/redux/reducers/graphReducer/visitorStayed'
import visitorTablet from '../../layouts/VmAdmin/redux/reducers/graphReducer/visitorTablet'
import vmsetting from '../../layouts/VmAdmin/VmSetting/Redux/reducers/vmsetting'

const allReducers = combineReducers({

    getAllMessageReducer:getAllMessage,
    getDetailForVisitorReducer:getDetailForVisitor,
    reportUserReducer:reportUser,
    rejectUserReducer:rejectUser,
    cancelAppt,
    getVisitorsListTableReducer : getVisitorsListTable,
    getListOfAppointmentsReducer : getListOfAppointments,
    getMyFrequentVisitorsReducer : getMyFrequentVisitors,
    getAddAppointmentReducer : addAppointment,
    getAllAvailableRoomsReducer : getAllAvailableRooms,
    getAllBookedRoomsReducer : getAllBookedRooms,
    getAllUpcomingEventsFromSidebarAppReducer : getAllUpcomingEventsFromSidebarApp,
    getVisitorChatListSideBarReducer : getVisitorChatListSideBar,
    getUpcomingHostsWithAParticularVisitorReducer : getUpcomingHostsWithAParticularVisitor,
    // getApplicationReducer : getApplications,
    // getAllNotificationsReducer : getAllNotifications,
    // userReducer : user,
    getVerifyOtpReducer:verifyOtp,
    sendOtpReducer:sendOtp,
    rescheduleAppointmentReducer : rescheduleAppointment,
    refresherReducer : refresher,
    getAllEnumsReducer : getAllEnums,
    approveVisitorAppointmentReducer : approveVisitorAppointment,
    approveVisitorRegistrationReducer: approveVisitorRegistration,
    rejectUseRegistrationReducer:rejectUseRegistration,
    blockUserReducer:blockUser,
    unblockUserReducer:unblockUser,
    addToFrequentVisitorReducer:addToFrequentVisitor,
    removeFrequentVisitorReducer:removeFrequentVisitor,
    userReducer, 
    getAllNotificationsReducer, 
    getApplicationReducer,
    fireDrawerReducer:fireDrawer,
    appCookieRecv,
    getParticularVisitorInfoReducer:getParticularVisitorInfo,
    replyNotification,
    getTenantReducer,
    totalHost,
    totalAppt,
    totalVisitor,
    totalNotCheckOut,
    totalOverStayed,
    totalVisitorTrend,
    totalBlocked,
    totalRegistered,
    visitorCycle,
    visitorStatus,
    visitorStayed,
    visitorTablet,
    vmsetting
});

const rootReducer = (state, action) => {
    if (action.type === 'CLEAR_STORE') {
        state = {}
    }
    return allReducers(state, action)
}

export default rootReducer