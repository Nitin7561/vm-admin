import {FETCH_REQUEST_GET_ALL_ENUMS, FETCH_FAILURE_GET_ALL_ENUMS,GET_ALL_ENUMS} from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data:{
        
        "reportUserEnum": [
           
        ],
        "blockChatUser": [
           
        ],
        "rejectApptUser": [
          
        ],
        "cancelApptEnum": [
            
        ]
    },
    isSuccess: true,
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_GET_ALL_ENUMS:
            return { ...state, isLoading: true }; 
    
        case GET_ALL_ENUMS:
            console.log("Data from Reducer",action.payload);
            return {

                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
            
            };

       

        case FETCH_FAILURE_GET_ALL_ENUMS:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
            };

        default:
            return state;
    }
}