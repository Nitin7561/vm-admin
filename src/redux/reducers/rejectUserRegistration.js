import {  FETCH_REQUEST_REJECT_USER_REG, FETCH_FAILURE_REJECT_USER_REG, REJECT_USER_REG  } from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    isSuccess: true,
    error: '',
    isError: false  
}

export default function (state = initialState, action) {
    console.log(action)
    switch (action.type) {
        case FETCH_REQUEST_REJECT_USER_REG:
            return { ...state, isLoading: true };          
        case REJECT_USER_REG:
            return {
                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
              };     
        case FETCH_FAILURE_REJECT_USER_REG:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}