import { FETCH_REQUEST, FETCH_FAILURE, VERIFY_OTP  } from '../../actions/actionsConfig';

const initialState = {
    loading: true,
    data: [],
    error: ''
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST:
            return { ...state, loading: true };
        case VERIFY_OTP:
            console.log("VERIFY OTP",action.payload)
            if(action.payload.found==false){
                return {
                    loading: false,
                    data: [],
                    error: action.payload
                };
            }
            return{
                loading: false,
                data: [action.payload],
                error: ''
            }

        case FETCH_FAILURE:
            return {
                loading: false,
                data: [],
                error: action.payload
            };

        default:
            return state;
    }
}