import { FETCH_REQUEST, FETCH_FAILURE, SEND_OTP  } from '../../actions/actionsConfig';

const initialState = {
    loading: true,
    data: [],
    session_id:"",
    error: ''
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST:
            return { ...state, loading: true };
        case SEND_OTP:
            console.log("send OTP ------------------",action.payload)
            return{
                loading: false,
                data: [action.payload],
                session_id:action.payload.Details,
                error: ''
            }

        case FETCH_FAILURE:
            return {
                loading: false,
                data: [],
                error: action.payload
            };

        default:
            return state;
    }
}