import { FETCH_REQUEST_APPOINTMENT,FETCH_FAILURE_APPOINTMENT,ADD_APPOINTMENT} from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    isSuccess: true,
    error: '',
    isError: false  
}
 
export default function (state = initialState, action) {
    console.log(action)
    switch (action.type) {
        case FETCH_REQUEST_APPOINTMENT:
            return { ...state, isLoading: true };          
        case ADD_APPOINTMENT:
            return {
                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
              };     
        case FETCH_FAILURE_APPOINTMENT:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}