import { FETCH_REQUEST_REMOVE_FREQUENT_VISITORS, FETCH_FAILURE_REMOVE_FREQUENT_VISITORS,REMOVE_FREQUENT_VISITORS } from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    isSuccess: true,
    error: '',
    isError: false  
}
 
export default function (state = initialState, action) {
    console.log(action)
    switch (action.type) {
        case FETCH_REQUEST_REMOVE_FREQUENT_VISITORS:
            return { ...state, isLoading: true };          
        case REMOVE_FREQUENT_VISITORS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
              };     
        case FETCH_FAILURE_REMOVE_FREQUENT_VISITORS:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}