import { FETCH_REQUEST_GET_ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP,FETCH_FAILURE_GET_ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP,ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP} from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    isSuccess: true,
    error: '',
    isError: false  
}
 
export default function (state = initialState, action) {
    console.log(action)
    switch (action.type) {
        case FETCH_REQUEST_GET_ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP:
            return { ...state, isLoading: true };          
        case ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP:
            return {
                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
              };     
        case FETCH_FAILURE_GET_ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}