import {FETCH_REQUEST_GET_VISITOR_CHAT_LIST, FETCH_FAILURE_GET_VISITOR_CHAT_LIST,GET_VISITOR_CHAT_LIST} from '../actions/actionsConfig';


const initialState = {
    isLoading: true,
    data:{
        status:{},
        data:{
            status:{},
            data:[]
        }
    },
    isSuccess: true,
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_GET_VISITOR_CHAT_LIST:
            return { ...state, isLoading: true };    
        case GET_VISITOR_CHAT_LIST:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
              };   
        case FETCH_FAILURE_GET_VISITOR_CHAT_LIST:
            return {
                ...state,
                isError : true,
                isLoading: false,
                error: action.payload
              };   

        default:
            return state;
    }
}