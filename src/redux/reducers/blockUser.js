import { FETCH_REQUEST_BLOCK_USER, FETCH_FAILURE_BLOCK_USER, BLOCK_USER  } from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data:{},
    isSuccess: true,
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_BLOCK_USER:
            return { ...state, isLoading: true }; 
    
        case BLOCK_USER:
            // console.log(state, aci);
            return {

                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
            };

       

        case FETCH_FAILURE_BLOCK_USER:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
            };

        default:
            return state;
    }
}