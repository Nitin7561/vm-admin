import {FETCH_REQUEST_GET_PART_VISITOR, FETCH_FAILURE_GET_PART_VISITOR,PART_VISITOR} from '../actions/actionsConfig';

const initialState = {
    isLoading: true,
    data:{
        // status:{},
        // data:{}
    },
    isSuccess: true,
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {

        case FETCH_REQUEST_GET_PART_VISITOR:
            return { ...state, isLoading: true }; 
    
        case PART_VISITOR:
            console.log("Data from Reducer",action.payload);
            return {

                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
            
            };

        case FETCH_FAILURE_GET_PART_VISITOR:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
            };

        default:
            return state;
    }
}