import {
    REFRESH_PAGE
  } from "../actions/actionsConfig";
  
  let initialState = {
    myAppointments: true,
    cancelledAppointment: true,
    rejectedVisitor:true,
    reportedVisitor:true,
    reportedVisitorReg:true,
    blockedUser:true,
    unblockedUser:true,
    addAsFreqVisitor:true,
    removeAsFreqVisitor:true
  };
  
  export default function(state = initialState, action) {
    switch (action.type) {
  
      case REFRESH_PAGE:
        return { ...state, ...action.payload };
     
      default:
        return state;
    }
  }
  