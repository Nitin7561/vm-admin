import { FETCH_REQUEST_GET_ALL_BOOKED_MEETING_ROOMS, FETCH_FAILURE_GET_ALL_BOOKED_MEETING_ROOMS,GET_ALL_BOOKED_MEETING_ROOMS } from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data:{
        'data':[]
    },
    isSuccess: true,
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_GET_ALL_BOOKED_MEETING_ROOMS:
            return { ...state, isLoading: true }; 
    
        case GET_ALL_BOOKED_MEETING_ROOMS:
            // console.log(state, aci);
            return {

                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
            };

       

        case FETCH_FAILURE_GET_ALL_BOOKED_MEETING_ROOMS:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
            };

        default:
            return state;
    }
}