import { FETCH_REQUEST_APPROVE_VISITOR, FETCH_FAILURE_APPROVE_VISITOR, APPROVE_VISITOR  } from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data:{},
    isSuccess: true,
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_APPROVE_VISITOR:
            return { ...state, isLoading: true }; 
    
        case APPROVE_VISITOR:
            // console.log(state, aci);
            return {

                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
            };

       

        case FETCH_FAILURE_APPROVE_VISITOR:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
            };

        default:
            return state;
    }
}