import { FETCH_REQUEST_GET_MY_APPOINTMENTS, FETCH_FAILURE_GET_MY_APPOINTMENTS,GET_MY_APPOINTMENTS} from '../../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: {
        status:{},
        data:{
            total:0,
            data:[]
        }
       
    },
    isSuccess: true,
    error: '',
    isError: false  
}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_REQUEST_GET_MY_APPOINTMENTS:
            return { ...state, isLoading: true };    
        case GET_MY_APPOINTMENTS:
            return { ...initialState, 
                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload, 
            };    
        case FETCH_FAILURE_GET_MY_APPOINTMENTS:
            return { ...initialState, 
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
            };     

        default:
            return state;
    }
}