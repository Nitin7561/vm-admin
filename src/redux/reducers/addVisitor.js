import { FETCH_REQUEST,FETCH_FAILURE,ADD_VISITOR} from '../actions/actionsConfig';

const initialState = {
    isLoading: false,
    data: [],
    isSuccess: true,
    error: '',
    isError: false  
}
 
export default function (state = initialState, action) {
    console.log(action)
    switch (action.type) {
        case FETCH_REQUEST:
            return { ...state, isLoading: true };          
        case ADD_VISITOR:
            return {
                ...state,
                isLoading: false,
                isError: false,
                isSuccess : true,
                data: action.payload,
              };     
        case FETCH_FAILURE:
            return {
                ...state,
                isError : true,
                isSuccess : false,
                isLoading: false,
                error: action.payload
              };    

        default:
            return state;
    }
}