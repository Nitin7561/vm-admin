import {
    SET_ALL_APPLICATIONS,
    VERIFY_APP_REQUEST,
    VERIFY_APP_RECIEVE,
    VERIFY_APP_FAILURE,
    VERIFY_APP_FAILURE_ALL,
    VERIFY_APP_RECIEVE_ALL
} from "../actions/actionsConfig";
const initialState = {
    isLoading: true,
    allVerified: false,
    apps: {
    },
    count: 0,
    isError: false,
};
export default function (state = initialState, action) {
    switch (action.type) {
        case VERIFY_APP_REQUEST:
            return { ...state, isLoading: true, isError: false, count: 0 };
        case VERIFY_APP_RECIEVE_ALL:
            return { ...state, isLoading: false, count: state.count + 1 };
        case VERIFY_APP_FAILURE:
            return {
                ...state,
                apps: { ...state.apps, ...action.payload }
            };
        case VERIFY_APP_RECIEVE:
            return {
                ...state,
                apps: { ...state.apps, ...action.payload }
            };
        case VERIFY_APP_FAILURE_ALL:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case SET_ALL_APPLICATIONS:
            return { ...state, apps: action.payload };
        // case UPDATE_AVAILABLE_ROOMS_RECIEVE:
        // return { ...state, isLoading: false, isError: false, isSuccess:true, ...action.payload };
        // case UPDATE_AVAILABLE_ROOMS_REQUEST:
        //   return { ...state, isLoading: true };
        default:
            return state;
    }
}