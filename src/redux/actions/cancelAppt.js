import { FETCH_REQUEST_CANCEL_APPT, FETCH_FAILURE_CANCEL_APPT, CANCEL_APPT  } from '../actions/actionsConfig';
import {cancelAppointmentEndPoint} from './apiUrls';
//-- Get Room settings
export const cancelAppt = (reqBody) => {

    //  console.log("REQ BODY-->",reqBody);
    return dispatch => {
        dispatch({ type: FETCH_REQUEST_CANCEL_APPT });
        //TODO NEW API
        fetch(cancelAppointmentEndPoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqBody) 
        })
        .then(res =>  res.json().then(data => ({status: res.status, body: data})))
        .then(resp => {
        console.log("data",resp.status);
        var status = resp.status
        var data = resp.body;
        if (status != 200 ) {          
            dispatch({
            type: FETCH_FAILURE_CANCEL_APPT,
            payload: data 
            });
        }
        else {
            dispatch({
            type: CANCEL_APPT,
            payload: data
            });
        }        
        })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: FETCH_FAILURE_CANCEL_APPT,
                    payload: error
                });

            });
    };
};








