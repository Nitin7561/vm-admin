import {FETCH_REQUEST_REMOVE_FREQUENT_VISITORS, FETCH_FAILURE_REMOVE_FREQUENT_VISITORS,REMOVE_FREQUENT_VISITORS} from './actionsConfig';
import {removeFrequentVisitorEndPoint} from './apiUrls'

export const removeFrequentVisitor = (reqBody) => {

    console.log(reqBody);

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_REMOVE_FREQUENT_VISITORS });
      fetch(removeFrequentVisitorEndPoint, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(reqBody)      
      })
        .then(res =>  res.json().then(data => ({status: res.status, body: data})))
        .then(resp => {
          console.log("data",resp.status);
          var status = resp.status
          var data = resp.body;
          if (status != 200 ) {          
            dispatch({
              type: FETCH_FAILURE_REMOVE_FREQUENT_VISITORS,
              payload: data 
            });
          }
          else {
            dispatch({
              type: REMOVE_FREQUENT_VISITORS,
              payload: data
            });
          }        
        })
        .catch(error =>{
          console.log("Add App failed",error)
          dispatch({
          type: FETCH_FAILURE_REMOVE_FREQUENT_VISITORS,
          payload: error
        });
        
      });
    };
  };