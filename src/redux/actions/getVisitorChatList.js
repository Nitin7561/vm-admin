import {FETCH_REQUEST_GET_VISITOR_CHAT_LIST, FETCH_FAILURE_GET_VISITOR_CHAT_LIST,GET_VISITOR_CHAT_LIST} from './actionsConfig';
import {getVmChatList} from './apiUrls';

//-- Get Room settings
export const getVisitorChatList = (reqBody) => {

    console.log(reqBody);

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_GET_VISITOR_CHAT_LIST });
      fetch(getVmChatList, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(reqBody)      
      })
      .then(res =>  res.json().then(data => ({status: res.status, body: data})))
      .then(resp => {
      console.log("data",resp.status);
      var status = resp.status
      var data = resp.body;
      if (status != 200 ) {          
          dispatch({
          type: FETCH_FAILURE_GET_VISITOR_CHAT_LIST,
          payload: data 
          });
      }
      else {
          dispatch({
          type: GET_VISITOR_CHAT_LIST,
          payload: data
          });
      }        
      })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_VISITOR_CHAT_LIST,
          payload: error
        });
        
      });
    };
  };