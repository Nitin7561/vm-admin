import { FETCH_REQUEST_REJECT_USER_REG, FETCH_FAILURE_REJECT_USER_REG, REJECT_USER_REG  } from '../actions/actionsConfig';
import {rejectUserRegistrationEndPoint} from './apiUrls';

//-- Get Room settings
export const rejectUserRegistration = (reqBody) => {

    return dispatch => {
        dispatch({ type: FETCH_REQUEST_REJECT_USER_REG });
        fetch(rejectUserRegistrationEndPoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqBody)
        })
        .then(res =>  res.json().then(data => ({status: res.status, body: data})))
        .then(resp => {
        console.log("data",resp.status);
        var status = resp.status
        var data = resp.body;
        if (status != 200 ) {          
            dispatch({
            type: FETCH_FAILURE_REJECT_USER_REG,
            payload: data 
            });
        }
        else {
            dispatch({
            type: REJECT_USER_REG,
            payload: data
            });
        }        
        })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: FETCH_FAILURE_REJECT_USER_REG,
                    payload: error
                });

            });
    };
};








