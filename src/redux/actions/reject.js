import { FETCH_REQUEST_REJECT_USER, FETCH_FAILURE_REJECT_USER, REJECT_USER  } from '../actions/actionsConfig';
import {rejectUserEndPoint} from './apiUrls';

//-- Get Room settings
export const rejectUser = (reqBody) => {

    return dispatch => {
        dispatch({ type: FETCH_REQUEST_REJECT_USER });
        fetch(rejectUserEndPoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqBody)
        })
        .then(res =>  res.json().then(data => ({status: res.status, body: data})))
        .then(resp => {
        console.log("data",resp.status);
        var status = resp.status
        var data = resp.body;
        if (status != 200 ) {          
            dispatch({
            type: FETCH_FAILURE_REJECT_USER,
            payload: data 
            });
        }
        else {
            dispatch({
            type: REJECT_USER,
            payload: data
            });
        }        
        })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: FETCH_FAILURE_REJECT_USER,
                    payload: error
                });

            });
    };
};








