import {FETCH_REQUEST_APPOINTMENT, FETCH_FAILURE_APPOINTMENT,ADD_APPOINTMENT} from './actionsConfig';
import {addAppointment} from './apiUrls';
//-- Get Room settings
export const getAddAppointment = (reqBody) => {

    console.log(reqBody);

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_APPOINTMENT });
      fetch(addAppointment, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(reqBody)      
      })
        .then(res =>  res.json().then(data => ({status: res.status, body: data})))
        .then(resp => {
          console.log("data",resp.status);
          var status = resp.status
          var data = resp.body;
          if (status != 200 ) {          
            dispatch({
              type: FETCH_FAILURE_APPOINTMENT,
              payload: data 
            });
          }
          else {
            dispatch({
              type: ADD_APPOINTMENT,
              payload: data
            });
          }        
        })
        .catch(error =>{
          console.log("Add App failed",error)
          dispatch({
          type: FETCH_FAILURE_APPOINTMENT,
          payload: error
        });
        
      });
    };
  };