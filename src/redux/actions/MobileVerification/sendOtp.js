import { FETCH_REQUEST, FETCH_FAILURE, SEND_OTP } from '../../actions/actionsConfig';

//-- Get Room settings
export const sendOtp = (reqBody) => {

    //  console.log("REQ BODY-->",reqBody);

    return dispatch => {
        dispatch({ type: FETCH_REQUEST });
        //TODO get UserId from reqBody
        console.log('kkkkkkkkkkkkkkkkkkkk',reqBody)
        fetch("https://fe.leantron.dev:3003/api/ver1.2/visitor/sendOtp", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",              
            },
            body:JSON.stringify(reqBody)
            
        })
            .then(resp => resp.json())
            .then(data => {
                console.log("----------------OTP sending RESULT---------------->",data);

                   if(data.data.Status=="Success"){
                       dispatch({
                           type: SEND_OTP,
                           payload: data.data,
                       });

                   }
                   else{
                    dispatch({
                        type: FETCH_FAILURE,
                        payload: data.data.Status
                    });
                   }
                   
            })
            .catch(error => {
                console.log("----------------error-----------------",error)
                dispatch({
                    type: FETCH_FAILURE,
                    payload: error
                });

            });
    };
};





