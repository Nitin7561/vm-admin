import { FETCH_REQUEST, FETCH_FAILURE, VERIFY_OTP } from '../../actions/actionsConfig';

//-- Get Room settings
export const verifyOtp = (reqBody) => {

    //  console.log("REQ BODY-->",reqBody);

    return dispatch => {
        dispatch({ type: FETCH_REQUEST });
        //TODO get UserId from reqBody
        // console.log('kkkkkkkkkkkkkkkkkkkk',reqBody.visitorSub)
        // https://fe.leantron.dev:3003/api/ver1.2/appointment/verifyPin
        console.log("inside actions of verifyOtp",reqBody)
        fetch("https://fe.leantron.dev:3003/api/ver1.2/visitor/verifyOtp", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body:JSON.stringify(reqBody)
            
        })
            .then(resp => resp.json())
            .then(data => {
                   console.log("----------------OTP VERIFICATION RESULT---------------->",data);
                   if(data.data="Otp Not Matched"){
                    dispatch({
                        type: FETCH_FAILURE,
                        payload: "otp not Matched"
                    });
                   }
                   else{
                       dispatch({
                           type: VERIFY_OTP,
                           payload: data,
                       });

                   }



            })
            .catch(error => {
                console.log("----------------error-----------------",error)
                dispatch({
                    type: FETCH_FAILURE,
                    payload: error
                });

            });
    };
};





