import {FETCH_REQUEST_ADD_FREQUENT_VISITORS, FETCH_FAILURE_ADD_FREQUENT_VISITORS,ADD_FREQUENT_VISITORS} from './actionsConfig';
import {addFrequentVisitorEndPoint} from './apiUrls'

export const addToFrequentVisitor = (reqBody) => {

    console.log(reqBody);

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_ADD_FREQUENT_VISITORS });
      fetch(addFrequentVisitorEndPoint, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(reqBody)      
      })
        .then(res =>  res.json().then(data => ({status: res.status, body: data})))
        .then(resp => {
          console.log("data",resp.status);
          var status = resp.status
          var data = resp.body;
          if (status != 200 ) {          
            dispatch({
              type: FETCH_FAILURE_ADD_FREQUENT_VISITORS,
              payload: data 
            });
          }
          else {
            dispatch({
              type: ADD_FREQUENT_VISITORS,
              payload: data
            });
          }        
        })
        .catch(error =>{
          console.log("Add App failed",error)
          dispatch({
          type: FETCH_FAILURE_ADD_FREQUENT_VISITORS,
          payload: error
        });
        
      });
    };
  };