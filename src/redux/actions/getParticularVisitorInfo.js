import {FETCH_REQUEST_GET_PART_VISITOR, FETCH_FAILURE_GET_PART_VISITOR,PART_VISITOR} from './actionsConfig';
import {getParticularVisitorInfoEndPoint} from './apiUrls';

//-- Get Room settings
export const getParticularVisitorInfo = (reqBody) => {

    // console.log(reqBody);

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_GET_PART_VISITOR });
      let url = `${getParticularVisitorInfoEndPoint}?encryptedId=${reqBody.visitorId}`;
      console.log("Url from particular visitor inf0===>",url);
      fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
      })
      .then(resp => resp.json())
      .then(data => {
          console.log("data",data);
          dispatch({
            type:PART_VISITOR,
            payload: data.data
          });
        })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_PART_VISITOR,
          payload: error
        });
        
      });
    };
  };