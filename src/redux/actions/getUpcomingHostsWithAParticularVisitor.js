import {FETCH_REQUEST_GET_APPT_BETWEEN_HOST_AND_VISITOR, FETCH_FAILURE_GET_APPT_BETWEEN_HOST_AND_VISITOR,GET_APPT_BETWEEN_HOST_AND_VISITOR} from './actionsConfig';
import {getApptBetwHostAndVisitorEndPoint} from './apiUrls';


export const getUpcomingHostsWithAParticularVisitor  = (reqBody) => {

    console.log(reqBody)

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_GET_APPT_BETWEEN_HOST_AND_VISITOR });
      let url = `${getApptBetwHostAndVisitorEndPoint}?visitorSub=${reqBody.visitorSub}`;
      
      fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" },
        // body: JSON.stringify(reqBody)      
      })
      .then(res =>  res.json().then(data => ({status: res.status, body: data})))
      .then(resp => {
      console.log("data",resp.status);
      var status = resp.status
      var data = resp.body;
      if (status != 200 ) {          
          dispatch({
          type: FETCH_FAILURE_GET_APPT_BETWEEN_HOST_AND_VISITOR,
          payload: data 
          });
      }
      else {
          dispatch({
          type: GET_APPT_BETWEEN_HOST_AND_VISITOR,
          payload: data
          });
      }        
      })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_APPT_BETWEEN_HOST_AND_VISITOR,
          payload: error
        });
        
      });
    };
  };