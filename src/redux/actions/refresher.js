import {
    REFRESH_PAGE
} from "./actionsConfig";


export function refresher(page) {
    return (dispatch, getState) => {
        let { refresherReducer:refresherPage } = getState();
        console.log("REFRESHER PAGE",refresherPage);
        let newValue = refresherPage[page];

        dispatch({
            type: REFRESH_PAGE,
            payload : {
                [page]: !newValue,
            }
        })
    }
} 