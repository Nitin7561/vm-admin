import {FETCH_REQUEST_GET_VISITORS_LIST_TABLE, FETCH_FAILURE_GET_VISITORS_LIST_TABLE,GET_VISITORS_LIST_TABLE} from './actionsConfig';
import { getAllVisitorsByHostEndPoint } from './apiUrls'

export const getVisitorsListTableAction = (reqBody) => {

    console.log("REQ BODY====>",reqBody)

    return dispatch => {
      let url;
      dispatch({ type: FETCH_REQUEST_GET_VISITORS_LIST_TABLE });
      if(reqBody.status === undefined)
      {
        url = `${getAllVisitorsByHostEndPoint}?from=${reqBody.from}&to=${reqBody.to}&page=${reqBody.page}&offset=${reqBody.offset}&order=${reqBody.order}&sortBy=${reqBody.sortBy}`; 
        console.log("Url",url)
      }
      else
      {
        url = `${getAllVisitorsByHostEndPoint}?from=${reqBody.from}&to=${reqBody.to}&page=${reqBody.page}&offset=${reqBody.offset}&order=${reqBody.order}&sortBy=${reqBody.sortBy}&status=${reqBody.status}`; 
        console.log("Url",url)
      }
      
      fetch(url,{
        method: "GET",
        headers: { "Content-Type": "application/json" },     
      })
      .then(res =>  res.json().then(data => ({status: res.status, body: data})))
      .then(resp => {
      console.log("data",resp.status);
      var status = resp.status
      var data = resp.body;
      if (status != 200 ) {          
          dispatch({
          type: FETCH_FAILURE_GET_VISITORS_LIST_TABLE,
          payload: data 
          });
      }
      else {
          dispatch({
          type: GET_VISITORS_LIST_TABLE,
          payload: data
          });
      }        
      })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_VISITORS_LIST_TABLE,
          payload: error
        });
        
      });
    };
  };