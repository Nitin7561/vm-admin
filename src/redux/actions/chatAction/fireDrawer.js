import {
    FIRE_DRAWER
} from "../actionsConfig";


export function fireDrawer(flag) {
    return (dispatch, getState) => {
        let { fireDrawerReducer } = getState();
        console.log("FIREDRAWER PAGE",fireDrawerReducer);
        let newValue = fireDrawerReducer[flag];

        dispatch({
            type: FIRE_DRAWER,
            payload : {
                [flag]: !newValue,
            }
        })
    }
} 