import { FETCH_REQUEST_GET_VISITOR_DETAILS, FETCH_FAILURE_GET_VISITOR_DETAILS, GET_VISITOR_DETAILS } from '../../actions/actionsConfig';
import {getAllDetailMessageEndPoint} from '../apiUrls';

//-- Get Room settings
export const getDetailForVisitor = (reqBody) => {

    //  console.log("REQ BODY-->",reqBody);

    return dispatch => {
        dispatch({ type: FETCH_REQUEST_GET_VISITOR_DETAILS });
            let url = `${getAllDetailMessageEndPoint}?id=${reqBody.visitorSub}&findById=true`;
            console.log("GET DETAILS FOR VISITOR",url)
            fetch(url, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer mybearertoken_2_leantron_auth_to"
                },
                
            })
                .then(resp => resp.json())
                .then(data => {
                       console.log("----------------visitorDetails---------------->",data);
                    dispatch({
                        type: GET_VISITOR_DETAILS,
                        payload: data.data,
                    });
                })
                .catch(error => {
                    console.log("----------------error-----------------",error)
                    dispatch({
                        type: FETCH_FAILURE_GET_VISITOR_DETAILS,
                        payload: error
                    });
    
                });
        
      
    };
};





