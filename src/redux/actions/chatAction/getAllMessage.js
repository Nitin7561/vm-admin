import { FETCH_REQUEST_GET_ALL_MESSAGE, FETCH_FAILURE_GET_ALL_MESSAGE, GET_ALL_MESSAGE } from '../../actions/actionsConfig';
import {getAllMessageEndPoint,sendMessageEndPoint} from '../apiUrls';

//-- Get Room settings
export const getAllMessage = (reqBody) => {

    //  console.log("REQ BODY-->",reqBody);
    return dispatch => {
        dispatch({ type: FETCH_REQUEST_GET_ALL_MESSAGE });
        let url = `${getAllMessageEndPoint}?id=${reqBody.id}`
        console.log("***&^&^^&^&",url)
        fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            },
            // body: JSON.stringify(reqBody)
        })
            .then(resp => resp.json())
            .then(data => {
                console.log("TEST dispatch", data);
                dispatch({
                    type: GET_ALL_MESSAGE,
                    payload: data.data
                });
            })
            
            .catch(error => {
                console.log("TEST dispatch", error)
                dispatch({
                    type: FETCH_FAILURE_GET_ALL_MESSAGE,
                    payload: error
                });

            });
    };
};


// export const sendMessageJustOnChatCloud = (message, msg) => {
//     return dispatch => {

//         console.log("Message SENT",message)
//         console.log("Message SENT 2",msg)

//         dispatch({ type: 'SEND_MESSAGE', payload: { message, timestamp: (new Date()).getTime(), sender: 'host' } });
       
//         // fetch(sendMessageEndPoint, {
//         //   method: "POST",
//         //   headers:{
//         //       "Content-Type":"application/json"
//         //   },
//         //   body: JSON.stringify(msg)
//         // })
//         //   .then(resp => resp.json())
//         //   .then(data => {
//         //     console.log("---------------Data----------------->", data);
//         //   })
//         //   .catch(error =>{
//         //     console.log(error)
//         //     dispatch({
//         //     type: FETCH_FAILURE_GET_ALL_MESSAGE,
//         //     payload: error
//         //   });

//         // });
//     }
// }

export const sendMessage = (message, msg) => {
    return dispatch => {

        console.log("Message SENT",message)
        console.log("Message SENT 2",msg)
        dispatch({ type: 'SEND_MESSAGE', payload: { message, timestamp: (new Date()).getTime(), sender: 'host' } });
        ///
        // dispatch({ type: FETCH_REQUEST });
        fetch(sendMessageEndPoint, {
          method: "POST",
          headers:{
              "Content-Type":"application/json"
          },
          body: JSON.stringify(msg)
        })
          .then(resp => resp.json())
          .then(data => {
            console.log("---------------Data----------------->", data);
          })
          .catch(error =>{
            console.log(error)
            dispatch({
            type: FETCH_FAILURE_GET_ALL_MESSAGE,
            payload: error
          });

        });
    }
}

export const receiveMessage = (message,visitorSub)=>{

    return (dispatch, getState) => {

        let { getDetailForVisitorReducer } = getState();

        let { isLoading: isLoadingVisitorDetails, data: visitorDetails, error:visitorDetailsError, isError: isErrorVisitorDetailsError } = getDetailForVisitorReducer;

        console.log("VISITOR DETAILS==>",visitorDetails);
        
        var visitorSubFromReducer = visitorDetails.sub;

        if(visitorSubFromReducer === visitorSub)
        {
            dispatch({ type: 'RECEIVE_MESSAGE', payload: { message, timestamp: (new Date()).getTime(), sender: 'visitor' } });
        }
        else
        {
            console.log("Visitor is someone else, dont append data");
        }

    }

}






