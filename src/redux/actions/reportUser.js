import { FETCH_REQUEST_REPORT_USER, FETCH_FAILURE_REPORT_USER, REPORT_USER  } from '../actions/actionsConfig';
import {reportVisitor} from './apiUrls'

export const reportUser = (reqBody) => {

    return dispatch => {
        dispatch({ type: FETCH_REQUEST_REPORT_USER });
        fetch(reportVisitor, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqBody) 
        })
        .then(res =>  res.json().then(data => ({status: res.status, body: data})))
        .then(resp => {
        console.log("data",resp.status);
        var status = resp.status
        var data = resp.body;
        if (status != 200 ) {          
            dispatch({
            type: FETCH_FAILURE_REPORT_USER,
            payload: data 
            });
        }
        else {
            dispatch({
            type: REPORT_USER,
            payload: data
            });
        }        
        })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: FETCH_FAILURE_REPORT_USER,
                    payload: error
                });

            });
    };
};








