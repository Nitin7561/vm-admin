import {FETCH_REQUEST_GET_ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP, FETCH_FAILURE_GET_ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP,ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP} from './actionsConfig';
import {getUpcomingMeetingsSidebarAppEndPoint} from './apiUrls';

export const getAllUpcomingEventsFromSidebarApp = (reqBody) => {

    console.log(reqBody)

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_GET_ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP });
      let url = `${getUpcomingMeetingsSidebarAppEndPoint}`;
      console.log("Url from get upcoming",url);
      fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" },
      })
      .then(res =>  res.json().then(data => ({status: res.status, body: data})))
      .then(resp => {
      console.log("data",resp.status);
      var status = resp.status
      var data = resp.body;
      if (status != 200 ) {          
          dispatch({
          type: FETCH_FAILURE_GET_ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP,
          payload: data 
          });
      }
      else {
          dispatch({
          type: ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP,
          payload: data
          });
      }        
      })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_ALL_UPCOMING_APPOINTMENTS_SIDEBAR_APP,
          payload: error
        });
        
      });
    };
  };