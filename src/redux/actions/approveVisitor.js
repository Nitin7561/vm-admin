import { FETCH_REQUEST_APPROVE_VISITOR, FETCH_FAILURE_APPROVE_VISITOR, APPROVE_VISITOR  } from '../actions/actionsConfig';
import {approveVisitorAppointmentEndPoint} from './apiUrls'

//-- Get Room settings
export const approveVisitorAppointment = (reqBody) => {

    //  console.log("REQ BODY-->",reqBody);
    return dispatch => {
        dispatch({ type: FETCH_REQUEST_APPROVE_VISITOR });
        //TODO NEW API
        fetch(approveVisitorAppointmentEndPoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqBody) 
        })
            .then(res =>  res.json().then(data => ({status: res.status, body: data})))
            .then(resp => {
            console.log("data",resp.status);
            var status = resp.status
            var data = resp.body;
            if (status != 200 ) {          
                dispatch({
                type: FETCH_FAILURE_APPROVE_VISITOR,
                payload: data 
                });
            }
            else {
                dispatch({
                type: APPROVE_VISITOR,
                payload: data
                });
            }        
            })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: FETCH_FAILURE_APPROVE_VISITOR,
                    payload: error
                });

            });
    };
};








