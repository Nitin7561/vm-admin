import {FETCH_REQUEST_GET_ALL_ENUMS, FETCH_FAILURE_GET_ALL_ENUMS,GET_ALL_ENUMS} from './actionsConfig';
import {getAllEnumsEndPoint} from './apiUrls';

//-- Get Room settings
export const getAllEnums = () => {

    // console.log(reqBody);

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_GET_ALL_ENUMS });
      fetch(getAllEnumsEndPoint, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
      })
      .then(resp => resp.json())
      .then(data => {
          console.log("data",data);
          dispatch({
            type:GET_ALL_ENUMS,
            payload: data.data
          });
        })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_ALL_ENUMS,
          payload: error
        });
        
      });
    };
  };