import {FETCH_REQUEST_GET_ALL_AVAILABLE_MEETING_ROOMS, FETCH_FAILURE_GET_AVAILABLE_MEETING_ROOMS,GET_AVAILABLE_MEETING_ROOMS} from './actionsConfig';
import {getAllAvailableRooms} from './apiUrls';
//-- Get Room settings
export const getAllAvailableMeetingRooms = (reqBody) => {

    console.log("AVAIL REQ BODY",reqBody);
    let url = `${getAllAvailableRooms}?fromTime=${reqBody.fromTime}&toTime=${reqBody.toTime}`;
    console.log("GET AVAILABLE ROOMS URL",url) 

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_GET_ALL_AVAILABLE_MEETING_ROOMS }); 
      fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" },
        // body: JSON.stringify(reqBody)      
      })
        .then(resp => resp.json())
        .then(data => {
          console.log("data",data);
          dispatch({
            type:GET_AVAILABLE_MEETING_ROOMS,
            payload: data
          });
        })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_AVAILABLE_MEETING_ROOMS,
          payload: error
        });
        
      });
    };
  };