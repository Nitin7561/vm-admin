import {FETCH_REQUEST_GET_MY_APPOINTMENTS, FETCH_FAILURE_GET_MY_APPOINTMENTS,GET_MY_APPOINTMENTS} from './actionsConfig';
import { getAppointmentByHostEndPoint } from './apiUrls'
//-- Get Room settings
export const getMyAppointments = (reqBody) => {

    console.log("Get Appointments Req body",reqBody)

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_GET_MY_APPOINTMENTS });
      let url = `${getAppointmentByHostEndPoint}?from=${reqBody.from}&to=${reqBody.to}&page=${reqBody.page}&offset=${reqBody.offset}&order=${reqBody.order}&sortBy=${reqBody.sortBy}&status=${reqBody.status}`;
      console.log("URL==>",url)
      fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" },    
      })
      .then(res =>  res.json().then(data => ({status: res.status, body: data})))
      .then(resp => {
      console.log("data",resp.status);
      var status = resp.status
      var data = resp.body;
      if (status != 200 ) {          
          dispatch({
          type: FETCH_FAILURE_GET_MY_APPOINTMENTS,
          payload: data 
          });
      }
      else {
          dispatch({
          type: GET_MY_APPOINTMENTS,
          payload: data
          });
      }        
      })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_MY_APPOINTMENTS,
          payload: error
        });
        
      });
    };
  };