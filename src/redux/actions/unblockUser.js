import { FETCH_REQUEST_UNBLOCK_USER, FETCH_FAILURE_UNBLOCK_USER, UNBLOCK_USER  } from '../actions/actionsConfig';
import {unblockChatEndPoint} from './apiUrls'

//-- Get Room settings
export const unblockUser = (reqBody) => {

    return dispatch => {
        dispatch({ type: FETCH_REQUEST_UNBLOCK_USER });
        //TODO NEW API
        fetch(unblockChatEndPoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqBody) 
        })
            .then(res =>  res.json().then(data => ({status: res.status, body: data})))
            .then(resp => {
            console.log("data",resp.status);
            var status = resp.status
            var data = resp.body;
            if (status != 200 ) {          
                dispatch({
                type: FETCH_FAILURE_UNBLOCK_USER,
                payload: data 
                });
            }
            else {
                dispatch({
                type: UNBLOCK_USER,
                payload: data
                });
            }        
            })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: FETCH_FAILURE_UNBLOCK_USER,
                    payload: error
                });

            });
    };
};








