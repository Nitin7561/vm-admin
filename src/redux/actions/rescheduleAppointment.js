import {FETCH_REQUEST_RESCHEDULE_APPT, FETCH_FAILURE_RESCHEDULE_APPT,RESCHEDULE_APPT} from './actionsConfig';
import { rescheduleAppointment } from './apiUrls'
//-- Get Room settings
export const rescheduleAppt = (reqBody) => {

    console.log("Get Reschedule Req body",reqBody)

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_RESCHEDULE_APPT });
      let url = rescheduleAppointment;
      console.log("URL==>",url)
      fetch(url, {
        method: "POST",
        headers: { "Content-Type": "application/json" },    
        body: JSON.stringify(reqBody)
      })
      .then(res =>  res.json().then(data => ({status: res.status, body: data})))
      .then(resp => {
      console.log("data",resp.status);
      var status = resp.status
      var data = resp.body;
      if (status != 200 ) {          
          dispatch({
          type: FETCH_FAILURE_RESCHEDULE_APPT,
          payload: data 
          });
      }
      else {
          dispatch({
          type: RESCHEDULE_APPT,
          payload: data
          });
      }        
      })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_RESCHEDULE_APPT,
          payload: error
        });
        
      });
    };
  };