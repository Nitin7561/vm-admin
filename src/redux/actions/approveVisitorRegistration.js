import { FETCH_REQUEST_APPROVE_VISITOR_REG, FETCH_FAILURE_APPROVE_VISITOR_REG, APPROVE_VISITOR_REG  } from '../actions/actionsConfig';
import {approveVisitorRegistrationEndPoint} from './apiUrls'

//-- Get Room settings
export const approveVisitorRegistration = (reqBody) => {

    //  console.log("REQ BODY-->",reqBody);
    return dispatch => {
        dispatch({ type: FETCH_REQUEST_APPROVE_VISITOR_REG });
        //TODO NEW API
        fetch(approveVisitorRegistrationEndPoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqBody) 
        })
        .then(res =>  res.json().then(data => ({status: res.status, body: data})))
        .then(resp => {
        console.log("data",resp.status);
        var status = resp.status
        var data = resp.body;
        if (status != 200 ) {          
            dispatch({
            type: FETCH_FAILURE_APPROVE_VISITOR_REG,
            payload: data 
            });
        }
        else {
            dispatch({
            type: APPROVE_VISITOR_REG,
            payload: data
            });
        }        
        })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: FETCH_FAILURE_APPROVE_VISITOR_REG,
                    payload: error
                });

            });
    };
};








