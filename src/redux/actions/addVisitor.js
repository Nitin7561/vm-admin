import { FETCH_REQUEST, FETCH_FAILURE, ADD_VISITOR  } from '../actions/actionsConfig';

//-- Get Room settings
export const reportUser = (reqBody) => {

    //  console.log("REQ BODY-->",reqBody);
    return dispatch => {
        dispatch({ type: FETCH_REQUEST });
        fetch("https://fe.leantron.dev:3003/api/ver1.2/visitor/addVisitor", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqBody) 
        })
            .then(resp => resp.json())
            .then(data => {
                console.log("-------------------------------->", data);
                
                
                dispatch({
                    type: ADD_VISITOR,
                    payload: data
                });
            })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: FETCH_FAILURE,
                    payload: error
                });

            });
    };
};








