import {FETCH_REQUEST_GET_ALL_BOOKED_MEETING_ROOMS, FETCH_FAILURE_GET_ALL_BOOKED_MEETING_ROOMS,GET_ALL_BOOKED_MEETING_ROOMS} from './actionsConfig';

//-- Get Room settings
export const getAllBookedRooms = (reqBody) => {

    console.log(reqBody);

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_GET_ALL_BOOKED_MEETING_ROOMS });
      fetch("https://fe.leantron.dev:3003/api/ver1.2/appointment/getExistingRoom", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(reqBody)      
      })
        .then(resp => resp.json())
        .then(data => {
          console.log("data",data);
          dispatch({
            type:GET_ALL_BOOKED_MEETING_ROOMS,
            payload: data.data.result
          });
        })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_ALL_BOOKED_MEETING_ROOMS,
          payload: error
        });
        
      });
    };
  };