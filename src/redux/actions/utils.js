import {
    SET_ALL_APPLICATIONS,
    VERIFY_APP_REQUEST,
    VERIFY_APP_RECIEVE,
    VERIFY_APP_FAILURE,
    VERIFY_APP_RECIEVE_ALL,
    VERIFY_APP_FAILURE_ALL
} from "./actionsConfig";

export function setAllApplications(applications) {
    return (dispatch, getState) => {
        let appCookieRecv = {}
        applications.forEach(app => {
            appCookieRecv[app] = false
        });
        dispatch({
            type: SET_ALL_APPLICATIONS,
            payload: appCookieRecv
        })
    }
}
export function verifyApps() {
    return (dispatch, getState) => {
        // let { refresher } = getState();
        // let newValue = refresher[page];
        dispatch({
            type: VERIFY_APP_REQUEST,
        });
        let { getApplicationReducer, appCookieRecv } = getState();
        let { data: applns } = getApplicationReducer;
        let { apps: appCookieRecv1 } = appCookieRecv;
        console.log("getApplicationReducer", applns);
        let urls = []
        let appsSelected = []
        let redirectUrls = []
        Object.keys(appCookieRecv1).forEach(app => {
            let appDets = applns.filter(e => app === e.applicationId)[0];
            let { instance, verifyPath, loginPath } = appDets;
            let url = "https://google.com" //TODO: 404 Page
            let { protocol, host, port } = instance;
            url = `${protocol}://${host}:${port}${verifyPath}`;
            let redirectUrl = `${protocol}://${host}:${port}${loginPath}?return_to=${window.location.href}`;
            urls.push(url);
            redirectUrls.push(redirectUrl)
            appsSelected.push(appDets);
        });
        console.log("getApplicationReducer", urls, appsSelected);
        urls.forEach((url, index) => {
            console.log("VERIFY_APP", url)
            fetch(url, { redirect: 'manual' })
                .then(data1 => {
                    console.log("VERIFY_APP", data1.type);
                    if (data1.type !== 'basic') {
                        window.location.href = redirectUrls[index];
                    } else {
                        data1.json().then(data => {
                            dispatch({
                                type: VERIFY_APP_RECIEVE,
                                payload: { [appsSelected[index].applicationId]: true }
                            });
                            dispatch({
                                type: VERIFY_APP_RECIEVE_ALL
                            });
                        }).catch(err => {
                            console.log("VERIFY_APP_ERROR", err);
                            window.location.href = redirectUrls[index]
                        })
                    }
                })
        })
    }
}