import {FETCH_REQUEST_GET_MY_FREQUENT_VISITORS, FETCH_FAILURE_GET_MY_FREQUENT_VISITORS,GET_MY_FREQUENT_VISITORS} from './actionsConfig';
import {getAllFrequentVisitorsByHostEndPoint} from './apiUrls';
//-- Get Room settings
export const getMyFrequentVisitors = (reqBody) => {

    console.log(reqBody)

    return dispatch => {
      dispatch({ type: FETCH_REQUEST_GET_MY_FREQUENT_VISITORS });
      let url = `${getAllFrequentVisitorsByHostEndPoint}?from=${reqBody.from}&to=${reqBody.to}&page=${reqBody.page}&offset=${reqBody.offset}&oder=${reqBody.order}&sortBy=${reqBody.sortBy}`;
      fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" },    
      })
      .then(res =>  res.json().then(data => ({status: res.status, body: data})))
      .then(resp => {
      console.log("data",resp.status);
      var status = resp.status
      var data = resp.body;
      if (status != 200 ) {          
          dispatch({
          type: FETCH_FAILURE_GET_MY_FREQUENT_VISITORS,
          payload: data 
          });
      }
      else {
          dispatch({
          type: GET_MY_FREQUENT_VISITORS,
          payload: data
          });
      }        
      })
        .catch(error =>{
          console.log(error)
          dispatch({
          type: FETCH_FAILURE_GET_MY_FREQUENT_VISITORS,
          payload: error
        });
        
      });
    };
  };