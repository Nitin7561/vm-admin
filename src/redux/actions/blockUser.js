import { FETCH_REQUEST_BLOCK_USER, FETCH_FAILURE_BLOCK_USER, BLOCK_USER  } from '../actions/actionsConfig';
import {blockChatEndPoint} from './apiUrls'

//-- Get Room settings
export const blockUser = (reqBody) => {

    //  console.log("REQ BODY-->",reqBody);
    return dispatch => {
        dispatch({ type: FETCH_REQUEST_BLOCK_USER });
        //TODO NEW API
        fetch(blockChatEndPoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqBody) 
        })
            .then(res =>  res.json().then(data => ({status: res.status, body: data})))
            .then(resp => {
            console.log("data",resp.status);
            var status = resp.status
            var data = resp.body;
            if (status != 200 ) {          
                dispatch({
                type: FETCH_FAILURE_BLOCK_USER,
                payload: data 
                });
            }
            else {
                dispatch({
                type: BLOCK_USER,
                payload: data
                });
            }        
            })
            .catch(error => {
                console.log(error)
                dispatch({
                    type: FETCH_FAILURE_BLOCK_USER,
                    payload: error
                });

            });
    };
};








