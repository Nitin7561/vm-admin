import { applyMiddleware, compose, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import monitorReducersEnhancer from "./enhancers/monitorReducers";
import loggerMiddleware from "./middleware/logger";
import rootReducer from "./reducers";

export default function configureStore(preloadedState) 
{
  const middlewares = [loggerMiddleware, thunkMiddleware];
  //-- use applyMiddleware to create a store enhancer
  const middlewareEnhancer = applyMiddleware(...middlewares);
  const enhancers = [middlewareEnhancer, monitorReducersEnhancer];
  //-- use compose to compose our new middlewareEnhancer and our monitorReducerEnhancer into one function. Because you can only pass one enhancer into createStore
  const composedEnhancers = compose(...enhancers);

  //-- We pass our reducers to the Redux createStore function, which returns a store object + middlewares
  //-- Extending Redux functionality by adding middlewares: logger, enhancer
  const store = createStore(rootReducer, preloadedState, composedEnhancers);
  
  return store;
}
