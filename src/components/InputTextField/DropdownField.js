import React from "react";
import TextField from "@material-ui/core/TextField";
import { InputLabel, MenuItem } from "@material-ui/core";
import "./inputTextField.scss";

export const DropdownField = props => {
  let {
    value,
    eventHandler,
    label,
    placeholder,
    labelClass,
    inputClass,
    menuItems,
    extraInputProps,
    ...inputProps
  } = props;
  
  menuItems = menuItems?menuItems:[];
  return (
    <div className="lean-form-element">
      {label ? (
        <InputLabel shrink className={`lean-form-label ${labelClass?labelClass:""}`}>
          {label}
        </InputLabel>
      ) : null}
      {/* <TextField value={props.value} onChange={props.onValueChange} placeholder="Placeholder" name="test" variant="outlined" InputProps={{className:"lean-form-text-field"}}/> */}
      {/* <TextField value={value} onChange={(e) => setValue(e.target.value)} placeholder="Placeholder" name="test" variant="outlined" InputProps={{className:"lean-form-text-field"}}/> */}
      <TextField
        select
        variant="outlined"
        className="lean-form-field"
        value={value}
        onChange={e => eventHandler(e.target.value)}
        placeholder={placeholder}
        SelectProps={{
          MenuProps: {
            getContentAnchorEl: null,
            // anchorEl: document.getElementById(id),
            anchorOrigin: {
              horizontal: 'left',
              vertical: 'bottom'
            }
          }
        }}
        InputProps={{ className: `lean-form-text-field ${inputClass?inputClass:""}`, ...extraInputProps }}
        {...inputProps}
      >
        {props.isAddAppointment === "true" ? <MenuItem className="lean-form-element-menu-item" value="Select a Room">Select a Room</MenuItem> : null}
        {menuItems.map(item => (<MenuItem className="lean-form-element-menu-item" value={item.value}>{item.name}</MenuItem>))}
        
      </TextField>
    </div>
  );
};
