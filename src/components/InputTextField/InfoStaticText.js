import React from "react";
import { InputLabel } from "@material-ui/core";
import "./inputTextField.scss";

export const InfoStaticText = props => {
  let {
    value,
    label,
    labelClass,
    inputClass,
    children,
    ...otherProps
  } = props;
  return (
    <div className="lean-form-element">
      {label ? (
        <InputLabel
          shrink
          className={`lean-form-label ${labelClass ? labelClass : ""}`}
        >
          {label}
        </InputLabel>
      ) : null}
      {/* <TextField value={props.value} onChange={props.onValueChange} placeholder="Placeholder" name="test" variant="outlined" InputProps={{className:"lean-form-text-field"}}/> */}
      
      <div className={`lean-static-text ${inputClass ? inputClass : ""}`} {...otherProps}>{children}</div>
      
      {/* <TextField
        variant="outlined"
        className="lean-form-field"
        value={value}
        onChange={e => eventHandler(e.target.value)}
        placeholder={placeholder}
        InputProps={{
          className: `lean-form-text-field ${inputClass ? inputClass : ""}`
        }}
        {...otherProps}
      /> */}
    </div>
  );
};
