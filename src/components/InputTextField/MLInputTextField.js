import React from "react";
import TextField from "@material-ui/core/TextField";
import { InputLabel } from "@material-ui/core";
import "./inputTextField.scss";

export const MLInputTextField = props => {
  let {
    value,
    eventHandler,
    label,
    placeholder,
    labelClass,
    inputClass,
    ...otherProps
  } = props;

  return (
    <div className="lean-form-element">
      {label ? (
        <InputLabel
          shrink
          className={`lean-form-label ${labelClass ? labelClass : ""}`}
        >
          {label}
        </InputLabel>
      ) : null}

      {/* <TextField value={props.value} onChange={props.onValueChange} placeholder="Placeholder" name="test" variant="outlined" InputProps={{className:"lean-form-text-field"}}/> */}
      <TextField
        multiline
        rows="4"
        variant="outlined"
        className="lean-form-field"
        value={value}
        onChange={e => eventHandler(e.target.value)}
        placeholder={placeholder}
        InputProps={{
          className: `lean-form-text-field ${inputClass ? inputClass : ""}`
        }}
        {...otherProps}
      />


        {otherProps.inputProps.maxLength? (
          <div className="lean-text-field-character-limit">
            <span>Remaining Characters: {otherProps.inputProps.maxLength - value.length} </span>
          </div>
        ) : null}
      
    </div>
  );
};
