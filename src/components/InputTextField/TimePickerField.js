import React from "react";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker
} from "@material-ui/pickers";
import { InputLabel } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";

import TimePickerIcon from "../../assets/timepicker.svg";

import "./inputTextField.scss";

export const TimePickerField = props => {
  let {
    value,
    eventHandler,
    label,
    placeholder,
    labelClass,
    inputClass,
    ...otherProps
  } = props;

  return (
    <div className="lean-form-element">
      {label ? (
        <InputLabel
          shrink
          className={`lean-form-label ${labelClass ? labelClass : ""}`}
        >
          {label}
        </InputLabel>
      ) : null}

      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardTimePicker
          autoOk
          disablePast
          variant="inline"
          inputVariant="outlined"
          className="lean-form-field"
          mask="__:__ _M"
          keyboardIcon={<img src={TimePickerIcon} />}
          value={value}
          onChange={eventHandler}
          placeholder={placeholder}
          PopoverProps={{
            classes: {
              paper: "lean-form-time-picker-paper"
            }
          }}
          InputProps={{
            disableUnderline: true,
            className: `lean-form-text-field ${inputClass ? inputClass : ""}`
          }}
          KeyboardButtonProps={{
            className: "lean-form-picker-button"
          }}
          InputAdornmentProps={{
            className: "lean-form-picker-adornment"
          }}
          {...otherProps}
        />
      </MuiPickersUtilsProvider>
    </div>
  );
};
