import React from "react";
import TextField from "@material-ui/core/TextField";
import { InputLabel, Chip } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import "./inputTextField.scss";

export const InputAutoField = props => {
  let {
    value,
    eventHandler,
    label,
    placeholder,
    labelClass,
    inputClass,
    menuItems,
    onEveryChange,
    ...otherProps
  } = props;
  return (
    <div className="lean-form-element lean-form-element-chip-element">
      {label ? (
        <InputLabel
          shrink
          className={`lean-form-label ${labelClass ? labelClass : ""}`}
        >
          {label}
        </InputLabel>
      ) : null}


      <Autocomplete
        freeSolo

        onChange={(e, v) => eventHandler(v)}
        value={value}
        // classes={{ root: "lean-filters-textfield-time", listbox: 'lean-filters-textfield-time-list' }}
        options={menuItems ? menuItems : []}
        onInputChange={(e, v, r) => { if (onEveryChange && r !=="reset") { eventHandler(v) } }}
        {...otherProps}
        renderInput={params => {
          if (params.InputProps.className) {
            params.InputProps.className = `${
              params.InputProps.className
              } lean-form-text-field lean-form-chips ${
              inputClass ? inputClass : ""
              }`;
          }

          return (
            <TextField
              variant="outlined"
              fullWidth
              type="time"
              {...params}
              {...otherProps}
            />
          );
        }}
      />

      {/* <TextField
        placeholder="Placeholder"
        variant="outlined"
        className="lean-form-field"
        value={value}
        onChange={e => eventHandler(e.target.value)}
        placeholder={placeholder}
        InputProps={{
          className: `lean-form-text-field ${inputClass ? inputClass : ""}`
        }}
        {...otherProps}
      /> */}
    </div>
  );
};
