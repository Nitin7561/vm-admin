import React,{useState,useEffect} from 'react';
import facefinder from './facefinder';

function FaceDetectCam(props){
    useEffect(()=>{
      FC();
    },[])
    return(
      <div>            
         <canvas id="cann"  width={props.width?props.width:619} height={props.height?props.height:440}></canvas>
       </div> 
    )
     function FC(){
      console.log(facefinder)
         var pico = {}

          pico.unpack_cascade = function(bytes)
          {
            const dview = new DataView(new ArrayBuffer(4));
      
            let p = 8;
            dview.setUint8(0, bytes[p+0]);
             dview.setUint8(1, bytes[p+1]);
              dview.setUint8(2, bytes[p+2]);
               dview.setUint8(3, bytes[p+3]);
            const tdepth = dview.getInt32(0, true);
            p = p + 4;
            dview.setUint8(0, bytes[p+0]);
             dview.setUint8(1, bytes[p+1]);
              dview.setUint8(2, bytes[p+2]);
               dview.setUint8(3, bytes[p+3]);
            const ntrees = dview.getInt32(0, true);
            p = p + 4;
            const tcodes_ls = [];
            const tpreds_ls = [];
            const thresh_ls = [];
            for(let t=0; t<ntrees; ++t)
            {
              Array.prototype.push.apply(tcodes_ls, [0, 0, 0, 0]);
              Array.prototype.push.apply(tcodes_ls, bytes.slice(p, p+4*Math.pow(2, tdepth)-4));
              p = p + 4*Math.pow(2, tdepth)-4;
              for(let i=0; i<Math.pow(2, tdepth); ++i)
              {
                dview.setUint8(0, bytes[p+0]);
                 dview.setUint8(1, bytes[p+1]);
                  dview.setUint8(2, bytes[p+2]);
                   dview.setUint8(3, bytes[p+3]);
                tpreds_ls.push(dview.getFloat32(0, true));
                p = p + 4;
              }
              dview.setUint8(0, bytes[p+0]);
               dview.setUint8(1, bytes[p+1]);
                dview.setUint8(2, bytes[p+2]);
                 dview.setUint8(3, bytes[p+3]);
              thresh_ls.push(dview.getFloat32(0, true));
              p = p + 4;
            }
            const tcodes = new Int8Array(tcodes_ls);
            const tpreds = new Float32Array(tpreds_ls);
            const thresh = new Float32Array(thresh_ls);
            function classify_region(r, c, s, pixels, ldim){
              r = 256*r; c = 256*c; let root = 0;
              let o = 0.0;
              const pow2tdepth = Math.pow(2, tdepth) >> 0; 
              for(let i=0; i<ntrees; ++i)
              {
                let idx = 1;
                for(let j=0; j<tdepth; ++j)
                  idx = 2*idx + (pixels[((r+tcodes[root + 4*idx + 0]*s) >> 8)*ldim+((c+tcodes[root + 4*idx + 1]*s) >> 8)]<=pixels[((r+tcodes[root + 4*idx + 2]*s) >> 8)*ldim+((c+tcodes[root + 4*idx + 3]*s) >> 8)]);
                o = o + tpreds[pow2tdepth*i + idx-pow2tdepth];
                if(o<=thresh[i])
                  return -1;
      
                root += 4*pow2tdepth;
              }
              return o - thresh[ntrees-1];
            }
            return classify_region; }
      
          pico.run_cascade = function(image, classify_region, params)
          {
            const pixels = image.pixels;
            const nrows = image.nrows;
            const ncols = image.ncols;
            const ldim = image.ldim;
      
            const shiftfactor = params.shiftfactor;
            const minsize = params.minsize;
            const maxsize = params.maxsize;
            const scalefactor = params.scalefactor;
      
            let scale = minsize;
            const detections = [];
      
            while(scale<=maxsize)
            {
              const step = Math.max(shiftfactor*scale, 1) >> 0; // '>>0' transforms this number to int
              const offset = (scale/2 + 1) >> 0;
      
              for(let r=offset; r<=nrows-offset; r+=step)
                for(let c=offset; c<=ncols-offset; c+=step)
                {
                  const q = classify_region(r, c, scale, pixels, ldim);
                  if (q > 0.0)
                    detections.push([r, c, scale, q]);
                }
              
              scale = scale*scalefactor;
            }
      
            return detections;
          }
      
          pico.cluster_detections = function(dets, iouthreshold)
          {
      
            dets = dets.sort(function(a, b) {
              return b[3] - a[3];
            });
      
            function calculate_iou(det1, det2)
            {
              const r1=det1[0], c1=det1[1], s1=det1[2];
              const r2=det2[0], c2=det2[1], s2=det2[2];
              const overr = Math.max(0, Math.min(r1+s1/2, r2+s2/2) - Math.max(r1-s1/2, r2-s2/2));
              const overc = Math.max(0, Math.min(c1+s1/2, c2+s2/2) - Math.max(c1-s1/2, c2-s2/2));
              return overr*overc/(s1*s1+s2*s2-overr*overc);
            }
            const assignments = new Array(dets.length).fill(0);
            const clusters = [];
            for(let i=0; i<dets.length; ++i)
            {
              if(assignments[i]==0)
              {
                let r=0.0, c=0.0, s=0.0, q=0.0, n=0;
                for(let j=i; j<dets.length; ++j)
                  if(calculate_iou(dets[i], dets[j])>iouthreshold)
                  {
                    assignments[j] = 1;
                    r = r + dets[j][0];
                    c = c + dets[j][1];
                    s = s + dets[j][2];
                    q = q + dets[j][3];
                    n = n + 1;
                  }
              clusters.push([r/n, c/n, s/n, q]);
              }
            }
      
            return clusters;
          }
      
          pico.instantiate_detection_memory = function(size)
          {
            let n = 0;
            const memory = [];
            for(let i=0; i<size; ++i)
              memory.push([]);
      
              function update_memory(dets)
            {
              memory[n] = dets;
              n = (n+1)%memory.length;
              dets = [];
              for(let i=0; i<memory.length; ++i)
                dets = dets.concat(memory[i]);
              return dets;
            }
      
            return update_memory;
          }



          
              function camvas(ctx, callback) {
                var self = this
                this.ctx = ctx
                this.callback = callback
                var streamContainer = document.createElement('div')
                this.video = document.createElement('video')
                this.video.setAttribute('autoplay', '1')
                this.video.setAttribute('playsinline', '1') 
                this.video.setAttribute('width', 1)
                this.video.setAttribute('height', 1)
      
                streamContainer.appendChild(this.video)
                document.body.appendChild(streamContainer)
      
                navigator.mediaDevices.getUserMedia({video: true, audio: false}).then(function(stream) {     
                  self.video.srcObject = stream      
                  self.update()
                }, function(err) {
                  throw err
                })
              
                this.update = function() {
                  var self = this
                  var last = Date.now()
                  var loop = function() {
                  var dt = Date.now() - last
                  self.callback(self.video, dt)
                  last = Date.now()
                  requestAnimationFrame(loop) 
                  }
                  requestAnimationFrame(loop) 
                } 
              }
      
      
          var imageCaptured = props.imageCaptured? props.imageCaptured:false;
          var imageArray = "";
          var initialized = false;
          function button_callback() {      
            if(initialized)
              return;       
            var update_memory = pico.instantiate_detection_memory(5); 
            var facefinder_classify_region = function(r, c, s, pixels, ldim) {return -1.0;};
            var cascadeurl = facefinder;
            fetch(cascadeurl).then(function(response) {
              response.arrayBuffer().then(function(buffer) {
                var bytes = new Int8Array(buffer);
                facefinder_classify_region = pico.unpack_cascade(bytes);
                console.log('* facefinder loaded');
              })
            })      
            var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
            function rgba_to_grayscale(rgba, nrows, ncols) {
              var gray = new Uint8Array(nrows*ncols);
              for(var r=0; r<nrows; ++r)
                for(var c=0; c<ncols; ++c)
                  gray[r*ncols + c] = (2*rgba[r*4*ncols+4*c+0]+7*rgba[r*4*ncols+4*c+1]+1*rgba[r*4*ncols+4*c+2])/10;
              return gray;
             }
      
            var processfn = function(video, dt) {


              ctx.drawImage(video, 0, 0);
              var rgba = ctx.getImageData(0, 0, 640, 480);         
              var image = {
                "pixels": rgba_to_grayscale(rgba.data, 480, 640),
                "nrows": 480,
                "ncols": 640,
                "ldim": 640
              }
              var params = {
                "shiftfactor": 0.1, 
                "minsize": 100,     
                "maxsize": 1000,    
                "scalefactor": 1.1  
              }

              if(props.type === "photo"){
              var dets = pico.run_cascade(image, facefinder_classify_region, params);
              dets = update_memory(dets);
              dets = pico.cluster_detections(dets, 0.2);               
              for(let i=0; i<dets.length; ++i)      
                if(dets[i][3]>100.0)
                {                 
                  if(!FaceDetectCam.imageCaptured){
                    imageArray = rgba.data;
                    imageCaptured = true;
                    ctx.putImageData(rgba,0,0);
                    var imgg = document.getElementById('cann').toDataURL('image/jpeg',1.0);	                                               
                    props.onDetect?props.onDetect(imgg):console.log("Image Captured");  
                    FaceDetectCam.imageCaptured = true;                        
                  }	
                  ctx.beginPath();
                  ctx.arc(dets[i][1], dets[i][0], dets[i][2]/2, 0, 2*Math.PI, false);
                  ctx.lineWidth = 3;
                  ctx.strokeStyle = 'rgba(255,0,255)';
                  ctx.stroke();          
                }              
            } else if( props.type === "id")if(!FaceDetectCam.imageCaptured){
              imageArray = rgba.data;
              imageCaptured = true;
              ctx.putImageData(rgba,0,0);
              var imgg = document.getElementById('cann').toDataURL('image/jpeg',1.0);	                                               
              props.onDetect?props.onDetect(imgg):console.log("Image Captured");  
              FaceDetectCam.imageCaptured = true;                        
            } 
          }         
            var mycamvas = new camvas(ctx, processfn);       
            initialized = true;
          }
      
          button_callback();
          
         }

}

export default FaceDetectCam;
