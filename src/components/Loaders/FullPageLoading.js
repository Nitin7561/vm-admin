import React from 'react'
import './loader.scss'
import { CircularProgress } from '@material-ui/core'
const FullPageLoading = ({children, mainClassNames, customClassNames}) => {
    return (
        <div className={`${mainClassNames} ${customClassNames?customClassNames:""}`} >
            <CircularProgress disableShrink />
            <span className={`${mainClassNames}-loading`}>{children}</span>
        </div>
    )
}
export default FullPageLoading