import React, { useState, useEffect } from "react";
import Loader from "react-loader-spinner";
import Button from '../Button/Button';
import "./PhotoCapture.scss";

function PhotoCapture(props) {

  const FaceDetectCam = props.FDC;
  if(props.type == "photo")
  FaceDetectCam.imageCaptured = false;
  else if(props.type == "id")
  FaceDetectCam.imageCaptured = true;

  FaceDetectCam.imageSelected = [false,false,false];

  FaceDetectCam.primaryDone = false;

  const [imageOneSelected, setImageOneSelected] = useState(false);
  const [imageTwoSelected, setImageTwoSelected] = useState(false);
  const [imageThreeSelected, setImageThreeSelected] = useState(false); 
  const [imagesClicked,setImagesClicked] = useState(false);  
  var imgArr = [];


  function click() {
    FaceDetectCam.imageCaptured = false;
  }
  
  function sendImagesBack(){
    props.sendImages?props.sendImages(imgArr):console.log("no func sent");
  }

  function handleImage(imageData) {
    if(imgArr.length != ((props.type === "id")?1:3))  
      imgArr.push(imageData);

      if (imgArr.length < ((props.type === "id")?1:3)) {
        setTimeout(click, 100 );
      } else if (imgArr.length == ((props.type === "id")?1:3) && !FaceDetectCam.primaryDone) {
        updateImages();
      }else {        
        if (FaceDetectCam.imageSelected[0]) {
          document.getElementById("img1").setAttribute("src", imageData);
          imgArr[0] = imageData;
          
          sendImagesBack();          
        } else if (FaceDetectCam.imageSelected[1]) {
          document.getElementById("img2").setAttribute("src", imageData);
          imgArr[1] = imageData;
          sendImagesBack();          
        } else if (FaceDetectCam.imageSelected[2]) {
          document.getElementById("img3").setAttribute("src", imageData);
          imgArr[2] = imageData; 
          sendImagesBack();         
        } else{
          imgArr = [];
          document.getElementById("img1").setAttribute("src", "");
          if(props.type === "photo"){
          document.getElementById("img2").setAttribute("src", "");
          document.getElementById("img3").setAttribute("src", "");
          }          
          setImagesClicked(false);
          FaceDetectCam.primaryDone = false;
          setTimeout(()=>{FaceDetectCam.imageCaptured = false}, 700);         
        }
      }

      function updateImages() {
        setImagesClicked(true);
        FaceDetectCam.primaryDone = true;               
        document.getElementById("img1").setAttribute("src", imgArr[0]);
        if(props.type === "photo"){
        document.getElementById("img2").setAttribute("src", imgArr[1]);
        document.getElementById("img3").setAttribute("src", imgArr[2]);
          }       
        sendImagesBack();
      }    
  }

    function photoOrId(){

      if(props.type === "photo" ){

        return(<div className="PCcapturedImages">
        <div
          onClick={() => {
            setImageOneSelected(!imageOneSelected);
            setImageTwoSelected(false);
            setImageThreeSelected(false);
            FaceDetectCam.imageSelected = [!imageOneSelected,false,false];
          }}
          className={
            imageOneSelected ? "PCImageItem itemSelected" : "PCImageItem"
          }
        >
          <div className={imagesClicked ? "loader none" : "loader"}>
            <Loader type="ThreeDots" color="#111064d7" height={40} width={40} />
          </div>
          <img
            className={imageOneSelected ? "imgSelected" : ""}
            id="img1"
            src={""}
          ></img>
        </div>

        <div
          onClick={() => {
            setImageTwoSelected(!imageTwoSelected);
            setImageOneSelected(false);
            setImageThreeSelected(false);
            FaceDetectCam.imageSelected = [false,!imageTwoSelected,false];
          }}
          className={
            imageTwoSelected ? "PCImageItem itemSelected" : "PCImageItem"
          }
        >
          <div className={imagesClicked ? "loader none" : "loader"}>
            <Loader type="ThreeDots" color="#111064d7" height={40} width={40} />
          </div>

          <img
            id="img2"
            className={imageTwoSelected ? "imgSelected" : ""}
            src={""}
          ></img>
        </div>

        <div
          onClick={() => {
            setImageThreeSelected(!imageThreeSelected);
            setImageOneSelected(false);
            setImageTwoSelected(false);
            FaceDetectCam.imageSelected = [false,false,!imageThreeSelected];
          }}
          className={
            imageThreeSelected ? "PCImageItem itemSelected" : "PCImageItem"
          }
        >
          <div className={imagesClicked ? "loader none" : "loader"}>
            <Loader type="ThreeDots" color="#111064d7" height={40} width={40} />
          </div>
          <img
            id="img3"
            className={imageThreeSelected ? "imgSelected" : ""}
            src={""}
          ></img>
        </div>
      </div>)
      }
      else if(props.type === "id"){
        return(<div className="PCcapturedImages">
             <div
         
          className="PCImageItem id"
        >
          <div className={imagesClicked ? "loader none" : "loader"}>
            <Loader type="ThreeDots" color="#111064d7" height={40} width={40} />
          </div>
          <img
            className={imageOneSelected ? "imgSelected" : ""}
            id="img1"
            src={""}
          ></img>
        </div>
        </div>)
      }

    }

  return (
    <div className="PCMainContainer"> 
      <FaceDetectCam
        onDetect={e => {
          handleImage(e);
        }}
        type = {props.type}
      />
     {photoOrId()}

      <Button
        onClick={() => {          
          FaceDetectCam.imageCaptured = false;
        }}
        type = "btn-rounded"
        text = {props.type == "id"?"Click":"Retake"}
      >
        
      </Button>
    </div>
  );
}

export default PhotoCapture;
