import React from 'react'
import { connect } from "react-redux";
import { getCustomDate, getCustomTime, getCustomTimeDiff } from '../../utils/timeFunctions';
import DeleteIcon from '../../assets/cancel.svg';
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { ModalContext } from "../../utils/ModalContext";
import { getUpcomingHostsWithAParticularVisitor } from '../../redux/actions/getUpcomingHostsWithAParticularVisitor';
import { getFromAndTo } from '../../utils/timeFunctions';
import "./tables.scss";
import EmptySVG from '../../assets/emptyFav.svg';
import { Grid } from "@material-ui/core";



import {
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Chip,
    TablePagination,
    Button,
    Typography,
    Toolbar,
    Tooltip,
    IconButton,
    TableContainer
} from "@material-ui/core";
import FullPageLoading from '../Loaders/FullPageLoading';

const MenuIcon = ({ handleClick }) => (
    <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
    >
        <MoreVertIcon />
    </IconButton>
);

const MoreMenu = props => {

    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = event => {
        setAnchorEl(event.currentTarget);

        
    };

    console.log("PROPS FROM PROFILE MODAL",props)

    function openListOfVisitorTableModal(name, visitorName, visitorEmail, visitorId, checkInTime, checkOutTime, date, appointmentId, subject,roomName) {

        
        console.log("Which Modal to open?", name);
        console.log("Which User?", visitorName);
        console.log("Which email?", visitorEmail);

        if (name === 'RescheduleAppointment') {
            console.log("On click of rescehdule")
            openModal({ name: 'AddAppointmentInAModal', props: { "visitorName": visitorName, "visitorEmail": visitorEmail, "isDisabled": true, "checkInTime": checkInTime, "checkOutTime": checkOutTime, "date": date, "roomName":roomName, "appointmentId": appointmentId, "subject": subject, "classStyles": { paperScrollPaper: 'lean-reschedule-appointment-modal' } } })
        
        }
        else if (name === 'CancelledAppointment') {
            console.log("Modal Name===>", name);
            openModal({ name: 'CancelAppointmentInAModal', props: { "visitorName": visitorName, "visitorEmail": visitorEmail, "isDisabled": true, "checkInTime": checkInTime, "checkOutTime": checkOutTime, "date": date, "roomName":roomName, "appointmentId": appointmentId, "subject": subject } })
        }
        handleClose();
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <MenuIcon handleClick={handleClick} />
            <Menu
                classes={{ paper: "lean-table-body-menu lean-small-table-popover" }}
                // getContentAnchorEl={null}
                anchorEl={document.getElementById(props.uniqueCellID)}
                anchorOrigin={{
                    horizontal: "left",
                    vertical: "center"
                }}
                getContentAnchorEl={null}
                open={open}
                onClose={handleClose}
            >
                <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("RescheduleAppointment", props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId, props.checkInTime, props.checkOutTime, props.date, props.appointments.appointmentId,props.appointments.purpose,props.appointments.roomName)}>
                    Reschedule
                </MenuItem>
                <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("CancelledAppointment", 
                props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId, props.checkInTime, props.checkOutTime, props.date, props.appointments.appointmentId, props.appointments.purpose,props.appointments.roomName
                )}>
                    Cancel Appointment
                </MenuItem>
            </Menu>
        </>
    );
};

const RecurrentTable = (props) => {


    let tableHeaders = [
        { name: "Meeting Date" },
        { name: "Meeting Subject" },
        { name: "Meeting Time" },
        { name: "Duration" },
        { name: "Action" },
    ];

    console.log("PPPRRRRRRRROOOPPPPPSSS=>", props)

    let statusEnum = {
        approved: "approved",
        registered: "registered",
        blocked: "blocked",
        invited: "invited"
    };

    React.useEffect(
        () => {
            console.log("on load")
            props.getUpcomingHostsWithAParticularVisitor({
                "visitorSub": props.eachVisitorinList.visitorSub
            })
        }, []
    );

    let { isLoading, data, error } = props.getUpcomingHostsWithAParticularVisitorReducer;
    console.log("HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", props.getUpcomingHostsWithAParticularVisitorReducer)


    return (
        <div style={{ position: "relative", minHeight: '300px' }}>
            {isLoading ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading> :
                (
                    <>
                        {

                            data.data.length == 0 ?
                                (
                                    <>
                                        <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                                            <img src={EmptySVG} />
                                            <span className="lean-table-empty-screen-text">No Appointments with this Visitor</span>
                                        </Grid>
                                    </>
                                ) :
                                (
                                    <>
                                        <TableContainer style={{ maxHeight: "300px" }}>
                                            <Table stickyHeader>
                                                <TableHead classes={{ root: "lean-table-header" }}>
                                                    <TableRow>
                                                        {tableHeaders.map(tableHeader => (
                                                            <TableCell align="center">
                                                                {tableHeader ? (
                                                                    <Button
                                                                        disableRipple
                                                                        name={tableHeader ? tableHeader.sortIndex : null}
                                                                    // onClick={handleTableHeaderClick}
                                                                    >
                                                                        {tableHeader.name}
                                                                    </Button>
                                                                ) : null}
                                                            </TableCell>
                                                        ))}

                                                    </TableRow>
                                                </TableHead>


                                                <TableBody classes={{ root: "lean-table-body" }} >

                                                    {data.data.map((appointments, index) => {

                                                        var duration = appointments.duration;
                                                        var arrivalTime = appointments.arrivalTime;

                                                        var dateNew = new Date(arrivalTime)
                                                        var timeNew = dateNew.toLocaleTimeString()

                                                        console.log("Date ===>", dateNew.toDateString())

                                                        var checkincheckoutTime = getFromAndTo(dateNew, dateNew, Number(duration))
                                                        var checkInTime = new Date(checkincheckoutTime.from * 1000).toLocaleTimeString();
                                                        var checkOutTime = new Date(checkincheckoutTime.to * 1000).toLocaleTimeString();


                                                        console.log("checkincheckoutTime from particular visitor", checkincheckoutTime);
                                                        console.log("checkInTime from particular visitor", checkInTime);
                                                        console.log("checkOutTime from particular visitor", checkOutTime);

                                                        return (
                                                            <TableRow
                                                                key={index}
                                                                hover
                                                                className="lean-table-row lean-table-body-recurrent"
                                                            >
                                                                <TableCell align="center">{dateNew.toDateString()}</TableCell>
                                                                <TableCell align="center">{appointments.purpose}</TableCell>
                                                                <TableCell align="center">{checkInTime}</TableCell>
                                                                <TableCell align="center">{duration/3600000}</TableCell>
                                                                <TableCell id={"action" + index} align="center">
                                                                    <IconButton>
                                                                        <MoreMenu uniqueCellID={"action" + index} eachUser={props.eachVisitorinList} appointments={appointments} date={dateNew.toDateString()} checkInTime={checkInTime} checkOutTime={checkOutTime} status={statusEnum[props.eachVisitorinList.status]} />
                                                                    </IconButton>
                                                                </TableCell>
                                                            </TableRow>
                                                        );
                                                    })}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </>
                                )


                        }
                    </>
                )}
        </div>

    )
}

const mapStateToProps = state => {
    const { getUpcomingHostsWithAParticularVisitorReducer } = state;
    console.log("Visitors-->", getUpcomingHostsWithAParticularVisitorReducer);
    return { getUpcomingHostsWithAParticularVisitorReducer };
};

export default connect(mapStateToProps, { getUpcomingHostsWithAParticularVisitor })(
    RecurrentTable
);


// export default RecurrentTable
