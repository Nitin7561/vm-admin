import React, { Component } from "react";
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Kevin from '../../assets/userImg.svg'
import {getVisitorChatList} from '../../redux/actions/getVisitorChatList';
import { connect } from "react-redux";


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));

const EachVisitorChat = (props) => {
    const classes = useStyles();
    var getDate = new Date(props.arrivalTime * 1000).toDateString();
    return (
        <div
            className="lean-app-sidebar-card lean-app-sidebar-card-chat" 
            style={{
                display: "flex",
                flex: 1,
                flexDirection: "column"
            }}
        >

            <div style={{ display: "flex"}}>
                <ListItem classes={{root:'lean-visitors-chat-sidebar-app-list-items'}} alignItems="flex-start">
                    <ListItemAvatar classes={{root:'lean-visitors-chat-sidebar-app-list-items list-item-avatar'}}>
                        <Avatar src={`data:image/png;base64,${props.thumbnailImage}`} />
                    </ListItemAvatar>
                    <ListItemText
                        classes={{root:'lean-sidebar-app-chat-text'}}
                        primary={props.userName.length>0 ? props.userName : "John Doe"}
                        secondary={
                            <React.Fragment>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    className={classes.inline}
                                    color="textPrimary"
                                >
                                {props.sender === "host" ? (<>You: {props.message}</>) : props.message  }
                                </Typography>
                            </React.Fragment>
                        }
                    />
                    {/* <ListItemText
                        classes={{root:'lean-sidebar-app-chat-date'}}
                        primary={getDate}
                    /> */}
                </ListItem>
            </div>

        </div>
    );
}

// const mapStateToProps = state => {
//     const { getVisitorChatListSideBarReducer } = state;
//     console.log("Visitors from side bar app-->", getVisitorChatListSideBarReducer);
//     return { getVisitorChatListSideBar: getVisitorChatListSideBarReducer };
// };

// export default LeanAppSideBar;

// export default connect(mapStateToProps, { getVisitorChatList })(
//     EachVisitorChat
// );

export default EachVisitorChat;