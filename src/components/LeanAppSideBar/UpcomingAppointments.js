import React, { Component } from "react";
import TimerSvg from '../../assets/Timer.svg'

// import amenities from "../assets/images/amenities.svg";

const UpcomingAppointmentsCard = (props) =>  {

  return (
    <div
      className="lean-app-sidebar-card"
      style={{
        display: "flex",
        flex: 1,
        flexDirection: "column"
      }}
    >
      <div style={{ display: "flex",textTransform: "capitalize" }} className="lean-text-header">
        {props.title}
      </div>

      <div
        style={{ display: "flex", flex: 1, flexDirection: "row" }}
        className="lean-text-sub-header"
      >
        <div style={{ display: "flex", padding: "0px 4px 0px 0px" }}>
          <img src={TimerSvg}/> 
        </div>
        <div style={{ display: "flex" }}>{props.sub_title}</div>
      </div>
    </div>
  );
  }
export default UpcomingAppointmentsCard;