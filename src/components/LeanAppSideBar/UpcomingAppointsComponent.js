import React, { Component } from "react";
import UpcomingAppointments from './UpcomingAppointments'
import { getFromAndTo } from '../../utils/timeFunctions';
import { getAllUpcomingEventsFromSidebarApp } from '../../redux/actions/upcomingEventsSidebarApp';
import { connect } from "react-redux";
import EmptyAppointments from '../../assets/Empty-Appointments.svg';
import { Link } from 'react-router-dom';
import FullPageLoading from '../Loaders/FullPageLoading';

// import {refresher} from '../../redux/actions/refresher'


const UpcomingAppointsComponent = props => {

    var getDate = new Date();
    console.log("DDDDDDDDd", getDate.toDateString())
    var fromTime = getDate.toLocaleTimeString();
    console.log("Tiiiimmmeeee", fromTime)
    var getFromandTo = getFromAndTo(getDate, getDate, Number(168));
    console.log("Duration MF", getFromandTo)


    React.useEffect(
        () => {
            console.log("on load")
            props.getAllUpcomingEventsFromSidebarApp({
                // "visitorSub":visitorSub
            })
        }, [props.refresherReducerNow.myAppointments,props.refresherReducerNow.cancelledAppointment]
    );

    let { isLoading: isLoadingListOfAppointments, data: dataListOfAppointments, isError: isErrorListOfAppointmentsIssues } = props.getAllUpcomingEvents


    console.log("dataListOfAppointments", dataListOfAppointments.data)

    return (

        <div>
            {isLoadingListOfAppointments ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading> : (
            <>
            {dataListOfAppointments.data === undefined || dataListOfAppointments.data.length == 0 ?
                (
                    <div>
                        <div className="lean-app-sidebar-item-heading">
                            <span>Upcoming Appointments</span>
                        </div >
                        <div className="lean-sidebar-app-upcoming-appointments-empty-div">
                            <div className="lean-sidebar-upcoming-appointments-empty-img">
                                <img src={EmptyAppointments}></img>
                            </div>
                        </div>
                        <div className="lean-sidebar-upcoming-appointments-empty-text">
                            <p>You dont have any Upcoming Appointments</p>
                        </div>
                    </div>
                    
                )
                :
                (
                    <>
                        <div className="lean-app-sidebar-item-heading">
                            <span>Upcoming Appointments</span>
                            <Link to="/MyAppointments">
                                <span className="lean-app-sidebar-item-heading-sub-heading">
                                    See All
                                </span>
                            </Link>
                        </div>
                        {dataListOfAppointments.data.map((eachAppointment) => {
                            return (
                                <>
                                    <div>
                                        <div className="lean-app-sidebar-list-item">
                                            <UpcomingAppointments title={eachAppointment.visitorName + " " + "meet at" + " " + (new Date(eachAppointment.arrivalTime).toLocaleTimeString())} sub_title={"Duration" + " " + ((eachAppointment.duration/3600000).toFixed(2) ) + " " + "Hr"} />
                                        </div>
                                    </div>
                                </>
                            )

                        })}

                    </>
                )}
                </>
        )}
            </div>

    );
}

const mapStateToProps = state => {
    const { getAllUpcomingEventsFromSidebarAppReducer,refresherReducer } = state;
    console.log("Visitors from side bar app-->", getAllUpcomingEventsFromSidebarAppReducer);
    return { getAllUpcomingEvents: getAllUpcomingEventsFromSidebarAppReducer,refresherReducerNow:refresherReducer };
};

// export default LeanAppSideBar;

export default connect(mapStateToProps, { getAllUpcomingEventsFromSidebarApp })(
    UpcomingAppointsComponent
);
// export default UpcomingAppointsComponent;