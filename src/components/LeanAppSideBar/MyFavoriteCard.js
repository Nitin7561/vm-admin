import React, { Component } from "react";

export default class MyFavoriteCard extends Component {
  render() {

    let status_class = 'lean-text-success'
    let status_text = 'Available'
    if(!this.props.status){
      status_text = 'Occupied'
      status_class = 'lean-text-failure'
    }
    return (
      <div
        className="lean-app-sidebar-card"
        style={{
          display: "flex",
          flex: 1,
          flexDirection: "column",
          padding: "8px"
        }}
      >
        <div
          style={{ display: "flex", flex: 1, flexDirection: "row" }}
          className="lean-text-header"
        >
          <div style={{ display: "flex", padding:"0px 4px 0px 0px"}}>
            {/* <img src={amenities}/> */}
            </div>
          
          
           <div style={{ display: "flex"}}>{this.props.title}</div>
        </div>

        <div
          style={{ display: "flex", flex: 1, flexDirection: "row" }}
          className="lean-text-sub-header"
        >
          <div style={{ display: "flex", alignItems:'center' }} className={`${status_class}`}>
            <div
              style={{ display: "flex" }}
              className={`${status_class}-dot`}
            ></div>
            {status_text}
          </div>
        </div>
      </div>
    );
  }
}
