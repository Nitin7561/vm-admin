import React, { Component } from "react";
import EachVisitorChat from './EachVisitorChat'
import { connect } from "react-redux";
import { getVisitorChatList } from '../../redux/actions/getVisitorChatList';
import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor';
import {fireDrawer} from '../../redux/actions/chatAction/fireDrawer';
import FullPageLoading from '../Loaders/FullPageLoading';
import EmptyAppointments from '../../assets/Empty-Appointments.svg';




const VisitorsChat = (props) => {

    React.useEffect(
        () => {
            console.log("on load")
            props.getVisitorChatList({})
        }, [props.fireDrawerReducerNow.fireDrawer]
    );

    let { isLoading: isLoadingGetVisitorChatListSidebar, data: dataGetVisitorChatListSidebar, isError: isErrorGetVisitorChatListSidebar } = props.getVisitorChatListSideBar;

    console.log("Visitor Chat===>", dataGetVisitorChatListSidebar.data)

    
    function openChat(visitorId) {
        
        console.log("Clicked on open chat", visitorId)

        console.log("VISITOR CHAT VISITOR SUB===>",visitorId)

        props.getDetailForVisitor({
        "visitorSub": visitorId
        });

        props.fireDrawer("fireDrawer");

        // setState(true);
        // handleClose();

    }
  
    return (
        <div style={{position:"relative"}}>
            <div className="lean-app-sidebar-item-heading">
                <span>Visitor Chat</span>
            </div>
            {isLoadingGetVisitorChatListSidebar ? <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading> : (
                <>
                {dataGetVisitorChatListSidebar.data === undefined || dataGetVisitorChatListSidebar.data.length == 0 ?
                (
                    <div>
                        {/* <div className="lean-app-sidebar-item-heading">
                            <span>Upcoming Appointments</span>
                        </div > */}
                        <div className="lean-sidebar-app-upcoming-appointments-empty-div">
                            <div className="lean-sidebar-upcoming-appointments-empty-img">
                                <img src={EmptyAppointments}></img>
                            </div>
                        </div>
                        <div className="lean-sidebar-upcoming-appointments-empty-text">
                            <p>You havent had any recent chats</p>
                        </div>
                    </div>
                )
                :
                (   
                    <>
                        {dataGetVisitorChatListSidebar.data.map((eachDataGetVisitorChatListSidebar) => {
                            return (
                                <div>
                                    <div className="lean-app-sidebar-list-item" onClick={()=>openChat(eachDataGetVisitorChatListSidebar.visitorId)}>
                                        <EachVisitorChat  thumbnailImage={eachDataGetVisitorChatListSidebar.icon} userName={eachDataGetVisitorChatListSidebar.name} message={eachDataGetVisitorChatListSidebar.chat[0].message} arrivalTime={eachDataGetVisitorChatListSidebar.chat[0].timestamp} sender={eachDataGetVisitorChatListSidebar.chat[0].sender} />
                                    </div>
                                </div>
                            )
                        })}
                    </>
                )}
                </>       
            )}
           
        </div>
    );
}

const mapStateToProps = state => {
    const { getVisitorChatListSideBarReducer, fireDrawerReducer } = state;
    console.log("Visitors from side bar app-->", getVisitorChatListSideBarReducer);
    return { getVisitorChatListSideBar: getVisitorChatListSideBarReducer,fireDrawerReducerNow:fireDrawerReducer };
};

export default connect(mapStateToProps, { getVisitorChatList ,getDetailForVisitor, fireDrawer })(
    VisitorsChat
);