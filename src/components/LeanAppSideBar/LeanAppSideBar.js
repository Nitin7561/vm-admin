import React from "react";
import { Drawer, Button, makeStyles } from "@material-ui/core";
import { NavLink, useLocation} from "react-router-dom";

import MyAppointments from "../../assets/my_appointments.svg";
import ListOfVisitors from "../../assets/listOfVisitors.svg";
import MyFrequentVisitorNavIcon from "../../assets/Frequent Visitors.svg";

import "./leanAppSideBar.scss";
import UpcomingAppointsComponent from "./UpcomingAppointsComponent";
import VisitorsChat from './VisitorsChat'
import MyFavoriteCard from "./MyFavoriteCard";



let drawerWidth = 240;

const LeanAppSideBar = () => {

  const menuItems = [
    {
      name: "My Appointments",
      icon: MyAppointments,
      link: "/MyAppointments",
      padding: "3px"
    },
    {
      name: "List Of Visitors",
      icon: ListOfVisitors,
      link: "/VisitorTable",
      padding: "4px"
    },
    {
      name: "Frequent Visitors",
      icon: MyFrequentVisitorNavIcon,
      link: "/MyFrequentVisitor",
      padding: "2px 1px"
    }
  ];

  let location = useLocation();
  console.log(location);

  return (
    <div>
      <Drawer
        variant="permanent"
        classes={{
          paper: "lean-app-sidebar"
        }}
      >
        <div className="lean-app-sidebar-toolbar-spacing" />
        <div className="sidebar-padding">
          <div className="lean-app-sidebar-item-heading">Visitor Management</div>

            {menuItems.map((item, index) => (
              // <NavLink
              //   to={item.link}
              //   exact
              //   key={item.name}
              // activeClassName="lean-app-sidebar-item-nav-selected"
              // >

              <div className="lean-app-sidebar-item-nav">
                <NavLink to={item.link} exact activeClassName="lean-nav-selected">
                  <Button
                    className={`lean-app-sidebar-item-nav-btn ${location.pathname===item.link?"lean-nav-selected":""}`}
                    disableElevation
                  >
                    
                    <img
                      style={{ padding: item.padding }}
                      className="sidebar-btn-icon"
                      src={item.icon}
                      alt={item.name}
                    />
                    {item.name}
                  </Button>
                </NavLink>
            </div>

            // </NavLink>
          ))}
        <UpcomingAppointsComponent />
        <VisitorsChat/>
        </div>
      </Drawer>
    </div>
  );
};

export default LeanAppSideBar

