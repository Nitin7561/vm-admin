import React from 'react';
import Button from '../Button/Button';
import { DropdownField } from '../InputTextField/DropdownField'
import AddAppointmentInAModal from '../../layouts/AddAppointment/AddAppointmentInAModal'
import { Grid, Hidden } from "@material-ui/core";
import EventNoteIcon from '@material-ui/icons/EventNote';
import './HeaderComponent.scss';
import { ModalContext } from "../../utils/ModalContext";
import { InputAdornment } from '@material-ui/core';
// import EventNoteIcon from '@material-ui/icons/EventNote';

export const HeaderComponent = props => {
    const [status, changeStatus] = React.useState("All");
    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    const handleNext = () => {
        openModal({ name: 'AddAppointmentInAModal', props: { "isDisabled": false, "classStyles": { paperScrollPaper: 'lean-add-appointment-modal' } } })
    }

    return (


        <div className="lean-header-table-page">

            <div className="lean-header-table-page-item lean-page-title">
                <div className="lean-header-table-page lean-header-page">{props.titleName ? props.titleName : "List Of Appointments"}</div>
            </div>
            <div className="lean-header-table-page-item lean-page-content">
                {props.isRequired === "yes" ? (
                    <>

                        <div className="lean-header-table-page-container" >
                            <span className="lean-header-table-page-item lean-status-label">Status</span>

                            <div className="lean-header-table-page-item lean-status-dropdown">
                                <DropdownField
                                    // label="Status"
                                    value={status}
                                    eventHandler={changeStatus}
                                    menuItems={props.StatusValues ? props.StatusValues : null}

                                />
                            </div>
                        </div>

                        <div className="lean-header-table-page-container" >

                            <Hidden smUp>
                                <span className="lean-header-table-page-item lean-status-label">Period</span>
                            </Hidden>
                            <div className="lean-header-table-page-item lean-time-status-dropdown">
                                <DropdownField
                                    // label="Status"
                                    value={props.timeStatus}
                                    eventHandler={props.onTimeRangeChange}
                                    menuItems={props.TimeStatusValues ? props.TimeStatusValues : null}
                                    extraInputProps={{
                                        startAdornment: <InputAdornment>
                                            <EventNoteIcon fontSize='small' />
                                        </InputAdornment>
                                    }}
                                />
                            </div>
                        </div>
                        <div className="lean-header-table-page-item lean-addAppointment-button lean-header-table-page-container" >

                            <Button
                                text={props.ButtonText ? props.ButtonText : "Add Appointment"}
                                size="small-btn"
                                variant="contained"
                                color="primary"
                                onClick={handleNext}

                            />

                        </div>
                    </>

                ) : (
                        <>
                            <div className="lean-header-table-page-item lean-addAppointment-button lean-header-table-page-container" >

                                <Button
                                    text={props.ButtonText ? props.ButtonText : "Add Appointment"}
                                    size="small-btn"
                                    variant="contained"
                                    color="primary"
                                    onClick={handleNext}
                                />

                            </div>
                        </>
                    )}


            </div>


        </div>


    );
};
