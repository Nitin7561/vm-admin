import React, { useContext } from "react";
import { connect } from "react-redux";
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Chip,
  TablePagination,
  Button,
  Typography,
  Toolbar,
  Tooltip,
  Grid
} from "@material-ui/core";

import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";

// import LeanovateIcon from "../../assets/leanovate.svg";
// import OfficeIcon from "../../assets/office.svg";
// import MapIcon from "../../assets/map1.svg";
import EmptySVG from '../../assets/emptyFav.svg'
import EventNoteIcon from '@material-ui/icons/EventNote';

import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

import "./tables.scss";
import { ModalContext } from "../../utils/ModalContext";
import { STATUS_ENUM } from "../../utils/Enums";
import FullPageLoading from "../Loaders/FullPageLoading";

// import { addItem, loadMyBookings } from "../../actions/myBookings";

// import { adaptMoreInfo, adaptBooking, adaptCancel } from '../../utils/adapters';

const MenuIcon = ({ handleClick, iconClass }) => (
  <IconButton
    aria-label="more"
    aria-controls="long-menu"
    aria-haspopup="true"
    onClick={handleClick}

  >
    <MoreVertIcon className={iconClass} />
  </IconButton>
);

const MoreMenu = ({ menuItems, modalOpener }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);


  const handleClick = event => {
    // if(event)
    setAnchorEl(event.currentTarget);
  };

  // const openModal = () => {

  //   handleClose();
  // };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <MenuIcon handleClick={handleClick} iconClass="lean-table-body-menu-icon" />
      <Menu
        classes={{ paper: "lean-table-body-menu" }}
        anchorEl={anchorEl}
        anchorOrigin={{
          horizontal: "right",
          vertical: "bottom"
        }}
        getContentAnchorEl={null}
        open={open}
        disablePortal
        onClose={handleClose}
      >
        {menuItems.map(({ name, clickHandler }) => {
          return (
            <MenuItem
              className="lean-table-body-menu-item"
              onClick={() => {
                console.log(clickHandler);
                modalOpener(clickHandler);
                handleClose();
              }}
            >
              {name}
            </MenuItem>
          );
        })}
      </Menu>
    </>
  );
};

const TableActions = ({ page, rowsPerPage, count, pagination, onChangePage }) => {
  let noOfPages = Math.ceil(count / rowsPerPage);

  return (
    <div className="lean-table-pagination-action">
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, 1, pagination)}
      >
        First
      </Button>
      <Button
        className="lean-table-pagination-btn lean-table-pagination-btn-prev"
        disabled={page === 1}
        onClick={() => onChangePage({}, page - 1, pagination)}
      >
        <ChevronLeftIcon />
      </Button>
      <Button
        disabled={page === noOfPages}
        className="lean-table-pagination-btn lean-table-pagination-btn-next"
        onClick={() => onChangePage({}, page + 1, pagination)}
      >
        <ChevronRightIcon />
      </Button>
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, noOfPages, pagination)}
      >
        Last
      </Button>
    </div>
  );
};

const ListOfSupportTable = ({ page, setPage, onClickSort, rowsPerPage, setRowsPerPage, handleChangePage, handleChangeRowsPerPage, onChangeRX, sortColumnIndex, sortOrder }) => {

  const { setCurrentModal } = useContext(ModalContext);

  const openModal = ({ name, props }) => setCurrentModal({ name, props });

  const isLoading = false;
  const listOfSupport = {
    data:[
      {
        description:"lorem impsum..",
        moduleName: "Room Booking",
        submitDateAndTime:"24 Feb 2020,10:00 AM",
        whoSubmitter:"Grigoriy Kozhukhov",
        owner:"Admin_1",
        severity:"Critical",
        status:"New",
        resolveDateTime:""
      },
      {
        description:"lorem impsum..",
        moduleName: "Visitor Management",
        submitDateAndTime:"24 Feb 2020,10:00 AM",
        whoSubmitter:"Grigoriy Kozhukhov",
        owner:"Admin_1",
        severity:"Error",
        status:"Resolved",
        resolveDateTime:""
      },
      {
        description:"lorem impsum..",
        moduleName: "Way Finder",
        submitDateAndTime:"24 Feb 2020,10:00 AM",
        whoSubmitter:"Grigoriy Kozhukhov",
        owner:"Admin_1",
        severity:"Warning",
        status:"WIP",
        resolveDateTime:""
      },
      {
        description:"lorem impsum..",
        moduleName: "Way Finder",
        submitDateAndTime:"24 Feb 2020,10:00 AM",
        whoSubmitter:"Grigoriy Kozhukhov",
        owner:"Admin_1",
        severity:"Critical",
        status:"New",
        resolveDateTime:"23 Feb 2020,11:00 AM "
      },
      {
        description:"lorem impsum..",
        moduleName: "Visitor Management",
        submitDateAndTime:"24 Feb 2020,10:00 AM",
        whoSubmitter:"Grigoriy Kozhukhov",
        owner:"Admin_1",
        severity:"Critical",
        status:"New",
        resolveDateTime:"23 Feb 2020,11:00 AM"
      }
    ]
  }

  let pagination = 5;
  // let { singleBookings, recurrantBookings, pagination, isLoading, rxdBatchIds } = myBookings;

  // React.useEffect(() => {
  //   // if (!rxdBatchIds) {
  //   //   onChangeRX([])
  //   // } else {
  //   //   onChangeRX(rxdBatchIds);
  //   // }
  // }, [])

  // if (!singleBookings) {
  //   singleBookings = [];
  // }

  // if (!recurrantBookings) {
  //   recurrantBookings = [];
  // }


  function getCustomDate(date) {
    return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`;
  }

  function getCustomTime(time) {
    let ampm = "am";

    var h = time.getHours();
    var m = time.getMinutes();
    // add a zero in front of numbers<10
    if (h >= 12) {
      ampm = "pm";
      h = h % 12 === 0 ? h : h % 12;
    }

    h = checkTime(h);
    m = checkTime(m);

    return `${h}:${m} ${ampm}`;
  }

  function getCustomTimeDiff(from, to) {
    let diff = Math.abs(to - from);

    let hours = Math.floor(diff / (60 * 60));
    let h = checkTime(hours);
    let mins = diff / 60 - hours * 60;
    let m = checkTime(mins);
    return `${h}:${m}`;
  }

  function checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }

  let months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  const handleTableHeaderClick = event => {

  };

  const handleMouseOverRow = index => {
    console.log("handleMouseOverRow", index);
  };

  // const handleChangePage = (event, newPage) => {
  //   setPage(newPage);

  //   onLoad({
  //     fromTime: Math.ceil((((new Date()).getTime()) / 1000) - 2 * 365 * 24 * 60 * 60),
  //     toTime: Math.ceil((((new Date()).getTime()) / 1000) + 2 * 365 * 24 * 60 * 60),
  //     rowsPerPage: rowsPerPage,
  //     actualCount: pagination ? pagination.pages[newPage].actualCount : 0,
  //     sortColumnIndex: 3,
  //     sortOrder: -1,
  //     statusFilter: 4
  //   });
  // };

  // const handleChangeRowsPerPage = event => {
  //   setRowsPerPage(parseInt(event.target.value, 10));
  //   setPage(0);
  //   onLoad({
  //     fromTime: Math.ceil((((new Date()).getTime()) / 1000) - 2 * 365 * 24 * 60 * 60),
  //     toTime: Math.ceil((((new Date()).getTime()) / 1000) + 2 * 365 * 24 * 60 * 60),
  //     rowsPerPage: event.target.value,
  //     actualCount: 0,
  //     sortColumnIndex: 3,
  //     sortOrder: -1,
  //     statusFilter: 4
  //   });
  // };

  let tableHeaders = [
    { name: "Description", sortIndex: 0 },
    { name: "Module Name", sortIndex: 1 },
    { name: "Submit Date and Time", sortIndex: 3 },
    { name: "Who Submitter", sortIndex: null },
    { name: "Owner", sortIndex: null },
    { name: "Severity", sortIndex: null },
    { name: "Status", sortIndex: null },
    { name: "Resolve Date/Time", sortIndex: null },
    { name: "Action", sortIndex: null },
  ];

  return (
    <div>
      {(isLoading) ? <FullPageLoading mainClassNames="lean-loaders-full-page">Getting My Bookings...</FullPageLoading> :

      listOfSupport.data.length !== 0 ? (

          <Table>
            <TableHead classes={{ root: "lean-table-header" }}>
              <TableRow>
                {tableHeaders.map(tableHeader => (
                  <TableCell>
                    {tableHeader ? (

                      tableHeader.sortIndex !== null ? (
                        <Button
                          disableRipple
                          name={tableHeader ? tableHeader.sortIndex : null
                          }
                          onClick={() => onClickSort(tableHeader.sortIndex)}
                        >
                          {tableHeader.name}
                          {sortColumnIndex === tableHeader.sortIndex ? sortOrder === 1 ? <i className="lean-icon-arrow_drop_down" /> : <i className="lean-icon-arrow_drop_up" /> : null}
                        </Button>
                      ) : (
                          <span>
                            {tableHeader.name}
                          </span>
                        )) : null
                    }

                  </TableCell>
                ))}
              </TableRow>
            </TableHead>

            <TableBody classes={{ root: "lean-table-body" }}>
              {listOfSupport.data.map((eachSupportIssue, index) => {
              

                let moreMenuProps = [

                  {
                    name: "Reschedule",
                   
                  },

                  {
                    name: "Cancel",
          
                  }
                ];

                return (
                  <TableRow
                    key={index}
                    hover
                    className="lean-table-row"
                    onClick={(e) => {}
                    }
                  >
                    <TableCell>
                      {eachSupportIssue.description}
                    </TableCell>

                    <TableCell className="lean-table-body-subj-cell">
                      {eachSupportIssue.moduleName}
                    </TableCell>


                    <TableCell>{eachSupportIssue.submitDateAndTime}</TableCell>
                    
                    <TableCell>{eachSupportIssue.whoSubmitter}</TableCell>
                    
                    <TableCell>{eachSupportIssue.owner}</TableCell>
                    
                    <TableCell>{eachSupportIssue.severity}</TableCell>
                
                    
                    <TableCell className={`lean-table-body-status lean-room-${STATUS_ENUM[eachSupportIssue.status]}`}>
                      {eachSupportIssue.status}
                    </TableCell>
                    
                    <TableCell>{eachSupportIssue.resolveDateTime}</TableCell>

                    <TableCell>
                      {/* {eachSupportIssue.hideAction || eachSupportIssue.status === 1 || eachSupportIssue.status === 2 ? null : ( */}
                        <MoreMenu
                          menuItems={moreMenuProps}
                          modalOpener={openModal}
                        />
                      {/* )} */}
                    
                    </TableCell>
                  
                  
                  </TableRow>
                );
              })}
            </TableBody>


            <TableCell
              colspan="3"
              className="lean-table-pagination lean-table-pagination-left-heading"
            >
              <Toolbar className="lean-table-pagination-left-heading-tb">
                <div></div>
                <Typography
                  variant="caption"
                  className="lean-table-pagination-heading"
                >
                Showing {(page - 1) * rowsPerPage + 1}-
                {Math.min(
                  rowsPerPage * (page),
                  pagination ? (pagination <= 5 ? listOfSupport.data.length : pagination) : 0
                )}{" "}
                  out of {pagination ? (pagination <= 5 ? listOfSupport.data.length : pagination) : 0} entries
                </Typography>
              </Toolbar>
            </TableCell>


            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              count={pagination ? (pagination <= 5 ? listOfSupport.data.length : pagination) : 0}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={(e, np) => handleChangePage(e, np, pagination)}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              colspan="100"
              align="center"
              labelDisplayedRows={({ from, to, count }) => {
                console.log("labelDisplayedRows", from, to, count)
                return `${page} of ${Math.ceil(count / rowsPerPage)}`
              }

              }
              classes={{
                select: "lean-table-pagination-select",
                spacer: "lean-table-pagination-spacer",
                caption: "lean-table-pagination-heading",
                root: "lean-table-pagination lean-table-pagination-left-heading",
                toolbar: "lean-table-pagination-left-heading-tb",
                selectRoot: "lean-table-pagination-select-root"
              }}
              // SelectProps = {{
              //   className:'lean-table-pagination-select'
              // }}
              ActionsComponent={() => (
                <TableActions
                  rowsPerPage={rowsPerPage}
                  page={page}
                  count={pagination ? pagination.uniqueDocs : 0}
                  pagination={pagination}
                  onChangePage={(e, np) => handleChangePage(e, np, pagination)}
                />
              )}
            />
          </Table>) : (
            <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
              <img src={EmptySVG} />
              <span className="lean-table-empty-screen-text">No Bookings</span>
            </Grid>
          )
      }
    </div >
  );
};

const mapStateToProps = state => {
  // const {  } = state;
  // console.log("ListOfSupportTable", myBookings);
  // return {  };
};

export default connect(null)(
  ListOfSupportTable
);
