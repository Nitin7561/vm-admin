import React from 'react'
import { Redirect } from 'react-router-dom'
import NotificationVisitorApproval from "./NotificationVisitorApproval";
import NotificationMessage from "./NotificationMessage";
import NotificationUserInput from "./NotificationUserInput";
import NotificationChat from "./NotificationChat";
import './notificationPopUp.scss'
import { getAllNotificationsAction,seeNotificationAction, replyNotificationAction } from '../../redux/actions/notifications';
import { connect } from 'react-redux';
import { CircularProgress } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function PopUpNotificationPage(props){

  let {getAllNotificationsAction, seeNotificationAction, replyNotificationAction} = props
  let {getAllNotificationsReducer} = props
  
  React.useEffect(() => {
    getAllNotificationsAction()
  }, [])

  let [redirect,setRedirect] = React.useState(false)

  //--------------- SnackBar Open for displaying errro messages-----------------
  const [open, setOpen] = React.useState(true);  

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
  function loadSnackBar(message) {
    return (      
        <Snackbar open={open} autoHideDuration={6000}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          onClose={handleClose}>
          <Alert onClose={handleClose} severity="error">
            {/* This is a success message!                 */}
            {message}
          </Alert>
        </Snackbar>      
    )
  }
//--------------------------------------------------

let { isLoading: isLoadingNotifications, data: notificationsData, error: errorNotifications, isError: isErrorNotifications  } = getAllNotificationsReducer

//--------------- Handle reply Request ---------------
const handleReplyClick = (e,id, newValue,type) => {        
  console.log('handleReplyClick ==>',id,newValue,type)    
  const reqBody = {
    "notifications":  [ id ]
  }
  // props.deleteNotificationAction(reqBody)
};

//--------------- Handle delete Request ---------------
const handleSeenClick = (e,id, isSeen) => {    
  console.log('handleSeenClick ==>',id,isSeen)    
  // e.preventDefault()    
  const reqBody = {
    "notifications":  [ id ]
  }
  if(!isSeen)
    seeNotificationAction(reqBody)    
};

//-------------
const renderRedirect = () => { 
  console.log('renderRedirect') 
  if (redirect) {
    setRedirect(false)
    return <Redirect to='/notifications' />
  }
    
  
}
  
    return (
      <div className="lean-notification-card">        
        
        {/* <div style={{overflowY:'auto',height:'90%'}}> */}
        <div >
          {isLoadingNotifications ? <CircularProgress /> : isErrorNotifications ? loadSnackBar('View All Notifications  : '+errorNotifications) : 
          mapNotifications(notificationsData,handleSeenClick,handleReplyClick)} 
        </div>
       
        {/* <div className='div-viewAll' onClick={()=>{setRedirect(true)}}>
          {renderRedirect()}
          <span className='text'>View All</span>
        </div> */}
        <div className='div-viewAll'>
          <span className='text'>View All</span>
        </div>
      </div>
    );
  }

  const mapStateToProps = (state) => {
    let { getAllNotificationsReducer, replyNotificationReducer } = state;
    return { getAllNotificationsReducer, replyNotificationReducer };
  }
  
  
  export default connect(mapStateToProps, {
    getAllNotificationsAction, seeNotificationAction, replyNotificationAction
  })(PopUpNotificationPage)


//------------------- Timer Utility function -----------------------
function mapNotifications(rxdMessg,handleSeenClick,handleReplyClick){
  var messgList =[]
  rxdMessg.slice(0,5).map((data) => {
    // const { seen, replied, timestamp } = data      
    // const { id, subject, content, type, redirects, icon, typespec } = data.data  
    const { timestamp } = data      
    const { type} = data.data                  
                    
    
    var date1 = new Date();
    var date2 = new Date(timestamp);
    var displayTime, displayDate =getCustomDateTime(new Date(timestamp))

    // To calculate the time difference of two dates 
    var Difference_In_Time = date2.getTime() - date1.getTime();

    // To calculate the no. of days between two dates 
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    
    if(Difference_In_Days>2)
      displayTime = getCustomDate(date2)
    else if(Difference_In_Days == 1)
      displayTime = 'yesterday'
    else
      displayTime = getCustomTimeDiff(date2.getTime()/1000,date1.getTime()/1000)


    if (type == 'plain')
      messgList.push(<NotificationMessage value={data} time={displayTime} date={displayDate}  seenHandler={handleSeenClick}/>)
    else if (type == 'select')
      messgList.push(<NotificationUserInput value={data} time={displayTime} replyHandler={handleReplyClick}  seenHandler={handleSeenClick} />)
    else if (type == 'confirm')
      // messgList.push(<VisitorApproval value={seen, replied, id, subject, content, type, redirects, icon, typespec, timestamp} />)
      messgList.push(<NotificationVisitorApproval value={data} time={displayTime} replyHandler={handleReplyClick}  seenHandler={handleSeenClick} />)
    else if (type == 'string')
      messgList.push(<NotificationChat value={data} time={displayTime} replyHandler={handleReplyClick}  seenHandler={handleSeenClick} />)

  })
  return messgList
}
//-------------- Display Date & Time Logic -------------------
let months = [
  "Jan",    "Feb",    "Mar",    "Apr",    "May",    "Jun",    "Jul",    "Aug",    "Sep",    "Oct",    "Nov",    "Dec"
];
function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}
function getCustomDate(date) {
  return `${date.getDate()}-${months[date.getMonth()]}-${date.getFullYear()}`;
}

function getCustomDateTime(date) {
  return `${date.getDate()}-${months[date.getMonth()]}-${date.getFullYear()} 
    ${checkTime(date.getHours())}:${checkTime(date.getMinutes())}:${checkTime(date.getSeconds())}`;
}

function getCustomTimeDiff(from, to) {
  let diff = Math.abs(to - from);
  let hours = Math.floor(diff / (60 * 60));
  let h = checkTime(hours);
  let mins = diff / 60 - hours * 60;
  let m = checkTime(mins);

  if(h == 0)
    return `${Math.round(m)} Min`
  else
   return `${h} hr ${Math.round(m)} Min` ;
}