import React from 'react'
import Notification_bell from "../../assets/images/Notification_bell.png";
import './notificationPopUp.scss'

export default function NotificationMessage(props) {
  // const { seen, replied, timestamp } = props.value  
  // const { id, subject, content, redirects, icon, typespec } = props.value.data
  const { seen } = props.value    
  const { id, subject } = props.value.data

  const clickspan1 = (event) => {    
    // if(!seen)
       props.seenHandler(event,id,seen)
  }

  const  isMessgSeen =  seen ? {} : {backgroundColor:'#ffe5e5'}  
  
    return (

      <div className='lean-notification-box-root' style={isMessgSeen}>
        <div className='lean-notification-box-flex'>
          <div className='div-1'>
            <div className="div-logo">
              <img className="logo-bell" src={Notification_bell} alt='loading icon..'/>
            </div>
          </div>
          <div className='div-2'>
            <div className='message'>              
              <span className="message-content" onClick={clickspan1}>{subject}</span>              
            </div>
            <div className='input'>
              <span className='span-date'>{props.date}</span>
              {/* <span className='span-time-2'>{props.time}</span> */}
            </div>
          </div>
          <div className='div-3'>
            <span className='span-time-2'>{props.time}</span>
          </div>
        </div>
        
      </div>

    );
  }
