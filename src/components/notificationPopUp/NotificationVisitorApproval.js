import React from 'react'
// import Settings from "../../assets/images/Settings.png";
// import Notification from "../../assets/images/Notification.png";
import Notification_image from "../../assets/images/notification_image.svg";
import './notificationPopUp.scss'

export default function NotificationVisitorApproval(props){

  // const { seen, replied, timestamp } = props.value  
  // const { id, subject, content, type, redirects, icon, typespec } = props.value.data
  const { seen } = props.value  
  const { id, content, type, typespec } = props.value.data
  
  
  //-- Message seen?
    const  isMessgSeen =  seen ? {} : {backgroundColor:"#ffe5e5"}

    const clickspan1 = (event) => {    
      // if(!seen)
        props.seenHandler(event,id)
    }

    return (
      <div className="lean-notification-box-root" style={isMessgSeen}>
        <div className='lean-notification-box-flex'>
          <div className='div-1'>
            <div className="div-logo">
              <img className="logo-visitor" src={Notification_image} alt='loading icon..'/>
            </div>  
          </div>
          <div className='div-2'>
            <div className='message'>
            {/* <div className="div-cgp-header"> */}
              <span className="message-content" onClick={clickspan1}>{content}</span>
            {/* </div> */}
            </div>
            <div className='input'>
              <button className='input-button button-1-text' onClick={(event) => props.replyHandler(event,id,true,type)}> {typespec['yesText']} </button>
              <button className='input-button-2 button-2-text' onClick={(event) => props.replyHandler(event,id,false,type)}>{typespec['noText']}</button>
              {/* <span className='span-time'>{props.time}</span> */}
            </div>            
          </div>  

          <div className='div-3'>
            <span className='span-time-2'>{props.time}</span>
          </div>        
        </div>        

      </div>
      
    );
  }