import React from 'react'
import Notification_bell from "../../assets/images/Notification_bell.png";
import './notificationPopUp.scss'

export default function NotificationVisitorApproval(props){

  // const { seen, replied, timestamp } = props.value  
  // const { id, subject, content, type, redirects, icon, typespec } = props.value.data
  const { seen } = props.value  
  const { id, content, type, typespec } = props.value.data

  const clickspan1 = (event) => {    
    // if(!seen)
       props.seenHandler(event,id)
  }

  const  isMessgSeen =  seen ? {} : {backgroundColor:"#ffe5e5"}
 
    return (
      <div className="lean-notification-box-root" style={isMessgSeen}>
        <div className='lean-notification-box-flex'>
          <div className='div-1'>
            <div className="div-logo">
              <img className="logo-bell" src={Notification_bell} alt='loading icon..'/>
            </div>  
          </div>
          <div className='div-2'>
            <div className='message'>
            {/* <div className="div-cgp-header"> */}
              <span className="message-content" onClick={clickspan1}>{ content}</span>
            {/* </div> */}
            </div>
            <div className='input'>
            {typespec.enums.map((enumVal)=>{
              return <button className='input-button button-1-text' onClick={(event) => props.replyHandler(event,id,enumVal,type)} > {enumVal} </button>
            })}
              {/* <span className='span-time'>{props.time}</span> */}
            </div>
          </div>   

          <div className='div-3'>
            <span className='span-time-2'>{props.time}</span>
          </div>       
        </div>        
      </div>
      
    );
  }
