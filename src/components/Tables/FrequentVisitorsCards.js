import React from "react";
import { connect } from "react-redux";
import { IconButton, Drawer } from "@material-ui/core";
import Menu from "@material-ui/core/Menu";
import Avatar from "@material-ui/core/Avatar";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { getMyFrequentVisitors } from '../../redux/actions/frequentVisitors';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import UserImage from '../../assets/kevin-hart.jpg';
import { HeaderComponent } from '../HeaderComponent/HeaderComponent';
import EventNoteIcon from '@material-ui/icons/EventNote';
import { ModalContext } from "../../utils/ModalContext";
import AddAppointmentInAModal from '../../layouts/AddAppointment/AddAppointmentInAModal';
import ReportUser from "../../layouts/Report/ReportUser"
import RejectVis from "../../layouts/RejectVisitor/RejectVis"
import CancelAppointment from '../../layouts/CancelAppt/CancellAppointment';
import ChatIcon from '../../assets/chat-copy.svg';
import ChatDisabledIcon from '../../assets/chat-disabled.svg';
import ChatBox from "../../layouts/Chat/ChatBox";
import CloseIcon from '@material-ui/icons/Close';
import ChatButton from "../../layouts/Chat/ChatButton";
import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor'
import EmptySVG from '../../assets/emptyFav.svg'
import { Grid, CircularProgress } from "@material-ui/core";
import FavIcon from '../../assets/star.png'
import UnFavIcon from '../../assets/unblocked.png'
import MoreMenu from './MoreMenuFrequentVisitors'
import { addToFrequentVisitor } from '../../redux/actions/addToFrequentVisitor';
import { removeFrequentVisitor } from '../../redux/actions/removeFrequentVisitor';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import { fireDrawer } from '../../redux/actions/chatAction/fireDrawer';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import BusinessIcon from '@material-ui/icons/Business';

import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Chip,
  TablePagination,
  Button,
  Typography,
  Toolbar,
  Tooltip
} from "@material-ui/core";

import "./FrequentVisitors.scss";
import FullPageLoading from "../Loaders/FullPageLoading";

const TableActions = ({ page, rowsPerPage, count, onChangePage }) => {
  let noOfPages = Math.ceil(count / rowsPerPage);

  return (
    <div className="lean-table-pagination-action">
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, 1)}
      >
        First
      </Button>
      <Button
        className="lean-table-pagination-btn lean-table-pagination-btn-prev"
        disabled={page === 0}
        onClick={() => onChangePage({}, page - 1)}
      >
        <ChevronLeftIcon />
      </Button>
      <Button
        disabled={page === noOfPages}
        className="lean-table-pagination-btn lean-table-pagination-btn-next"
        onClick={() => onChangePage({}, page + 1)}
      >
        <ChevronRightIcon />
      </Button>
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, noOfPages)}
      >
        Last
      </Button>
    </div>
  );
};

const FrequentVisitorsCards = ({ visitorsList, onLoad, getDetailForVisitor, refresherState, removeFrequentVisitor, fireDrawerNow }) => {

  const [page, setPage] = React.useState(1);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [modal, setModal] = React.useState(null);
  const [state, setState] = React.useState(false);
  const [order, changeOrder] = React.useState("asc");
  const [sortBy, changeSortBy] = React.useState("name");
  const [sortColumnIndex, setSortIndex] = React.useState(0);
  const { setCurrentModal } = React.useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });

  React.useEffect(
    () => {
      console.log("on load ")
      changeListOfVisitors([])
      setPage(1)
      onLoad({
        "page": 1,
        "offset": rowsPerPage,
        "order": order,
        "sortBy": sortBy
      })
    }, [order, sortBy, refresherState.reportedVisitor, refresherState.reportedVisitorReg, refresherState.blockedUser, refresherState.unblockedUser, refresherState.addAsFreqVisitor, refresherState.removeAsFreqVisitor]
  );

  const [listOfVisitors, changeListOfVisitors] = React.useState([])

  let { isLoading: loading, data, error } = visitorsList;

  console.log("visitor list-->", visitorsList)

  let apiData = data.data.data;

  let totalNoOfPages = Math.ceil(data.data.total / rowsPerPage)

  React.useEffect(() => {
    changeListOfVisitors([...listOfVisitors, ...data.data.data])
  }, [data.data.data])

  let statusEnum = {
    approved: "approved",
    Approved: "Approved",
    Registered: "Registered",
    // Blocked:"blocked"
  };

  let tableHeaderNameEnum = {

    "Visitor Name": "name",
    "Visitor Email ID": "email"
  };

  const handleTableHeaderClick = (tableHeaderName, index) => {

    console.log("Table header name==>", tableHeaderName)
    console.log("Table header name enum==>", tableHeaderNameEnum[tableHeaderName])

    setSortIndex(index)

    if (tableHeaderNameEnum[tableHeaderName] == sortBy) {
      if (order == "asc") {
        changeOrder("desc");
      }
      else {
        changeOrder("asc");
      }
    }
    else {
      changeSortBy(tableHeaderNameEnum[tableHeaderName]);
      changeOrder("asc");
    }

  };

  const handleMouseOverRow = index => {
    // console.log("handleMouseOverRow", index);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    onLoad({
      "page": newPage,
      "offset": rowsPerPage,
      "order": order,
      "sortBy": sortBy
    });
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(1);
    onLoad({
      "page": 1,
      "offset": parseInt(event.target.value, 10),
      "order": order,
      "sortBy": sortBy
    });
  };

  function openChat(visitorId) {

    console.log("Clicked on open chat", visitorId)
    getDetailForVisitor({
      "visitorSub": visitorId
    })

    fireDrawerNow("fireDrawer");
    // setState(true)

  }

  let tableHeaders = [
    null,
    { name: "Visitor Name", sortIndex: 0 },
    { name: "Visitor Mobile Number", sortIndex: null },
    { name: "Visitor Email ID", sortIndex: 2 },
    { name: "Comapany Name/Location", sortIndex: 3 },
    { name: "Status", sortIndex: null },
    { name: "", sortIndex: null },
    { name: "", sortIndex: null },
    { name: "", sortIndex: null },
    // null
  ];

  function openFav(e, eachVisitorinList) {
    console.log("EVENT", e.target.tagName)
    if (e.target.tagName === "IMG") {

      removeFrequentVisitor({
        "visitorSub": eachVisitorinList.visitorSub
      })
      openModal({ name: 'RemoveAsFreq', props: { "visitorName": eachVisitorinList.name } })

    }
    else {
      e.persist();
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
    }
  }

  const handleScroll = (e) => {

    // if (!fullScreen) return;

    if (loading || error || (totalNoOfPages < (page + 1))) return;

    if (e.currentTarget.offsetHeight + e.currentTarget.scrollTop >= e.currentTarget.scrollHeight - 50) {

      console.log("LAZY LOAD", e.currentTarget.offsetHeight, e.currentTarget.scrollTop, e.currentTarget.scrollHeight, e.currentTarget.offsetHeight + e.currentTarget.scrollTop >= e.currentTarget.scrollHeight - 50)

      handleChangePage(null, page + 1, null);
      console.log('SCROLL eop')
    }
  }


  return (
    <div className="lean-frequent-visitors-table" onScroll={(e) => handleScroll(e)}>
      <HeaderComponent titleName={`Frequent Visitors ${((data.data.total === 0) || !data.data.total) ? "" : `   (${data.data.total})`}`} ButtonText="Add Appointment" />

      <div>

        {(loading && page === 1) ? <FullPageLoading mainClassNames="lean-loaders-full-page">Getting Visitors...</FullPageLoading> :

          <>
            {
              listOfVisitors.length !== 0 ? (

                <>
                  {listOfVisitors.map((eachVisitorinList, index) => {

                    // var duration = eachVisitorinList.duration / 3600000;
                    // var arrivalTime = eachVisitorinList.arrivalTime;

                    // var dateNew = new Date(arrivalTime)
                    // var timeNew = dateNew.toLocaleTimeString()

                    // console.log("Date ===>", dateNew.toDateString());
                    // console.log("Duration==>", duration);

                    // var checkincheckoutTime = getFromAndTo(dateNew, dateNew, Number(duration))
                    // var checkInTime = new Date(checkincheckoutTime.from * 1000).toLocaleTimeString();
                    // var checkOutTime = new Date(checkincheckoutTime.to * 1000).toLocaleTimeString();


                    // console.log("checkincheckoutTime", checkincheckoutTime);
                    // console.log("checkInTime", checkInTime);
                    // console.log("checkOutTime", checkOutTime);

                    // let moreMenuProps = [

                    //     {
                    //         name: "Reschedule",
                    //         clickHandler: { name: "RescheduleModal", props: adaptBooking('mybookings', booking) }
                    //     },

                    //     {
                    //         name: "Cancel",
                    //         clickHandler: { name: "CancelModal", props: adaptCancel('mybookings', booking) }
                    //     }
                    // ];

                    return (


                      <div key={index}
                        hover
                        className="lean-table-row"
                        onMouseOver={e => handleMouseOverRow(index)}
                      >
                        <Grid container className="lean-card lean-table-card">
                          <Grid item className="lean-table-card-icon-container">
                            <div className="lean-table-card-icon">

                              {

                                statusEnum[eachVisitorinList.status] === "invited" || statusEnum[eachVisitorinList.status] === "Invited" ?
                                  (<>
                                    <Avatar />
                                  </>
                                  ) :
                                  (<>
                                    <Avatar src={`data:image/png;base64,${eachVisitorinList.thumbnailImage}`} />
                                  </>
                                  )
                              }


                            </div>
                          </Grid>
                          <Grid item className="lean-table-card-headline-container">
                            <div className="lean-table-card-headline">
                              <Chip
                                label={statusEnum[eachVisitorinList.status]}
                                className={`lean-table-body-status lean-room-${
                                  statusEnum[eachVisitorinList.status]
                                  }`}
                              />

                              <div className="lean-table-card-headline-name" >
                                <span>{eachVisitorinList.name}</span>
                                {

                                  eachVisitorinList.isChatBlocked ?
                                    (
                                      <img src={ChatDisabledIcon} className='lean-list-visitors-table-diableschat-icon' />
                                    )
                                    :
                                    (

                                      <img src={ChatIcon} onClick={() => openChat(eachVisitorinList.visitorSub)} />
                                    )
                                }
                              </div>
                            </div>
                          </Grid>

                          <Grid item className="lean-table-card-menu-container">
                            <MoreMenu eachUser={eachVisitorinList} status={statusEnum[eachVisitorinList.status]} isBlockedChat={eachVisitorinList.isChatBlocked} />

                          </Grid>

                          <Grid item xs={9} className="lean-table-card-subtext">
                            <Grid container>
                              <Grid item xs={12} className="lean-table-card-subtext-container">

                                {/* <i className="lean-icon-timer lean-table-card-subtext" /> */}

                                {/* <BookmarkIcon fontSize='small' /> */}
                                <PhoneIcon fontSize='small' />

                                <span className="lean-table-card-subtext">
                                  {eachVisitorinList.Mobile}
                                </span>
                              </Grid>

                              <Grid item xs={12} className="lean-table-card-subtext-container ">

                                {/* <i className="lean-icon-timer lean-table-card-subtext" /> */}

                                {/* <BookmarkIcon fontSize='small' /> */}
                                <EmailIcon fontSize='small' />

                                <span className="lean-table-card-subtext">
                                  {eachVisitorinList.email}
                                </span>
                              </Grid>



                              {eachVisitorinList.currentCompany ? (
                                <Grid item xs={12} className="lean-table-card-subtext-container ">
                                  <BusinessIcon fontSize='small' />

                                  <span className="lean-table-card-subtext">
                                    {eachVisitorinList.currentCompany}


                                  </span>
                                </Grid>
                              ) : null}
                            </Grid>
                          </Grid>

                          <Grid item xs={3} className="lean-table-card-fav-icon">
                            <img src={FavIcon} onClick={(event) => openFav(event, eachVisitorinList)} />
                          </Grid>



                          {/* <Grid className="lean-table-card-subtext-container " item xs={12}>

                                        <EventNoteIcon fontSize='small' />

                                        <span className="lean-table-card-subtext">

                                            {`${dateNew.toDateString()}, ${checkInTime} - ${checkOutTime}`}

                                        </span>

                                    </Grid> */}


                          

                        </Grid>



                      </div>)
                  }



                  )}
                  <Grid container style={{ minHeight: "5px", justifyContent: 'center' }}>
                    {
                      loading && page !== 1 ? (

                        <Grid item style={{ padding: "4px 0" }}>
                          <CircularProgress disableShrink />
                        </Grid>

                      ) : null
                    }
                  </Grid>

                </>

              ) : (
                  <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                    <img src={EmptySVG} />
                    <span className="lean-table-empty-screen-text">No Appointments</span>
                  </Grid>

                )}
          </>
        }
      </div>

      {/* <div style={{ position: 'relative' }}>

        {isLoading ? <FullPageLoading mainClassNames="lean-loaders-full-page">Loading</FullPageLoading> : (

          <>
            {apiData.length == 0 ?
              (
                <>
                  <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                    <img src={EmptySVG} />
                    <span className="lean-table-empty-screen-text">No Frequent Visitors</span>
                  </Grid>
                </>
              ) :
              (



                <Table>

                  <TableHead classes={{ root: "lean-table-header" }}>

                    <TableRow>
                      {tableHeaders.map(tableHeader => (
                        <TableCell>
                          {tableHeader ?
                            (tableHeader.sortIndex !== null ? (
                              <Button
                                disableRipple
                                name={tableHeader ? tableHeader.sortIndex : null}
                                onClick={() => { handleTableHeaderClick(tableHeader.name, tableHeader.sortIndex) }}
                              >
                                {tableHeader.name}
                                {sortColumnIndex === tableHeader.sortIndex ? order === "desc" ? (<ArrowDropDownIcon />) : (<ArrowDropUpIcon />) : null}
                              </Button>
                            ) :
                              <span>
                                {tableHeader.name}
                              </span>) : null
                          }
                        </TableCell>
                      ))}
                    </TableRow>

                  </TableHead>

                  <TableBody classes={{ root: "lean-table-body" }}>
                    {apiData.map((eachVisitorinList, index) => {

                      console.log("lean-table-body", apiData.length);
                      return (
                        <TableRow
                          key={index}
                          hover
                          className="lean-table-row"
                          onMouseOver={e => handleMouseOverRow(index)}
                        // onClick = {() => { console.log("wtf", index); setModal(<ModalOpener type="MOREINFO" args={booking}/>)}}
                        >
                          <TableCell>
                            {

                              statusEnum[eachVisitorinList.status] === "invited" || statusEnum[eachVisitorinList.status] === "Invited" ?
                                (<>
                                  <Avatar />
                                </>
                                ) :
                                (<>
                                  <Avatar src={`data:image/png;base64,${eachVisitorinList.thumbnailImage}`} />
                                </>
                                )
                            }
                          </TableCell>

                          <TableCell className="lean-table-body-room-cell">
                            <div className="lean-table-body-room-cell-contents">
                              <span className="lean-table-body-room-cell-data">
                                {eachVisitorinList.name}
                              </span>
                            </div>
                          </TableCell>


                          <TableCell className="lean-table-body-subj-cell">
                            {eachVisitorinList.Mobile}
                          </TableCell>

                          <TableCell className="lean-table-body-subj-cell">
                            {eachVisitorinList.email}
                          </TableCell>

                          <TableCell className="lean-table-body-subj-cell">
                            {eachVisitorinList.currentCompany}
                          </TableCell>


                          <TableCell>
                            <Chip
                              label={statusEnum[eachVisitorinList.status]}
                              className={`lean-table-body-status lean-room-${
                                statusEnum[eachVisitorinList.status]
                                }`}
                            />
                          </TableCell>

                          <TableCell>
                            {
                              <img src={FavIcon} onClick={(event) => openFav(event, eachVisitorinList)} />
                            }
                          </TableCell>


                          <TableCell>
                            {

                              eachVisitorinList.isChatBlocked ?
                                (
                                  <img src={ChatDisabledIcon} className='lean-list-visitors-table-diableschat-icon' />
                                )
                                :
                                (

                                  <img src={ChatIcon} onClick={() => openChat(eachVisitorinList.visitorSub)} />
                                )
                            }

                          </TableCell>

                          <TableCell>
                            <MoreMenu eachUser={eachVisitorinList} status={statusEnum[eachVisitorinList.status]} isBlockedChat={eachVisitorinList.isChatBlocked} />
                          </TableCell>


                        </TableRow>
                      );
                    })}
                  </TableBody>

                  <TableCell
                    colspan="3"
                    className="lean-table-pagination lean-table-pagination-left-heading"
                  >
                    <Toolbar className="lean-table-pagination-left-heading-tb">
                      <div></div>
                      <Typography
                        variant="caption"
                        className="lean-table-pagination-heading"
                      >
                        Showing {((page - 1) * rowsPerPage) + 1}-
                        {Math.min(
                        rowsPerPage * (page),
                        Number(data.data.total)

                      )}{" "}
                        out of {Number(data.data.total)}
                      </Typography>
                    </Toolbar>
                  </TableCell>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    count={Number(data.data.total)}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    colspan="100"
                    align="center"
                    labelDisplayedRows={({ from, to, count }) =>
                      `${page} of ${Math.ceil(count / rowsPerPage)}`
                    }
                    classes={{
                      select: "lean-table-pagination-select",
                      spacer: "lean-table-pagination-spacer",
                      caption: "lean-table-pagination-heading",
                      root: "lean-table-pagination lean-table-pagination-left-heading",
                      toolbar: "lean-table-pagination-left-heading-tb",
                      selectRoot: "lean-table-pagination-select-root"
                    }}
                    // SelectProps = {{
                    //   className:'lean-table-pagination-select'
                    // }}
                    ActionsComponent={() => (
                      <TableActions
                        rowsPerPage={rowsPerPage}
                        page={page}
                        count={Number(data.data.total)}
                        onChangePage={handleChangePage}
                      />
                    )}
                  />
                </Table>

              )}
          </>
        )}
      </div> */}
      {/* <Drawer variant="persistent" anchor="right" open={state} >
        <IconButton style={{ position: "absolute", right: 0, top: 0 }} onClick={(e) => {
          setState(false)
          getDetailForVisitor({
            "visitorSub": ""
          })
        }} color="inherit" aria-label="open drawer" > <CloseIcon /> </IconButton>
        <ChatBox />
      </Drawer> */}

      {/* {modal} */}
    </div>
  );


};

const mapStateToProps = state => {
  const { getMyFrequentVisitorsReducer, getDetailForVisitorReducer, refresherReducer } = state;
  console.log("Visitors-->", getMyFrequentVisitorsReducer);
  return { visitorsList: getMyFrequentVisitorsReducer, visitorDetails: getDetailForVisitorReducer, refresherState: refresherReducer };
};

// export default connect(mapStateToProps, {onLoad: getMyFrequentVisitors, getDetailForVisitor: getDetailForVisitor })
// (FrequentVisitorsCards);
export default connect(mapStateToProps, { onLoad: getMyFrequentVisitors, getDetailForVisitor: getDetailForVisitor, addToFrequentVisitor, removeFrequentVisitor, fireDrawerNow: fireDrawer })(
  FrequentVisitorsCards
)
