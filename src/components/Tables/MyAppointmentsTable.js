import React from "react";
import { connect } from "react-redux";
// import IconButton from "@material-ui/core/IconButton";
import Drawer from '@material-ui/core/Drawer';
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import Avatar from "@material-ui/core/Avatar";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { getMyAppointments } from '../../redux/actions/listOfAppointments';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import UserImage from '../../assets/kevin-hart.jpg'
import EventNoteIcon from '@material-ui/icons/EventNote';
import { HeaderComponent } from '../../components/HeaderComponent/HeaderComponent';
import { ModalContext } from "../../utils/ModalContext";
import ChatIcon from '../../assets/chat-copy.svg';
import AddAppointmentInAModal from '../../layouts/AddAppointment/AddAppointmentInAModal';
import ReportUser from "../../layouts/Report/ReportUser"
import RejectVis from "../../layouts/RejectVisitor/RejectVis"
import CancelAppointment from '../../layouts/CancelAppt/CancellAppointment';
import ChatDisabledIcon from '../../assets/chat-disabled.svg';
import ChatBox from "../../layouts/Chat/ChatBox";
import CloseIcon from '@material-ui/icons/Close';
import { getFromAndTo } from '../../utils/timeFunctions'
import EmptySVG from '../../assets/emptyFav.svg'
import { Grid } from "@material-ui/core";
import ChatButton from "../../layouts/Chat/ChatButton";
import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor'
import {approveVisitorAppointment} from '../../redux/actions/approveVisitor';
import MoreMenu from './MoreMenuMyAppointments';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import {fireDrawer} from '../../redux/actions/chatAction/fireDrawer';
import { useLocation } from "react-router-dom";

import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Chip,
  TablePagination,
  Button,
  Typography,
  Toolbar,
  Tooltip
} from "@material-ui/core";

import "./MyAppointmentsTable.scss";
import FullPageLoading from "../Loaders/FullPageLoading";


// const MenuIcon = ({ handleClick }) => (
//   <IconButton
//     aria-label="more"
//     aria-controls="long-menu"
//     aria-haspopup="true"
//     onClick={handleClick}
//   >
//     <MoreVertIcon />
//   </IconButton>
// );

const TableActions = ({ page, rowsPerPage, count, onChangePage }) => {


  let noOfPages = Math.ceil(count / rowsPerPage);

  return (
    <div className="lean-table-pagination-action">
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, 1)}
      >
        First
      </Button>
      <Button
        className="lean-table-pagination-btn lean-table-pagination-btn-prev"
        disabled={page === 1}
        onClick={() => onChangePage({}, page - 1)}
      >
        <ChevronLeftIcon />
      </Button>
      <Button
        disabled={page === noOfPages} 
        className="lean-table-pagination-btn lean-table-pagination-btn-next"
        onClick={() => onChangePage({}, page + 1)}
      >
        <ChevronRightIcon />
      </Button>
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, noOfPages)}
      >
        Last
      </Button>
    </div>
  );
};

function useQuery() {
  return new URLSearchParams(useLocation().search);
}
const VisitorTables = ({ visitorsList, onLoad, getDetailForVisitor,refresher,fireDrawerNow }, ...props) => {

  const [page, setPage] = React.useState(1);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [modal, setModal] = React.useState(null);
  const { setCurrentModal } = React.useContext(ModalContext);
  const openModalCustomDate = ({ name, props }) => setCurrentModal({ name, props });
  const [state, setState] = React.useState(false);
  const [status,changeStatus] = React.useState("");
  const [order,changeOrder] = React.useState("desc");
  const [sortBy,changeSortBy] = React.useState("time");
  const [timeStatus, changeTimeStatus] = React.useState("All");
  const [sortColumnIndex, setSortIndex] = React.useState(0);
  const [timestamp, setTimestamp] = React.useState({
    fromTime: null,
    toTime: null
  })


  let query = useQuery();


  function setTimeFilter() {
    var days = 7; // Days you want to subtract
    var date = new Date();
    var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
    var day = last.getDate();
    var month = last.getMonth() + 1;
    var year = last.getFullYear();
    console.log(last.toDateString())

  }

  var lastday = function (y, m) {
    return new Date(y, m + 1, 0).getDate();
  }

  function openAll()
  {
    var fromTime = null;
    var toTime = null;
    console.log("FROM TO==>",fromTime,toTime);
    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
  }


  function openToday() {

    var newDate = new Date();
    var fromTime = newDate.setHours("00", "00");
    var toTime = newDate.setHours("23", "59");

    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })

    console.log("New Date", fromTime, toTime);
    console.log("New Time", timestamp);


  }

  function openLastWeek() {
    var curr = new Date();
    var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay() - 6));
    var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
    var fromTime = firstday.setHours("00", "00");
    var toTime = lastday.setHours("23", "59");

    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })

    console.log("New Time", firstday, lastday);
  }

  function openLastMonth() {

    var lastMonth = new Date().getMonth() - 1;  //return number
    var setLastMonthDate = new Date().setMonth(lastMonth)  //set month
    var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
    var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
    var lastDayOfMonth = lastday(new Date(fromTime).getFullYear(), lastMonth)
    var temp = new Date(setLastMonthDate).setDate(lastDayOfMonth);
    var toTime = new Date(temp).setHours("23", "59")

    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })

    console.log("openLastMonth->", fromTime, toTime);

  }

  function openLastSixMonths() {

    var lastSixMonth = new Date().getMonth() - 6;  //return number
    var setLastMonthDate = new Date().setMonth(lastSixMonth)  //set month
    var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
    var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
    var lastMonth = (new Date().getMonth() - 1)

    var lastMonthDate = new Date().setMonth(lastMonth)
    console.log(lastMonth, lastMonthDate)
    var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
    var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
    var toTime = new Date(temp).setHours("23", "59");

    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });

    console.log("openLastSixMonths->",fromTime, toTime)

  }
  function openLastYear() {

    //Check last year
    var lastElevenMonth = new Date().getMonth() - 12;  //return number
    var setLastMonthDate = new Date().setMonth(lastElevenMonth)  //set month
    var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
    var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")

    console.log("From time ===>" + fromTime);

    var lastMonth = (new Date().getMonth() - 1)
    var lastMonthDate = new Date().setMonth(lastMonth)
    console.log(lastMonth, lastMonthDate)
    var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
    var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
    var toTime = new Date(temp).setHours("23", "59");

    console.log("openLastYear->", fromTime, toTime)

    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });


  }

  function openCD(fromTime, toTime) {
    console.log("Ntitit", fromTime, toTime);
    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });
  }

  function openCustomDateModal() {
    console.log("On click of custom date");
    openModalCustomDate({ name: 'CustomDate', props: { openCD } })
  }

  function changeStatusValue(status){
    console.log("STATUS==>",status)
    changeStatus(status)
  }

  const onTimeRangeChange = (newRange) => {
    changeTimeStatus(newRange)
    switch (newRange) {
        case "All": openAll(); break;
        case "Today": openToday(); break;
        case "Last Week": openLastWeek(); break;
        case "Last Month": openLastMonth(); break;
        case "Last 6 Months": openLastSixMonths(); break;
        case "Last Year": openLastYear(); break;
        case "Custom Date": openCustomDateModal(); break;
    }
}

  const statusValues = [
    {value: "All", name: <div className='lean-status-filter-appointments-table' onClick={() => { changeStatusValue("") }}><span>All</span></div> },
    {value:"Approved" , name:<div className='lean-status-filter-appointments-table' onClick={() => { changeStatusValue("Approved") }}><span>Approved</span></div>},
    {value:"Rejected" , name:<div className='lean-status-filter-appointments-table' onClick={() => { changeStatusValue("Rejected") }}><span>Rejected</span></div>},
    {value:"Cancelled" , name:<div className='lean-status-filter-appointments-table' onClick={() => { changeStatusValue("Cancelled") }}><span>Cancelled</span></div>},
    {value:"Completed" , name:<div className='lean-status-filter-appointments-table' onClick={() => { changeStatusValue("Completed") }}><span>Completed</span></div>},
    {value:"Ongoing" , name:<div className='lean-status-filter-appointments-table' onClick={() => { changeStatusValue("Ongoing") }}><span>Ongoing</span></div>},
    {value:"Pending" , name:<div className='lean-status-filter-appointments-table' onClick={() => { changeStatusValue("Pending") }}><span>Pending</span></div>},
  ]

  const timeStatusValues = [
    { value: "All", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">All</span></div> },
    { value: "Today", name: <div className="lean-calendar-image"> <span className="lean-calendar-image lean-calendar-image-text">Today</span></div> },
    { value: "Last Week", name: <div className="lean-calendar-image"> <span className="lean-calendar-image lean-calendar-image-text">Last Week</span></div> },
    { value: "Last Month", name: <div className="lean-calendar-image"> <span className="lean-calendar-image lean-calendar-image-text">Last Month</span></div> },
    { value: "Last 6 Months", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text"> Last 6 Months</span></div> },
    { value: "Last Year", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Last Year</span></div> },
    { value: "Custom Date", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Custom Date</span></div> }
  ]


  React.useEffect(
    () => {
      console.log("on load")
      onLoad({
        "from": timestamp.fromTime,
        "to": timestamp.toTime,
        "page": page,
        "offset": rowsPerPage,
        "order": order,
        "sortBy": sortBy,
        "status":status
      })
    }, [timestamp,status,order,sortBy,refresher.myAppointments,refresher.rejectedVisitor,refresher.cancelledAppointment,refresher.reportedVisitorReg,refresher.blockedUser,refresher.unblockedUser]
  );

  React.useEffect(() => {
    console.log("Visitor Id", query.get("id"));
    if(query.get("id"))
    {
      console.log("Encrypted id is present, open the modal");
      openModalCustomDate({name:'VisitorInfoModal',props:{"visitorId":query.get("id")}})
    }
    else
    {
      console.log("No ID, dont do anything")
    }
  }, []);


  let { isLoading: loading, data, error } = visitorsList;

  console.log("visitor list-->", visitorsList)

  let apiData = data.data.data;

  let statusEnum = {
    Requested:"Requested",
    requested:"requested",
    Ongoing:"Ongoing",
    ongoing:"ongoing",
    Completed:"Completed",
    completed:"completed",
    Cancelled:"Cancelled",
    cancelled:"cancelled",
    Approved: "Approved",
    approved: "approved",
    rejected: "rejected",
    Rejected: "Rejected",
    Registered: "Registered",
    registered: "registered",
    Pending:"Pending",
    pending:"Pending"
  };

  let tableHeaderNameEnum ={

    "Visitor Name": "name",
    "Check In Time": "time"
  };


  const handleTableHeaderClick = (tableHeaderName,index) => {

    console.log("Table header name==>",tableHeaderName)
    console.log("Table header name enum==>",tableHeaderNameEnum[tableHeaderName])
    
    setSortIndex(index)

    if(tableHeaderNameEnum[tableHeaderName] == sortBy ){
      if(order == "asc"){
        changeOrder("desc");
      }
      else{
        changeOrder("asc");
      }
    }
    else{
      changeSortBy(tableHeaderNameEnum[tableHeaderName]);
      changeOrder("asc");
    }

   };

  const handleMouseOverRow = index => {
    // console.log("handleMouseOverRow", index);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    onLoad({
      "from": timestamp.fromTime,
      "to": timestamp.toTime,
      "page": newPage,
      "offset": rowsPerPage,
      "order": order,
      "sortBy": sortBy,
      "status":status
    });
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(1);
    onLoad({
      "from": timestamp.fromTime,
      "to": timestamp.toTime,
      "page": 1,
      "offset": parseInt(event.target.value, 10),
      "order": order,
      "sortBy": sortBy,
      "status":status
    });
  };

  function openChat(visitorId) {
    
    console.log("Clicked on open chat", visitorId)

    console.log("VISITOR CHAT VISITOR SUB===>",visitorId)

    getDetailForVisitor({
      "visitorSub": visitorId
    });

    fireDrawerNow("fireDrawer");

    // setState(true);

  }
  let tableHeaders = [
    null,
    { name: "Visitor Name", sortIndex: 0 },
    { name: "Meeting Subject", sortIndex: null },
    { name: "Meeting Room", sortIndex: null },
    { name: "Meeting Date", sortIndex: null },
    { name: "Check In Time", sortIndex: 4 },
    { name: "Check Out Time", sortIndex: null },
    { name: "Status", sortIndex: null },
    { name: "", sortIndex: null },
    { name: "", sortIndex: null }
  ];



  return (

    <div className="lean-my-appointments-table">
      <HeaderComponent isRequired="yes" titleName="My Appointments" ButtonText="Add Appointment" TimeStatusValues={timeStatusValues} StatusValues={statusValues} onTimeRangeChange={onTimeRangeChange} timeStatus={timeStatus}/>


      <div style={{ position:'relative' }}>

        {loading ? <FullPageLoading mainClassNames="lean-loaders-full-page">Loading</FullPageLoading> : (

          <>
          { apiData.length == 0 ? 
          (
            <>
              <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                <img src={EmptySVG} />
                <span className="lean-table-empty-screen-text">No Appointments</span>
              </Grid>
            </>
          ) : 
          (

          <Table>

            <TableHead classes={{ root: "lean-table-header" }}>

              <TableRow>
                {tableHeaders.map(tableHeader => (
                  <TableCell>
                    {tableHeader ? 
                      (tableHeader.sortIndex !== null ? (
                      <Button
                        disableRipple
                        name={tableHeader ? tableHeader.sortIndex : null}
                        onClick={() => {handleTableHeaderClick(tableHeader.name,tableHeader.sortIndex)}}
                      >
                        {tableHeader.name}
                        {sortColumnIndex === tableHeader.sortIndex ?  order === "desc" ? ( <ArrowDropDownIcon/> ): (<ArrowDropUpIcon/>)  : null }
                      </Button>
                      ) :   
                      <span>
                        {tableHeader.name}
                      </span>) : null 
                    }
                  </TableCell>
                ))}
              </TableRow>

            </TableHead>

            <TableBody classes={{ root: "lean-table-body" }}>
              {apiData.map((eachVisitorinList, index) => {

                var duration = eachVisitorinList.duration/3600000;
                var arrivalTime = eachVisitorinList.arrivalTime;

                var dateNew = new Date(arrivalTime)
                var timeNew = dateNew.toLocaleTimeString()

                console.log("Date ===>", dateNew.toDateString());
                console.log("Duration==>",duration);

                var checkincheckoutTime = getFromAndTo(dateNew, dateNew, Number(duration))
                var checkInTime = new Date(checkincheckoutTime.from * 1000).toLocaleTimeString();
                var checkOutTime = new Date(checkincheckoutTime.to * 1000).toLocaleTimeString();


                console.log("checkincheckoutTime", checkincheckoutTime);
                console.log("checkInTime", checkInTime);
                console.log("checkOutTime", checkOutTime);

                // console.log("hbhuhuubu", eachVisitorinList.thumbnail[0].img.data.data)


                return (
                  <TableRow
                    key={index}
                    hover
                    className="lean-table-row"
                    onMouseOver={e => handleMouseOverRow(index)}
                  >
                    <TableCell>
                    {

                        // statusEnum[eachVisitorinList.status] === "invited" || statusEnum[eachVisitorinList.status] === "Invited" ?
                        // ( <>
                        //   <Avatar />
                        //   </>
                        // ):
                          <>
                            <Avatar src={`data:image/png;base64,${eachVisitorinList.thumbnail}`} />
                          </>
                        // )
                    }
                      
                    </TableCell>

                    <TableCell className="lean-table-body-room-cell">
                      <div className="lean-table-body-room-cell-contents">
                        <span className="lean-table-body-room-cell-data">
                          {eachVisitorinList.visitorName}
                        </span>
                      </div>
                    </TableCell>


                    <TableCell className="lean-table-body-subj-cell">
                      {eachVisitorinList.purpose}
                    </TableCell>

                    <TableCell className="lean-table-body-meeting-room-cell">
                      {eachVisitorinList.roomName}
                    </TableCell>

                    <TableCell >
                      {dateNew.toDateString()}
                    </TableCell>

                    <TableCell >
                      {checkInTime}
                    </TableCell>

                    <TableCell >
                      {checkOutTime}
                    </TableCell>

                    <TableCell>
                      <Chip
                        label={statusEnum[eachVisitorinList.status]}
                        className={`lean-table-body-status lean-room-${
                          statusEnum[eachVisitorinList.status]
                          }`}
                      />
                    </TableCell>

                    <TableCell >
                      {

                        statusEnum[eachVisitorinList.status] === "invited" || statusEnum[eachVisitorinList.status] === "Invited" || statusEnum[eachVisitorinList.status] === "approved" || statusEnum[eachVisitorinList.status] === "Approved" ?
                          ( <>{eachVisitorinList.thumbnail ? (<img src={ChatIcon} onClick={() => openChat        (eachVisitorinList.visitorSub)} />):(<img src={ChatDisabledIcon} className='lean-my-appointments-table-diable-chat-icon' />)}
                            </>
                          )
                          :
                          (
                            <img src={ChatDisabledIcon} className='lean-my-appointments-table-diable-chat-icon' />
                          )
                      }
                    </TableCell>

                    <TableCell>
                      {statusEnum[eachVisitorinList.status] === statusEnum["rejected"] || 
                      statusEnum[eachVisitorinList.status] === statusEnum["completed"] || 
                      statusEnum[eachVisitorinList.status] === statusEnum["ongoing"] || 
                      statusEnum[eachVisitorinList.status] === statusEnum["Ongoing"] || 
                      statusEnum[eachVisitorinList.status] === statusEnum["Rejected"] || 
                      statusEnum[eachVisitorinList.status] === statusEnum["Completed"] ||
                      statusEnum[eachVisitorinList.status] === statusEnum["Cancelled"] ||
                      statusEnum[eachVisitorinList.status] === statusEnum["cancelled"]
                      ? null : <MoreMenu eachUser={eachVisitorinList} date={dateNew.toDateString()} checkInTime={checkInTime} checkOutTime={checkOutTime} status={statusEnum[eachVisitorinList.status]} />}
                    </TableCell>


                  </TableRow>
                );
              })}
            </TableBody>

            <TableCell
              colspan="3"
              className="lean-table-pagination lean-table-pagination-left-heading"
            >
              <Toolbar className="lean-table-pagination-left-heading-tb">
                <div></div>
                <Typography
                  variant="caption"
                  className="lean-table-pagination-heading"
                >
                  Showing { ((page - 1) * rowsPerPage) + 1 }-
                  {Math.min(
                    rowsPerPage * (page),
                    Number(data.data.total)
                  )}{" "}
                  out of {Number(data.data.total)} entries
                </Typography>
              </Toolbar>
            </TableCell>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              count={Number(data.data.total)}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              colspan="100"
              align="center"
              labelDisplayedRows={({ from, to, count }) =>
                `${page} of ${Math.ceil(count / rowsPerPage )}`
              }
              classes={{
                select: "lean-table-pagination-select",
                spacer: "lean-table-pagination-spacer",
                caption: "lean-table-pagination-heading",
                root: "lean-table-pagination lean-table-pagination-left-heading",
                toolbar: "lean-table-pagination-left-heading-tb",
                selectRoot: "lean-table-pagination-select-root"
              }}
              // SelectProps = {{
              //   className:'lean-table-pagination-select'
              // }}
              ActionsComponent={() => (
                <TableActions
                  rowsPerPage={rowsPerPage}
                  page={page}
                  count={Number(data.data.total)}
                  onChangePage={handleChangePage}
                />
              )}
            />
          </Table>
          
          )}
          </>
        )}

      </div>
      {/* <Drawer variant="persistent" anchor="right" open={state} >
        <IconButton style={{ position: "absolute", right: 0, top: 0 }} onClick={(e) => {
          setState(false)
        }} color="inherit" aria-label="open drawer" > <CloseIcon /> </IconButton>
        <ChatBox />
      </Drawer> */}

      {/* {modal} */}

    </div>
  );


};

const mapStateToProps = state => {
  const { getListOfAppointmentsReducer, getDetailForVisitorReducer,refresherReducer } = state;
  console.log("Visitors-->", getListOfAppointmentsReducer);
  return { visitorsList: getListOfAppointmentsReducer, visitorDetails: getDetailForVisitorReducer, refresher:refresherReducer };
};

export default connect(mapStateToProps, { onLoad: getMyAppointments, getDetailForVisitor: getDetailForVisitor,fireDrawerNow:fireDrawer })(
  VisitorTables
);

// export default {
//   MyAppointments: connect(mapStateToProps)(VisitorTables),
//   ApproveVisitor: connect(mapStateToProps)(MoreMenu)
// }