import React from "react";
import { connect } from "react-redux";
// import IconButton from "@material-ui/core/IconButton";
import Drawer from '@material-ui/core/Drawer';
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import Avatar from "@material-ui/core/Avatar";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { getMyAppointments } from '../../redux/actions/listOfAppointments';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import UserImage from '../../assets/kevin-hart.jpg'
import EventNoteIcon from '@material-ui/icons/EventNote';
import { HeaderComponent } from '../../components/HeaderComponent/HeaderComponent';
import { ModalContext } from "../../utils/ModalContext";
import ChatIcon from '../../assets/chat-copy.svg';
import AddAppointmentInAModal from '../../layouts/AddAppointment/AddAppointmentInAModal';
import ReportUser from "../../layouts/Report/ReportUser"
import RejectVis from "../../layouts/RejectVisitor/RejectVis"
import CancelAppointment from '../../layouts/CancelAppt/CancellAppointment';
import ChatDisabledIcon from '../../assets/chat-disabled.svg';
import ChatBox from "../../layouts/Chat/ChatBox";
import CloseIcon from '@material-ui/icons/Close';
import { getFromAndTo } from '../../utils/timeFunctions'
import EmptySVG from '../../assets/emptyFav.svg'
import { Grid } from "@material-ui/core";
import ChatButton from "../../layouts/Chat/ChatButton";
import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor'
import { approveVisitorAppointment } from '../../redux/actions/approveVisitor';

const MenuIcon = ({ handleClick }) => (
  <IconButton
    aria-label="more"
    aria-controls="long-menu"
    aria-haspopup="true"
    onClick={handleClick}
  >
    <MoreVertIcon />
  </IconButton>
);

const MoreMenu = props => {

    const { setCurrentModal } = React.useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    console.log("Whatssssss sup???", props.status)

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    // console.log("MOTHERFUCKING PROPS",props)


    function openListOfVisitorTableModal(name, visitorName, visitorEmail, visitorId, checkInTime, checkOutTime, date, roomName, appointmentId, subject) {
        console.log("Which Modal to open?", name);
        console.log("Which User?", visitorName);
        console.log("Which email?", visitorEmail);
        // console.log("MOTHERFUCKING PROPS 1",appointmentId)
        // console.log("MOTHERFUCKING PROPS 1",appointmentId)


        if (name === 'RescheduleAppointment') {
            console.log("On click of rescehdule")
            openModal({ name: 'AddAppointmentInAModal', props: { "visitorName": visitorName, "visitorEmail": visitorEmail, "isDisabled": true, "checkInTime": checkInTime, "checkOutTime": checkOutTime, "date": date, "roomName": roomName, "appointmentId": appointmentId, "subject": subject, "classStyles": { paperScrollPaper: 'lean-reschedule-appointment-modal' } } })
        }
        else if (name === 'CancelledAppointment') {
            console.log("Modal Name===>", name);
            console.log("APPOINTMENT ID=>",appointmentId)
            openModal({ name: 'CancelAppointmentInAModal', props: {"visitorName": visitorName, "visitorEmail": visitorEmail, "isDisabled": true, "checkInTime": checkInTime, "checkOutTime": checkOutTime, "date": date, "roomName": roomName, "appointmentId": appointmentId, "subject": subject} })
        }
        else if (name === "AcceptVisitorInvite") {
            
            props.approveVisitorAppointment({
                "appointmentId": appointmentId,
                "approve": true
            })
            openModal({ name: 'ApprovedVisitorModal', props: { "visitorName": visitorName } })
        }
        else if (name === "RejectVisitor") {
            openModal({ name: name, props: { "visitorName": visitorName, "visitorId": visitorId, "appointmentId": appointmentId} })
        }
        handleClose();
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <MenuIcon handleClick={handleClick} />
            <Menu
                classes={{ paper: "lean-table-body-menu" }}
                anchorEl={anchorEl}
                anchorOrigin={{
                    horizontal: "right",
                    vertical: "bottom"
                }}
                getContentAnchorEl={null}
                open={open}
                onClose={handleClose}
            >
                {props.status == "Pending" || props.status == "pending" ?
                    (
                        <>
                            <MenuItem className="lean-table-body-menu-item-Accept" onClick={() => openListOfVisitorTableModal("AcceptVisitorInvite",props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId, props.checkInTime, props.checkOutTime, props.date, props.eachUser.roomName, props.eachUser.appointmentId, props.eachUser.purpose)}>
                                Approve
                            </MenuItem>
                            <MenuItem className="lean-table-body-menu-item-Reject" onClick={() => openListOfVisitorTableModal("RejectVisitor", props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId, props.checkInTime,           props.checkOutTime, props.date, props.eachUser.roomName, props.eachUser.appointmentId, props.eachUser.purpose)}>
                                Reject
                            </MenuItem>
                        </>
                    )
                    :
                    (
                        <>
                            <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("RescheduleAppointment", props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId, props.checkInTime, props.checkOutTime, props.date, props.eachUser.roomName, props.eachUser.appointmentId, props.eachUser.purpose)}>
                                Reschedule
                            </MenuItem>
                            <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("CancelledAppointment", props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId, props.checkInTime, props.checkOutTime, props.date, props.eachUser.roomName, props.eachUser.appointmentId, props.eachUser.purpose)}>
                                Cancel Appointment
                            </MenuItem>
                        </>
                    )}

            </Menu>
        </>
    );
};

const mapStateToProps = state => {
    const { approveVisitorAppointmentReducer } = state;
    // console.log("approveVisitorAppointmentReducer-->", approveVisitorAppointmentReducer);
    return { approveVisitorAppointmentReducer };
};

export default connect(mapStateToProps, { approveVisitorAppointment })(
    MoreMenu
);