import React from "react";
import { connect } from "react-redux";
import { IconButton, Drawer } from "@material-ui/core";
import Menu from "@material-ui/core/Menu";
import Avatar from "@material-ui/core/Avatar";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { getMyFrequentVisitors } from '../../redux/actions/frequentVisitors';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import UserImage from '../../assets/kevin-hart.jpg';
import { HeaderComponent } from '../../components/HeaderComponent/HeaderComponent';
import EventNoteIcon from '@material-ui/icons/EventNote';
import { ModalContext } from "../../utils/ModalContext";
import AddAppointmentInAModal from '../../layouts/AddAppointment/AddAppointmentInAModal';
import ReportUser from "../../layouts/Report/ReportUser"
import RejectVis from "../../layouts/RejectVisitor/RejectVis"
import CancelAppointment from '../../layouts/CancelAppt/CancellAppointment';
import ChatIcon from '../../assets/chat-copy.svg';
import ChatDisabledIcon from '../../assets/chat-disabled.svg';
import ChatBox from "../../layouts/Chat/ChatBox";
import CloseIcon from '@material-ui/icons/Close';
import ChatButton from "../../layouts/Chat/ChatButton";
import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor'
import EmptySVG from '../../assets/emptyFav.svg'
import { Grid } from "@material-ui/core";
import FavIcon from '../../assets/star.png'
import UnFavIcon from '../../assets/unblocked.png'

const MenuIcon = ({ handleClick }) => (
  <IconButton
    aria-label="more"
    aria-controls="long-menu"
    aria-haspopup="true"
    onClick={handleClick}
  >
    <MoreVertIcon />
  </IconButton>
);

const MoreMenu = props => {

  const { setCurrentModal } = React.useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });

  console.log("Whatssssss sup???", props.status)

  console.log("Which user is also clicking?-->", props.eachUser)

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  // console.log("isBlockedChat",props.eachUser.is)

  // const openModal = event => {
  //   console.log("openModal", event);
  //   handleClose();
  // };

  function openListOfVisitorTableModal(name, visitorName, visitorEmail, visitorId,visitorSub) {

    console.log("Which Modal to open?", name);
    console.log("Which User?", visitorName);
    console.log("Which email?", visitorEmail);

   
    
    
    if (name === 'AddAppointmentInAModal') {
      openModal({ name: 'AddAppointmentInAModal', props: { "visitorName": visitorName, "visitorEmail": visitorEmail, "addAppointmentToParticularVisitor": "true", "classStyles": { paperScrollPaper: 'lean-add-appointment-from-table-modal' } } })
    }


    else if(name === "BlockChat"){
        // console.log("BLOCK CHAT")
        openModal({ name:'MoreInfoModal', props: { "type": name, "visitorName": visitorName, "visitorId": visitorId, "visitorSub": visitorSub } })
    }

    else if (name === 'ReportVisitor' ) {

        console.log("VISITOR ID",visitorId)
        openModal({ name: name, props: { "visitorId": visitorId } })
    }

    else if(name === "UnblockChat"){
        console.log("BLOCK CHAT")
        openModal({ name:'MoreInfoModal', props: { "type": name, "visitorName": visitorName, "visitorId": visitorId, "visitorSub": visitorSub } })
    }


    handleClose();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <MenuIcon handleClick={handleClick} />
      <Menu
        classes={{ paper: "lean-table-body-menu" }}
        anchorEl={anchorEl}
        anchorOrigin={{
          horizontal: "right",
          vertical: "bottom"
        }}
        getContentAnchorEl={null}
        open={open}
        onClose={handleClose}
      >
      {( props.isBlockedChat ? (
            <>
              <MenuItem className="lean-table-body-menu-item-Accept" onClick={() => openListOfVisitorTableModal("UnblockChat", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
                Unblock Chat
              </MenuItem>
              <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("ReportVisitor", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
                  Report Visitor
              </MenuItem>
              <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("AddAppointmentInAModal", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
                  Add Appointment
              </MenuItem>

            </>
          ):
          (<>
           <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("ReportVisitor", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
            Report Visitor
          </MenuItem>
          <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("BlockChat", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
            Block Chat
          </MenuItem>
          <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("AddAppointmentInAModal", props.eachUser.name, props.eachUser.email)}>
            Add Appointment
          </MenuItem>
          </>)
         
      )}
      </Menu>
    </>
  );
};

const mapStateToProps = state => {
    // const {  } = state;
    // return {  };
};

export default connect(mapStateToProps,null)(
    MoreMenu
);
