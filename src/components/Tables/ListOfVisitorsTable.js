import React from "react";
import { connect } from "react-redux";
import IconButton from "@material-ui/core/IconButton";
import Drawer from '@material-ui/core/Drawer'
import Menu from "@material-ui/core/Menu";
import Avatar from "@material-ui/core/Avatar";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { HeaderComponent } from '../../components/HeaderComponent/HeaderComponent';
import { getVisitorsListTableAction } from '../../redux/actions/visitorsListTable';
import ChatIcon from '../../assets/chat-copy.svg';
import UserImage from '../../assets/kevin-hart.jpg'
import EventNoteIcon from '@material-ui/icons/EventNote';
import { ModalContext } from "../../utils/ModalContext";
import AddAppointmentInAModal from '../../layouts/AddAppointment/AddAppointmentInAModal';
import ReportUser from "../../layouts/Report/ReportUser"
import RejectVis from "../../layouts/RejectVisitor/RejectVis"
import CancelAppointment from '../../layouts/CancelAppt/CancellAppointment';
import ChatDisabledIcon from '../../assets/chat-disabled.svg';
import ChatBox from "../../layouts/Chat/ChatBox";
import CloseIcon from '@material-ui/icons/Close';
import ChatButton from "../../layouts/Chat/ChatButton";
import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor'
import EmptySVG from '../../assets/emptyFav.svg'
import { Grid } from "@material-ui/core";
import "./ListOfVisitorsTable.scss";
import FavIcon from '../../assets/star.png'
import UnFavIcon from '../../assets/unblocked.png'
import UpArrow from '../../assets/up-arrow-1.png'
import MoreMenu from './MoreMenuMyVisitors';
import {refresher} from '../../redux/actions/refresher';
import { addToFrequentVisitor } from '../../redux/actions/addToFrequentVisitor';
import { removeFrequentVisitor } from '../../redux/actions/removeFrequentVisitor';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import {fireDrawer} from '../../redux/actions/chatAction/fireDrawer';


import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Chip,
  TablePagination,
  Button,
  Typography,
  Toolbar,
  Tooltip
} from "@material-ui/core";
import FullPageLoading from "../Loaders/FullPageLoading";


const TableActions = ({ page, rowsPerPage, count, onChangePage }) => {
  let noOfPages = Math.ceil(count / rowsPerPage);

  return (
    <div className="lean-table-pagination-action">
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, 1)}
      >
        First
      </Button>
      <Button
        className="lean-table-pagination-btn lean-table-pagination-btn-prev" 
        disabled={page === 0}
        onClick={() => onChangePage({}, page - 1)}
      >
        <ChevronLeftIcon />
      </Button>
      <Button
        disabled={page === noOfPages}
        className="lean-table-pagination-btn lean-table-pagination-btn-next"
        onClick={() => onChangePage({}, page + 1)}
      >
        <ChevronRightIcon />
      </Button>
      <Button
        className="lean-table-pagination-btn"
        onClick={() => onChangePage({}, noOfPages)}
      >
        Last
      </Button>
    </div>
  );
};

const VisitorTables = ({ visitorsList, visitorDetails, getVisitorsListTableAction, getDetailForVisitor, addToFrequentVisitor, removeFrequentVisitor, refresherState,refresher,fireDrawerNow }) => {
  // var newDate = new Date();
  const [page, setPage] = React.useState(1);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [modal, setModal] = React.useState(null);
  const { setCurrentModal } = React.useContext(ModalContext);
  const openModalCustomDate = ({ name, props }) => setCurrentModal({ name, props });
  const [state, setState] = React.useState(false);
  const [status, changeStatus] = React.useState("All");
  const [sortBy, changeSortBy] = React.useState("email");
  const [order, changeOrder] = React.useState("asc");
  const [timeStatus, changeTimeStatus] = React.useState("All");
  const [sortColumnIndex, setSortIndex] = React.useState(0);
  const [timestamp, setTimestamp] = React.useState({
    fromTime: null,
    toTime: null
  })

  function setTimeFilter() {
    var days = 7; // Days you want to subtract
    var date = new Date();
    var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
    var day = last.getDate();
    var month = last.getMonth() + 1;
    var year = last.getFullYear();
    console.log(last.toDateString())

  }

  var lastday = function (y, m) {
    return new Date(y, m + 1, 0).getDate();
  }

  function openAll() {

    var fromTime = null;
    var toTime = null;
    console.log("FROM TO==>", fromTime, toTime)
    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
  }

  function openToday() {

    var newDate = new Date();
    var fromTime = newDate.setHours("00", "00");
    var toTime = newDate.setHours("23", "59");

    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })

    console.log("New Date", fromTime, toTime);
    console.log("New Time", timestamp);


  }

  function openLastWeek() {
    var curr = new Date();
    var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay() - 6));
    var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
    var fromTime = firstday.setHours("00", "00");
    var toTime = lastday.setHours("23", "59");
    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })
    console.log("New Time", firstday, lastday);
  }

  function openLastMonth() {

    var lastMonth = new Date().getMonth() - 1;  //return number
    var setLastMonthDate = new Date().setMonth(lastMonth)  //set month
    var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
    var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
    var lastDayOfMonth = lastday(new Date(fromTime).getFullYear(), lastMonth)
    var temp = new Date(setLastMonthDate).setDate(lastDayOfMonth);
    var toTime = new Date(temp).setHours("23", "59")

    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime })

    console.log("openLastMonth->", fromTime, toTime);

  }

  function openLastSixMonths() {

    var lastSixMonth = new Date().getMonth() - 6;  //return number
    var setLastMonthDate = new Date().setMonth(lastSixMonth)  //set month
    var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
    var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")
    var lastMonth = (new Date().getMonth() - 1)

    var lastMonthDate = new Date().setMonth(lastMonth)
    console.log(lastMonth, lastMonthDate)
    var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
    var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
    var toTime = new Date(temp).setHours("23", "59");

    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });

    console.log("openLastSixMonths->", toTime)

  }
  function openLastYear() {

    //Check last year
    var lastElevenMonth = new Date().getMonth() - 12;  //return number
    var setLastMonthDate = new Date().setMonth(lastElevenMonth)  //set month
    var getLastMonthFirstDay = new Date(setLastMonthDate).setDate(1)  //
    var fromTime = new Date(getLastMonthFirstDay).setHours("00", "00")

    console.log("From time ===>" + fromTime);

    var lastMonth = (new Date().getMonth() - 1)
    var lastMonthDate = new Date().setMonth(lastMonth)
    console.log(lastMonth, lastMonthDate)
    var lastDayOfMonth = lastday(new Date(lastMonthDate).getFullYear(), lastMonth)
    var temp = new Date(lastMonthDate).setDate(lastDayOfMonth);
    var toTime = new Date(temp).setHours("23", "59");

    console.log("openLastYear->", fromTime, toTime)

    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });



  }

  function openCD(fromTime, toTime) {
    console.log("Ntitit", fromTime, toTime);
    setTimestamp({ ...timestamp, 'fromTime': fromTime, 'toTime': toTime });
  }

  function openCustomDateModal() {
    console.log("On click of custom date");
    openModalCustomDate({ name: 'CustomDate', props: { openCD } })
  }

  function changeStatusValue(status) {
    console.log("STATUS==>", status)
    changeStatus(status)
  }

  const onTimeRangeChange = (newRange) => {
    changeTimeStatus(newRange)
    switch (newRange) {
        case "All": openAll(); break;
        case "Today": openToday(); break;
        case "Last Week": openLastWeek(); break;
        case "Last Month": openLastMonth(); break;
        case "Last 6 Months": openLastSixMonths(); break;
        case "Last Year": openLastYear(); break;
        case "Custom Date": openCustomDateModal(); break;
    }
}

  const statusValues = [
    { value: "All", name: <div className='lean-status-filter-list-visitors-table' onClick={() => { changeStatusValue("") }}><span>All</span></div> },
    { value: "Invited", name: <div className='lean-status-filter-list-visitors-table' onClick={() => { changeStatusValue("Invited") }}><span>Invited</span></div> },
    { value: "Registered", name: <div className='lean-status-filter-list-visitors-table' onClick={() => { changeStatusValue("Registered") }}><span>Registered</span></div> },
    { value: "Approved", name: <div className='lean-status-filter-list-visitors-table' onClick={() => { changeStatusValue("Approved") }}><span>Approved</span></div> },
    { value: "Blocked", name: <div className='lean-status-filter-list-visitors-table' onClick={() => { changeStatusValue("Blocked") }}><span>Blocked</span></div> },
    { value: "Blacklisted", name: <div className='lean-status-filter-list-visitors-table' onClick={() => { changeStatusValue("Blacklisted") }}><span>Blacklisted</span></div> }
  ]

  const timeStatusValues = [
    { value: "All", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">All</span></div> },
    { value: "Today", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Today</span></div> },
    { value: "Last Week", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Last Week</span></div> },
    { value: "Last Month", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Last Month</span></div> },
    { value: "Last 6 Months", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text"> Last 6 Months</span></div> },
    { value: "Last Year", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Last Year</span></div> },
    { value: "Custom Date", name: <div className="lean-calendar-image"><span className="lean-calendar-image lean-calendar-image-text">Custom Date</span></div> }
  ]

  React.useEffect(
    () => {
      console.log("on load ")
      if(status === "All")
      {
        getVisitorsListTableAction({
          "from": timestamp.fromTime,
          "to": timestamp.toTime,
          "page": page,
          "offset": rowsPerPage,
          "order": order,
          "sortBy": sortBy
        })
      }
      else
      {
        getVisitorsListTableAction({
          "from": timestamp.fromTime,
          "to": timestamp.toTime,
          "page": page,
          "offset": rowsPerPage,
          "order": order,
          "sortBy": sortBy,
          "status": status
        })
      }
    }, [timestamp, status, sortBy, order, refresherState.myAppointments, refresherState.reportedVisitor, refresherState.reportedVisitorReg, refresherState.blockedUser, refresherState.unblockedUser, refresherState.addAsFreqVisitor, refresherState.removeAsFreqVisitor]
  );


  let { isLoading, data, error } = visitorsList;

  console.log("visitor list-->", visitorsList)

  let apiData = data.data.data;

  let statusEnum = {
    Approved: "Approved",
    Registered: "Registered",
    Blocked: "Blocked",
    Invited: "Invited",
    approved: "approved",
    registered: "registered",
    blocked: "blocked",
    invited: "invited",
    blacklisted:"blacklisted",
    Blacklisted:"Blacklisted",
    Ongoing:"Ongoing",
    ongoing:"ongoing",

  };

  let tableHeaderNameEnum = {

    "Visitor Name": "name",
    "Visitor Email ID": "email",
    "Company Name/Location": "company"
  };

  const handleTableHeaderClick = (tableHeaderName,index) => {

    console.log("Table header name==>", tableHeaderName)
    console.log("Table header name enum==>", tableHeaderNameEnum[tableHeaderName])

    setSortIndex(index)


    if (tableHeaderNameEnum[tableHeaderName] == sortBy) {
      if (order == "asc") {
        changeOrder("desc");
      }
      else {
        changeOrder("asc");
      }
    }
    else {
      changeSortBy(tableHeaderNameEnum[tableHeaderName]);
      changeOrder("asc");
    }
  };

  const handleRowClick = (index, eachVisitorinList, e) => {
    console.log("E Tager", e.target.tagName);
    if (e.target.tagName === "TD") {
      if (eachVisitorinList.visitorSub === null || eachVisitorinList.visitorSub === "") {

        openModalCustomDate({ name: 'MoreInfoModal', props: { "type": "NotApprovedVisitor" } })
      }
      else {

        openModalCustomDate({ name: 'EachVisitorModal', props: { eachVisitorinList } })
      }
    }
    else {
      e.persist();
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
    }
  };

  const handleChangePage = (event, newPage) => {

    setPage(newPage);

    console.log("handleChangePage", JSON.stringify({
      "from": timestamp.fromTime,
      "to": timestamp.toTime,
      "page": newPage,
      "offset": rowsPerPage,
      "order": order,
      "sortBy": sortBy,
      "status": status
    }))

    if(status === "All")
    {
      getVisitorsListTableAction({
        "from": timestamp.fromTime,
        "to": timestamp.toTime,
        "page": newPage,
        "offset": rowsPerPage,
        "order": order,
        "sortBy": sortBy,
        // "status": status
      });
    }
    else
    {
      getVisitorsListTableAction({
        "from": timestamp.fromTime,
        "to": timestamp.toTime,
        "page": newPage,
        "offset": rowsPerPage,
        "order": order,
        "sortBy": sortBy,
        "status": status
      });
    }
    
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(1);
    if(status === "All")
    {
      getVisitorsListTableAction({
        "from": timestamp.fromTime,
        "to": timestamp.toTime,
        "page": 1,
        "offset": parseInt(event.target.value, 10),
        "order": order,
        "sortBy": sortBy,
        // "status": status
      });

    }
    else
    {
      getVisitorsListTableAction({
        "from": timestamp.fromTime,
        "to": timestamp.toTime,
        "page": 1,
        "offset": parseInt(event.target.value, 10),
        "order": order,
        "sortBy": sortBy,
        "status": status
      });
    }
  };

  function openChat(visitorId) {
    
    console.log("Clicked on open chat", visitorId)
    
    getDetailForVisitor({
      "visitorSub": visitorId
    })

    fireDrawerNow("fireDrawer");
    // setState(true)
  }

  let tableHeaders = [
    null,
    { name: "Visitor Name", sortIndex: 0 },
    { name: "Visitor Mobile Number", sortIndex: null },
    { name: "Visitor Email ID", sortIndex: 2 },
    { name: "Company Name/Location", sortIndex: 3 },
    { name: "Status", sortIndex: null },
    { name: "", sortIndex: null },
    { name: "", sortIndex: null },
    { name: "", sortIndex: null },

  ];

  function openFav(e, eachVisitorinList) {
    console.log("EVENT", e.target.tagName)
    if (e.target.tagName === "IMG") {
      if (!eachVisitorinList.isFrequent) {

        addToFrequentVisitor({
          "visitorSub": eachVisitorinList.visitorSub
        })
        refresher("addAsFreqVisitor");
        // openModalCustomDate({ name: 'AddAsFreqVis', props: { "visitorName": eachVisitorinList.name } })
        // e.preventDefault();

      }
      else {
        removeFrequentVisitor({
          "visitorSub": eachVisitorinList.visitorSub
        })
        // refresher("removeAsFreqVisitor");
        openModalCustomDate({ name: 'RemoveAsFreq', props: { "visitorName": eachVisitorinList.name } })
      }
    }
    else {
      e.persist();
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
    }
  }


  return (

    <div className="lean-visitor-table">
      <HeaderComponent isRequired="yes" titleName="List Of Visitors" ButtonText="Add Appointment" TimeStatusValues={timeStatusValues} StatusValues={statusValues} onTimeRangeChange={onTimeRangeChange} timeStatus={timeStatus} />

      <div style={{ position: 'relative' }}>

        {isLoading ? <FullPageLoading mainClassNames="lean-loaders-full-page">Loading</FullPageLoading> : (

          <>
            {apiData.length == 0 ?
              (
                <>
                  <Grid item xs={12} className="lean-grid-item lean-table-empty-screen">
                    <img src={EmptySVG} />
                    <span className="lean-table-empty-screen-text">No Visitors</span>
                  </Grid>
                </>
              ) :
              (

                <Table>

                  <TableHead classes={{ root: "lean-table-header" }}>

                    <TableRow>
                      {tableHeaders.map(tableHeader => (
                        <TableCell>
                          {tableHeader ?
                            (tableHeader.sortIndex !== null ? (
                              <Button
                                disableRipple
                                name={tableHeader ? tableHeader.sortIndex : null}
                                onClick={() => { handleTableHeaderClick(tableHeader.name,tableHeader.sortIndex) }}
                              >
                                {tableHeader.name}
                                {sortColumnIndex === tableHeader.sortIndex ?  order === "desc" ? ( <ArrowDropDownIcon/> ): (<ArrowDropUpIcon/>)  : null }
                              </Button>
                            ) :
                              <span>
                                {tableHeader.name}
                              </span>) : null
                          }
                        </TableCell>
                      ))}
                    </TableRow>

                  </TableHead>

                  <TableBody classes={{ root: "lean-table-body" }}>
                    {apiData.map((eachVisitorinList, index) => {

                      console.log("lean-table-body", apiData.length);
                      return (
                        <TableRow
                          key={index}
                          hover
                          className="lean-table-row"
                          onClick={eachVisitorinList.status !== "invited" ? (e) => { handleRowClick(index, eachVisitorinList, e) } : null}
                        >
                          <TableCell >
                            {

                              statusEnum[eachVisitorinList.status] === "invited" || statusEnum[eachVisitorinList.status] === "Invited" ?
                                (<>
                                  <Avatar />
                                </>
                                ) :
                                (<>
                                  <Avatar src={`data:image/png;base64,${eachVisitorinList.thumbnailImage}`} />
                                </>
                                )
                            }
                          </TableCell>

                          <TableCell className="lean-table-body-room-cell">
                            <div className="lean-table-body-room-cell-contents">
                              <span className="lean-table-body-room-cell-data">
                                {eachVisitorinList.name}
                              </span>
                            </div>
                          </TableCell>


                          <TableCell className="lean-table-body-subj-cell">
                            {eachVisitorinList.Mobile}
                          </TableCell>

                          <TableCell >
                            {eachVisitorinList.email}
                          </TableCell>

                          <TableCell className="lean-table-body-subj-cell">
                            {eachVisitorinList.currentCompany}
                          </TableCell>


                          <TableCell>
                            <Chip
                              label={statusEnum[eachVisitorinList.status]}
                              className={`lean-table-body-status lean-room-${
                                statusEnum[eachVisitorinList.status]
                                }`}
                            />
                          </TableCell>

                          <TableCell >
                            {
                              statusEnum[eachVisitorinList.status] === "approved" || statusEnum[eachVisitorinList.status] === "Approved" ?

                                (<>
                                  {eachVisitorinList.isFrequent ?
                                    (
                                      <img src={FavIcon} onClick={(event) => openFav(event, eachVisitorinList)} />

                                    )
                                    :
                                    (
                                      <img src={UnFavIcon} onClick={(event) => openFav(event, eachVisitorinList)} />
                                    )}
                                </>
                                )
                                :
                                (
                                  null
                                )
                            }

                          </TableCell>

                          <TableCell >
                            {

                              statusEnum[eachVisitorinList.status] === "approved" || statusEnum[eachVisitorinList.status] === "Approved" ?
                                (
                                  <img src={ChatIcon} onClick={() => openChat(eachVisitorinList.visitorSub)} />
                                )
                                :
                                (
                                  <img src={ChatDisabledIcon} className='lean-list-visitors-table-diableschat-icon' />
                                )
                            }
                          </TableCell>

                          <TableCell>
                            { statusEnum[eachVisitorinList.status] === statusEnum["Invited"] || 
                              statusEnum[eachVisitorinList.status] === statusEnum["invited"] ||
                              statusEnum[eachVisitorinList.status] === statusEnum["Blacklisted"] || 
                              statusEnum[eachVisitorinList.status] === statusEnum["blacklisted"]
                              ? null :<MoreMenu eachUser={eachVisitorinList} status={statusEnum[eachVisitorinList.status]} /> }
                          </TableCell>


                        </TableRow>
                      );
                    })}
                  </TableBody>

                  <TableCell
                    colspan="3"
                    className="lean-table-pagination lean-table-pagination-left-heading"
                  >
                    <Toolbar className="lean-table-pagination-left-heading-tb">
                      <div></div>
                      <Typography
                        variant="caption"
                        className="lean-table-pagination-heading"
                      >
                        Showing {((page - 1) * rowsPerPage) + 1}-
                        {Math.min(
                          rowsPerPage * (page),
                          Number(data.data.total)
                        )}{" "}
                        out of {Number(data.data.total)} entries
                      </Typography>
                    </Toolbar>
                  </TableCell>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    count={Number(data.data.total)}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    colspan="100"
                    align="center"
                    labelDisplayedRows={({ from, to, count }) =>
                      `${page} of ${Math.ceil(count / rowsPerPage)}`
                    }
                    classes={{
                      select: "lean-table-pagination-select",
                      spacer: "lean-table-pagination-spacer",
                      caption: "lean-table-pagination-heading",
                      root: "lean-table-pagination lean-table-pagination-left-heading",
                      toolbar: "lean-table-pagination-left-heading-tb",
                      selectRoot: "lean-table-pagination-select-root"
                    }}

                    ActionsComponent={() => (
                      <TableActions
                        rowsPerPage={rowsPerPage}
                        page={page}
                        count={Number(data.data.total)}
                        onChangePage={handleChangePage}
                      />
                    )}
                  />
                </Table>

              )}
          </>
        )}
      </div>

      {/* <Drawer variant="persistent" anchor="right" open={state} >
        <IconButton style={{ position: "absolute", right: 0, top: 0 }} onClick={(e) => {
          setState(false)
          getDetailForVisitor({
            "visitorSub": ""
          })
        }} color="inherit" aria-label="open drawer" > <CloseIcon /> </IconButton>
        <ChatBox />
      </Drawer> */}
    </div>
  );


};

const mapStateToProps = state => {
  const { getVisitorsListTableReducer, getDetailForVisitorReducer, refresherReducer } = state;
  console.log("Visitors-->", getVisitorsListTableReducer);
  return { visitorsList: getVisitorsListTableReducer, visitorDetails: getDetailForVisitorReducer, refresherState: refresherReducer };
};

export default connect(mapStateToProps, {refresher, getVisitorsListTableAction, getDetailForVisitor, addToFrequentVisitor, removeFrequentVisitor,fireDrawerNow:fireDrawer })(
  VisitorTables
);
