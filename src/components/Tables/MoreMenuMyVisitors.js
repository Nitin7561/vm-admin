import React from "react";
import { connect } from "react-redux";
import IconButton from "@material-ui/core/IconButton";
import Drawer from '@material-ui/core/Drawer'
import Menu from "@material-ui/core/Menu";
import Avatar from "@material-ui/core/Avatar";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { HeaderComponent } from '../../components/HeaderComponent/HeaderComponent';
import { getVisitorsListTableAction } from '../../redux/actions/visitorsListTable';
import ChatIcon from '../../assets/chat-copy.svg';
import UserImage from '../../assets/kevin-hart.jpg'
import EventNoteIcon from '@material-ui/icons/EventNote';
import { ModalContext } from "../../utils/ModalContext";
import AddAppointmentInAModal from '../../layouts/AddAppointment/AddAppointmentInAModal';
import ReportUser from "../../layouts/Report/ReportUser"
import RejectVis from "../../layouts/RejectVisitor/RejectVis"
import CancelAppointment from '../../layouts/CancelAppt/CancellAppointment';
import ChatDisabledIcon from '../../assets/chat-disabled.svg';
import ChatBox from "../../layouts/Chat/ChatBox";
import CloseIcon from '@material-ui/icons/Close';
import ChatButton from "../../layouts/Chat/ChatButton";
import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor'
import EmptySVG from '../../assets/emptyFav.svg'
import { Grid } from "@material-ui/core";
import "./ListOfVisitorsTable.scss";
import FavIcon from '../../assets/star.png'
import UnFavIcon from '../../assets/unblocked.png'
import UpArrow from '../../assets/up-arrow-1.png'
import { approveVisitorRegistration } from '../../redux/actions/approveVisitorRegistration';


const MenuIcon = ({ handleClick }) => (
  <IconButton
    aria-label="more"
    aria-controls="long-menu"
    aria-haspopup="true"
    onClick={handleClick}
  >
    <MoreVertIcon />
  </IconButton>
);

const MoreMenu = props => {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const { setCurrentModal } = React.useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });

  console.log("List of visitors", props.status)
  console.log("List of visitors => Which user is also clicking?-->", props.eachUser)


  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  function openListOfVisitorTableModal(name, visitorName, visitorEmail, visitorId,visitorSub) {

    console.log("Which Modal to open?", name);
    console.log("Which User?", visitorName);
    console.log("Which email?", visitorEmail);

    console.log("VISITOR SUB ==>",visitorSub)

    console.log("VISITOR ID ==>",visitorId)

    // console.log("VISITOR ID ==>",visito)


    if (name === 'AddAppointmentInAModal') {

      openModal({ name: 'AddAppointmentInAModal', props: { "visitorName": visitorName, "visitorEmail": visitorEmail, "addAppointmentToParticularVisitor": "true", "classStyles": { paperScrollPaper: 'lean-add-appointment-from-table-modal' } } })

    }

    else if (name === 'AcceptVisitorInvite') {
      console.log("Modal Name===>", name);

        props.approveVisitorRegistration({
                "visitorSub":visitorSub,
                "approve": true
            })
        openModal({ name: 'ApprovedVisitorRegistrationModal', props: { "visitorName": visitorName } })

    }

    else if (name === 'ReportVisitor' ) {
        openModal({ name: name, props: { "visitorId": visitorId } })
    }

    else if(name === 'RejectVisitor'){

        openModal({ name: name, props: { "visitorName": visitorName, "visitorId": visitorId, "visitorSub": visitorSub,"registrationRejection":true } })
    }

    else if(name === "BlockChat"){
        console.log("BLOCK CHAT")
        openModal({ name:'MoreInfoModal', props: { "type": name, "visitorName": visitorName, "visitorId": visitorId, "visitorSub": visitorSub } })
    }

    else if(name === "UnblockChat"){
        console.log("BLOCK CHAT")
        openModal({ name:'MoreInfoModal', props: { "type": name, "visitorName": visitorName, "visitorId": visitorId, "visitorSub": visitorSub } })
    }


    handleClose();
  };


  return (
    <>
      <MenuIcon handleClick={handleClick} />
      <Menu
        classes={{ paper: "lean-table-body-menu" }}
        anchorEl={anchorEl}
        anchorOrigin={{
          horizontal: "right",
          vertical: "bottom"
        }}
        getContentAnchorEl={null}
        open={open}
        onClose={handleClose}
      >
        {(props.status === "approved" || props.status === "Approved"  ?
          <>
            <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("ReportVisitor", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
              Report Visitor
            </MenuItem>
            <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("BlockChat", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
              Block Chat
            </MenuItem>
        
            <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("AddAppointmentInAModal", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
              Add Appointment
            </MenuItem>
          </>
          : 
          (props.status === "blocked" || props.status === "Blocked" ? (
            <>
              <MenuItem className="lean-table-body-menu-item-Accept" onClick={() => openListOfVisitorTableModal("UnblockChat", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
                Unblock Chat
              </MenuItem>
              <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("ReportVisitor", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
                  Report Visitor
              </MenuItem>
              <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("AddAppointmentInAModal", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
                  Add Appointment
              </MenuItem>

            </>
          ):
          (
            <>
              <MenuItem className="lean-table-body-menu-item-Accept" onClick={() => openListOfVisitorTableModal("AcceptVisitorInvite",props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub, props.checkInTime, props.checkOutTime, props.date, props.eachUser.roomName, props.eachUser.appointmentId, props.eachUser.purpose)}>
                  Approve
              </MenuItem>
              <MenuItem className="lean-table-body-menu-item-Reject" onClick={() => openListOfVisitorTableModal("RejectVisitor", props.eachUser.visitorName, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub, props.checkInTime,props.checkOutTime, props.date, props.eachUser.roomName, props.eachUser.appointmentId, props.eachUser.purpose)}>
                  Reject
              </MenuItem>
              <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("ReportVisitor", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
                Report Visitor
              </MenuItem>
              <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("BlockChat", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
                Block Chat
              </MenuItem>
              <MenuItem className="lean-table-body-menu-item" onClick={() => openListOfVisitorTableModal("AddAppointmentInAModal", props.eachUser.name, props.eachUser.email, props.eachUser.visitorId,props.eachUser.visitorSub)}>
                Add Appointment
              </MenuItem>
            </>
        )))}
      </Menu>
    </>
  );
};

const mapStateToProps = state => {
    const { approveVisitorRegistrationReducer } = state;
    return { approveVisitorRegistrationReducer };
};

export default connect(mapStateToProps, { approveVisitorRegistration })(
    MoreMenu
);