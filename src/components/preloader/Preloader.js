import React from "react";
import spacesenseLogo from "../../assets/images/spacesensLogo.svg";
import Grid from "@material-ui/core/Grid"
import './preloader.scss'
import Cookies from 'universal-cookie';
import { Link } from "react-router-dom";
import { URL_GetTenantFromUserName } from "../../config";


export default function Preloader(props) {


  React.useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get('lean-username')) {
      fetchCall(cookies.get('lean-username'))
      // return window.location.href = '/terms';
      console.log("fetchCall if",cookies.get('lean-username'))
    }
    else {
      console.log("fetchCall else")
      // return <Link to="/login" />
      return window.location.href = '/login';
    }

    }, [])

  function fetchCall(email) {
    console.log('fetchCall-->', email)
    if (email != undefined) {

      fetch(URL_GetTenantFromUserName + email, {method: "GET",
      }).then(resp => resp.json())
        .then(async (data) => {          
          // console.log("fetchCall", data)
          const { data: tenantData, status } = data
          if(!status.error){
            const { tenantPlatformHome } = tenantData
            const { protocol, host, httpsPort, path } = tenantPlatformHome
            var URL = protocol + "://" + host + ":" + httpsPort + path + "?login_hint=" + email
            console.log("redirectToHome /////", URL)
            window.location.href = URL;
          }
          else{
            console.log('login failed', status.message);
            // return <Link to="/login" />
            return window.location.href = '/login';
          }
          
        }).catch((e) => {
          console.log('login excemption', e);
          // return <Link to="/login" />
          return window.location.href = '/login';
        })

    }

  }

  return (
    <Grid container alignItems="center" justify="center" style={{ height: "100vh" }} >
      {/* <Grid item xs={10} sm={4}  className='lean-preloader-gridItem'> */}
      <Grid item xs={0}  className='lean-preloader-gridItem' >
        <img className='lean-preloader-logoSize' src={spacesenseLogo} />
        <div className='lean-preloader-flexDiv-bar'>
          <span className="lean-preloader-spanStatic"></span>
          <span className="lean-preloader-spanAnimated"></span>
        </div>
      </Grid>
    </Grid>

  );
}
