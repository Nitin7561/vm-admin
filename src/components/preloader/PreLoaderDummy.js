import React from "react";
import spacesenseLogo from "./spacesensLogo.svg";
import Grid from "@material-ui/core/Grid"
import './preloader.scss'
// import Cookies from 'universal-cookie';
// import { Link } from "react-router-dom";
// import { URL_GetTenantFromUserName } from "../../config";


export default function PreloaderDummy(props) {


  return (
    <Grid container alignItems="center" justify="center" style={{ height: "100vh"}} >
      {/* <Grid item xs={10} sm={4} className='lean-preloader-gridItem' > */}
      <Grid item xs={0}  className='lean-preloader-gridItem' >
        <img className='lean-preloader-logoSize' src={spacesenseLogo} />
        <div className='lean-preloader-flexDiv-bar'>
          <span className="lean-preloader-spanStatic"></span>
          <span className="lean-preloader-spanAnimated"></span>
        </div>
      </Grid>
    </Grid>

  );
}
