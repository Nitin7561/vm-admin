import React from "react";
import { connect } from 'react-redux';

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import ErrorIcon from "../../assets/failed.svg";
import SuccessIcon from "../../assets/success.svg";
import ApproveVisitor from "../../assets/approve-visitor.svg";


import { CircularProgress } from "@material-ui/core";
import StatusModalBasedOnStatus from "./StatusModalBasedOnStatus";

import { approveVisitorAppointment } from '../../redux/actions/approveVisitor';
import { refresher } from '../../redux/actions/refresher';
import FullPageLoading from '../Loaders/FullPageLoading';



import "./modal.scss";

const getStatusComponent = (type, closeModal, visitorName) => {

  switch (type) {
    case "Success":
      return (

        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          icon={ApproveVisitor}
          heading={`Request approved successfully from ${visitorName}`}
          message={`An acknowledgement will be sent to your mail`}
          buttonMessage={`Okay`}
          buttonTwoMessage={''}
          handleClose={closeModal}
        />
      );

    case "Error":

      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          buttonTwoMessage={''}
          icon={ErrorIcon}
          heading={`Approving visitor has Failed`}
          message={`Your approval request has failed to complete`}
          buttonMessage={`OK`}
          handleClose={closeModal}
        />
      );

    default:
      console.log("IN STATUS MODAL visitorSub loool")
      return <StatusModalBasedOnStatus
        isLoading={true}
        isVisitorTableAcionModal={null}
        buttonTwoMessage={null}
        icon={null}
        heading={null}
        message={null}
        buttonMessage={null}
        handleClose={closeModal}
      />;
  }

};

const ApprovedVisitorModal = ({ props, closeModal, visitorName, approveVisitorAppointmentReducer, refresher }) => {

  let { isLoading, isSuccess, isError, error } = approveVisitorAppointmentReducer;
  let type = "Loading";


  if (!isLoading) {
    if (isSuccess) {
      type = "Success";
    } else if (isError) {
      type = "Error";
    }
  }

  let onHandleClose = () => {
    console.log("HandleClose On Modal")
    refresher("myAppointments");
    closeModal()
  }

  console.log("ApprovedVisitorModal", type, isLoading, isSuccess, isError, error);

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="xs"
        onClose={() => {
          if (!isLoading) {
            onHandleClose();
          }
        }}
        open={true}
        disableBackdropClick
        disableEscapeKeyDown
        classes={{ root: 'lean-customized-modal-size' }}
      >
        {/* <DialogContent>
          {isLoading ? (
            <CircularProgress />
          ) : (
            getStatusComponent(type,onHandleClose,visitorName)
          )}
        </DialogContent> */}

        <DialogContent style={{ position: "relative" }}>
          {
            getStatusComponent(type, onHandleClose, visitorName)
          }
        </DialogContent>

      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  let { approveVisitorAppointmentReducer } = state;
  return { approveVisitorAppointmentReducer };
};

export default connect(mapStateToProps, { approveVisitorAppointment, refresher })(ApprovedVisitorModal);
