import React from "react";
import { connect } from 'react-redux';

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import ErrorIcon from "../../assets/failed.svg";
import SuccessIcon from "../../assets/success.svg";

import { CircularProgress } from "@material-ui/core";
import StatusModalBasedOnStatus from "./StatusModalBasedOnStatus";

import { refresher } from '../../redux/actions/refresher';
import RescheduledIcon from '../../assets/RescheduledAppointments.png';
import FullPageLoading from '../Loaders/FullPageLoading';



import "./modal.scss";

const getStatusComponent = (type, closeModal, isDisabled) => {
  if (isDisabled) {
    switch (type) {
      case "Success":
        return (

          <StatusModalBasedOnStatus
            isVisitorTableAcionModal={'false'}
            buttonTwoMessage={''}
            icon={RescheduledIcon}
            heading={`Appointment Rescehduled Successfully`}
            message={`Your appointment has been rescheduled successfully and an email has been seen to you!`}
            buttonMessage={`OK`}
            handleClose={closeModal}

          />
        );

      case "Error":

        return (
          <StatusModalBasedOnStatus
            isVisitorTableAcionModal={'false'}
            buttonTwoMessage={''}
            icon={ErrorIcon}
            heading={`Resheduling the Appointment has Failed`}
            message={`Your rescheduling appointment failed to complete`}
            buttonMessage={`OK`}
            handleClose={closeModal}
          />
        );

      default:
        console.log("IN STATUS MODAL visitorSub loool")
        return <StatusModalBasedOnStatus
          isLoading={true}
          isVisitorTableAcionModal={null}
          buttonTwoMessage={null}
          icon={null}
          heading={null}
          message={null}
          buttonMessage={null}
          handleClose={closeModal}
        />;
    }
  }
  else {
    switch (type) {
      case "Success":
        return (

          <StatusModalBasedOnStatus
            isVisitorTableAcionModal={'false'}
            buttonTwoMessage={''}
            icon={SuccessIcon}
            heading={`Appointment Successfully Rescheduled`}
            message={`An acknowledgement email has been sent to your email id`}
            buttonMessage={`OK`}
            handleClose={closeModal}

          />
        );

      case "Error":

        return (
          <StatusModalBasedOnStatus
            isVisitorTableAcionModal={'false'}
            buttonTwoMessage={''}
            icon={ErrorIcon}
            heading={`Appointment Rescheduling has Failed`}
            message={`Your appointment reschedule failed to complete`}
            buttonMessage={`OK`}
            handleClose={closeModal}
          />
        );

      default:
        return <></>;
    }
  }

};

const RescheduledAppointmentBooked = ({ props, isDisabled, closeModal, rescheduleAppointmentReducer, refresher }) => {

  let { isLoading, isSuccess, isError, error } = rescheduleAppointmentReducer;
  let type = "Loading";


  if (!isLoading) {
    if (isSuccess) {
      type = "Success";
    } else if (isError) {
      type = "Error";
    }
  }

  let onHandleClose = () => {
    // refresh
    console.log("HandleClose On Modal")
    // console.log("REFRESHER",refresher)
    refresher("myAppointments");
    closeModal()
  }

  console.log("RescheduledAppointmentBooked", type, isLoading, isSuccess, isError, error, isDisabled);

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="xs"
        onClose={() => {
          if (!isLoading) {
            onHandleClose();
          }
        }}
        open={true}
        disableBackdropClick
        disableEscapeKeyDown
      >
        {/* <DialogContent>
          {isLoading ? (
            <CircularProgress />
          ) : (
            getStatusComponent(type,onHandleClose,isDisabled)
          )}
        </DialogContent> */}

        <DialogContent style={{ position: "relative" }}>
          {
            getStatusComponent(type, onHandleClose, isDisabled)
          }
        </DialogContent>
      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  let { rescheduleAppointmentReducer } = state;
  return { rescheduleAppointmentReducer };
};

export default connect(mapStateToProps, { refresher })(RescheduledAppointmentBooked);
