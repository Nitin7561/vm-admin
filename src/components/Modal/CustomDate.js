import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { DatePickerField } from "../../components/InputTextField/DatePickerField";
import { Grid, Button } from "@material-ui/core";


import "./modal.scss";
import { datePickerDefaultProps } from "@material-ui/pickers/constants/prop-types";
// import { Gr, Button } from "@material-ui/core";


const CustomDate = ({closeModal,...props}) => {
  const [open, setOpen] = React.useState(true);
  const [fromMeetingDate, changeFromMeetingDate] = React.useState(new Date());
  const [toMeetingDate, changeToMeetingDate] = React.useState(new Date());

  var fromTime = fromMeetingDate.setHours("00","00");
  var toTime = toMeetingDate.setHours("23","59");

  console.log("Time==>",fromTime,toTime)

  function submit(){
    props.openCD(fromTime,toTime)
    // setOpen(...open,false)
  }
  

  return (
    <div>
      <Dialog classes={{root:'lean-custom-date-modal'}} fullWidth maxWidth="xs" onClose={closeModal} open={true}>
        <DialogTitle disableTypography className="lean-modal-header">
          <div className="lean-modal-header-text">Custom Date</div>
          <div>
            <IconButton onClick={closeModal}>
              <CloseIcon fontSize="large" />
            </IconButton>
          </div>
        </DialogTitle>
        <DialogContent dividers>
          <Grid container>
            <Grid item xs={12}>
              <div className="lean-modal-main-content-custom-date">
                <div className="lean-modal-item">
                  <div className="lean-modal-content">
                        <DatePickerField
                            value={fromMeetingDate}
                            eventHandler={changeFromMeetingDate}
                            label="From Date"
                        />
                  </div>
                </div>
                <div className="lean-modal-item">
                    <DatePickerField
                        value={toMeetingDate}
                        eventHandler={changeToMeetingDate}
                        label="To Date"
                        minDate={fromMeetingDate}
                        minDateMessage="Date should not be before from date"
                    />
                </div>
                <div className="lean-modal-item lean-submit-button-custom-date">
                    <Button variant="contained" className="lean-btn-primary" onClick= {()=> {submit(); closeModal();}} >
                        Submit
                    </Button>
                </div>
              </div>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default CustomDate;
