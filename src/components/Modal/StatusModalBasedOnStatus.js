import React from "react";
import { Grid, Hidden, Button, CircularProgress } from "@material-ui/core";
import { ModalContext } from "../../utils/ModalContext";
import './StatusModalBasedOnStatus.scss';
import { connect } from "react-redux";
import { cancelAppt } from "../../redux/actions/cancelAppt";
import { blockUser } from '../../redux/actions/blockUser';
import { unblockUser } from '../../redux/actions/unblockUser'
import FullPageLoading from "../Loaders/FullPageLoading";


const StatusModalBasedOnStatus = ({ isVisitorTableAcionModal, icon, heading, message, buttonMessage, buttonTwoMessage, handleClose, visitorName, appointmentId, visitorSub, isCancelTheRoom, cancelAppt, blockUser, unblockUser, reason, description, isLoading }) => {

  const { setCurrentModal } = React.useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });


  console.log("IN STATUS MODAL APPT ID", appointmentId)
  console.log("IN STATUS MODAL visitorSub", visitorSub, isLoading)


  function firstButtonModalAction(clickedAction) {
    console.log("Clicked on second button===>", clickedAction.buttonMessage)
    if (clickedAction.buttonMessage === "Yes" && isCancelTheRoom) {
      cancelAppt({
        "appointmentId": appointmentId,
        "doRoomCancel": true,
        "reason": reason,
        "description": description
      })

      openModal({ name: 'CancelledAppointmentModal', props: {} })
    }

    else if (clickedAction.buttonMessage === "Cancel") {
      setCurrentModal(null);
    }

  }

  function secondButtonModalAction(clickedAction) {
    console.log("Clicked on second button===>", clickedAction.buttonTwoMessage)


    if (clickedAction.buttonTwoMessage === "Block") {
      blockUser({
        "id": visitorSub,
      })

      openModal({ name: 'BlockedVisitor', props: {} })
    }


    else if (clickedAction.buttonTwoMessage === "Yes") {
      openModal({ name: 'RemoveAsFrequentVisitor', props: { "visitorName": visitorName } })
    }



    else if (clickedAction.buttonTwoMessage === "Keep It") {

      cancelAppt({
        "appointmentId": appointmentId,
        "doRoomCancel": false
      })

      openModal({ name: 'CancelledAppointmentModal', props: {} })
    }

    else if (clickedAction.buttonTwoMessage === "Unblock") {
      unblockUser({
        "id": visitorSub,
      })

      openModal({ name: 'UnblockedVisitor', props: {} })
    }

  }

  return (
    <Grid container>
      <Grid item xs={12}>
        <div className="lean-modal-main-content">

          {isLoading ? (
            <FullPageLoading mainClassNames="lean-loaders-full-page" customClassNames="lean-modal-loader">Loading</FullPageLoading>
          ) : null}

          <div className="lean-modal-item">
            <div className="lean-modal-icon">
              <img src={icon} />
            </div>
          </div>

          <div className="lean-modal-item">
            <div className="lean-modal-content">
              <div className="lean-modal-content-header lean-section-text-header">
                {heading}
              </div>
              <div className="lean-modal-content-message lean-text-content">
                {message}
              </div>
            </div>
          </div>

          <div className="lean-modal-item ">
            <div className="lean-modal-action">
              {isVisitorTableAcionModal == 'true' ?
                (<>
                  <Button
                    classes={{ root: "lean-visitor-cancel-button" }}
                    variant="contained"
                    className="lean-btn-primary"
                    onClick={() => { firstButtonModalAction({ buttonMessage }) }}
                  >
                    {buttonMessage}
                  </Button>
                  <Button
                    variant="contained"
                    className="lean-btn-primary"
                    onClick={() => { secondButtonModalAction({ buttonTwoMessage }) }}
                  >
                    {buttonTwoMessage}
                  </Button>
                </>
                )
                : (
                  <>
                    <Button
                      variant="contained"
                      className="lean-btn-primary"
                      onClick={() => handleClose()}
                    >
                      {buttonMessage}
                    </Button>
                  </>
                )}

            </div>
          </div>
        </div>
      </Grid>
    </Grid>
  );
};

// const mapStateToProps = state => {
//   const { getAllEnumsReducer } = state;
//   return { getAllEnumsReducer };
// };

export default connect(null, { cancelAppt, blockUser, unblockUser })(StatusModalBasedOnStatus);


// export default StatusModalBasedOnStatus;
