import React from "react";
import StatusModal from '../StatusModal'
import FunctionalModal from '../FunctionalModal'
import bridgeUX from '../../../assets/images/bridgeUX.svg'
import myProfile from '../../../assets/images/myProfile.svg'
import './myProfile.scss'
import { Grid, useTheme, useMediaQuery } from "@material-ui/core";
import { CircularProgress } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
// import { getUserInfoAction } from '../../../redux/actions/user';
import { connect } from 'react-redux';
import SmallTable from '../../SmallTable/RecurrentTable';
import SmallTableCards from "../../SmallTable/SmallTableCards";
// import { getDetailForVisitor } from '../../redux/actions/chatAction/getDetailForVisitor';


const MyProfile = (props) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

  function mapUserData() {
    let myProfileData = [
      { title: 'Contact Number', value: props.eachVisitorinList.Mobile },
      { title: 'Email ID', value: props.eachVisitorinList.email },
      { title: 'Location', value: props.eachVisitorinList.currentCompany },
      { title: 'Visitor ID', value: props.eachVisitorinList.visitorSub },
    ]

    return myProfileData
  }

  const [open, setOpen] = React.useState(true);
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  return (
    <div>
      <FunctionalModal title={props.eachVisitorinList.name} isAppointment="true" status={props.eachVisitorinList.status} children={modalContent(mapUserData(), props, fullScreen)} maxWidth='md' handleClose={props.closeModal} visitorSub={props.eachVisitorinList.visitorSub} classStyles={{ root: 'lean-each-visitor-host-meetings-list' }} />
    </div>
  )
}
function modalContent(myProfileData, props, fullScreen) {
  return (
    <>

      <Grid container classes={{ root: 'lean-my-profile-vm-app-visitor-appointment-info' }}>

        <Grid item xs={12} sm={4} >
          <img className='my-profile-image-from-list-of-appointments' src={`data:image/png;base64,${props.eachVisitorinList.thumbnailImage}`}  ></img>
        </Grid>

        <Grid item xs={12} sm={8}>

          {myProfileData.map((data) => {
            return (

              <Grid container style={{ minHeight: '50px' }} >
                <Grid item xs={4} >
                  <div className='lean-myprofile-title'>{data.title}</div>
                </Grid>
                <Grid item xs={8} >
                  <div className='lean-myprofile-description '>{data.value}</div>
                </Grid>
              </Grid>
            )
          })}

        </Grid>

        <Grid item xs={12}>
          <p className='lean-table-header-appointment-details-visitor'>List of My Appointment Details</p>

          {fullScreen ?
            <SmallTableCards eachVisitorinList={props.eachVisitorinList} /> :
            <SmallTable eachVisitorinList={props.eachVisitorinList} />}
        </Grid>



      </Grid>
    </>
  )
}

// export default connect(null, { getDetailForVisitor: getDetailForVisitor })(
//   MyProfile
// );


export default MyProfile;


// export default MyProfile