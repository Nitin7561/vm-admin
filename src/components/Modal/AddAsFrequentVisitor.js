import React from "react";
import { connect } from 'react-redux';

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import ErrorIcon from "../../assets/failed.svg";
import SuccessIcon from "../../assets/success.svg";

import RemoveFrequentVisitor from '../../assets/remove-frequent-visitor.svg'

import { CircularProgress } from "@material-ui/core";
import StatusModalBasedOnStatus from "./StatusModalBasedOnStatus";

import { addToFrequentVisitor } from '../../redux/actions/addToFrequentVisitor';
import { refresher } from '../../redux/actions/refresher';
import FullPageLoading from '../Loaders/FullPageLoading';


import "./modal.scss";

const getStatusComponent = (type, closeModal, visitorName) => {
  switch (type) {
    case "Success":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          buttonTwoMessage={''}
          icon={RemoveFrequentVisitor}
          heading={`${visitorName} added to your Frequent Visitor`}
          message={`An acknowledgement email has been sent to your email id`}
          buttonMessage={`OK`}
          handleClose={closeModal}
        />
      );

    case "Error":

      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          buttonTwoMessage={''}
          icon={ErrorIcon}
          heading={`Add Failed`}
          message={`An acknowledgement email has been sent to your email id`}
          buttonMessage={`OK`}
          handleClose={closeModal}
        />
      );

    default:
      console.log("IN STATUS MODAL visitorSub loool")
      return <StatusModalBasedOnStatus
        isLoading={true}
        isVisitorTableAcionModal={null}
        buttonTwoMessage={null}
        icon={null}
        heading={null}
        message={null}
        buttonMessage={null}
        handleClose={closeModal}
      />;
  }
};

const AddAsFrequentVisitor = ({ props, closeModal, visitorName, refresherPage, addToFrequentVisitorReducer }) => {

  console.log("visitor name =====----====-=-=-=-=-=-==-=-=-=-=-=-=-=--=>", visitorName)

  let { isLoading, isSuccess, isError, error } = addToFrequentVisitorReducer;
  let type = "Loading";


  if (!isLoading) {
    if (isSuccess) {
      type = "Success";
    } else if (isError) {
      type = "Error";
    }
  }


  let onHandleClose = () => {
    console.log("HandleClose On Modal")
    refresherPage("addAsFreqVisitor");
    closeModal()
  }


  //   console.log("AddAppointmentBookingModal", type, isLoading, isSuccess, isError, error);

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="xs"
        onClose={() => {
          if (!isLoading) {
            onHandleClose();
          }
        }}
        open={true}
        disableBackdropClick
        disableEscapeKeyDown
        classes={{ root: 'lean-customized-modal-size' }}
      >
        {/* <DialogContent>
          {isLoading ? (
            <CircularProgress />
          ) : (
            getStatusComponent(type,onHandleClose,visitorName)
          )}
        </DialogContent> */}

        <DialogContent style={{ position: "relative" }}>
          {getStatusComponent(type, onHandleClose, visitorName)}
        </DialogContent>
      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  let { addToFrequentVisitorReducer } = state;
  return { addToFrequentVisitorReducer };
};

export default connect(mapStateToProps, { refresherPage: refresher, addToFrequentVisitor })(AddAsFrequentVisitor);
