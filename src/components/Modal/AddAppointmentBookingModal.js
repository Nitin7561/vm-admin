import React from "react";
import { connect } from 'react-redux';

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import ErrorIcon from "../../assets/failed.svg";
import SuccessIcon from "../../assets/success.svg";

import { CircularProgress } from "@material-ui/core";
import StatusModalBasedOnStatus from "./StatusModalBasedOnStatus";

import { refresher } from '../../redux/actions/refresher';
import FullPageLoading from '../Loaders/FullPageLoading';


import "./modal.scss";

const getStatusComponent = (type, closeModal, isDisabled) => {
  if (isDisabled) {
    switch (type) {
      case "Success":
        return (

          <StatusModalBasedOnStatus
            isVisitorTableAcionModal={'false'}
            buttonTwoMessage={''}
            icon={SuccessIcon}
            heading={`Appointment Rescehduled Successfully`}
            message={`Your appointment has been rescheduled successfully and an email has been seen to you!`}
            buttonMessage={`OK`}
            handleClose={closeModal}

          />
        );

      case "Error":

        return (
          <StatusModalBasedOnStatus
            isVisitorTableAcionModal={'false'}
            buttonTwoMessage={''}
            icon={ErrorIcon}
            heading={`Resheduling the Appointment has Failed`}
            message={`Your rescheduling appointment failed to complete`}
            buttonMessage={`OK`}
            handleClose={closeModal}
          />
        );

      default:
        return <></>;
    }
  }
  else {
    switch (type) {
      case "Success":
        return (

          <StatusModalBasedOnStatus
            isVisitorTableAcionModal={'false'}
            buttonTwoMessage={''}
            icon={SuccessIcon}
            heading={`Appointment Successfully Booked`}
            message={`An acknowledgement email has been sent to your email id`}
            buttonMessage={`OK`}
            handleClose={closeModal}

          />
        );

      case "Error":

        return (
          <StatusModalBasedOnStatus
            isVisitorTableAcionModal={'false'}
            buttonTwoMessage={''}
            icon={ErrorIcon}
            heading={`Appointment Booking is Failed`}
            message={`Your booking failed to complete`}
            buttonMessage={`OK`}
            handleClose={closeModal}
          />
        );

      default:
        console.log("IN STATUS MODAL visitorSub loool")
        return <StatusModalBasedOnStatus
          isLoading={true}
          isVisitorTableAcionModal={null}
          buttonTwoMessage={null}
          icon={null}
          heading={null}
          message={null}
          buttonMessage={null}
          handleClose={closeModal}
        />;
    }
  }

};

const AddAppointmentBookingModal = ({ props, isDisabled, closeModal, getAddAppointmentReducer, refresher }) => {

  let { isLoading, isSuccess, isError, error } = getAddAppointmentReducer;
  let type = "Loading";


  if (!isLoading) {
    if (isSuccess) {
      type = "Success";
    } else if (isError) {
      type = "Error";
    }
  }

  let onHandleClose = () => {
    // refresh
    console.log("HandleClose On Modal")
    // console.log("REFRESHER",refresher)
    refresher("myAppointments");
    closeModal()
  }

  console.log("AddAppointmentBookingModal", type, isLoading, isSuccess, isError, error, isDisabled);

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="xs"
        onClose={() => {
          if (!isLoading) {
            onHandleClose();
          }
        }}
        open={true}
        disableBackdropClick
        disableEscapeKeyDown
        classes={{ root: 'lean-customized-modal-size' }}
      >
        <DialogContent style={{ position: "relative" }}>
          {getStatusComponent(type, onHandleClose, isDisabled)}
        </DialogContent>
      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  let { getAddAppointmentReducer } = state;
  return { getAddAppointmentReducer };
};

export default connect(mapStateToProps, { refresher })(AddAppointmentBookingModal);
