import React from "react";
import { connect } from 'react-redux';

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import ErrorIcon from "../../assets/failed.svg";
import SuccessIcon from "../../assets/success.svg";

import SuccessfullyBlockedIcon from '../../assets/blocked-successfully.svg'

import { CircularProgress } from "@material-ui/core";
import StatusModalBasedOnStatus from "./StatusModalBasedOnStatus";
import FullPageLoading from '../Loaders/FullPageLoading';
import { refresher } from '../../redux/actions/refresher';



import "./modal.scss";

const getStatusComponent = (type, closeModal) => {
  switch (type) {
    case "Success":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          buttonTwoMessage={''}
          icon={SuccessfullyBlockedIcon}
          heading={`Unblocked Visitor Chat Successfully`}
          message={`An acknowledgement email has been sent to your email id`}
          buttonMessage={`OK`}
          handleClose={closeModal}
        />
      );

    case "Error":

      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          buttonTwoMessage={''}
          icon={ErrorIcon}
          heading={`Unblocking Chat Visitor Failed`}
          message={`An acknowledgement email has been sent to your email id`}
          buttonMessage={`OK`}
          handleClose={closeModal}
        />
      );

    default:
      console.log("IN STATUS MODAL visitorSub loool")
      return <StatusModalBasedOnStatus
        isLoading={true}
        isVisitorTableAcionModal={null}
        buttonTwoMessage={null}
        icon={null}
        heading={null}
        message={null}
        buttonMessage={null}
        handleClose={closeModal}
      />;
  }
};

const UnBlockedVisitor = ({ props, closeModal, unblockUserReducer }) => {

  let { isLoading, isSuccess, isError, error } = unblockUserReducer;
  let type = "Loading";

  // var isLoading = false

  if (!isLoading) {
    if (isSuccess) {
      type = "Success";
    } else if (isError) {
      type = "Error";
    }
  }

  let onHandleClose = () => {
    // refresh
    console.log("HandleClose On Modal")
    // console.log("REFRESHER",refresher)
    refresher("unblockedUser");
    closeModal()
  }

  //   console.log("AddAppointmentBookingModal", type, isLoading, isSuccess, isError, error);

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="xs"
        onClose={closeModal}
        open={true}
        disableBackdropClick
        disableEscapeKeyDown
        classes={{ root: 'lean-customized-modal-size' }}
      >
        {/* <DialogContent>
          {isLoading ? (
            <CircularProgress />
          ) : (
            getStatusComponent("Success", closeModal)
          )}
        </DialogContent> */}

        <DialogContent style={{ position: "relative" }}>

          {getStatusComponent(type, closeModal)}
        </DialogContent>
      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  let { unblockUserReducer } = state;
  return { unblockUserReducer };
};

export default connect(mapStateToProps, { refresher })(UnBlockedVisitor);
