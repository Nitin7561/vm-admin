import React from "react";
import { connect } from 'react-redux';

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import ErrorIcon from "../../assets/failed.svg";
import SuccessIcon from "../../assets/success.svg";
import ApproveVisitor from "../../assets/approve-visitor.svg";


import { CircularProgress } from "@material-ui/core";
import StatusModalBasedOnStatus from "./StatusModalBasedOnStatus";

import { approveVisitorRegistration } from '../../redux/actions/approveVisitorRegistration';
import { refresher } from '../../redux/actions/refresher';
import FullPageLoading from '../Loaders/FullPageLoading';


import "./modal.scss";

const getStatusComponent = (type, closeModal, visitorName) => {

  switch (type) {
    case "Success":
      return (

        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          icon={ApproveVisitor}
          heading={`Request approved successfully from ${visitorName}`}
          message={`Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do`}
          buttonMessage={`Okay`}
          buttonTwoMessage={''}
          handleClose={closeModal}
        />
      );

    case "Error":

      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          buttonTwoMessage={''}
          icon={ErrorIcon}
          heading={`Request to approve the visitor has Failed`}
          message={`Your request to approve the visitor has failed to complete`}
          buttonMessage={`OK`}
          handleClose={closeModal}
        />
      );

    default:
      console.log("IN STATUS MODAL visitorSub loool")
      return <StatusModalBasedOnStatus
        isLoading={true}
        isVisitorTableAcionModal={null}
        buttonTwoMessage={null}
        icon={null}
        heading={null}
        message={null}
        buttonMessage={null}
        handleClose={closeModal}
      />;
  }

};

const ApprovedVisitorRegistrationModal = ({ props, closeModal, visitorName, approveVisitorRegistrationReducer, refresher }) => {

  let { isLoading, isSuccess, isError, error } = approveVisitorRegistrationReducer;
  let type = "Loading";


  if (!isLoading) {
    if (isSuccess) {
      type = "Success";
    } else if (isError) {
      type = "Error";
    }
  }

  let onHandleClose = () => {
    // refresh
    console.log("HandleClose On Modal")
    // console.log("REFRESHER",refresher)
    refresher("myAppointments");
    closeModal()
  }

  console.log("ApprovedVisitorRegistrationModal", type, isLoading, isSuccess, isError, error);

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="xs"
        onClose={() => {
          if (!isLoading) {
            onHandleClose();
          }
        }}
        open={true}
        disableBackdropClick
        disableEscapeKeyDown
        classes={{ root: 'lean-customized-modal-size' }}
      >
        {/* <DialogContent>
          {isLoading ? (
            <CircularProgress />
          ) : (
            getStatusComponent(type,onHandleClose,visitorName)
          )}
        </DialogContent> */}

        <DialogContent style={{ position: "relative" }}>
          {
            getStatusComponent(type, onHandleClose, visitorName)
          }
        </DialogContent>

      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  let { approveVisitorRegistrationReducer } = state;
  return { approveVisitorRegistrationReducer };
};

export default connect(mapStateToProps, { approveVisitorRegistration, refresher })(ApprovedVisitorRegistrationModal);
