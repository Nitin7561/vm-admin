import React from "react";
import { connect } from 'react-redux';

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import ErrorIcon from "../../assets/failed.svg";
import ReportSuccessIcon from "../../assets/report-visitor.svg";
import { refresher } from '../../redux/actions/refresher';


import { CircularProgress } from "@material-ui/core";
import StatusModalBasedOnStatus from "./StatusModalBasedOnStatus";
import FullPageLoading from '../Loaders/FullPageLoading';


import "./modal.scss";

const getStatusComponent = (type, closeModal) => {
  switch (type) {
    case "Success":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          buttonTwoMessage={''}
          icon={ReportSuccessIcon}
          heading={`Reported Visitor Successfully`}
          message={`An acknowledgement email has been sent to your email id`}
          buttonMessage={`OK`}
          handleClose={closeModal}
        />
      );

    case "Error":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          buttonTwoMessage={''}
          icon={ErrorIcon}
          heading={`Reporting the visitor has Failed`}
          message={`Your request to report the visitor failed`}
          buttonMessage={`OK`}
          handleClose={closeModal}
        />
      );

    default:
      console.log("IN STATUS MODAL visitorSub loool")
      return <StatusModalBasedOnStatus
        isLoading={true}
        isVisitorTableAcionModal={null}
        buttonTwoMessage={null}
        icon={null}
        heading={null}
        message={null}
        buttonMessage={null}
        handleClose={closeModal}
      />;
  }
};

const ReportedVisitor = ({ props, closeModal, reportUserReducer, refresher }) => {

  let { isLoading, isSuccess, isError, error } = reportUserReducer;
  let type = "Loading";


  if (!isLoading) {
    if (isSuccess) {
      type = "Success";
    } else if (isError) {
      type = "Error";
    }
  }


  let onHandleClose = () => {
    console.log("HandleClose On Modal")
    refresher("reportedVisitor");
    closeModal()
  }

  console.log("Report User", type, isLoading, isSuccess, isError, error);

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="xs"
        onClose={() => {
          if (!isLoading) {
            onHandleClose();
          }
        }}
        open={true}
        disableBackdropClick
        disableEscapeKeyDown
        classes={{ root: 'lean-customized-modal-size' }}
      >
        {/* <DialogContent>
          {isLoading ? (
            <CircularProgress />
          ) : (
            getStatusComponent(type, onHandleClose)
          )}
        </DialogContent> */}


        <DialogContent style={{ position: "relative" }}>
          {
            getStatusComponent(type, onHandleClose)
          }
        </DialogContent>
      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  let { reportUserReducer } = state;
  return { reportUserReducer };
};

export default connect(mapStateToProps, { refresher })(ReportedVisitor);
