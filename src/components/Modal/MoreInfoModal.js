import React from "react";
import { connect } from 'react-redux';

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import ApproveVisitor from "../../assets/approve-visitor.svg";
import FrequentVisitor from "../../assets/frequent-visitor.svg";
import ErrorIcon from "../../assets/failed.svg";
import SuccessIcon from "../../assets/success.svg";
import BlockChatIcon from '../../assets/block-chat.svg';
import ConfirmRemoveFrequentVisitor from '../../assets/confirm-remove-frequent-visitor.svg'

import StatusModalBasedOnStatus from "./StatusModalBasedOnStatus";

import "./modal.scss";

const getStatusComponent = (props, closeModal, type, visitorName, appointmentId, visitorSub, reason, description) => {

  console.log("Type==>", type)

  console.log("Appoinment Id from More Info Modal===>", appointmentId)

  console.log("visitorSub from More Info Modal===>", visitorSub)


  switch (type) {

    case "ConfrimRoomCancel":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'true'}
          icon={ConfirmRemoveFrequentVisitor}
          heading={"Are you sure you want to cancel the Room Booked along with the appointment?"}
          message={`If cancelled an acknowledgement will be sent to your mail`}
          buttonMessage={`Yes`}
          buttonTwoMessage={'Keep It'}
          handleClose={closeModal}
          visitorName={""}
          appointmentId={appointmentId}
          isCancelTheRoom={true}
          reason={reason}
          description={description}

        />
      );

    case "BlockChat":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'true'}
          icon={BlockChatIcon}
          heading={`Are you sure want to block chat with ${visitorName} ?`}
          message={` Blocked contacts will no longer be able to send you messages`}
          buttonMessage={`Cancel`}
          buttonTwoMessage={'Block'}
          handleClose={closeModal}
          visitorName={""}
          appointmentId={""}
          isCancelTheRoom={""}
          visitorSub={visitorSub}
          reason={""}
          description={""}
        />
      );

    case "UnblockChat":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'true'}
          icon={BlockChatIcon}
          heading={`Are you sure want to unblock chat with ${visitorName} ?`}
          message={` Unblocked contacts will be able to send you messages`}
          buttonMessage={`Cancel`}
          buttonTwoMessage={'Unblock'}
          handleClose={closeModal}
          visitorName={""}
          appointmentId={""}
          isCancelTheRoom={""}
          visitorSub={visitorSub}
          reason={""}
          description={""}
        />
      );

    case "AddFrequentVisitor":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          icon={FrequentVisitor}
          heading={`Successfully added as a Frequent Visitor`}
          message={`You can find visitors easily accessing frequent visitors table`}
          buttonMessage={`Okay`}
          buttonTwoMessage={''}
          handleClose={closeModal}
        />
      );

    case "NotApprovedVisitor":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          icon={BlockChatIcon}
          heading={`Approval Required`}
          message={`Please approve the visitor to view the appointments with the visitor`}
          buttonMessage={`Okay`}
          buttonTwoMessage={''}
          handleClose={closeModal}
        />
      );


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    case "RemoveAsFrequentVisitor":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'true'}
          icon={ConfirmRemoveFrequentVisitor}
          heading={`Are you sure want to remove ${visitorName} as Frequent Visitor?`}
          message={`Added back to Visitors List`}
          buttonMessage={`Cancel`}
          buttonTwoMessage={'Yes'}
          handleClose={closeModal}
          visitorName={visitorName}
        />
      );

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    default:
      console.log("IN STATUS MODAL visitorSub loool")
      return <StatusModalBasedOnStatus
        isLoading={true}
        isVisitorTableAcionModal={null}
        buttonTwoMessage={null}
        icon={null}
        heading={null}
        message={null}
        buttonMessage={null}
        handleClose={closeModal}
      />;
  }
};

const MoreInfoModal = ({ props, closeModal, type, visitorName, appointmentId, visitorSub, reason, description }) => {

  console.log("Type=================>", type)
  console.log("Props from more info modal", visitorName)

  console.log("visitorId from more info modal", visitorSub)


  console.log("<===== Appoinment Id from More Info Modal in ===>", appointmentId)

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="sm"
        onClose={closeModal}
        open={true}
        disableBackdropClick
        disableEscapeKeyDown
        classes={{ root: 'lean-customized-modal-size' }}
      >
        <DialogContent>
          {(getStatusComponent(props, closeModal, type, visitorName, appointmentId, visitorSub, reason, description))}
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default MoreInfoModal;
