import React from "react";
import StatusModal from '../StatusModal'
import FunctionalModal from '../FunctionalModal'
import bridgeUX from '../../../assets/images/bridgeUX.svg'
import myProfile from '../../../assets/images/myProfile.svg'
import { Grid } from "@material-ui/core";
import { CircularProgress } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { connect } from 'react-redux';
import { getParticularVisitorInfo } from '../../../redux/actions/getParticularVisitorInfo';
import { Button } from "@material-ui/core";
import { approveVisitorRegistration } from '../../../redux/actions/approveVisitorRegistration';
import { ModalContext } from "../../../utils/ModalContext";
import FullPageLoading from '../../Loaders/FullPageLoading';
import './VisitorApproveReject.scss'

const VisitorApproveReject = (props) => {

  const { setCurrentModal } = React.useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });

  let { getParticularVisitorInfoReducer, enqueueSnackbar, closeSnackbar } = props  

  console.log("Visitor Id from part visi",props.visitorId);

  React.useEffect(() => {
    props.getParticularVisitorInfo({
      "visitorId" : encodeURIComponent(props.visitorId)
    });
  }, [props.visitorId])

  let { isLoading: isLoadingUser, data: userData, error: errorUser, isError: isErrorUser } = getParticularVisitorInfoReducer


  function mapUserData() {

    console.log("DATA RECVD IN MAP USER DATA",userData)
    let myProfileData = [
      
      { title: 'Name', value: userData.name },
      { title: 'Employee ID', value: userData.visitorSub },
      { title: 'Contact Number', value: userData.Mobile },
      { title: 'Email ID', value: userData.email },
      { title: 'Company Name/Location', value: userData.currentCompany},

      // { title: 'Name', value: "name" },
      // { title: 'Employee ID', value: "visitorSub" },
      // { title: 'Contact Number', value: "Mobile" },
      // { title: 'Email ID', value: "email" },
      // { title: 'Company Name/Location', value: "currentCompany"},


    ]

    return myProfileData
  }

  return (
    
    <div>
        {isLoadingUser ? <CircularProgress></CircularProgress> : <FunctionalModal title='Visitor Information' children={modalContent(mapUserData(),userData,props.approveVisitorRegistration,openModal)} maxWidth='md' handleClose={props.closeModal} classStyles={{paperScrollPaper:'lean-particular-visitor'}}
        />
        }
    
    </div>

  )
}

const modalContent = (myProfileData,userData,approveVisitorRegistration,openModal) => {

 

  function apprejvisitor(action)
  {
    console.log("Action on click",action)

    console.log("userData.visitorSub",userData.visitorSub)

    if(action === "Approve")
    {
        approveVisitorRegistration({
          "visitorSub":userData.visitorSub,
          "approve": true
        })
        openModal({ name: 'ApprovedVisitorRegistrationModal', props: { "visitorName": userData.name } })
    }
    else if (action === "Reject")
    {
      openModal({ name: "RejectVisitor", props: { "visitorName": userData.name, "visitorId": userData.visitorId, "visitorSub": userData.visitorSub,"registrationRejection":true } })
    }
  }

  return (
    <>

      <Grid container classes={{root:'lean-my-profile-vm-particular-visitor-info'}} >

        <Grid item xs={8} sm={4} >
          {/* <img src = {myProfile}></img> */}
          <img className='my-profile-image-from-part-visitor-info-modal' src={`data:image/png;base64,${userData.thumbnailImage}`}  ></img>
        </Grid>

        <Grid item xs={12} sm={8} >

          {myProfileData.map((data) => {
            return (
              // <></>
              <Grid container style={{ minHeight: '35px' }} >
                <Grid item xs={4} >
                  <div className='lean-myprofile-title'>{data.title}</div>
                </Grid>
                <Grid item xs={8} >
                  <div className='lean-myprofile-description '>{data.value}</div>
                </Grid>
              </Grid>
            )
          })}

        </Grid>

        <Grid className = 'approve-reject-visitor-info-buttons' item xs={12} sm={12} >
          <Button
            classes={{root:"lean-visitor-cancel-button"}}
            variant="contained"
            className="lean-btn-primary visitor-rejected-button"
            onClick={() => {apprejvisitor("Reject")}}
          >
            Reject
          </Button>
          <Button
            variant="contained"
            className="lean-btn-primary visitor-approval-button"
            onClick={() => {apprejvisitor("Approve")}}
          >
            Approve
          </Button>
        </Grid>
      </Grid>
    </>
  )
}

const mapStateToProps = (state) => {
  let { getParticularVisitorInfoReducer } = state;
  return { getParticularVisitorInfoReducer };
}

export default connect(mapStateToProps, { getParticularVisitorInfo,approveVisitorRegistration })(VisitorApproveReject)