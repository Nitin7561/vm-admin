import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import "./modal.scss";
import { Grid, Hidden, Button } from "@material-ui/core";


const StatusModal = ({icon, heading, message, buttonMessage }) => {
  const [open, setOpen] = React.useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog fullWidth maxWidth="xs" onClose={handleClose} open={open}>
        {/* <DialogTitle disableTypography className="lean-modal-header">
          <div className="lean-text-header">Book a Room</div>
          <div>
            <IconButton>
              <CloseIcon fontSize="large" />
            </IconButton>
          </div>
        </DialogTitle> */}
        <DialogContent>
          <Grid container>
            <Grid item xs={12}>
              <div className="lean-modal-main-content">
                <div className="lean-modal-item">
                  <div className="lean-modal-icon">
                    <img src={icon} />
                  </div>
                </div>

                <div className="lean-modal-item">
                  <div className="lean-modal-content">
                    <div className="lean-modal-content-header lean-section-text-header">
                      {heading}
                    </div>
                    <div className="lean-modal-content-message lean-text-content">
                      {message}
                    </div>
                  </div>
                </div>

                <div className="lean-modal-item ">
                  <div className="lean-modal-action">
                    <Button variant="contained" className="lean-btn-primary">
                      {buttonMessage}
                    </Button>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default StatusModal;
