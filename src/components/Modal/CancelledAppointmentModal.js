import React from "react";
import { connect } from 'react-redux';

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import ErrorIcon from "../../assets/failed.svg";
import SuccessIcon from "../../assets/success.svg";
import { CircularProgress } from "@material-ui/core";
import StatusModalBasedOnStatus from "./StatusModalBasedOnStatus";
import CancelledAppointmentModal from "../../assets/cancelled-appointment.png";
import { refresher } from '../../redux/actions/refresher';
import FullPageLoading from '../Loaders/FullPageLoading';


import "./modal.scss";

const getStatusComponent = (type, closeModal) => {
  switch (type) {

    case "Success":
      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          icon={CancelledAppointmentModal}
          heading={`Appointment cancelled successfully`}
          message={`An acknowledgement email has been sent to your email id`}
          buttonMessage={`Okay`}
          buttonTwoMessage={''}
          handleClose={closeModal}
        />
      );

    case "Error":

      return (
        <StatusModalBasedOnStatus
          isVisitorTableAcionModal={'false'}
          buttonTwoMessage={''}
          icon={ErrorIcon}
          heading={`Appointment Cancellation is Failed`}
          message={`Your cancellation failed to complete`}
          buttonMessage={`OK`}
          handleClose={closeModal}
        />
      );

    default:
      console.log("IN STATUS MODAL visitorSub loool")
      return <StatusModalBasedOnStatus
        isLoading={true}
        isVisitorTableAcionModal={'false'}
        buttonTwoMessage={''}
        icon={ErrorIcon}
        heading={`Appointment Cancellation is Failed`}
        message={`Your cancellation failed to complete`}
        buttonMessage={`OK`}
        handleClose={closeModal}
      />;
  }
};

const CancelledAppointment = ({ props, closeModal, cancelAppt, refresher }) => {

  let { isLoading, isSuccess, isError, error } = cancelAppt;
  let type = "Loading";


  if (!isLoading) {
    if (isSuccess) {
      type = "Success";
    } else if (isError) {
      type = "Error";
    }
  }

  let onHandleClose = () => {
    // refresh
    console.log("HandleClose On Modal", refresher)
    // console.log("REFRESHER",refresher)
    refresher("rejectedVisitor");
    closeModal()
  }

  console.log("Cancel Appt", type, isLoading, isSuccess, isError, error);

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="xs"
        onClose={() => {
          if (!isLoading) {
            onHandleClose();
          }
        }}
        open={true}
        disableBackdropClick
        disableEscapeKeyDown
        classes={{ root: 'lean-customized-modal-size' }}
      >
        {/* <DialogContent>
          {isLoading ? (
            <CircularProgress />
          ) : (
            getStatusComponent(type, onHandleClose)
          )}
        </DialogContent> */}

        <DialogContent style={{ position: "relative" }}>
          {getStatusComponent(type, onHandleClose)}
        </DialogContent>

      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  let { cancelAppt } = state;
  return { cancelAppt };
};

export default connect(mapStateToProps, { refresher })(CancelledAppointment);
