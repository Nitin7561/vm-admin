import React,{useEffect,useState} from 'react';
import PinBox from './ReactPinInput'

function PinInput(props){        
    console.log(props.length)
    return(    
            <PinBox 
            length = {props.length?props.length:6}                        
            type="numeric" 
            inputStyle={{borderColor: 'rgba(14,15,66,0.33)',
                         borderRadius: "4px",
                         height:props.height?props.height:"80px",
                         width:props.width?props.width:"80px",
                         fontSize:"18px",
                         margin:"8px",
                         color:"#0F0F42",
                         fontFamily:'"Nanum Gothic", sans-serif'
                         }}
            inputFocusStyle={{borderColor: 'rgba(14,15,66,0.66)'}}          
            {...props}    
            onComplete={(value, index) => {
                console.log(value);
            }}
            //TODO Redux Integration
            />  
                  
    )
}

export default PinInput;